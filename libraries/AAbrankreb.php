<?php

/*
 分组功能
*/

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class AAbrankreb
{
    public function __construct()
    {
        $this->ci =& get_instance();
        $CI =& get_instance();
        
    }

     

    function get_rank_by_prev_score(&$para, $index)
    {
        //由上一洞的成绩,和上一洞的排名,决定本洞的排名
        //如果有成绩相同的情况,要考虑出身 
       
        
        
        if ($index == 0) {
            $para['gamedata'][0]['rank'] = $para['rank_init'];
            return;
        } 

        $prev_index = $index - 1;
        $prev_rank  = $para['gamedata'][$prev_index]['rank'];
        //上一洞成绩.
        $raw_score = $para['gamedata'][$prev_index]['after_max'];  //是否用原始杆数?

        $score_ass=array();
        foreach ($raw_score as $key => $one_value) {
           $score_ass[]=array('user'=>$key,"sc"=>$one_value);
        }

        // 考虑出身 ,prev_rank代表出身.
        usort($score_ass, function ($item1, $item2) use($prev_rank) {
                  //如果成绩相同,比较出生.
                 if($item1['sc']  == $item2['sc']   ){
  
                    $key1=$item1['user'];
                    $key2=$item2['user'];
                    $birth1=array_search($key1,$prev_rank);
                    $birth2=array_search($key2,$prev_rank);
                    return $birth1 > $birth2;
        
                 }
                 return $item1['sc']  > $item2['sc'] ;
          });
        $para['gamedata'][$index]['rank'] = array_retrieve($score_ass,'user');  ;
    }
    
      
 
    
    
    function set_ab_by_rank_2_fixed($para, $index)
    {
        $rank_init = $para['rank_init'];
        return array(
            'A' => array(
                $rank_init[0]
            ),
            'B' => array(
                $rank_init[1]
            )
        );
    }
    

 function set_ab_by_rank_3_random($para, $index,$kpis)
    {

     //3人斗地主乱拉:
          // 3人-乱拉-斗大地主: 第一洞:按照初始排名,第一名做地主.  后面的洞以上洞排名第一的做地主.
          // 3人-乱拉-斗2地主: 第一洞:按照初始排名,第2名做地主.   后面的洞以上洞排名第2的做地主.
          // 3人-乱拉-斗小地主: 第一洞:按照初始排名,第3名做地主.  后面的洞以上洞排名第3的做地主.

   
        $kpi=$kpis[0];
        $rank_init = $para['rank_init'];
        $attender_num=$para['attender_num'];
        
        if($kpi=='kpi_best'){ $type='大地主';$etype='big';}
        if($kpi=='kpi_total_add'){ $type='二地主';$etype='middle';}
        if($kpi=='kpi_total_plus'){ $type='二地主';$etype='middle';}
        if($kpi=='kpi_worst'){ $type='小地主';$etype='small';}

        if($attender_num==4){

               $this->ci->xlog->getInstance()->log("4人斗{$type}/乱拉",3);
        }else
        {
               $this->ci->xlog->getInstance()->log("3人斗{$type}/乱拉",3);
        }

        if($index==0){

            if($etype=='big')   {return array('A'=>array($rank_init[0]),'B'=>array($rank_init[1],$rank_init[2]));}
            if($etype=='middle'){return array('A'=>array($rank_init[1]),'B'=>array($rank_init[0],$rank_init[2]));}
            if($etype=='small'){return array('A'=>array($rank_init[2]),'B'=>array($rank_init[0],$rank_init[1]));}
        }
      

        $rank = $para['gamedata'][$index]['rank'];
        
        if($etype=='big'){      
            return array(
                'A' => array(
                    $rank[0]
                ),
                'B' => array(
                    $rank[1],
                    $rank[2]
                )
            ); 
        }

          if($etype=='middle'){
            return array(
                'A' => array(
                    $rank[1]
                ),
                'B' => array(
                    $rank[0],
                    $rank[2]
                )
            ); 
        }

           if($etype=='small'){
            return array(
                'A' => array(
                    $rank[2]
                ),
                'B' => array(
                    $rank[0],
                    $rank[1]
                )
            ); 
        }

         
            
    }

      function set_ab_by_rank_3_fixed($para, $index,$kpis)
    {
         $attender_num=$para['attender_num'];
        
        if($attender_num==4){

               $this->ci->xlog->getInstance()->log("4人斗地主固拉",3);
        }else
        {
               $this->ci->xlog->getInstance()->log("3人斗地主固拉",3);
        }


            $rank_init = $para['rank_init'];
            return array(
                'A' => array(
                    $rank_init[0]
                ),
                'B' => array(
                    $rank_init[1],
                    $rank_init[2]
                )
            );
    }
    
    function set_ab_by_rank_4_random($para, $index)
    {
            
            $rank = $para['gamedata'][$index]['rank'];
            return array(
                'A' => array(
                    $rank[0],
                    $rank[3]
                ),
                'B' => array(
                    $rank[1],
                    $rank[2]
                )
            );
        
    }

    function set_ab_by_rank_4_fixed($para, $index)
    {
            

            $rank_init = $para['rank_init'];
             
            $agroup=  $para['agroup'];
            $bgroup=  $para['bgroup'];

            $a_arr=explode(',', $agroup);

            $b_arr=explode(',', $bgroup);
            

            return array(
                'A' => array(
                    $a_arr[0],
                    $a_arr[1]
                ),
                'B' => array(
                    $b_arr[0],
                    $b_arr[1]
                )
            );
        
    }

 
    function set_ab_by_rank_for_4_follow($para, $index)
    {


            //先选地主,地主是上局的第一名(大地主,二地主,小地主)
            //地主婆模式,地主和地主婆两人按照最好成绩取一个人.

            $kpis=$para['kpi_cfg'];
            $kpi=$kpis[0];  //4人地主婆模式只有一个指标.
            
            if($kpi=='kpi_best'){
                 $type='da_di_zhu';

            } 

             if($kpi=='kpi_total_add'){
                 $type='er_di_zhu';
            } 

             if($kpi=='kpi_total_plus'){
                 $type='er_di_zhu';
            } 

            //实际配置中,27个游戏不包含4人斗小地主.
            if($kpi=='kpi_worst'){
                 $type='xiao_di_zhu';
            } 


            $rank_init = $para['rank_init']; //初始排序
            
            if($index==0){
                $this->ci->xlog->getInstance()->log("第一个洞,使用初始分组" ,2,null);

                if($type=='da_di_zhu'){

                            return array(
                        'A' => array(
                            $rank_init[0],
                            $rank_init[3]
                        ),
                        'B' => array(
                            $rank_init[1],
                            $rank_init[2]
                        ));
                } 

                 if($type=='er_di_zhu'){

                            return array(
                        'A' => array(
                            $rank_init[1],
                            $rank_init[3]
                        ),
                        'B' => array(
                            $rank_init[0],
                            $rank_init[2]
                        ));
                } 

                 

            }

            $follower=$para['follower'];
            $this_rank=$para['gamedata'][$index ]['rank'];
            $players_without_follower=array_diff( $this_rank,array($follower));         
       
            
           // debug($players_without_follower);
            //除了 follower的人员
            $first_element=array_slice($players_without_follower, 0,1);
            $second_element=array_slice($players_without_follower, 1,1);
            $third_element=array_slice($players_without_follower, 2,1);
            
            //先选地主
            if($type=='da_di_zhu'){
                   $A=array($follower,$first_element[0]);
                   $B=array($second_element[0],$third_element[0]);
            }

            if($type=='er_di_zhu'){
                   $A=array($follower,$second_element[0]);
                   $B=array($first_element[0],$third_element[0]);
            }

            if($type=='xiao_di_zhu'){
                   $A=array($follower,$third_element[0]);
                   $B=array($first_element[0],$second_element[0]);
            }

            return array('A'=>$A,'B'=>$B);
            
        
    }

    function set_ab_by_rank_4_ab($para, $index)
    {

            // A组第一名和B组第二名
            
            
            $strong_a_s=explode(',', $para['agroup']);

            $weak_b_s  =explode(',',  $para['bgroup']);
           
            $this_hole_raw_score = $para['gamedata'][$index]['raw_scores'];
             
            if ($index == 0) {
                
                return array(
                    'A' => array(
                        $strong_a_s[0],  //a1,b2
                        $weak_b_s[1]     //a2,b1
                    ),
                    'B' => array(
                        $strong_a_s[1],
                        $weak_b_s[0]
                        
                    )
                );
            } else 
               {

                    $this_rank=$para['gamedata'][$index ]['rank'];
                     
                    $tmp_a=array();
                    $tmp_b=array(); 
                    foreach ($this_rank as $key => $one_player) {
                         
                         if(in_array($one_player, $strong_a_s)){
                            $tmp_a[]=$one_player;
                         }else
                         {
                            $tmp_b[]=$one_player;
                         }
                         
                         
                    }
                
                     
                    
                    
                    return array(
                        'A' => array(
                            $tmp_a[0],
                            $tmp_b[1]
                        ),
                        'B' => array(
                            $tmp_a[1],
                            $tmp_b[0]
                            
                        )
                    );
            }
        
    }

     function set_ab_by_rank_4_3vs1($para, $index)
    {
            $rank_init = $para['rank_init'];
            return array(
                'A' => array(
                    $rank_init[0]
                    
                ),
                'B' => array(
                    $rank_init[1],
                    $rank_init[2],
                    $rank_init[3]
                    
                )
            );
        
    }
 



    public function  set_ab_by_rank_for_skin(&$para, $index)
    { 
       //A:最好成绩的用户 B:去除最好成绩选手后的其他人
       //A方永远只有1人.
      
    
       //用让完的杆数,选择打最好的一个人.
       $score= $para['gamedata'][$index]['after_max_weak'];


       /*
         选出一个最后成绩的选手,与剩下的人比较
       */
       $best_player = array_search(min($score),$score);


        // debug($score);
        // debug($best_player);
        // die;

       unset($score[$best_player]);
       $AB=array('A'=>array($best_player),'B'=>array_keys($score));
    
       return $AB;

    }
    
 
    
    
    function set_ab_by_rank(&$para, $index)
    {  
        /*
                '2_fixed',   //2人,自然分边
                '3_random',  //3人乱拉 1vs2 ,第1名为单独一组
                '3_fixed',   //3人固拉 1vs2 ,第2名为单独一组
                '3_skin',    //3人skin,各自为战
                '4_random',  //4人乱拉 2vs2,(1,4)vs(2,3)
                '4_fixed',   //4人固拉 2vs2.固定分组.
                '4_skin',    //4人skin,各自为战
                '4_3vs1',    //4人,固定3打1
                '4_ab',      //4人ab分组. 2vs2(a组两人永远不在同一边)
                '4_flow'     //跟随模式
         */
       
        //上局平局,沿用上局的AB分组. 
      
        $kpis=$para['kpi_cfg'];

        if ($index > 0) {
            if ($para['gamedata'][$index - 1]['kpi_summary']['drawinfo']['draw'] == true) {
                $para['gamedata'][$index]['AB'] = $para['gamedata'][$index - 1]['AB'];
            }
        }

        if( $para['div_option']=='2_fixed'){
              $para['gamedata'][$index]['AB'] = $this->set_ab_by_rank_2_fixed($para, $index);
        }


       if( $para['div_option']=='3_random'){
              $para['gamedata'][$index]['AB'] = $this->set_ab_by_rank_3_random($para, $index,$kpis);
        }
        
        
        if( $para['div_option']=='3_fixed'){
              $para['gamedata'][$index]['AB'] = $this->set_ab_by_rank_3_fixed($para, $index,$kpis);
        }
      
      
        if( $para['div_option']=='3_skin'){
              $para['gamedata'][$index]['AB'] = $this->set_ab_by_rank_for_skin($para, $index);
        }


        if( $para['div_option']=='4_random'){
              $para['gamedata'][$index]['AB'] = $this->set_ab_by_rank_4_random($para, $index);
        }
      
         if( $para['div_option']=='4_fixed'){
              $para['gamedata'][$index]['AB'] = $this->set_ab_by_rank_4_fixed($para, $index);
        }
      

      if( $para['div_option']=='4_skin'){
              $para['gamedata'][$index]['AB'] = $this->set_ab_by_rank_for_skin($para, $index);
        }
      

      if( $para['div_option']=='4_ab'){
              $para['gamedata'][$index]['AB'] = $this->set_ab_by_rank_4_ab($para, $index);
        } 
      

       if( $para['div_option']=='4_3vs1'){
              $para['gamedata'][$index]['AB'] = $this->set_ab_by_rank_4_3vs1($para, $index);
        }
    

      if( $para['div_option']=='4_follow'){
              $para['gamedata'][$index]['AB'] = $this->set_ab_by_rank_for_4_follow($para, $index);
        }


    }
    

    
    
    //设置初始排名
    function set_init_rank(&$para)
    {
        $initorder         = $para['initorder'];
        $para['rank_init'] = explode(',', $initorder);
    }
}
?> 
