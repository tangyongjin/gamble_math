<?php

 /*
 


*/

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class ABasepointreb
{
    public function __construct()
    {
        $this->ci =& get_instance();
        $CI =& get_instance();
    }
    
    

      function get_base_point($winner, $Indicator,$para,$kpi_name )
    {



       
        $indicator_a = $Indicator['indicator_a'];
        $indicator_b = $Indicator['indicator_b'];
        
        $playernum = $para['playernum'];
         

        
        //如果打平了,会直接返回了.
        
        if ($winner == '-') {
            return array(
                'K_point'=>0
            );
        }
        
        if ($playernum == 2) {
            $res = $this->get_base_point_2($kpi_name, $indicator_a, $indicator_b);
            return $res;
        }
        
        if ($playernum == 3) {
            $res = $this->get_base_point_3();
            return $res;
        }
        
        if ($playernum == 4) {
                        $weight_cfg=$para['weight_cfg'];
                        $res = $this->get_base_point_4($kpi_name,$weight_cfg, $indicator_a, $indicator_b);
                        return $res;
                }
    } 

    function get_base_point_2($kpi_name, $indicator_a, $indicator_b)
    {
        if ($kpi_name == 'kpi_gross') // 2人比杆数
            {
            return array(
                'K_point'=>abs($indicator_a - $indicator_b) 
            );
            
        } else {
            return array(
                'K_point'=>1 
            );
        }
    }
    
    function get_base_point_3()
    {
        //3人赛全是比洞.
        return array(
            'K_point'=>1 
        );
    }
    
     

     function  get_base_point_4($kpi_name,$weight_cfg,$indicator_a, $indicator_b){
         //头N尾N在这处理 and 211/421
         if ($kpi_name=='kpi_case_8421'){
              return array('K_point'=>abs($indicator_a - $indicator_b) );
         }
         
         $weight_array=explode(',',trim($weight_cfg));
         

         //有权重.
         if(count($weight_array)>=2){

             //头尾
             if (  count($weight_array)==2){
                 
                     if($kpi_name=='kpi_best'){ return array('K_point'=>$weight_array[0]);}
                      return array('K_point'=>$weight_array[1]);
             }

            //头尾总
            if (  count($weight_array)==3){
                    if($kpi_name=='kpi_best'){ return array('K_point'=>$weight_array[0]);}
                    if($kpi_name=='kpi_worst'){ return array('K_point'=>$weight_array[1]);}
                    return array('K_point'=>$weight_array[2]);
                   
             }
         }
         return array('K_point'=>1);
         
    }
}

?> 
