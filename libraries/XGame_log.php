<?php

if (!defined('BASEPATH')) {
	exit('No direct script access allowed');
}

define("GAME_INITED", 0);//准备中
define("GAME_REGISTER_ENDED", 1);//报名完成
define("GAME_GROUPED", 2);//分组完成
define("GAME_RUNNING", 3);//进行中
define("GAME_ENDED", 4);//结束
define("GAME_CANCELED", 5);//已取消
define("GAME_OVERDATE", 6);//已过期

define("GAME_ATTENTION_PUBLIC", 2);//对关注我的公开
define("GAME_PUBLIC", 1);//对好友公开
define("GAME_NOTPUBLIC", 0);//不公开

define("GAME_TYPE_INNER", 1);//队内比赛
define("GAME_TYPE_YUEQIU", 2);//约球赛
define("GAME_TYPE_INSTANT", 3);//立即比赛
define("GAME_TYPE_TEAMVSTEAM", 4);//队际赛

define("GAME_GROUP_USER_NOT_VOTED", 0);//
define("GAME_GROUP_USER_VOTED", 1);//

define("GAME_GROUP_NOT_ALL_CONFIRMED", 0);//

define("GAME_GROUP_ALL_CONFIRMED", 1);//

define("IS_GAME_FRIEND", '1');//好友
define("IS_GAME_ATTENTION", '0');//关注


define("SERVER_PIC_DIR", "'http://t1.golf-brother.com/data/attach/'");
class XGame {
	private $ci = null;

	public function __construct() {
		$this->ci = &get_instance();
	}
	public function debug($r) {
		echo "<pre>";
		print_r($r);
		echo "</pre>";
	}

	public function relationship($me, $other) {

		/*
		define("NO_ RELATIONSHIP", 'NO_RELATIONSHIP');//没有任何关系
		define("IS_FRIEND", 'IS_FRIEND');//是好友
		define("I_ASK_OTHER", 'I_ASK_OTHER');//我请求别人
		define("OTHER_ASK_ME", 'OTHER_ASK_ME');//别人请求我
		 */
		//row_array

		//	$me    = 70;
		//	$other = 65;

		$relationship = 'NO_RELATIONSHIP';

		$sql = "select status  from t_friend  where  userid=$me and fuserid=$other  ";

		$row = $this->db->query($sql)->row_array();
		if (!$row) {
			$relationship = 'NO_RELATIONSHIP';
		} else {
			if ($row['status'] == 1) {
				$relationship = 'IS_FRIEND';
			}

			if ($row['status'] == 0) {
				$relationship = 'I_ASK_OTHER';
			}
		}

		$sql = "select status  from t_friend  where  userid=$other and fuserid=$me  and status=0 ";
		$row = $this->db->query($sql)->row_array();
		$row = $this->db->query($sql)->row_array();
		if ($row) {
			$relationship = 'OTHER_ASK_ME';
		}

		//echo $relationship;

		return $relationship;
	}

	public function get_non_inner_game_groupid($gameid) {
		$this->ci->load->database();
		$sql                = "select groupid from t_game_group  where gameid=$gameid limit 1 ";
		$game_group_summary = $this->ci->db->query($sql)->row_array();
		$groupid            = $game_group_summary['groupid'];
		return $groupid;
	}

	public function detail($gameid, $userid = -1) {

		$this->ci->load->database();

		$sys_path  = str_replace("'", "", SERVER_PIC_DIR);
		$sql       = "select t_game.*,t_course.name as course_name from t_game,t_course where gameid=$gameid and t_course.courseid=t_game.courseid";
		$game_info = $this->ci->db->query($sql)->row_array();
		//只有球队管理员才可以创建,修改比赛

		$game_info['isadmin'] = 1;

		$creatorid                     = $game_info['creatorid'];
		$sql                           = "select * from t_user where id=$creatorid";
		$creatorid_info                = $this->ci->db->query($sql)->row_array();
		$creatorid_info['user_picurl'] = $sys_path . $creatorid_info['coverpath'] . '/c240_' . $creatorid_info['covername'];

		$team_id = intval($game_info['teamid']);
		$sql     = "select * from t_team where teamid=$team_id";

		logtext($sql);

		$team_info                   = $this->ci->db->query($sql)->row_array();
		if (!$team_info) {$team_info = null;}

		if ($userid > 0) {
			$adminids = array();
			$sql      = "select userid from t_team_admins where teamid=$team_id";
			$admins   = $this->ci->db->query($sql)->result_array();
			foreach ($admins as $k => $v) {
				$adminids[] = $v['userid'];
			}
			if (in_array($userid, $adminids)) {
				$game_info['isadmin'] = 1;
			} else {
				$game_info['isadmin'] = 0;
			}
		}

		$sys_path    = SERVER_PIC_DIR;
		$sql         = "select t_game_group_user.*,nickname, concat($sys_path,t_user.coverpath,'/c240_',t_user.covername) as user_picurl from t_game_group_user,t_user where gameid=$gameid and t_user.id in (t_game_group_user.userid)";
		$palyer_list = $this->ci->db->query($sql)->result_array();
		$type = $game_info['gametype'];
		if($team_id > 1){
			$code = $this->is_team_user($team_id,$userid);
			foreach ($palyer_list as $key => $value) {
				$id = $value['userid'];

				$user_nickname = $this->get_game_applyer_nickname($gameid,$id);
                if(strlen($user_nickname) > 0){
                    $palyer_list[$key]['nickname'] = $user_nickname;
                }else{
                    if($code){
                        $user_nickname = $this->get_team_user_nickname($team_id,$id);
                        if(strlen($user_nickname) > 0){
                            $palyer_list[$key]['nickname'] = $user_nickname;
                        }
                     }
                }

				// $sql = "select team_user_nickname from t_team_user where teamid=$team_id and userid=$id";
				// $team_user_nickname = $this->ci->db->query($sql)->row_array();
				// if(strlen($team_user_nickname['team_user_nickname']) > 1){
				// 	unset($palyer_list[$key]['nickname']);
				// 	$palyer_list[$key]['nickname'] = $team_user_nickname['team_user_nickname'];
				// }
			}
		}else{
			foreach ($palyer_list as $key => $value) {
				$id = $value['userid'];

				if ($type == 2) {
                    $user_nickname = $this->get_game_applyer_nickname($gameid,$id);
                    if(strlen($user_nickname) > 0){
                        $palyer_list[$key]['nickname'] = $user_nickname;
                    }else{
                        $user_nickname = $this->get_friend_nickname($userid,$id);
                        if(strlen($user_nickname) > 0){
                            $palyer_list[$key]['nickname'] = $user_nickname;
                        }
                    }
                }else{
                    $user_nickname = $this->get_friend_nickname($userid,$id);
                    if(strlen($user_nickname) > 0){
                        $palyer_list[$key]['nickname'] = $user_nickname;
                    }
                }

				// $sql = "select nickname from t_friend where userid=$userid and fuserid=$id";
				// $user_remarks = $this->ci->db->query($sql)->row_array();
				// if(strlen($user_remarks['nickname']) > 1){
				// 	unset($palyer_list[$key]['nickname']);
				// 	$palyer_list[$key]['nickname'] = $user_remarks['nickname'];
				// }
			}
		}
		$haveme = false;

		foreach ($palyer_list as $index => $one_player) {
			$me    = $userid;
			$other = $one_player['id'];
			//relationship

			if ($me == $other) {
				$haveme = true;
				break;
			}

		}

		$user_num = count($palyer_list);
		logtext('XGAME >>> '.$gameid);
		$sql = "select courtid,courtname  from t_course_court where courtid in";
		$sql .= " (select courtid from t_game_court where gameid=$gameid ) ";
		logtext($sql);
		$court_info              = $this->ci->db->query($sql)->result_array();
		logtext($court_info);
		$game_info['court_info'] = $court_info;

		$group_info            = $this->m_get_group_info($gameid);

		if($team_id > 1){
			$code = $this->is_team_user($team_id,$userid);
			foreach ($group_info as $key => $value) {
				$group_user = $value['group_user'];
				foreach ($group_user as $k => $v) {
					$id = $v['userid'];

					$user_nickname = $this->get_game_applyer_nickname($gameid,$id);
	                if(strlen($user_nickname) > 0){
	                    $group_info[$key]['group_user'][$k]['nickname'] = $user_nickname;
	                }else{
	                    if($code){
	                        $user_nickname = $this->get_team_user_nickname($team_id,$id);
	                        if(strlen($user_nickname) > 0){
	                            $group_info[$key]['group_user'][$k]['nickname'] = $user_nickname;
	                        }
	                     }
	                }

					// $sql = "select team_user_nickname from t_team_user where teamid=$team_id and userid=$id";
					// $team_user_nickname = $this->ci->db->query($sql)->row_array();
					// if(strlen($team_user_nickname['team_user_nickname']) > 1){
					// 	unset($group_info[$key]['group_user'][$k]['nickname']);
					// 	$group_info[$key]['group_user'][$k]['nickname'] = $team_user_nickname['team_user_nickname'];
					// }
				}
			}
		}else{
			foreach ($group_info as $key => $value) {
				$group_user = $value['group_user'];
				foreach ($group_user as $k => $v) {
					$id = $v['userid'];

					if ($type == 2) {
                    $user_nickname = $this->get_game_applyer_nickname($gameid,$id);
                    if(strlen($user_nickname) > 0){
                        $group_info[$key]['group_user'][$k]['nickname'] = $user_nickname;
                    }else{
                        $user_nickname = $this->get_friend_nickname($userid,$id);
                        if(strlen($user_nickname) > 0){
                            $group_info[$key]['group_user'][$k]['nickname'] = $user_nickname;
                        }
                    }
                }else{
                    $user_nickname = $this->get_friend_nickname($userid,$id);
                    if(strlen($user_nickname) > 0){
                        $group_info[$key]['group_user'][$k]['nickname'] = $user_nickname;
                    }
                }

					// $sql = "select nickname from t_friend where userid=$userid and fuserid=$id";
					// $user_remarks = $this->ci->db->query($sql)->row_array();
					// if(strlen($user_remarks['nickname']) > 1){
					// 	unset($group_info[$key]['group_user'][$k]['nickname']);
					// 	$group_info[$key]['group_user'][$k]['nickname'] = $user_remarks['nickname'];
					// }
				}
			}
		}
		$ret                   = array();
		$ret['error_code']     = 1;
		$ret['error_descr']    = 'success';
		$ret['game_info']      = $game_info;
		$ret['team_info']      = $team_info;
		$ret['creatorid_info'] = $creatorid_info;
		$ret['player_num']     = $user_num;
		$ret['group_info']     = $group_info;
		$ret['palyer_list']    = $palyer_list;
		$ret['haveme']         = $haveme;

		return $ret;

	}

	public function get_game_detail($gameid) {
		$this->ci->load->database();
		$query   = $this->ci->db->get_where('t_game', array('gameid' => $gameid));
		$summary = $query->row_array();

		$gametype = $summary['gametype'];

		$teamid               = $summary['teamid'];
		$query                = $this->ci->db->get_where('team', array('teamid' => $teamid));
		$team_summary         = $query->row_array();
		$team_name            = $team_summary['team_name'];
		$summary['team_name'] = $team_name;

		$course_id      = $summary['courseid'];
		$course         = $this->ci->db->get_where('t_course', array('courseid' => $course_id))->row_array();
		$course_name    = $course['name'];
		$sys_path       = str_replace("'", "", SERVER_PIC_DIR);
		$course_pic_url = $sys_path . $course['coverpath'] . '/' . $course['covername'];
		if($gametype == GAME_TYPE_INNER){
				$summary['picurl']      = $sys_path.$team_summary['coverpath'].'/'.$team_summary['covername'];
		}else{
				$summary['picurl']      = $sys_path.$summary['game_group_coverpath'].'/'.$summary['game_group_covername'];
		}
		//$summary['picurl']      = $course_pic_url;
		$summary['course_name'] = $course_name;
		$groupinfo              = $this->m_get_group_info($gameid);
		$summary['group_info']  = $groupinfo;

		if ($gametype == GAME_TYPE_INNER) {
			$summary['players']      = $team_name;
			$summary['mask_groupid'] = -1;
		} else {
			$players = '';
			foreach ($groupinfo as $one_group) {
				$group_users = $one_group['group_user'];
				foreach ($group_users as $one_play_info) {
					$players .= $one_play_info['nickname'] . ",";
				}
			}
			$players            = rtrim($players, ',');
			$summary['players'] = $players;

			$sql                     = "select groupid from t_game_group  where gameid=$gameid limit 1 ";
			$game_group_summary      = $this->ci->db->query($sql)->row_array();
			$groupid                 = $game_group_summary['groupid'];
			$summary['mask_groupid'] = $groupid;
		}
		return $summary;
	}

	public function create_game_main_data($para) {
		$this->ci->load->database();
		$random_no = md5(microtime());
		$ts        = $para['pre_starttime'];

		$factor = 1;
		if ($ts > 1408000000000) {
			$factor = 1000;
		}

		$pre_starttime  = date('Y-m-d H:i:s', round($ts / $factor));
		$ts             = $para['apply_stoptime'];
		$apply_stoptime = date('Y-m-d H:i:s', round($ts / $factor));

		if (($para['gametype'] == GAME_TYPE_YUEQIU) || ($para['gametype'] == GAME_TYPE_INSTANT)) {
			$apply_stoptime = $pre_starttime;
		}

		$game = array(
			'name'           => $para['name'],
			'courseid'       => $para['courseid'],
			'privacy'        => $para['privacy'],
			'cost'           => $para['cost'],
			'remark'         => $para['remark'],
			'slongitude'     => $para['slongitude'],
			'slatitude'      => $para['slatitude'],
			'teamid'         => $para['teamid'],
			'pre_starttime'  => $pre_starttime,
			'apply_stoptime' => $apply_stoptime,
			'gamestate'      => GAME_INITED,
			'gametype'       => $para['gametype'],
			'gamerule'       => $para['gamerule'],
			'create_ip'      => $para['agent_ip'],
			'creatorid'      => $para['agent_userid'],
			'adminid'        => $para['agent_userid'],
			'creat_time'     => date('Y-m-d H:i:s'),
			'offline_gameid' => $para['offline_gameid'],
			'random_no'      => $random_no);

		$this->ci->db->insert('t_game', $game);
		$gameid = mysql_insert_id();

		$t_game_group = array(
			'gameid'            => $gameid,
			'group_name'        => '组1',
			'random_no'         => $random_no,
			'group_create_time' => date('Y-m-d H:i:s'));

		$this->ci->db->insert('t_game_group', $t_game_group);
		$groupid = mysql_insert_id();

		$newgame = array('gameid' => $gameid, 'groupid' => $groupid, 'random_no' => $random_no);
		return $newgame;

	}

	public function set_game_court($para) {
		$this->ci->load->database();
		$gameid    = $para['gameid'];
		$random_no = $para['random_no'];
		if ((array_key_exists('court_info', $para)) && (strlen($para['court_info']) > 0)) {
			$court_info = $para['court_info'];
			$court_info = array_filter(explode(",", $court_info));
			$this->ci->db->delete('t_game_court', array('gameid' => $gameid));
			$t_game_court = array();
			$index = 1;
			foreach ($court_info as $one_court) {
				$t_game_court[] = array('gameid' => $gameid, 'courtid' => $one_court, 'random_no' => $random_no,'court_key'=>$index);
				$index++;
			}
			$this->ci->db->insert_batch('t_game_court', $t_game_court);
		}
	}

	public function set_game_attenders($para) {
		$this->ci->load->database();
		$gameid    = $para['gameid'];
		$attenders = trim($para['attenders']);
		$sql       = "delete from t_game_group_user where gameid=$gameid  and  userid not in ( $attenders ) ";
		$this->ci->db->query($sql);
		$sql            = "select  max(groupid) as groupid from t_game_group where gameid=$gameid ";
		$groupinfo      = $this->ci->db->query($sql)->row_array();
		$groupid        = $groupinfo['groupid'];
		$attender_array = array_filter(explode(",", $attenders));
		foreach ($attender_array as $one_player) {
			$arr = array(
				'gameid'    => $gameid,
				'groupid'   => $groupid,
				'random_no' => $para['random_no'],
				'userid'    => $one_player);
			$this->ci->db->insert('t_game_group_user', $arr);
		}
	}

	public function set_game_attender_tland($para) {
		$this->ci->load->database();
		$attender_tland      = trim($para['attender_tland']);
		$attender_tland_list = array_filter(explode("|", $attender_tland));
		foreach ($attender_tland_list as $one) {
			$detail = array_filter(explode(",", $one));
			$this->ci->db->where(array('gameid' => $para['gameid'], 'userid' => $detail[0]));
			$this->ci->db->update('t_game_group_user', array('tland_chose' => $detail[1]));
			$this->ci->db->where(array('gameid' => $para['gameid'], 'userid' => $detail[0]));
			$this->ci->db->update('t_game_score', array('tland' => $detail[1]));
		}
	}

	public function m_get_group_info($gameid) {
		$this->ci->load->database();
		$sys_path  = SERVER_PIC_DIR;
		$sql_group = "";
		$sql_group .= "select groupid,group_name,group_start_status,group_slogan ";
		$sql_group .= " from t_game_group";
		$sql_group .= " where gameid=$gameid";
		$groups = $this->ci->db->query($sql_group)->result_array();
		$index  = 0;
		foreach ($groups as $one_group) {
			$groupid        = $one_group['groupid'];
			$sql_group_user = "";

			$sql_group_user = "select  userid,nickname as username,nickname, ";
			$sql_group_user .= "concat($sys_path,t_user.coverpath,'/c240_',t_user.covername) as user_picurl,  ";
			$sql_group_user .= " confirmed,confirmed_time,admin,t_game_group_user.longitude,t_game_group_user.latitude,t_game_group_user.addtime,t_game_group_user.addip";
			$sql_group_user .= " from t_game_group_user,t_user";
			$sql_group_user .= " where  t_game_group_user.groupid=$groupid";
			$sql_group_user .= "   and t_user.id=t_game_group_user.userid";
			$group_user                   = $this->ci->db->query($sql_group_user)->result_array();
			$groups[$index]['group_user'] = $group_user;
			$index++;
		}
		return $groups;
	}

	public function m_get_yueqiu_overdated($userid, $gametype) {
		$GAME_OVERDATE = GAME_OVERDATE;
		$this->ci->load->database();
		$sql        = "select gameid from t_game where gameid in (select gameid from t_game_group_user where userid=$userid) and gametype=$gametype and gamestate=$GAME_OVERDATE order by UNIX_TIMESTAMP(pre_starttime) desc";
		$rows       = $this->ci->db->query($sql)->result_array();
		$rows_fixed = $this->fix_game_detail($rows);
		return $rows_fixed;
	}

	public function m_get_yueqiu_inited_and_running($userid, $gametype) {
		$GAME_INITED = GAME_INITED;
		$this->ci->load->database();
		$sql = "select gameid from t_game where gameid in (select gameid from t_game_group_user where userid=$userid) and gamestate=$GAME_INITED and gametype=$gametype order by  UNIX_TIMESTAMP(pre_starttime) desc";

		$rows       = $this->ci->db->query($sql)->result_array();
		$rows_fixed = $this->fix_game_detail($rows);
		return $rows_fixed;
	}

	public function m_get_my_game_on_running($userid) {
		$GAME_STATUS_RUNNING = GAME_RUNNING;
		$this->ci->load->database();
		$sql = "select t_game.gameid,t_game.name,t_game.gametype from t_game where gamestate=$GAME_STATUS_RUNNING  and gameid in";
		$sql .= "( select gameid from  t_game_group_user where  userid=$userid )";
		$sql .= "order by gametype";

		$rows       = $this->ci->db->query($sql)->result_array();
		$rows_fixed = $this->fix_game_detail($rows);
		return $rows_fixed;
	}

	public function m_get_my_friends_public_games_on_waiting($userid) {
		//即将进行中的。

		$GAME_STATUS_WAIT_START = GAME_GROUPED;
		$this->ci->load->database();
		$friedn_str = $this->m_friends($userid);
		$game_open = GAME_PUBLIC;
		$friend_status = IS_GAME_FRIEND;
		//games from friends public game
		$sql1 = "select starttime,gametype, t_game.gameid,t_game.name from t_game where gamestate=$GAME_STATUS_WAIT_START";
		$sql1 .= " and privacy=$game_open and  gameid in ";
		$sql1 .= "(select gameid from  t_game_group_user where userid in ($friedn_str))";
		$sql1 .= " and gameid not in (select gameid from  t_game_group_user where userid=$userid)";

		$sql2 = "select starttime,gametype,t_game.gameid,t_game.name from t_game where gamestate=$GAME_STATUS_WAIT_START";
		$sql2 .= " and  gameid in ";
		$sql2 .= "(select gameid from  t_game_group_user where userid=$userid )";
		$sql2 .= "order by  starttime desc, gametype";

		$sql = $sql1 . " union " . $sql2;

		$rows       = $this->ci->db->query($sql)->result_array();
		$rows_fixed = $this->fix_game_detail($rows);
		return $rows_fixed;
	}

	public function m_get_my_friends_public_games_on_running($userid) {
		$GAME_STATUS_RUNNING = GAME_RUNNING;
		$game_open           = GAME_PUBLIC;
		$friend_status = IS_GAME_FRIEND;
		$friend_str = $this->m_friends($userid);
		$this->ci->load->database();
		$sql1 = "select starttime,gametype, t_game.gameid,t_game.name from t_game where gamestate=$GAME_STATUS_RUNNING";
		$sql1 .= " and privacy=$game_open and  gameid in ";
		$sql1 .= "(select gameid from  t_game_group_user where userid in ($friend_str))";
		$sql1 .= " and gameid not in (select gameid from  t_game_group_user where userid=$userid)";

		$sql2 = "select starttime,gametype,t_game.gameid,t_game.name from t_game where gamestate=$GAME_STATUS_RUNNING";
		$sql2 .= " and  gameid in ";
		$sql2 .= "(select gameid from  t_game_group_user where userid=$userid )";
		$sql2 .= "order by  starttime desc, gametype";

		$sql = $sql1 . " union " . $sql2;

		$rows       = $this->ci->db->query($sql)->result_array();
		$rows_fixed = $this->fix_game_detail($rows);
		return $rows_fixed;
	}

	public function m_get_closed_games_withme_or_with_friend($userid) {

		$GAME_STATUS_ENDED = GAME_ENDED;
		$game_open         = GAME_PUBLIC;
		$friend_status = IS_GAME_FRIEND;
		$this->ci->load->database();
		$sql_me = "select  starttime,gametype,  t_game.gameid,t_game.name from t_game where gamestate=$GAME_STATUS_ENDED";
		$sql_me .= " and  gameid in (select gameid from  t_game_group_user where userid=$userid )";
		$sql_friend = "select  starttime,gametype,t_game.gameid,t_game.name from t_game where gamestate=$GAME_STATUS_ENDED";
		$sql_friend .= " and privacy=$game_open and  gameid in ";
		$sql_friend .= "(select gameid from  t_game_group_user where userid in ( select fuserid from t_friend where userid=$userid and status=$friend_status))";
		$sql_friend .= " and gameid not in (select gameid from  t_game_group_user where userid=$userid)";
		$sql_friend .= "order by  starttime desc, gametype";
		$sql = $sql_me . " union " . $sql_friend;

		$rows       = $this->ci->db->query($sql)->result_array();
		$rows_fixed = $this->fix_game_detail($rows);
		return $rows_fixed;
	}

	public function push_route($receiver,$action,$uid,$note,$markname,$id = -1) {

		include_once ('/opt/space/webroot/ugf/api/v5/controllers/libs/push/xgpush.php');
		//include_once('libs/push/xgpush.php');
		$pushcfg = array(
			'add' => array(
				'memo'   => '创建球队',
				'title'  => '创建了球队',
				'module' => 'team',
				'_uid'   => 'teamid',

			),

			//'MODTG' => array(
			'editgame' => array(
				'memo'   => '修改队内比赛',
				'title'  => '修改队内比赛',
				'module' => 'team',
				'_uid'   => 'gameid',
			),

			'ADDYQ' => array(
				'memo'   => '创建约球',
				'title'  => '约球了',
				'module' => 'user',
				'module' => 'user',
				'_uid'   => 'gameid',
			),

			'CANCELG' => array(
				'memo'   => '取消比赛',
				'title'  => '比赛取消了',
				'module' => 'user',
				'module' => 'user',
				'_uid'   => 'gameid',
			),

			'INSTG' => array(
				'memo'   => '立即比赛',
				'title'  => '立即比赛',
				'module' => 'user',
				'_uid'   => 'gameid',
			),

			'MODSCORE' => array(
				'memo'   => '成绩修改了',
				'title'  => '成绩修改了',
				'module' => 'user',
				'_uid'   => 'gameid',
			),

			'gamechat' => array(
				'memo'   => '修改了成绩',
				'title'  => '修改了成绩',
				'module' => 'user',
				'_uid'   => 'gameid',
			),

			'MODIYQ' => array(
				'memo'   => '修改约球',
				'title'  => '修改了约球',
				'module' => 'user',
				'_uid'   => 'gameid',
			),

			//'ADDTG' => array(
			'addgame' => array(
				'memo'   => '创建对内比赛',
				'title'  => '创建了队内比赛',
				'module' => 'team',
				'_uid'   => 'teamid',
				'id'     => 'gameid',
			)

		);

		$num = func_num_args();

		$pc = array();

		$pc['action'] = $action;
		$pc['uid']    = $uid;
		//$pc['title']  = $pushcfg[$action]['title'];
		$pc['module'] = $pushcfg[$action]['module'];
		if($id > 0){
			$pc['id'] = $id;
		}
		// if ($num == 6) {
		// 	$another                   = func_get_arg(3);
		// 	$username                  = $another['username'];
		// 	$newpc                     = array_merge($pc, func_get_arg(3));
		// 	$pushcfg[$action]['title'] = $username . '已' . $pushcfg[$action]['title'];
		// } else {
		// 	$newpc = $pc;
		// }
		// $logarr = array('reciver' => $receiver, 'title' => $pushcfg[$action]['title'], 'pc' => $newpc);
		// logtext($logarr);
		if(!empty($note)){
			$title = $note['note'];
        	unset($note['desc']);
			$pc['content'] = json_encode($note);
		}else{
			$title = "创建一场比赛";
		}
		
		$pc['title'] = $markname;
		$pc['descr'] = $title;
		
		//$pret = pushUserMsg($receiver, $pushcfg[$action]['title'], json_encode($newpc));
		$pret = pushUserMsg($receiver, $markname.':'.$title, json_encode($pc));

	}

	public function fix_game_detail($rows) {
		$ret = array();
		for ($i = 0; $i < count($rows); $i++) {
			$one_game  = $rows[$i];
			$gameid    = $one_game['gameid'];
			$gametype  = $one_game['gametype'];
			$game_info = $this->get_game_detail($gameid);
			$ret[]     = $game_info;
		}
		return $ret;
	}

	public function html_experince($experience, $experience_rules, $rank) {
		//#00ccff
		//#e6e6e6
		if ($rank == 0) {
			$shengyu_jingyan = 300 - $experience;
		}
		if ($rank == 1) {
			$shengyu_jingyan = 300 - $experience;
		}
		if ($rank == 2) {
			$shengyu_jingyan = 900 - $experience;
		}
		if ($rank == 3) {
			$shengyu_jingyan = 2700 - $experience;
		}
		if ($rank == 4) {
			$shengyu_jingyan = 5000 - $experience;
		}
		if ($rank == 5) {
			$shengyu_jingyan = 7800 - $experience;
		}
		if ($rank == 6) {
			$shengyu_jingyan = 11500 - $experience;
		}
		if ($rank == 7) {
			$shengyu_jingyan = 15500 - $experience;
		}
		if ($rank == 8) {
			$shengyu_jingyan = 20000 - $experience;
		}
		if ($rank == 9) {
			$shengyu_jingyan = 25500 - $experience;
		}
		if ($rank == 10) {
			$shengyu_jingyan = 32000 - $experience;
		}
		if ($rank == 11) {
			$shengyu_jingyan = 40000 - $experience;
		}
		if ($rank == 12) {
			$shengyu_jingyan = 48500 - $experience;
		}
		if ($rank == 13) {
			$shengyu_jingyan = 60000 - $experience;
		}
		if ($rank == 14) {
			$shengyu_jingyan = 72000 - $experience;
		}
		if ($rank == 15) {
			$shengyu_jingyan = 72000 - $experience;
			if ($shengyu_jingyan == 0) {
				$shengyu_jingyan = '亲。你已经顶级了';
			}
		}
		$str = "<div>";
		$str .= "<table bgColor=#f5f5f5 width=305px>";
		$str .= "<tr><td>&nbsp;&nbsp;&nbsp;&nbsp;</td><td>&nbsp;&nbsp;</td></tr>";
		$str .= "<tr align='center'><th><font size=5 color=#cccccc>我的经验值</font></th></tr>";
		$str .= "<tr align='center'><td><font size=10 color=#333333>$experience</font></td></tr>";
		$str .= "<tr align='center'><td>LV.$rank</td><td>&nbsp;&nbsp;</td></tr>";
		$str .= "<tr align='center'><td><font size=2 color=#666666>升级到下一级还需经验值：$shengyu_jingyan</font></td></tr>";
		$str .= "<tr><td>&nbsp;&nbsp;&nbsp;&nbsp;</td><td>&nbsp;&nbsp;</td></tr>";
		$str .= "</table>";
		$str .= "</div>";
		$str .= "<div>";
		$str .= "<table>";
		$str .= "<tr><td>&nbsp;&nbsp;&nbsp;&nbsp;</td></tr>";
		$str .= "<tr><td>&nbsp;&nbsp;&nbsp;&nbsp;</td></tr>";
		$str .= "<tr><th align='left'><font size=5 color=#cccccc>$experience_rules[0]</font></th></tr>";
		$str .= "<tr><td>&nbsp;&nbsp;&nbsp;&nbsp;</td></tr>";
		$str .= "<tr><td><font size=2 color=#333333>$experience_rules[1]</font></td></tr>";
		$str .= "<tr><td><font size=2 color=#333333>$experience_rules[2]</font></td></tr>";
		$str .= "<tr><td><font size=2 color=#333333>$experience_rules[3]</font></td></tr>";
		$str .= "<tr><td>&nbsp;&nbsp;&nbsp;&nbsp;</td></tr>";
		$str .= "<tr><td>&nbsp;&nbsp;&nbsp;&nbsp;</td></tr>";
		$str .= "<tr><th align='left'><font size=5 color=#cccccc>$experience_rules[4]</font></th></tr>";
		$str .= "<tr><td>&nbsp;&nbsp;&nbsp;&nbsp;</td></tr>";
		$str .= "<tr><td><font size=2 color=#333333>$experience_rules[5]</font></td></tr>";
		$str .= "<tr><td><font size=2 color=#333333>$experience_rules[6]</font></td></tr>";
		$str .= "<tr><td><font size=2 color=#333333>$experience_rules[7]</font></td></tr>";
		$str .= "<tr><td><font size=2 color=#333333>$experience_rules[8]</font></td></tr>";
		$str .= "<tr><td><font size=2 color=#333333>$experience_rules[9]</font></td></tr>";
		$str .= "<tr><td><font size=2 color=#333333>$experience_rules[10]</font></td></tr>";
		$str .= "<tr><td>&nbsp;&nbsp;&nbsp;&nbsp;</td></tr>";
		$str .= "<tr><td>&nbsp;&nbsp;&nbsp;&nbsp;</td></tr>";
		$str .= "<tr><th align='left'><font size=5 color=#cccccc>$experience_rules[11]</font></th></tr>";
		$str .= "<tr><td>&nbsp;&nbsp;&nbsp;&nbsp;</td></tr>";
		$str .= "<tr><td><font size=2 color=#333333>$experience_rules[12]</font></td></tr>";
		$str .= "</table>";
		$str .= "</div>";
		iconv_set_encoding("output_encoding", "UTF-8");
		return $str;

	}

	public function html_credit($credit, $credit_rules) {
		$str = "<div>";
		$str .= "<table bgColor=#f5f5f5 width=305px>";
		$str .= "<tr><td>&nbsp;&nbsp;&nbsp;&nbsp;</td><td>&nbsp;&nbsp;</td></tr>";
		$str .= "<tr align='center'><th><font size=5 color=#cccccc>我的积分</font></th></tr>";
		$str .= "<tr align='center'><td><font size=10 color=#333333>$credit</font></td></tr>";
		$str .= "<tr><td>&nbsp;&nbsp;&nbsp;&nbsp;</td><td>&nbsp;&nbsp;</td></tr>";
		$str .= "</table>";
		$str .= "</div>";
		$str .= "<div>";
		$str .= "<table>";
		$str .= "<tr><td>&nbsp;&nbsp;&nbsp;&nbsp;</td></tr>";
		$str .= "<tr><td>&nbsp;&nbsp;&nbsp;&nbsp;</td></tr>";
		$str .= "<tr><th align='left'><font size=5 color=#cccccc>$credit_rules[0]</font></th></tr>";
		$str .= "<tr><td>&nbsp;&nbsp;&nbsp;&nbsp;</td></tr>";
		$str .= "<tr><td><font size=2 color=#333333>$credit_rules[1]</font></td></tr>";
		$str .= "<tr><td><font size=2 color=#333333>$credit_rules[2]</font></td></tr>";
		$str .= "<tr><td>&nbsp;&nbsp;&nbsp;&nbsp;</td></tr>";
		$str .= "<tr><td>&nbsp;&nbsp;&nbsp;&nbsp;</td></tr>";
		$str .= "<tr><th align='left'><font size=5 color=#cccccc>$credit_rules[3]</font></th></tr>";
		$str .= "<tr><td>&nbsp;&nbsp;&nbsp;&nbsp;</td></tr>";
		$str .= "<tr><td><font size=2 color=#333333>$credit_rules[4]</font></td></tr>";
		$str .= "<tr><td><font size=2 color=#333333>$credit_rules[5]</font></td></tr>";
		$str .= "<tr><td><font size=2 color=#333333>$credit_rules[6]</font></td></tr>";
		$str .= "<tr><td><font size=2 color=#333333>$credit_rules[7]</font></td></tr>";
		$str .= "<tr><td><font size=2 color=#333333>$credit_rules[8]</font></td></tr>";
		$str .= "<tr><td><font size=2 color=#333333>$credit_rules[9]</font></td></tr>";
		$str .= "<tr><td>&nbsp;&nbsp;&nbsp;&nbsp;</td></tr>";
		$str .= "<tr><td>&nbsp;&nbsp;&nbsp;&nbsp;</td></tr>";
		$str .= "<tr><th align='left'><font size=5 color=#cccccc>$credit_rules[10]</font></th></tr>";
		$str .= "<tr><td>&nbsp;&nbsp;&nbsp;&nbsp;</td></tr>";
		$str .= "<tr><td><font size=2 color=#333333>$credit_rules[11]</font></td></tr>";
		$str .= "</table>";
		$str .= "</div>";
		iconv_set_encoding("output_encoding", "UTF-8");
		return $str;

	}

	public function get_team_game_by_status($teamid, $status,$is_admin) {
		$GAME_STATUS = $status;
		$this->ci->load->database();
		$sql        = "select t_game.gameid,t_game.name from t_game where gametype=1 and gamestate=$GAME_STATUS  and teamid=$teamid order by t_game.gameid desc";
		$rows       = $this->ci->db->query($sql)->result_array();
		$rows_fixed = $this->fix_game_detail_team($rows,$is_admin);
		return $rows_fixed;
	}

	public function fix_game_detail_team($rows,$is_admin) {
		$ret = array();
		for ($i = 0; $i < count($rows); $i++) {
			$one_game  = $rows[$i];
			$gameid    = $one_game['gameid'];
			$game_info = $this->get_team_game_detail($gameid,$is_admin);
			$ret[]     = $game_info;
		}
		return $ret;
	}

	public function get_team_game_detail($gameid,$is_admin) {
		$this->ci->load->database();
		$query   = $this->ci->db->get_where('t_game', array('gameid' => $gameid));
		$summary = $query->row_array();
		$summary['isadmin'] = $is_admin;

		$course_id              = $summary['courseid'];
		$course                 = $this->ci->db->get_where('t_course', array('courseid' => $course_id))->row_array();
		$course_name            = $course['name'];
		$sys_path               = str_replace("'", "", SERVER_PIC_DIR);
		$course_pic_url         = $sys_path . $course['coverpath'] . '/' . $course['covername'];
		$summary['picurl']      = $course_pic_url;
		$summary['course_name'] = $course_name;

		return $summary;
	}
	
	
	
	public function  genGroupPic($users,$gameid) {
		if( !is_array($users) ||  count($users)< 1){
			return "group/holder_group_cover.png";
		}

		$group_cfg = array(
			'g1' => array(
		array('x' => 74, 'y' => 74, 'resize' => 124)
		),
			'g2' => array(
		array('x' => 8, 'y' => 74, 'resize' => 124),
		array('x' => 140, 'y' => 74, 'resize' => 124)
		),
			'g3' => array(
		array('x' => 74, 'y' => 8, 'resize' => 124),
		array('x' => 8, 'y' => 140, 'resize' => 124),
		array('x' => 140, 'y' => 140, 'resize' => 124)
		),
			'g4' => array(
		array('x' => 8, 'y' => 8, 'resize' => 124),
		array('x' => 140, 'y' => 8, 'resize' => 124),
		array('x' => 8, 'y' => 140, 'resize' => 124),
		array('x' => 140, 'y' => 140, 'resize' => 124)
		),

			'g5' => array(
		array('x' => 8, 'y' => 52, 'resize' => 80),
		array('x' => 96, 'y' => 52, 'resize' => 80),
		array('x' => 184, 'y' => 52, 'resize' => 80),
		array('x' => 52, 'y' => 140, 'resize' => 80),
		array('x' => 140, 'y' => 140, 'resize' => 80)
		),
			'g6' => array(
		array('x' => 8, 'y' => 52, 'resize' => 80),
		array('x' => 96, 'y' => 52, 'resize' => 80),
		array('x' => 184, 'y' => 52, 'resize' => 80),
		array('x' => 8, 'y' => 140, 'resize' => 80),
		array('x' => 96, 'y' => 140, 'resize' => 80),
		array('x' => 184, 'y' => 140, 'resize' => 80)
		),
			'g7' => array(
		array('x' => 96, 'y' => 8, 'resize' => 80),
		array('x' => 8, 'y' => 96, 'resize' => 80),
		array('x' => 96, 'y' => 96, 'resize' => 80),
		array('x' => 184, 'y' => 96, 'resize' => 80),
		array('x' => 8, 'y' => 184, 'resize' => 80),
		array('x' => 96, 'y' => 184, 'resize' => 80),
		array('x' => 184, 'y' => 184, 'resize' => 80)
		),
			'g8' => array(
		array('x' => 8, 'y' => 8, 'resize' => 80),
		array('x' => 96, 'y' => 8, 'resize' => 80),
		array('x' => 184, 'y' => 8, 'resize' => 80),
		array('x' => 8, 'y' => 96, 'resize' => 80),
		array('x' => 96, 'y' => 96, 'resize' => 80),
		array('x' => 184, 'y' => 96, 'resize' => 80),
		array('x' => 8, 'y' => 184, 'resize' => 80),
		array('x' => 96, 'y' => 184, 'resize' => 80)
		),
			'g9' => array(
		array('x' => 8, 'y' => 8, 'resize' => 80),
		array('x' => 96, 'y' => 8, 'resize' => 80),
		array('x' => 184, 'y' => 8, 'resize' => 80),
		array('x' => 8, 'y' => 96, 'resize' => 80),
		array('x' => 96, 'y' => 96, 'resize' => 80),
		array('x' => 184, 'y' => 96, 'resize' => 80),
		array('x' => 8, 'y' => 184, 'resize' => 80),
		array('x' => 96, 'y' => 184, 'resize' => 80),
		array('x' => 184, 'y' => 184, 'resize' => 80)
		)
		);
		$files = array();
		foreach ($users as $us) {
			if(strlen($us['coverpath'])>0){
				$files[] = ATTACH_PATH.'/'. $us['coverpath'] . '/c240_' . $us['covername'];
			}else{
				$files[] = ATTACH_PATH ."/group/holder_user_cover.jpg";
			}
		}
		$a = json_encode($files);
		logtext('genGroupPics   >>>>'.$a);
		$copy_option = 100;
		$image = imagecreatetruecolor(272, 272);
		$bg    = imagecolorallocate($image, 235, 235, 235);//定义背景颜色
		imagefill($image, 0, 0, $bg);
		$items = count($files);
		$index = 0;
		foreach ($files as $filename) {
			$small                = imagecreatefromjpeg($filename);
			list($width, $height) = getimagesize($filename);
			$grp                  = 'g' . $items;
			$newwidth             = $group_cfg[$grp][$index]['resize'];
			$x                    = $group_cfg[$grp][$index]['x'];
			$y                    = $group_cfg[$grp][$index]['y'];
			$newheight            = $newwidth;
			$thumb                = imagecreatetruecolor($newwidth, $newheight);
			imagecopyresized($thumb, $small, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);
			//imagecopymerge($image, $thumb, $x, $y, 0, 0, 92, 92, $copy_option);
			imagecopymerge($image, $thumb, $x, $y, 0, 0, $newwidth, $newwidth, $copy_option);
			$index++;
		}

		$opath = "group/". date("Y") . '/' . date("m") . '/' .date("d");
		mt_srand();
		$oname = md5(uniqid(mt_rand())).".png";

		$p = ATTACH_PATH ."/". $opath;
		$array_dir = explode("/",$p);
		for($i=0; $i<count($array_dir); $i++){
			$path .= $array_dir[$i]."/";
			if(!file_exists($path)){
				mkdir($path);
			}
		}
		$this->ci->load->database();
		$this->ci->db->where('gameid',$gameid);
		$this->ci->db->update('t_game',array('game_group_coverpath'=>$opath,'game_group_covername'=>$oname));
		$outfilename = $p ."/". $oname;
		imagepng($image, $outfilename);
		imagedestroy($image);
		imagedestroy($small);
		return $opath. "/" . $oname;
	}
	//判断某场比赛中正常用户的人数
	public function is_normal_user_by_game($gameid){
		$formal = 0;
		$this->ci->load->database();
		$sql = "select distinct  userid from t_game_score where gameid=$gameid";
    		

    	$users = $this->ci->db->query($sql)->result_array();


		foreach ($users as $key => $value) {
            $id = $value['userid'];
            $sql = "select type from t_user where id=$id";
            $type = $this->ci->db->query($sql)->row_array();
            if($type['type'] == 1 || $type['type'] == 3){
                $formal++;
            }
        }
        return $formal;
	}
	//差点
    public function handicap($userid){
    	$this->ci->load->database();
    	//$gamestate = 'GAME_ENDED';
    	/*
    	$sql = "select gameid,min(gross) as grossmin,sum(gross) as sumgross,sum(par) as sumpar,count(gameid) as countgameid,count(par) as parnum ";
    	$sql .= "from t_game_score ";
    	$sql .= "where userid=$userid and gameid in (select gameid from t_game where gamestate=4 and gameid in (select gameid from t_game_group_user where userid=$userid)) group by gameid";
    	logtext('handicap  >>>'.$sql);
    	$user_game_info = $this->ci->db->query($sql)->result_array();
    	*/
    	$user_game_info = $this->get_handicap_and_game_score_by_user($userid);
    	$sumgross = 0;
    	$sumpar = 0;
    	$gameidnum = 0;
    	foreach ($user_game_info as $k => $v) {
    		$formal = 0;
    		$gameid = $v['gameid'];
    		$formal = $this->is_normal_user_by_game($gameid);
	    	if($formal >= 2){
		    	if($v['sumgross'] != 0){
		    		if($v['grossmin'] > 0){
			    		if($v['parnum'] > 9){
			    			$sumgross = $sumgross+$v['sumgross'];
			    			$sumpar = $sumpar+$v['sumpar'];
			    			$gameidnum++;
			    		}
			    	}
		    	}
	    	}
    	}
    	if($sumgross == 0 && $sumpar == 0 && $gameidnum == 0){
    		$handicap = -6;
    	}else{
    		$handicap = ($sumgross - $sumpar)/$gameidnum;
	    	$handicap = round($handicap,1);
    	}

    	$this->ci->db->where('id',$userid);


	    if($handicap < -5){
	    	$this->ci->db->update('t_user',array('handicap'=>null));
	    }else{
	    	$this->ci->db->update('t_user',array('handicap'=>$handicap));
	    }
    }

    //我的公开的比赛
    public function m_get_my_public_game_on_running($userid) {
		$GAME_STATUS_RUNNING = GAME_RUNNING;
		$game_open = GAME_PUBLIC;
		$this->ci->load->database();
		$sql = "select t_game.gameid,t_game.name,t_game.gametype from t_game where gamestate=$GAME_STATUS_RUNNING and gameid in";
		$sql .= "( select gameid from  t_game_group_user where  userid=$userid ) and creatorid is not null ";
		$sql .= "order by UNIX_TIMESTAMP(pre_starttime) desc";

		$rows       = $this->ci->db->query($sql)->result_array();
		$rows_fixed = $this->fix_game_detail($rows);
		return $rows_fixed;
	}

	//我结束的比赛
	public function m_get_closed_games_by_me($userid,$offset,$pagesize){
		$GAME_STATUS_ENDED = GAME_ENDED;
		$this->ci->load->database();
		$sql = "select t_game.gameid,t_game.name,t_game.gametype from t_game where gamestate=$GAME_STATUS_ENDED and gameid in";
		$sql .= "( select gameid from  t_game_group_user where  userid=$userid ) and creatorid is not null ";
		$sql .= "order by UNIX_TIMESTAMP(pre_starttime) desc";
		$sql .= " limit $offset, $pagesize";

		$rows       = $this->ci->db->query($sql)->result_array();
		$rows_fixed = $this->fix_game_detail($rows);
		return $rows_fixed;
	}

	//我的和朋友的结束的比赛
	public function m_get_closed_games_by_me_and_friend($userid,$offset,$pagesize,$type){
		$GAME_STATUS_ENDED = GAME_ENDED;
        $game_open         = GAME_PUBLIC;

        $game_attention_open = GAME_ATTENTION_PUBLIC;
        $friend_status = IS_GAME_FRIEND;
        $friend_str = $this->m_friends($userid);
		$this->ci->load->database();
		$sql_me = "select  starttime,gametype,  t_game.gameid,t_game.name from t_game where gamestate=$GAME_STATUS_ENDED";
        $sql_me .= " and privacy=$game_open and  gameid in (select gameid from  t_game_group_user where userid=$userid ) and creatorid is not null";
        $sql_friend = "select  starttime,gametype,t_game.gameid,t_game.name from t_game where gamestate=$GAME_STATUS_ENDED";
        $sql_friend .= " and privacy=$game_open and  gameid in ";
        $sql_friend .= "(select gameid from  t_game_group_user where userid in ($friend_str))";
        $sql_friend .= " and gameid not in (select gameid from  t_game_group_user where userid=$userid) and creatorid is not null ";

        if($type == "all"){
            $sql_attention = "select  starttime,gametype,t_game.gameid,t_game.name from t_game where gamestate=$GAME_STATUS_ENDED";
            $sql_attention .= " and privacy=$game_attention_open and  gameid in ";
            $sql_attention .= "(select gameid from  t_game_group_user where userid in ( select fuserid from t_friend where userid=$userid and status=0))";
            $sql_attention .= " and gameid not in (select gameid from  t_game_group_user where userid=$userid) and creatorid is not null ";
            $sql_attention .= "order by  starttime desc, gametype";
            $sql = $sql_me . " union " . $sql_friend." union ".$sql_attention;
        }else{
            $sql = $sql_me . " union " . $sql_friend."order by  starttime desc, gametype";
        }   
        $sql .= " limit $offset, $pagesize";

		$rows       = $this->ci->db->query($sql)->result_array();
		$rows_fixed = $this->fix_game_detail($rows);
		return $rows_fixed;
	}

	public function set_game_group_user($para) {
		$this->ci->load->database();
		$gameid    = $para['gameid'];
		$attenders = trim($para['attenders']);
		$sql = "delete from t_game_group where gameid=$gameid";
		$this->ci->db->query($sql);
		$sql = "select count(userid) as usernum from t_game_group_user where gameid=$gameid";
		$num = $this->ci->db->query($sql)->row_array();
		if($num['usernum'] > 0){
			$sql = "delete from t_game_group_user where gameid=$gameid";
			$this->ci->db->query($sql);
		}
		$attender_array = explode('|',$attenders);
		foreach ($attender_array as $players) {
			$group_data = array(
				'gameid'=>$gameid,
				'group_name'=>"组1",
				'group_create_time'=>date('Y-m-d H:i:s'),
				'random_no'=>$para['random_no']
				);
			$this->ci->db->insert('t_game_group',$group_data);
			$groupid = $this->ci->db->insert_id();
			$one_player_array = explode(',',$players);
			foreach ($one_player_array as $one_player) {
				$arr = array(
					'gameid'    => $gameid,
					'groupid'   => $groupid,
					'random_no' => $para['random_no'],
					'userid'    => $one_player);
				$this->ci->db->insert('t_game_group_user', $arr);
			}
			
		}
	}

	public function my_get_yueqiu_inited_and_running_by_creatorid($userid, $gametype) {
		$GAME_INITED = GAME_INITED;
		$this->ci->load->database();
		$sql = "select gameid,gametype from t_game where gameid in (select gameid from t_game_applyer where userid=$userid) and gamestate<>4 and gamestate<>5 and gamestate<>6 and gamestate<>7 and gametype=$gametype order by UNIX_TIMESTAMP(pre_starttime) desc";
		logtext('my_get_yueqiu_inited_and_running_by_creatorid   >>>>>'.$sql);
		$rows       = $this->ci->db->query($sql)->result_array();
		$rows_fixed = $this->fix_game_detail($rows);
		return $rows_fixed;
	}

	public function get_handicap_and_game_score_by_user($userid){
		$this->ci->load->database();
		$sql = "select t_game_score.gameid,sum(t_game_score.gross) as grossnum,min(t_game_score.gross) as grossmin,sum(t_game_score.par) as sumpar,count(t_game_score.par) as holenum,t_game.courseid,t_game.starttime,t_course.name ";
        $sql .= " from t_game_score,t_game,t_course ";
        $sql .= " where t_game_score.userid=$userid and t_game_score.score_status=1 and t_game.gameid in (t_game_score.gameid) ";
        $sql .= " and t_game.gamestate=4 and t_course.courseid in (t_game.courseid) group by t_game_score.gameid order by t_game.starttime desc";

        $user_game_info               = $this->ci->db->query($sql)->result_array();
        return $user_game_info;
	}

	public function m_get_my_attention_games_on_running($userid) {
		$GAME_STATUS_RUNNING = GAME_RUNNING;
		$game_open           = GAME_ATTENTION_PUBLIC;
		$friend_status = IS_GAME_ATTENTION;
		$this->ci->load->database();
		$sql = "select t_game.gameid,t_game.name from t_game where gamestate=$GAME_STATUS_RUNNING";
		$sql .= " and privacy=$game_open and  gameid in ";
		$sql .= "(select gameid from  t_game_group_user where userid in ( select fuserid from t_friend where userid=$userid and status=$friend_status))";
		$sql .= " and gameid not in (select gameid from  t_game_group_user where userid=$userid)";
		$sql .= "order by  starttime desc, gametype";

		$rows       = $this->ci->db->query($sql)->result_array();
		$rows_fixed = $this->fix_game_detail($rows);
		return $rows_fixed;
	}

	public function m_get_my_attention_games_on_waiting($userid) {
		//即将进行中的。

		$GAME_STATUS_WAIT_START = GAME_GROUPED;
		$this->ci->load->database();

		$game_open = GAME_ATTENTION_PUBLIC;
		$friend_status = IS_GAME_ATTENTION;
		//games from friends public game
		$sql1 = "select starttime,gametype, t_game.gameid,t_game.name from t_game where gamestate=$GAME_STATUS_WAIT_START";
		$sql1 .= " and privacy=$game_open and  gameid in ";
		$sql1 .= "(select gameid from  t_game_group_user where userid in ( select fuserid from t_friend where userid=$userid and status=$friend_status))";
		$sql1 .= " and gameid not in (select gameid from  t_game_group_user where userid=$userid)";

		$sql2 = "select starttime,gametype,t_game.gameid,t_game.name from t_game where gamestate=$GAME_STATUS_WAIT_START";
		$sql2 .= " and  gameid in ";
		$sql2 .= "(select gameid from  t_game_group_user where userid=$userid ) and privacy=$game_open ";
		$sql2 .= "order by  starttime desc, gametype";

		$sql = $sql1 . " union " . $sql2;

		$rows       = $this->ci->db->query($sql)->result_array();
		$rows_fixed = $this->fix_game_detail($rows);
		return $rows_fixed;
	}

	public function m_friends($userid){
		$friend_status = IS_GAME_FRIEND;
		$m_friends_str = "";
		$this->ci->load->database();
		$sql = "select fuserid from t_friend where userid=$userid and status=$friend_status";
		$fuserids = $this->ci->db->query($sql)->result_array();
		foreach ($fuserids as $key => $value) {
			$id = $value['fuserid'];
			$sql = "select id from t_friend where userid=$id and fuserid=$userid and status=$friend_status";
			$friend_id = $this->ci->db->query($sql)->row_array();
			if($friend_id['id'] > 1){
				$sql = "select type from t_user where id=$id";
				$type = $this->ci->db->query($sql)->row_array();
				if($type['type'] != 2){
					$m_friends_str .= $id.',';
				}
			}
		}
		$m_friends_array = explode(',',$m_friends_str);
		array_pop($m_friends_array);
		$friend_str = implode(',',$m_friends_array);
		return $friend_str;
	}

	public function get_team_user_nickname($teamid,$userid){
		$this->ci->load->database();
		$sql = "select team_user_nickname from t_team_user where teamid=$teamid and userid=$userid";
		$team_user_nickname = $this->ci->db->query($sql)->row_array();
		$user_team_nickname = $team_user_nickname['team_user_nickname'];
		return $user_team_nickname;
	}

	public function get_game_applyer_nickname($gameid,$userid){
		$this->ci->load->database();
		$sql = "select username from t_game_applyer where gameid=$gameid and userid=$userid";
		$game_user_nickname = $this->ci->db->query($sql)->row_array();
		$user_game_nickname = $game_user_nickname['username'];
		return $user_game_nickname;
	}

	public function get_friend_nickname($userid,$fuserid){
		$this->ci->load->database();
		$sql = "select nickname from t_friend where userid=$userid and fuserid=$fuserid";
		$friend_nickname = $this->ci->db->query($sql)->row_array();
		$name = $friend_nickname['nickname'];
		return $name;
	}

	public function is_team_user($teamid,$userid){
		$this->ci->load->database();
		$sql = "select id from t_team_user where teamid=$teamid and userid=$userid";
		$id = $this->ci->db->query($sql)->row_array();
		if($id['id'] > 0){
			$code = ture;
		}else{
			$code = false;
		}
		return $code;
	}
	public function get_caddie_game_num($userid = 0){
		if($userid < 1){
			return 0;
		}
		$game_ended = GAME_ENDED;
		$caddie_score_state = CADDIE_ENDED;
		$this->ci->load->database();
		$sql = "select count(gameid) as num from t_game where gameid in (select gameid from t_c_caddie_game where caddieid=$userid) and gamestate=$game_ended";
		$game_num = $this->ci->db->query($sql)->row_array();
		return $game_num['num'];
		//return $sql;
	}
	public function get_course_info($courseid = 0){
		if($courseid < 1){
			return array();
		}
        $sys_path            = SERVER_PIC_DIR;
		$this->ci->load->database();
		$sql = "select $courseid as courseid,t_course.pid,t_course.lat,t_course.lgt,t_course.name,t_course.tnum,t_prov_list.prov_name,concat($sys_path,t_course.coverpath,'/',t_course.covername) as coverpath from t_course,t_prov_list where t_course.courseid=$courseid and t_prov_list.prov_code=t_course.pid ";
		$course_info = $this->ci->db->query($sql)->row_array();
		return $course_info;
	}

	public function ceshi_get_caddie_game_num($userid = 0){
		if($userid < 1){
			return 0;
		}
		$game_ended = GAME_ENDED;
		$caddie_score_state = CADDIE_ENDED;
		$this->ci->load->database();
		$sql = "select count(gameid) as num from t_game where gameid in (select gameid from t_c_caddie_game where caddieid=$userid and status=$caddie_score_state) and gamestate=$game_ended";
		$game_num = $this->ci->db->query($sql)->row_array();
		return $sql;
	}

	 public function get_caddie_by_course($courseid, $offset = -1, $pagesize)
    {
        
        $this->ci->load->database();
        $sys_path = SERVER_PIC_DIR;
        $sql      = "select id,nickname,sno,concat($sys_path,coverpath,'/c240_',covername) as user_picurl from t_c_user where domain=$courseid";
        if ($offset > -1) {
            $sql .= " limit $offset, $pagesize";
            $caddies = $this->ci->db->query($sql)->result_array();
            foreach ($caddies as $key => $value) {
                $caddieid = intval($value['id']);
                if ($this->see_caddie_whether_score($caddieid)) {
                    $caddies[$key]['caddie_game_score'] = CADDIE_ACCEPT;
                } else {
                    $caddies[$key]['caddie_game_score'] = 0;
                }
            }
        } else {
            $caddies = $this->ci->db->query($sql)->result_array();
        }
        return $caddies;
    }
    
    public function see_caddie_whether_score($userid)
    {
        $status = CADDIE_ACCEPT;
        $this->ci->load->database();
        $sql = "select gameid from t_c_caddie_game where caddieid=$userid and status=$status";
        $id  = $this->ci->db->query($sql)->result_array();
        if (count($id) < 1) {
            return false;
        } else {
            return ture;
        }
    }

    public function save_one_hole_score($gameid, $userid, $holeid, $court_key, $score_array)
    {
        $where = array(
            'gameid' => $gameid,
            'userid' => $userid,
            'holeid' => $holeid,
            'court_key' => $court_key
        );
        $this->ci->load->database();
        $this->ci->db->where($where);
        $this->ci->db->update('t_game_score', $score_array);
        return;
    }

    public function get_game_caddie($gameid, $groupid)
    {
        $status = CADDIE_ACCEPT;
        $this->ci->load->database();
        $sql     = "select caddieid from t_c_caddie_game where gameid=$gameid and groupid=$groupid and status=$status";
        $caddies = $this->ci->db->query($sql)->result_array();
        return $caddies;
    }

    public function create_game_and_init_group($para)
    {   
        $this->ci->load->database();
        $random_no = md5(microtime());
        $ts        = $para['pre_starttime'];
        
        $factor = 1;
        if ($ts > 1408000000000) {
            $factor = 1000;
        }
        
        $pre_starttime  = date('Y-m-d H:i:s', round($ts / $factor));
        $ts             = $para['apply_stoptime'];
        $apply_stoptime = date('Y-m-d H:i:s', round($ts / $factor));
        
        if ($para['gametype'] == GAME_TYPE_INSTANT) {
            $apply_stoptime = $pre_starttime;
        }
        
        $game = array(
            'name' => $para['name'],
            'courseid' => $para['courseid'],
            'privacy' => $para['privacy'],
            'cost' => $para['cost'],
            'remark' => $para['remark'],
            'slongitude' => $para['slongitude'],
            'slatitude' => $para['slatitude'],
            'teamid' => $para['teamid'],
            'pre_starttime' => $pre_starttime,
            'apply_stoptime' => $apply_stoptime,
            'gamestate' => GAME_INITED,
            'gametype' => $para['gametype'],
            'gamerule' => $para['gamerule'],
            'create_ip' => $para['agent_ip'],
            'creatorid' => $para['agent_userid'],
            'adminid' => $para['agent_userid'],
            'creat_time' => date('Y-m-d H:i:s'),
            'offline_gameid' => $para['offline_gameid'],
            'random_no' => $random_no
        );
        
        $this->ci->db->insert('t_game', $game);
        $gameid = mysql_insert_id();
        
        $t_game_group = array(
            'gameid' => $gameid,
            'group_name' => '组1',
            'random_no' => $random_no,
            'group_create_time' => date('Y-m-d H:i:s')
        );
        
        $this->ci->db->insert('t_game_group', $t_game_group);
        $groupid = mysql_insert_id();
        
        $newgame = array(
            'gameid' => $gameid,
            'groupid' => $groupid,
            'random_no' => $random_no
        );
        return $newgame;
        
    }


     public function have_by_game_group($gameid, $groupid, $userid)
    {   
        $sql = "select id from t_game_group_user where  gameid=$gameid and groupid=$groupid and userid=$userid ";
        $this->ci->load->database();
        $users = $this->ci->db->query($sql)->result_array();
        if (count($users) > 0) {
            return ture;
        } else {
            return false;
        }
        
    }


    public function user_info($userid)
    {   
        $sys_path = SERVER_PIC_DIR;
        $this->ci->load->database();
        $sql = "select id as userid,nickname,concat($sys_path,coverpath,'/c240_',covername) as user_picurl from t_user where id=$userid";
        $user_info = $this->ci->db->query($sql)->row_array();
        return $user_info;
    }


    public function add_caddie_game_chat($gameid, $groupid, $caddieid, $lng, $lat, $userid, $course_name, $username, $user_picurl, $court_name,$operator)
    {   
        $this->ci->load->database();
        $time = time();
        $data = array(
            'caddieid' => $caddieid,
            'gameid' => $gameid,
            'groupid' => $groupid,
            'addtime' => $time,
            'latitude' => $lat,
            'longitude' => $lng,
            'userid' => $userid,
            'course_name' => $course_name,
            'username' => $username,
            'user_picurl' => $user_picurl,
            'court_name' => $court_name
        );
        if($caddieid == $operator){
        	$data['status'] = CADDIE_ACCEPT;
        }
        $this->ci->db->insert('t_caddie_game_chat', $data);
        $id = $this->ci->db->insert_id();
        
        return $id;
    }


}
