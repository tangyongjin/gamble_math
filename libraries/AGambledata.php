<?php

 
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class AGambledata
{
    public function __construct()
    {
        $this->ci =& get_instance();
        $CI =& get_instance();

        
        $CI->load->library('XGame');
        
        $this->xgame = $CI->xgame;
    }


    public function df(){


        echo "<pre>";
        print_r($this->ci->db);
        print_r($this->ci->db->query("select groupid from t_gamble_game_alpha where  gambleid=11"));
        echo "</pre>"; 
    }
    

    public function getGameHoles($gameid)
    {

        $sql = 'set @cnt=0;';
        $this->ci->db->query($sql);
        $sql  = " select @cnt:=@cnt+1 as hindex,court_key,holeid,par,holename  from t_game_court,t_court_hole ";
        $sql .= " where t_game_court.courtid=t_court_hole.courtid ";
        $sql .= "and t_game_court.gameid=$gameid order by court_key,t_court_hole.courtid,holename ";
        $sql .= " limit 18";

        $holes = $this->ci->db->query($sql)->result_array();
        return $holes;
    }


    public function getHoleOrder($groupid, $holecounter){
        $sql = "select holeorder from t_gamble_game_holeorder  where groupid=$groupid ";
        $row = $this->ci->db->query($sql)->row_array();
        $holeorder= $row['holeorder'];

        if(strlen($holeorder) ==0){
            if ($holecounter==9){
                     $holeorder='1,2,3,4,5,6,7,8,9';
            }else
            {
                     $holeorder='1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18';
            }
        }
        return explode(",",$holeorder);
    }



      public function getHoleScore($gameid, $initorder)
    {
         
        $holes = $this->getGameHoles($gameid);

        $score = array();
        $index = 0;
        $score_table='t_game_score';
        foreach ($holes as $key => $one_hole) {
            $score[$index]['id']        = '#' . ($index + 1);
            $score[$index]['holeid']    = $one_hole['holeid'];
            $score[$index]['par']       = $one_hole['par'];
            $score[$index]['selected']       = 'n';
            
            $score[$index]['holename']  = $one_hole['holename'];
            $score[$index]['court_key'] = $one_hole['court_key'];
            $court_key                  = $one_hole['court_key'];
            $holeid                     = $one_hole['holeid'];

            $sql        = "select court_key,userid,gross from t_game_score where gameid=$gameid  and court_key=$court_key and holeid=$holeid and userid in  ($initorder) ";
 
            $s          = $this->ci->db->query($sql)->result_array();
            $raw_scores = array();
            foreach ($s as $one_value) {
                $userid              = $one_value['userid'];
                $raw_scores[$userid] = $one_value['gross'];
            }
            $score[$index]['raw_scores'] = $raw_scores;
            $index++;
        }
        
        return $score;
    }
    

    
    public function getgroupusers($gameid, $groupid, $userid)
    {
        
        $ret = array();
        $gamedetail = $this->xgame->game_detail($gameid, $userid, 'all');
        $gameinfo   = $gamedetail['game_info'];
        $group_info = $gamedetail['group_info'];
        
        $foundkey = -1;
        foreach ($group_info as $key => $one_group) {
            if ($one_group['groupid'] == $groupid) {
                $foundkey = $key;
            }
        }
        
        $found_group_users = $group_info[$foundkey]['group_user'];
        foreach ($found_group_users as $key => $one_user) {
            $found_group_users[$key]['cover'] = $one_user['user_picurl'];
            unset($found_group_users[$key]['user_picurl']);
            unset($found_group_users[$key]['confirmed']);
            unset($found_group_users[$key]['confirmed_time']);
            unset($found_group_users[$key]['admin']);
        }
        return $found_group_users;
    }


    //取游戏涉及到的洞的成绩.
      public function get_used_holedata($gameid, $groupid,$firstholeindex,$lastholeindex, $initorder)
    {
        
        $all_holes = $this->getHoleScore($gameid, $initorder);
        
        $holeorder        =$this->getHoleOrder($groupid,count($all_holes));
        $used_holes_sorted= $this->sortWithHoleOrder($all_holes,$holeorder);
        
        if($lastholeindex > $firstholeindex  ){
            $cross= $lastholeindex -  $firstholeindex +1;
            for ($i=0; $i <$cross ; $i++) { 
             $used_holes_sorted[$i]['selected']='y';
            }
        }
        else
        {
           $start=1;
           $end=$lastholeindex;
           $cross= $end -  $start +1;

            for ($i=0; $i <$cross ; $i++) { 
             $used_holes_sorted[$i]['selected']='y';
            }
           
           $all_counter=count($all_holes);
           $start=$firstholeindex;
           $end=$all_counter;
           $cross= $end -  $start +1;
            for ($i=0; $i <$cross ; $i++) { 
                 $index=$i + $firstholeindex -1;
             $used_holes_sorted[$index]['selected']='y';
            }
        }
 
        return $used_holes_sorted;
    }

     public function searchHoleScore($groupid, $holeid, $court_key)
    {
        
        $ret               = array();
        $sql               = "select holeid,holename,holeno,par from t_court_hole  where  holeid=$holeid";
        $one_hole          = $this->ci->db->query($sql)->row_array();
        $ret['selecthole'] = $one_hole;
        $sql               = "select userid, gross  from t_game_score where  holeid=$holeid and groupid=$groupid and  court_key=$court_key";
        $one_hole_score    = $this->ci->db->query($sql)->result_array();
        $ret['holegross']  = $one_hole_score;
        return $ret;
        
    }

      function create_blank_summary($holeids, $users)
    {
        $summary = array();
        foreach ($holeids as $one_hole) {
            foreach ($users as $one_user) {
                $tmp                  = array(
                    'uid' => $one_user['userid'],
                    'money' => 0
                );
                $summary[$one_hole][] = $tmp;
            }
        }
        return $summary;
    }


      public function array_search_with_kv($arr, $k, $v)
    {
        $found = -1;
        foreach ($arr as $key => $one_row) {
            
            if ($one_row[$k] == $v) {
                $found = $key;
                continue;
            }
            
        }
        return $found;
    }
    
    
    
    
    public function summary_all_hole_money($hole_summary, $base_money_logs)
    {
        
        foreach ($hole_summary as $holeid => $one_hole_info) {
            foreach ($base_money_logs as $gambleid => $one_gamble_data) {
                $one_hole_gamble_data = $one_gamble_data[$holeid];
                foreach ($one_hole_gamble_data as $key => $one_hole_one_user) {
                    $uid                                    = $one_hole_one_user['uid'];
                    $money                                  = $one_hole_one_user['money'];
                    $found                                  = $this->array_search_with_kv($one_hole_info, 'uid', $uid);
                    $hole_summary[$holeid][$found]['money'] = $hole_summary[$holeid][$found]['money'] + $money;
                    
                    
                }
            }
        }
        
        return $hole_summary;
    }

      function get_gamble_by_group($groupid)
    {
        
        $ret = array();
        $sql = "select gameid,gambleid,firstholeindex from t_gamble_game_alpha  where groupid=$groupid and showif=1 ";

 
        $rows = $this->ci->db->query($sql)->result_array();
        if (count($rows) > 0) {
            $ret['gameid']         = $rows[0]['gameid'];
            $ret['firstholeindex'] =6;
            // $rows[0]['firstholeindex'];
            
        } else {
            return null;
        }
        $s                = array_to_string($rows, 'gambleid');
        $ret['gambleids'] = $s;
        return $ret;
    }


    //本组的起始洞.
       function get_group_firstindex($groupid)
    {
        
        $ret = array();
        $sql = "select * from t_gamble_game_holeorder    where groupid=$groupid ";
        $rows = $this->ci->db->query($sql)->result_array();
        if (count($rows) > 0) {
            return $rows[0]['firstholeindex'];
        } else {
            
            return 1;
        }
     }
    

    

     public function getResultbyHole($simple_res, $holeid, $court_key)
    {
        
        $ret = array();
        foreach ($simple_res['gambleinfo'] as $key1 => $one_gamble) {
            $gambleid = $one_gamble['gambleid'];
            $ret[$key1]['gambleid'] = $gambleid;
            $ret[$key1]['rulename'] = $one_gamble['rulename'];
            $ret[$key1]['playernum'] = $one_gamble['playernum'];
            $ret[$key1]['div_option']    = $one_gamble['div_option'];
            $ret[$key1]['holeid']    = $holeid;
            $ret[$key1]['court_key'] = $court_key;
            
            //gambleid
            $gmdata = $simple_res['gambledata'][$gambleid];
            foreach ($gmdata as $key2 => $one_hole) {
                if (($holeid == $one_hole['holeid']) && ($court_key == $one_hole['court_key'])) {
                    $foundkey = $key2;
                    continue;
                }
            }
            $ret[$key1]['base_money_log'] = $gmdata[$foundkey]['base_money_log'];
            $ret[$key1]['rank']           = $gmdata[$foundkey]['rank'];
        }
        
        
        foreach ($ret as $key1 => $one_gamble) {
            $playernum          = $one_gamble['playernum'];
            $div_option             = $one_gamble['div_option'];
            $base_money_log_fix = array();
            $rank               = $one_gamble['rank'];
            foreach ($one_gamble['base_money_log'] as $key2 => $one_user_money) {
                $uid                         = $one_user_money['uid'];
                $rankindex                   = array_search($uid, $rank);
                $one_user_money['rankindex'] = ($rankindex + 1);
                $one_user_money['pcs']       = $one_user_money['score_order'];
                $one_user_money['used']      = 'true';
                $base_money_log_fix[$uid]    = $one_user_money;
            }
            
            $ret[$key1]['base_money_log'] = $base_money_log_fix;
            unset($ret[$key1]['rank']);
        }
        return $ret;
    }


      public function get_gamble_id_for_display_order($gameid, $groupid)
    {
         $sql              = "select gambleid,ruleid,rulesource,playernum  from t_gamble_game_alpha where gameid=$gameid and groupid=$groupid order by playernum desc";
        $game_gambles            = $this->ci->db->query($sql)->result_array();
        $player_num_max_gambleid = 0;
        foreach ($game_gambles as $key => $one_gamble) {
            $gambleid = intval($one_gamble['gambleid']);
            $ruleid   = intval($one_gamble['ruleid']);
            if ($one_gamble['rulesource'] == 'user') {
                $sql       = "select sysruleid from t_gamble_rule_user_alpha where userruleid=$ruleid ";
                $sysruleid = $this->ci->db->query($sql)->row_array();
                $ruleid    = intval($sysruleid['sysruleid']);
            }
            if ($one_gamble['playernum'] == 4) {
                if (in_array($ruleid, array(
                    6,
                    7,
                    8
                ))) {
                    $player_num_max_gambleid = $gambleid;
                    break;
                } else {
                    if ($player_num_max_gambleid == 0) {
                        $player_num_max_gambleid = $gambleid;
                    }
                }
            } elseif ($one_gamble['playernum'] == 3) {
                if ($player_num_max_gambleid == 0) {
                    $player_num_max_gambleid = $gambleid;
                }
            }
        }
        return $player_num_max_gambleid;
    }
 
    public function sortWithHoleOrder($holes,$holeorder){
         
          $ret=array();
          foreach ($holeorder as  $hindex) {
              foreach ($holes as $key => $one_hole) {
                if (($one_hole['hindex']== $hindex)||( $one_hole['id']=='#'.$hindex )){
                     $ret[]=$one_hole;
                }  
              }
          }
          return $ret;
    }

    
}

?> 
