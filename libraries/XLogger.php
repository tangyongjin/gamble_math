<?php

if (!defined('BASEPATH'))
exit('No direct script access allowed');

 

class XLogger{

    private static $instance;
    private $msg;
    private $players;

        function __construct(){
         $this->msg = array();
         $this->players=array();

         $this->ci =& get_instance();
         $CI =& get_instance();
        

    }

     public static function getInstance(){
       if (!self::$instance)
            {
                self::$instance = new self();
            }

        return self::$instance;

    }

     public function __clone(){
        throw new Exception("Singleton Class Can Not Be Cloned");
    }
 
    public function log($message,$tabx=0,$css='nu'){
         
          
         $tab='';
         for ($i=0; $i <$tabx ; $i++) { 
             $tab.='&nbsp;';
           }  
         $this->msg[]="<span class=$css>".$tab.$message.'</span>';
    	 
         
    }

     public function logGambleConfig($gamblecfg){

        
        // debug($gamblecfg);die;

         $this->log("规则:  {$gamblecfg['rulename']}/{$gamblecfg['gambleid']}");

         echo "<div style='margin-left:420px;'>"; 
         echo "</div>";

         $item='比赛项目:'. array_to_string($gamblecfg['kpi_cfg']) ;
         $this->log($item,2);


         $item='初始排名:'.$gamblecfg['initorder'];
         $this->log($item,2);



          
         if( strlen($gamblecfg['weight_cfg'])>2){
            $this->log('权重:'. $gamblecfg['weight_cfg'],2);
         }


         $item= '基本单位unit:'. $gamblecfg['unit'];
         $this->log($item,2);
         
         if($gamblecfg['weight_cfg']['weight_used']=='y'){
           $msg='权重:[';

           $msg.='kpi_best/'.$gamblecfg['weight_cfg']['kpi_best'].' ';
           
           if(array_key_exists('kpi_total_add', $gamblecfg['weight_cfg'])){
            $msg.='kpi_total_add/'.$gamblecfg['weight_cfg']['kpi_total_add'].' ';
           }

          if(array_key_exists('kpi_total_plus', $gamblecfg['weight_cfg'])){
            $msg.='kpi_total_plus/'.$gamblecfg['weight_cfg']['kpi_total_plus'].' ';
           }

          if(array_key_exists('kpi_worst', $gamblecfg['weight_cfg'])){
            $msg.='kpi_worst/'.$gamblecfg['weight_cfg']['kpi_worst'].' ';
           }

           $msg.=']';

           $this->log($msg,2);
         }



         $item='分组方式:'.  $gamblecfg['div_option'] ;
         $this->log($item,2);

        
         if ( intval($gamblecfg['follower'] )>0 ){
         $item='地主婆ID:'.  $gamblecfg['follower'] ;
         $this->log($item,2);
         }


         $item=  $gamblecfg['max_option']=='nomax'?'无封顶':$gamblecfg['max_option'] ;
         $this->log('封顶:'. $item,2);


         $this->log('------------------------------------------------------------------------',2);
 
         $item='什么算顶洞:'.  $gamblecfg['draw_option'] ;
         $this->log($item,2);
         


         $this->log('------------------------------------------------------------------------',2);
         $item= ($gamblecfg['duty_option']['duty_number']=='noduty') ? ('无包洞'):($gamblecfg['duty_option']['duty_number']) ;
         $this->log('包洞方式:'.$item,2);

         $item= ($gamblecfg['duty_option']['duty_hole_only']=='y') ? ('(不含肉)'):(不含肉) ;
         $this->log('包洞是否含肉?:'.$item,2);
         
         $item= $gamblecfg['duty_option']['duty_items'];
         $this->log('包洞项:'.$item,2);
         

         $this->log('------------------------------------------------------------------------',2);

         $weaker=$gamblecfg['weaker_cfg']['weaker'] ;

  
        if ( floatval($gamblecfg['weaker_cfg']['weak_par3_num']) < 0) {
              $this->log("让杆方向:负(1人让其他所有人)", 2);
          } else {
              $this->log("让杆方向:正(让1人)", 2);
          } 


          
         $item='让杆:'.$gamblecfg['weaker_cfg']['weaker_name']."($weaker)";   
         $this->log($item,2);


         $item='让杆数:(3杆洞:'.$gamblecfg['weaker_cfg']['weak_par3_num'].')(4杆洞:'.$gamblecfg['weaker_cfg']['weak_par4_num'].
         ')(5杆洞:'.$gamblecfg['weaker_cfg']['weak_par5_num'].')';
         $this->log($item,2);
         
       
         
         $this->log('------------------------------------------------------------------------',2);


         // $tmp='';
         // if($gamblecfg['meat_option']['meat_eat_condition']=='win_hole'){
         //         $tmp='win_hole(本洞赢了才吃)';
         // } 

         // if($gamblecfg['meat_option']['meat_eat_condition']=='win_kpi'){
         //         $tmp='win_kpi(与输赢无关,各店吃各点)';
         // } 


       
         // $item='吃肉的条件:'.  $tmp;
         // $this->log($item,2);
         

         $tmp=$gamblecfg['meat_option']['meat_hole_num_option'] ;
         $txt='';
         foreach ($tmp as $key => $one_value) {
           $txt.= $key.'/'.$one_value.'  ';
         }


         $item='吃肉的个数:'. $txt;
         $this->log($item,2);
         
         /* without_bonus (本洞输赢,不含奖励)
            with_bonus    (本洞输赢,含奖励)
            kpi_separate  (头吃头,尾吃尾)
         */
         

         $txt='';
         $meatvalue=$gamblecfg['meat_option']['meat_value_option'] ;

         if($meatvalue=='without_bonus'){
            $txt='without_bonus (本洞输赢,不含奖励)';
         }

         if($meatvalue=='with_bonus'){
            $txt='with_bonus (本洞输赢,含奖励)';
         }

         if($meatvalue=='kpi_separate'){
            $txt='kpi_separate (头吃头,尾吃尾)';
         }

         if($meatvalue=='meat_as_tou'){
            $txt='meat_as_tou(肉算头的值)';
         }


         if($meatvalue=='meat_as_wei'){
            $txt='meat_as_wei(肉算尾的值)';
         }

        if($meatvalue=='meat_as_zong'){
            $txt='meat_as_zong(肉算{头总}的总值)';
         }

          


         //meat_as_tou


         $item='肉的值:'.  $txt;
         $this->log($item,2);
         
         $this->log('------------------------------------------------------------------------',2);

         $item='8421成绩表:'.  $gamblecfg['value_8421'];
         $this->log($item,2);



         $item='8421打平后洞的成绩配置:'.  $gamblecfg['draw_8421'];
         $this->log($item,2);

         $item='8421包分选项:'.  $gamblecfg['duty_8421'];
         $this->log($item,2);
         $this->log('------------------------------------------------------------------------',2);
         
         




         //zong_win
         /*取值: 总赢才奖    zong_win
     总平或以上   zong_equal_or_up
     与总无关     zong_ignore 
     */
         
 

         // $switch= $gamblecfg['bonus_option']['bonus_switch'];
         
         // $s='';
         // if($switch=='zong_win'){
         //     $s='zong_win(总赢才奖)';
         // }

         // if($switch=='zong_equal_or_up'){
         //     $s='zong_equal_or_up(总平或以上)';
         // }

         // if($switch=='zong_ignore'){
         //     $s='zong_ignore(与总无关)';
         // }


         // $item="包括总指标时候的奖励开关:".$s;

         // $this->log($item,2);



         $item='奖励(乘/加):'.  $gamblecfg['bonus_option']['bonus_operator'] ;
         $this->log($item,2);

         $item='单奖/双奖:'.$gamblecfg['bonus_option']['bonus_single_or_double'];
         $this->log($item,2);

         if($gamblecfg['bonus_option']['bonus_used']=='y'){

           $item='单奖:DoubleBogey/'.$gamblecfg['bonus_option']['DoubleBogey'].' Bogey/'.$gamblecfg['bonus_option']['Bogey'].' Par/'.
           $gamblecfg['bonus_option']['Par'].' Birdie/'. $gamblecfg['bonus_option']['Birdie'].
           ' Eagle/'.$gamblecfg['bonus_option']['Eagle'].
           ' HIO/'.$gamblecfg['bonus_option']['HIO'];
           $this->log($item,2);

         }

         // if($gamblecfg['bonus_option']['bonus_single_or_double']=='double')
         {
           $item='双奖:Par+Par/'.$gamblecfg['bonus_option']['Par+Par'].' Birdie+Par/'.$gamblecfg['bonus_option']['Birdie+Par'].' Birdie+Birdie/'.
           $gamblecfg['bonus_option']['Birdie+Birdie'].' Eagle+Par/'. $gamblecfg['bonus_option']['Eagle+Par'].
           ' Eagle+Birdie/'.$gamblecfg['bonus_option']['Eagle+Birdie'].' Eagle+Eagle/'.$gamblecfg['bonus_option']['Eagle+Eagle'];
           $this->log($item,2);

         }


          $this->log('------------------------------------------------------------------------',2);
          $item='捐锅:'.$gamblecfg['juanguo_option']['juanguo_point']."/".$gamblecfg['juanguo_option']['juanguo_type'];
          $this->log($item,2);

       


         
    }

  public function logRedBlueGroup($para,$index,$s){
          $redbule= $para['gamedata'][$index]['AB'];
          $red=$redbule['A'];  //red
          $blue=$redbule['B']; //blue
          $redtxt='A方>>>';
          $bluetxt='B方>>>';
           //after_max_weak
          foreach ($red as $key => $uid) {
                $nickname=$this->players[$uid];
                $redtxt.=$nickname.'('.$uid.') 实际杆数:<span class=ui-badge-num>'.$s['raw_scores'][$uid].'杆</span>/封顶让完杆数<span class=ui-badge-num>'.$s['after_max_weak'][$uid].'杆</span> ';
              }       
          foreach ($blue as $key => $uid) {
                $nickname=$this->players[$uid];
                $bluetxt.=$nickname.'('.$uid.') 实际杆数:<span class=ui-badge-num>'.$s['raw_scores'][$uid].'杆</span>/封顶让完杆数<span class=ui-badge-num>'.$s['after_max_weak'][$uid].'杆</span> ';
              }             
          $this->log($redtxt,2);
		      $this->log($bluetxt,2);
                   
   }

     public function logIndicator($Indicator){
          $this->log('A方:<span class=ui-badge-num>'.$Indicator['indicator_a'].'</span>',6);
          $this->log('B方:<span class=ui-badge-num>'.$Indicator['indicator_b'].'</span>',6);
                   
   }
 
   
   
  public function log_kpidone($para,$index){


            $a_string = array_to_string($para['gamedata'][$index]['AB']['A']);
            $b_string = array_to_string($para['gamedata'][$index]['AB']['B']);
            
            $redblue   = $index + 2;
            $ab_string = "<span  id='redblue_$redblue'  class='hidden redblue'>" . $a_string . '|' . $b_string . '</span>';
            
            $this->log("所有指标处理完毕" . $ab_string, 3);
            
  

  }


   
   public function setPlayerName($players){
        $this->players=$players;
   }


   public function getlog(){
        $this->ci->load->library('user_agent');
        if (strpos($this->ci->agent->agent_string(), 'Macintosh') !== false){
            $txt='';
              $count=count($this->msg);
              for ($i=0; $i <$count ; $i++) { 
                 $txt.=$this->msg[$i].'<br/>';
              }

        }else
        {
            $txt='';
        }
     	return  $txt;
    }
}


?>