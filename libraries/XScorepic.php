<?php

if (!defined('BASEPATH'))
exit('No direct script access allowed');

class XScorepic {
	private $ci = null;

	public function __construct() {
		$this->ci = & get_instance();
	}
      
 public function score2image($score_arg)
    { 

        $savepath="/opt/space/webroot/ugf/data/attach/game_score_img/";
        $gameinfo=$score_arg['gameinfo'];
        $gameid=$gameinfo['gameid'];
        $picname='game'.$gameid.'.png';;
        $fname=$savepath.$picname;

        $data=$score_arg['score_detail'];
        $teaminfo = $score_arg['team_info'];

        $glf_web_txt='高尔夫江湖记分卡   www.golf-brother.com';
 //       $font_loc='/tmp/msyh.ttf';
        $font_loc='/opt/space/webroot/ugf/libraries/msyh.ttf';
        
        $left_margin = 10;
        $firsthole_x_margin = 120;
        $hole_col_distanct = 23;
        $gross_toal_distence_with_last_hole = 30;
        $net_distence_with_last_hole = 75;
        $jianju = 45;
        $lineheight = 40;
        $head_w = 640;
        $head_h = 264;
        $bottom_h = 50;
       
        $number = count($data[0]);
                 
        $image = imagecreatefrompng('controllers/score.png');
        $count = count($data);
        if($count >= 8){
                    $h = $head_h+$count*$lineheight+$bottom_h;
                    $score_image = imagecreate($head_w,$h);
                }else{
                    $score_image = imagecreate($head_w,640);
        }

        imagecolorallocatealpha($score_image,255,255,255,0); 
        imagecopymerge($score_image,$image,0,0,0,0,640,264,100);
        $font_color = imagecolorallocate($score_image, 153, 153, 153);
        $time_color = imagecolorallocate($score_image, 102, 102, 102);
        $rad = imagecolorallocate($score_image, 225, 225, 225);
        $black = imagecolorallocate($score_image, 0, 0, 0);
        $white = imagecolorallocate($score_image, 255, 255, 255);
        $bottom = imagecolorallocate($score_image, 255, 204, 0);
        
        $greater_than_par = imagecolorallocate($score_image, 255, 102, 0);//大于par
        $equal_par = imagecolorallocate($score_image, 0, 204, 255);//等于par
        $less_par = imagecolorallocate($score_image, 51, 51, 51);//小于par
        
        
        
        $eagle2 = imagecolorallocate($score_image, 255, 194, 154);//<=-3
        $eagle = imagecolorallocate($score_image, 255, 225, 205);
        $birdie = imagecolorallocate($score_image, 255, 247, 242);
        $pars = imagecolorallocate($score_image, 255, 255, 255);
        $bogey = imagecolorallocate($score_image, 230, 230, 230);
        $double_bogey = imagecolorallocate($score_image, 206, 206, 206);
        $eagle3 = imagecolorallocate($score_image, 181, 181, 181);//>=3
        
        if ( is_array($teaminfo))
        {
            imagettftext($score_image, 16, 0, 28, 132, $black, $font_loc, $teaminfo['team_name'].'-'.$gameinfo['name']);
        }
        
        imagettftext($score_image, 20, 0, 28, 170, $black, $font_loc, $gameinfo['course_name']);
        imagettftext($score_image, 15, 0, 28, 200, $time_color, $font_loc, $gameinfo['starttime']);
        logtext('pic >>>'.$gameinfo['course_name']);
        if($number == 22){
            imagettftext($score_image, 10, 0, $left_margin, 280, $font_color, $font_loc, $data[0]['p1']);
            imagettftext($score_image, 10, 0, $left_margin+$jianju, 280, $font_color, $font_loc, $data[0]['p2']);
            imagettftext($score_image, 10, 0, $firsthole_x_margin, 280, $font_color, $font_loc, $data[0]['p3']);
       
            for($k = 4;$k <= $number-2;$k++){
                //$chazhi = $firsthole_x_margin+$hole_col_distanct;
                $chazhi = $firsthole_x_margin+25;
                //imagettftext($score_image, 10, 0, $firsthole_x_margin+$hole_col_distanct, 280, $font_color, $font_loc, $data[0]['p'.$k]);
                imagettftext($score_image, 10, 0, $chazhi, 280, $font_color, $font_loc, $data[0]['p'.$k]);
                $firsthole_x_margin = $chazhi;
            } 


            imagettftext($score_image, 10, 0, $firsthole_x_margin+$gross_toal_distence_with_last_hole, 280, $font_color, $font_loc, $data[0]['p21']);
            //imagettftext($score_image, 10, 0, $firsthole_x_margin+$net_distence_with_last_hole, 280, $font_color, $font_loc, $data[0]['p22']);
            imageline($score_image,10,290,640,290,$rad);
        }else{
            imagettftext($score_image, 10, 0, $left_margin, 280, $font_color, $font_loc, $data[0]['p1']);
            imagettftext($score_image, 10, 0, $left_margin+$jianju, 280, $font_color, $font_loc, $data[0]['p2']);
            imagettftext($score_image, 10, 0, $firsthole_x_margin+10, 280, $font_color, $font_loc, $data[0]['p3']);

            for($k = 4;$k <= $number-2;$k++){
                $chazhi = $firsthole_x_margin+$hole_col_distanct;
                if($k == 4){
                    imagettftext($score_image, 10, 0, $firsthole_x_margin+$hole_col_distanct+40, 280, $font_color, $font_loc, $data[0]['p'.$k]);
                }else{
                    imagettftext($score_image, 10, 0, $firsthole_x_margin+$hole_col_distanct+40, 280, $font_color, $font_loc, $data[0]['p'.$k]);
                }
                $firsthole_x_margin = $chazhi+30;
            } 
            imagettftext($score_image, 10, 0, $firsthole_x_margin+$gross_toal_distence_with_last_hole+20, 280, $font_color, $font_loc, $data[0]['p12']);
            //imagettftext($score_image, 10, 0, $firsthole_x_margin+$net_distence_with_last_hole, 280, $font_color, $font_loc, $data[0]['p22']);
            imageline($score_image,10,290,640,290,$rad);
        }



        $score_one_hole = array();
        for($p = 1;$p <= $count-1;$p++){
            for($t = 3;$t<= $number-2;$t++){
                if($data[$p]['p'.$t] - $data[0]['p'.$t] <= -3){
                    $score_one_hole[$p]['p'.$t]['gross'] = $data[$p]['p'.$t];
                    $score_one_hole[$p]['p'.$t]['par'] = $data[0]['p'.$t];
                    $score_one_hole[$p]['p'.$t]['color'] = $eagle2;
                    $score_one_hole[$p]['p'.$t]['gross_color'] = $greater_than_par;
                }elseif ($data[$p]['p'.$t]-$data[0]['p'.$t] == -2) {
                    $score_one_hole[$p]['p'.$t]['gross'] = $data[$p]['p'.$t];
                    $score_one_hole[$p]['p'.$t]['par'] = $data[0]['p'.$t];
                    $score_one_hole[$p]['p'.$t]['color'] = $eagle;
                    $score_one_hole[$p]['p'.$t]['gross_color'] = $greater_than_par;
                }elseif ($data[$p]['p'.$t]-$data[0]['p'.$t] == -1) {
                    $score_one_hole[$p]['p'.$t]['gross'] = $data[$p]['p'.$t];
                    $score_one_hole[$p]['p'.$t]['par'] = $data[0]['p'.$t];
                    $score_one_hole[$p]['p'.$t]['color'] = $birdie;
                    $score_one_hole[$p]['p'.$t]['gross_color'] = $greater_than_par;
                }elseif ($data[$p]['p'.$t]-$data[0]['p'.$t] == 0) {
                    $score_one_hole[$p]['p'.$t]['gross'] = $data[$p]['p'.$t];
                    $score_one_hole[$p]['p'.$t]['par'] = $data[0]['p'.$t];
                    $score_one_hole[$p]['p'.$t]['color'] = $par;
                    $score_one_hole[$p]['p'.$t]['gross_color'] = $equal_par;
                }elseif ($data[$p]['p'.$t]-$data[0]['p'.$t] == 1) {
                    $score_one_hole[$p]['p'.$t]['gross'] = $data[$p]['p'.$t];
                    $score_one_hole[$p]['p'.$t]['par'] = $data[0]['p'.$t];
                    $score_one_hole[$p]['p'.$t]['color'] = $bogey;
                    $score_one_hole[$p]['p'.$t]['gross_color'] =  $less_par;
                }elseif ($data[$p]['p'.$t]-$data[0]['p'.$t] == 2) {
                    $score_one_hole[$p]['p'.$t]['gross'] = $data[$p]['p'.$t];
                    $score_one_hole[$p]['p'.$t]['par'] = $data[0]['p'.$t];
                    $score_one_hole[$p]['p'.$t]['color'] = $double_bogey;
                    $score_one_hole[$p]['p'.$t]['gross_color'] = $less_par;
                }elseif ($data[$p]['p'.$t]-$data[0]['p'.$t] >= 3) {
                    $score_one_hole[$p]['p'.$t]['gross'] = $data[$p]['p'.$t];
                    $score_one_hole[$p]['p'.$t]['par'] = $data[0]['p'.$t];
                    $score_one_hole[$p]['p'.$t]['color'] = $eagle3;
                    $score_one_hole[$p]['p'.$t]['gross_color'] = $less_par;
                }
            }
        } 
       
       if($number == 22){
                $y = 315;
                for($i=1;$i<= $count-1;$i++){
                     $firsthole_x_margin = 120;
                     imagettftext($score_image, 10, 0, $left_margin, $y, $black, $font_loc, $data[$i]['p1']);
                     if(strlen($data[$i]['p2'])>6){
                        imagettftext($score_image, 10, 0, $left_margin+$jianju-10, $y, $black, $font_loc, $data[$i]['p2']);
                     }else{
                        imagettftext($score_image, 10, 0, $left_margin+$jianju, $y, $black, $font_loc, $data[$i]['p2']);
                     }
                     /*
                     if($score_one_hole[$i]['p3']['gross'] > 10){

                            imagefilledrectangle($score_image, $firsthole_x_margin-5, $y+3, $firsthole_x_margin+11, $y-15, $score_one_hole[$i]['p'.$arr_index]['color']);
                            imagettftext($score_image, 8, 0, $firsthole_x_margin-2, $y, $score_one_hole[$i]['p'.$arr_index]['gross_color'], $font_loc, $score_one_hole[$i]['p'.$arr_index]['gross']);
                         }elseif ($score_one_hole[$i]['p'.$arr_index]['gross'] != 0){
                             
                            imagefilledrectangle($score_image, $firsthole_x_margin-5, $y+3, $firsthole_x_margin+11, $y-15, $score_one_hole[$i]['p'.$arr_index]['color']);
                            imagettftext($score_image, 10, 0, $firsthole_x_margin, $y, $score_one_hole[$i]['p'.$arr_index]['gross_color'], $font_loc, $score_one_hole[$i]['p'.$arr_index]['gross']);
                         }else{
                                 imagettftext($score_image, 10, 0, $firsthole_x_margin, $y, $font_color, $font_loc, '-');
                         }
                         */
                     $arr_index = 3;
                     $chazhi = $firsthole_x_margin;
                     for ($j=0; $j < count($score_one_hole[$i]); $j++) {
                         //$chazhi = $firsthole_x_margin+$hole_col_distanct;
                        
                         if($score_one_hole[$i]['p'.$arr_index]['gross'] > 10){
                             //imagefilledrectangle($score_image, ($firsthole_x_margin-5)+$addchazhi, $y+3, ($firsthole_x_margin+11)+$addchazhi, $y-15, $score_one_hole[$i]['p'.$arr_index]['color']);
                             //imagettftext($score_image, 8, 0, ($firsthole_x_margin-2)+$addchazhi, $y, $score_one_hole[$i]['p'.$arr_index]['gross_color'], $font_loc, $score_one_hole[$i]['p'.$arr_index]['gross']);
                            imagefilledrectangle($score_image, $chazhi-5, $y+3, $chazhi+11, $y-15, $score_one_hole[$i]['p'.$arr_index]['color']);
                            imagettftext($score_image, 8, 0, $chazhi-2, $y, $score_one_hole[$i]['p'.$arr_index]['gross_color'], $font_loc, $score_one_hole[$i]['p'.$arr_index]['gross']);
                         }elseif ($score_one_hole[$i]['p'.$arr_index]['gross'] != 0){
                             //imagefilledrectangle($score_image, ($firsthole_x_margin-5)+$addchazhi, $y+3, ($firsthole_x_margin+11)+$addchazhi, $y-15, $score_one_hole[$i]['p'.$arr_index]['color']);
                             //imagettftext($score_image, 10, 0, $firsthole_x_margin+$addchazhi, $y, $score_one_hole[$i]['p'.$arr_index]['gross_color'], $font_loc, $score_one_hole[$i]['p'.$arr_index]['gross']);
                            imagefilledrectangle($score_image, $chazhi-5, $y+3, $chazhi+11, $y-15, $score_one_hole[$i]['p'.$arr_index]['color']);
                            imagettftext($score_image, 10, 0, $chazhi, $y, $score_one_hole[$i]['p'.$arr_index]['gross_color'], $font_loc, $score_one_hole[$i]['p'.$arr_index]['gross']);
                         }else{
                                 imagettftext($score_image, 10, 0, $chazhi, $y, $font_color, $font_loc, '-');
                         }
                         //$firsthole_x_margin = $chazhi;
                         $arr_index = $arr_index+1;
                         $chazhi = $chazhi+25;
                     }
                     //imagettftext($score_image, 10, 0, $firsthole_x_margin+$gross_toal_distence_with_last_hole-13, $y, $black, $font_loc, $data[$i]['p21']);
                     imagettftext($score_image, 10, 0, $chazhi+17, $y, $black, $font_loc, $data[$i]['p21']);
                     //imagettftext($score_image, 10, 0, $firsthole_x_margin+$net_distence_with_last_hole-20, $y, $black, $font_loc, $data[$i]['p22']);
                     imageline($score_image,10,$y+15,640,$y+15,$rad);
                     $y = $y+$lineheight;
                }
        }else{
            $y = 315;
                for($i=1;$i<= $count-1;$i++){
                     $firsthole_x_margin = 120;
                     imagettftext($score_image, 10, 0, $left_margin, $y, $black, $font_loc, $data[$i]['p1']);
                     if(strlen($data[$i]['p2'])>6){
                        imagettftext($score_image, 10, 0, $left_margin+$jianju-10, $y, $black, $font_loc, $data[$i]['p2']);
                     }else{
                        imagettftext($score_image, 10, 0, $left_margin+$jianju, $y, $black, $font_loc, $data[$i]['p2']);
                     }

                     $arr_index = 3;
                     for ($j=0; $j < count($score_one_hole[$i]); $j++) {
                         $chazhi = $firsthole_x_margin+$hole_col_distanct;
                         if($score_one_hole[$i]['p'.$arr_index]['gross'] > 10){
                             imagefilledrectangle($score_image, ($firsthole_x_margin-5)+10, $y+3, ($firsthole_x_margin+11)+10, $y-15, $score_one_hole[$i]['p'.$arr_index]['color']);
                             imagettftext($score_image, 8, 0, ($firsthole_x_margin-2)*2, $y, $score_one_hole[$i]['p'.$arr_index]['gross_color'], $font_loc, $score_one_hole[$i]['p'.$arr_index]['gross']);
                         }elseif ($score_one_hole[$i]['p'.$arr_index]['gross'] != 0){
                             imagefilledrectangle($score_image, ($firsthole_x_margin-5)+10, $y+3, ($firsthole_x_margin+11)+10, $y-15, $score_one_hole[$i]['p'.$arr_index]['color']);
                             imagettftext($score_image, 10, 0, $firsthole_x_margin*2, $y, $score_one_hole[$i]['p'.$arr_index]['gross_color'], $font_loc, $score_one_hole[$i]['p'.$arr_index]['gross']);
                         }else{
                                 imagettftext($score_image, 10, 0, $firsthole_x_margin+10, $y, $font_color, $font_loc, '-');
                         }
                         $firsthole_x_margin = $chazhi+30;
                         $arr_index = $arr_index+1;
                     }
                     imagettftext($score_image, 10, 0, $firsthole_x_margin+$gross_toal_distence_with_last_hole-20, $y, $black, $font_loc, $data[$i]['p12']);
                     imageline($score_image,10,$y+15,640,$y+15,$rad);
                     $y = $y+$lineheight;
                }
        }


        $count = count($data)-1;
        if($count >= 8){
          imagefilledrectangle($score_image, 0, $h-$bottom_h, 640, $h, $bottom);
          imagettftext($score_image, 12, 0, 180, $h-$bottom_h+30, $white, $font_loc,$glf_web_txt);  
        }else{
            imagefilledrectangle($score_image, 0, 590, 640, 640, $bottom);
          imagettftext($score_image, 12, 0, 180, 620, $white, $font_loc,$glf_web_txt);
           
        }
        imagepng($score_image,$fname);
        imagedestroy($score_image);
        $picurl='http://s1.golf-brother.com/data/attach/game_score_img/'.$picname;
        return $picurl;
    }
  

} 
	 
  
