<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class XSession
{
	static private $_begin = 0;
	static private $_instance = null;
	static private $_debug = false;

	public function __construct()
	{
		/*
		 self::$_instance = new Session();
		 self::$_debug = $debug;
		 */
		session_start();
	}

	static public function set($name, $v)
	{
		$_SESSION[$name] = $v;
	}

	static public function get($name, $once=false)
	{
		$v = null;
		if ( isset($_SESSION[$name]) )
		{
			$v = $_SESSION[$name];
			if ( $once ) unset( $_SESSION[$name] );
		}
		return $v;
	}

	function __destruct()
	{

	}
}
?>
