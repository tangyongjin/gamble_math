<?php

 /*
 设置标题


*/

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class ATitlereb
{
    public function __construct()
    {
        $this->ci =& get_instance();
        $CI =& get_instance();
        
    
    }
    
    public function reset_title(&$para)
    {
        $rank_init = $para['rank_init'];
        foreach ($para['gamedata'] as $key => $one) {
            $score_rank     = $one['rank'];
            $base_money_log = $one['base_money_log'];
            foreach ($base_money_log as $k2 => $one_user) {
                $one_uid                                                      = $one_user['uid'];
                $score_order                                                  = $this->setplayertitle($one_uid, $score_rank, $para);
                $para['gamedata'][$key]['base_money_log'][$k2]['score_order'] = $score_order;
                unset($para['gamedata'][$key]['after_max']);
                unset($para['gamedata'][$key]['after_max_weak']);
                

                
            }
        }
    }
    

    public function setplayertitle($one_uid, $score_rank, $para)
    {
        $playernum         = $para['playernum'];
        $div_option            = $para['div_option'];
    
        $fixed_dizhu = $para['fixed_dizhu'];
      
        
        if (in_array($one_uid, $score_rank)) {
            $score_order = array_search($one_uid, $score_rank);
            $score_order = $score_order + 1;
        } 
        
        if ($playernum == 2) {
            return 'uinone';
        }
        
        if ($playernum == 3) {
            $title = $this->setplayertitle_3($one_uid, $div_option, $score_order, $fixed_dizhu);
            return $title;
        }
        
        if ($playernum == 4) {
            $title = $this->setplayertitle_4($one_uid, $div_option, $score_order, $fixed_dizhu);
            return $title;
        }
    }
    
    public function setplayertitle_3($one_uid, $div_option, $score_order, $fixed_dizhu)
    {
        //3人斗大地主
        if ($div_option == 'ab3_best') {
            if ($score_order == 1) {
                return 'uidz';
            } else {
                return 'ui' . $score_order;
            }
        }
        
        //3人斗二地主
        if ($div_option == 'ab3_second') {
            if ($score_order == 2) {
                return 'uidz';
            } else {
                return 'ui' . $score_order;
            }
        }
        //固定2打1
        if ($div_option == 'ab3_fixed') {
            if ($one_uid == $fixed_dizhu) {
                return 'uilz';
            } else {
                return 'ui' . $score_order;
            }
        }
    }
    
    public function setplayertitle_4($one_uid, $div_option, $score_order, $fixed_dizhu)
    {
        
        //ab4_kpi_one
        if (in_array($div_option, array(
            'ab4_kpi_one',
            'ab4_kpi_two',
            'ab4_kpi_three'
        ))) {
            return 'ui' . $score_order;
        }
        
        if ($div_option == 'ab4_fixed') {
            if ($one_uid == $fixed_dizhu) {
                return 'uilz';
            } else {
                return 'ui' . $score_order;
            }
        }
        
        
    }
    
    
    
}

?> 
