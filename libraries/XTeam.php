<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}


class XTeam  
{
    private $ci = null;
   
    public function __construct()
    {   
        $this->ci =& get_instance();
        include_once('/opt/space/webroot/ugf/api/web/controllers/gamefuncs.php');
    }
    
    
    
    public function debug($r)
    {   
        log_model_call(); 
        echo "<pre>";
        print_r($r);
        echo "</pre>";
    }

   public function my_admin_teams($userid){


        $this->ci->load->database();
        $this->ci->db->select('teamid');
        $query = $this->ci->db->get_where('team_admins',array('userid'=>$userid));

        $sql=$this->ci->db->last_query();



        $teamids = $query->result_array();


        $teamid_str = array_to_string($teamids,'teamid');
        if( strlen($teamid_str)==0){
            $teamid_str='-1';
        }

        $sys_path            = SERVER_PIC_DIR;
        $sql = "select teamid,team_name, concat($sys_path,coverpath,'/',covername) as team_picurl,usernum from t_team where teamid in ($teamid_str)";
        

        $ret=array();
        $team_list = $this->ci->db->query($sql)->result_array();
        foreach ($team_list as $key => $one) {

             $teamid=$one['teamid'];
             $sql=" select count(*) as  usernum from t_team_user where teamid=$teamid ";
             $teamer_count = $this->ci->db->query($sql)->row_array();
             $usernum=$teamer_count['usernum'];
             $one['usernum']=$usernum ;
             $ret[]=$one;
        }

        return $ret;
   }
    
   public function get_team_info($teamid){
        $team_info = array();
        if($teamid < 1){
            return $team_info;
        }
        $this->ci->load->database();
        $query = $this->ci->db->get_where('team',array('teamid' => $teamid));
        $team_info = $query->row_array();
        return $team_info;
   }


   public function get_team_users($teamid){
        $team_user_list = array();
        if($teamid < 1){
            return $team_user_list;
        }
        $this->ci->load->database();
        $query = $this->ci->db->get_where('team_user',array('teamid' => $teamid));
        $team_user_list = $query->result_array();
        return $team_user_list;
   }



   public function get_team_game_num($teamid){
        $this->ci->load->database();
        $sql = "select count(gameid) as game_num from t_game where teamid=$teamid and gamestate=4 and gametype=1 ";
        $game_num = $this->ci->db->query($sql)->row_array();



        $sql = "select count(g.gameid) as game_num from t_game as g,t_game_squad as gs where gs.teamid=$teamid and g.gameid=gs.gameid and g.gametype=4 and g.gamestate=4 ";
        $squad_game_num = $this->ci->db->query($sql)->row_array();
        $team_game_num = intval($game_num['game_num'])+intval($squad_game_num['game_num']);

        return intval($team_game_num);

   }


   public function get_prov_name($city_code){
        $this->ci->load->database();
        $query = $this->ci->db->get_where('prov',array('city_code' => $city_code));
        $prov_info = $query->result_array();
        return $prov_info[0]['prov_name'];
   }


   public function get_team_games($teamid){
        $this->ci->load->database();
        $sql1 = "select g.gameid,g.starttime,g.gametype,g.name as game_name,c.name,u.nickname from t_game as g,t_course as c,t_user as u ";
        $sql1 .= "where g.teamid=$teamid and g.gametype=1 and g.gamestate=4 and ";
        $sql1 .= " c.courseid=g.courseid and u.id=g.creatorid ";
        
        $sql2 = " select g.gameid,g.starttime,g.gametype,g.name as game_name,c.name,u.nickname from t_game_squad as gs,t_game as g,t_course as c,t_user as u ";
        $sql2 .= " where gs.teamid=$teamid and  g.gameid=gs.gameid and g.gamestate=4 and g.gametype=4 and c.courseid=g.courseid and u.id=g.creatorid ";

        $sql = $sql1 . " union " . $sql2;
        $sql .= " order by starttime desc, gametype ";

        $games = $this->ci->db->query($sql)->result_array();

        foreach ($games as $key => $one_game) {
           $gameid = intval($one_game['gameid']);
           $this->ci->db->from("t_game_applyer");
           $this->ci->db->where('gameid',$gameid);
           $games[$key]['applyer_num'] = $this->ci->db->count_all_results();
        }

        return $games;
   }
    
}
?>