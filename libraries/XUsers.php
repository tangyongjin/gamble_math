<?php

if (!defined('BASEPATH'))
exit('No direct script access allowed');

/**
 * 	XUsers
 *
 * @author		Nick Louis
 * @copyright	Copyright (c) 2013 Nick Louis
 * @since		Version 1.0
 *
 */
class XUsers {

	private $ci = null;

	public function __construct() {
		$this->ci = & get_instance();
	}

	/* ------------------------------- user query ------------------------- */

	//
	public function getUser($id) {
		$vs = array();
		if ($id < 1) {
			return $vs;
		}
		$this->ci->load->database();
		$query = $this->ci->db->get_where('c_user', array('id' => $id));
		$vs = $query->row_array();
		return $vs;
	}

	public function getUserInfo($userid) {
		$vs = array();
		if ($userid < 1) {
			return $vs;
		}
		$this->ci->load->database();

		$query = $this->ci->db->get_where('c_user_info', array('userid' => $userid));
		$vs = $query->row_array();

		return $vs;
	}

	//
	public function getUserByName($name) {
		$this->ci->load->database();

		$query = $this->ci->db->get_where('c_user', array('username' => $name));
		return $query->result_array($query);
	}

	public function getUserByEmail($email) {
		$this->ci->load->database();

		$query = $this->ci->db->get_where('c_user', array('email' => $email));
		return $query->result_array($query);
	}

	public function getUserByMobile($mobile) {
		$this->ci->load->database();

		$query = $this->ci->db->get_where('c_user', array('mobile' => $mobile));
		return $query->result_array($query);
	}

	//
	public function getUsers($type = -1, $status = -1, $pageidx = 1, $pagesize = 15, $orderby = null) {
		$where = array();
		$start = 0;
		$end = 15;
		if ($type >= 0) {
			$where['type'] = $type;
		}
		if ($status >= 0) {
			$where['status'] = $status;
		}
		if ($pagesize < 1) {
			$pagesize = 15;
		}
		if ($pageidx <= 0) {
			$pageidx = 1;
			$start = 0;
			$end = $pagesize;
		}
		$start = ($pageidx - 1) * $pagesize;

		$this->ci->load->database();
		if (count($where) > 0) {
			$this->ci->db->where($where);
		}
		$count = $this->ci->db->from('c_user')->count_all_results();
		if( $count < 1 ){
			return array('pageidx' => $pageidx, 'pagesize' => $pagesize, 'count' => $count, 'data' => array());
		}

		if (count($where) > 0) {
			$this->ci->db->where($where);
		}
		if ( $orderby ) {
			if(is_array($orderby) && count($orderby) > 0){
				foreach ($orderby as $k => $order) {
					$this->ci->db->order_by($order);
				}
			}else if( strlen($orderby)>0 ){
				$this->ci->db->order_by($orderby);
			}
		}
		$query = $this->ci->db->from('c_user')->limit($pagesize, $start)->get();
		$vs = $query->result_array();
		//test: dump($this->ci->db->last_query());
		return array('pageidx' => $pageidx, 'pagesize' => $pagesize, 'count' => $count, 'data' => $vs);
	}

	public function getUserCount($type = -1, $status = -1) {
		$where = array();
		if (is_array($type) && count($type) > 0) {
			foreach ($type as $k => $v) {
				$where[$k] = $v;
			}
		} else if ($type >= 0) {
			$where['type'] = $type;
		}
		if (is_array($status) && count($status) > 0) {
			foreach ($status as $k => $v) {
				$where[$k] = $v;
			}
		} else if ($status >= 0) {
			$where['status'] = $status;
		}
		$this->ci->load->database();
		if (count($where) > 0) {
			$this->ci->db->where($where);
		}
		$count = $this->ci->db->from('c_user')->count_all_results();
		//test: dump($this->ci->db->last_query());
		return $count;
	}

	//
	public function searchUsers($key = '', $ktype = 0, $type = -1, $status = -1, $pageidx = 1, $pagesize = 15, $orderby = null) {
		$where = array();
		$start = 0;
		$end = 15;
		if ($type >= 0) {
			$where['type'] = $type;
		}
		if ($status >= 0) {
			$where['status'] = $status;
		}
		if ($pagesize < 1) {
			$pagesize = 15;
		}
		if ($pageidx <= 0) {
			$pageidx = 1;
			$start = 0;
			$end = $pagesize;
		}
		$start = ($pageidx - 1) * $pagesize;

		$this->ci->load->database();
		// count
		if (count($where) > 0) {
			$this->ci->db->where($where);
		}
		if (strlen($key) > 0) {
			if (0 == $ktype) {
				$this->ci->db->like('nickname', $key);
			} else if (1 == $ktype) {
				$this->ci->db->like('username', $key);
			} else if (2 == $ktype) {
				$this->ci->db->like('email', $key);
			} else if (3 == $ktype){
				$this->ci->db->like('mobile', $key);
			}
		}
		$count = $this->ci->db->from('c_user')->count_all_results();
		if( $count < 1 ){
			return array('pageidx' => $pageidx, 'pagesize' => $pagesize, 'count' => $count, 'data' => array());
		}

		// list
		if (count($where) > 0) {
			$this->ci->db->where($where);
		}
		if (strlen($key) > 0) {
			if (0 == $ktype) {
				$this->ci->db->like('nickname', $key);
			} else if (1 == $ktype) {
				$this->ci->db->like('username', $key);
			} else if (2 == $ktype) {
				$this->ci->db->like('email', $key);
			}else if (3 == $ktype){
				$this->ci->db->like('mobile', $key);
			}
		}
		if ( $orderby ) {
			if(is_array($orderby) && count($orderby) > 0){
				foreach ($orderby as $k => $order) {
					$this->ci->db->order_by($order);
				}
			}else if( strlen($orderby)>0 ){
				$this->ci->db->order_by($orderby);
			}
		}
		$query = $this->ci->db->from('c_user')->limit($pagesize, $start)->get();
		$vs = $query->result_array();
		//test: dump($this->ci->db->last_query());

		return array('pageidx' => $pageidx, 'pagesize' => $pagesize , 'count' => $count, 'data' => $vs);
	}

	/* ------------------------------- users ------------------------------- */

	public function userRegister($name, $password, $nickname, $datas=null, $type=1, $status=1, $resetpwd=false){
		// return values
		$ret = array();
		$ret['status'] = -1;
		$ret['descr'] = '注册失败';

		// parameters
		$name = trim($name);
		$password = trim($password);
		$nickname = trim($nickname);
		$mobile = "";
		$email = "";

		// common parameters
		$tm = time();
		$ip = $this->ci->input->ip_address();
		$this->ci->load->database();

		// format checks
		if ( strlen($name)<1 || strlen($password)<1 ) {
			$ret['descr'] = '账号/密码为空';
			return $ret;
		}
		if (!$password || $password != addslashes($password)) {
			$code = 4;
			$ret['descr'] = $this->getRegError($code);
			return $ret;
		}

		$u = null;
		if( $this->check_mobile($name) ){
			$mobile = $this->formatMobile($name);
			$us = $this->getUserByMobile($mobile);
			if(count($us)>0)	$u = $us[0];
		}else if($this->check_email($name) ){
			$email = $name;
			$us = $this->getUserByEmail($email);
			if(count($us)>0)	$u = $us[0];
		}else if( false !== strpos($name, "wx_")){
			//
		}else{
			$ret['descr'] = '参数错误';
			return  $ret;
		}

		$v = array();
		$v['nickname'] = $nickname;
		$v['salt'] = rand(100000000, 900000000);
		$v['password'] = md5( md5($password).$v['salt'] );

		// logical check
		if ( count($u)>0 ) {
			if($resetpwd){ // reset old pwd
				$this->ci->db->where(array('id' => $u['id']))->update('c_user',$v);
			}

			$ret['id'] = $u['id'];
			$ret['status'] = 0;
			$ret['descr'] = "已经被注册";
			return $ret;
		}

		// new
		$v['email'] = $email;
		$v['mobile'] = $mobile;
		$v['type'] = $type;
		$v['status'] = $status;
		$v['logincount'] = 1;
		$v['addtime'] = $v['lasttime'] = $tm;
		$v['addip'] = $v['lastip'] = $ip;
		if( is_array($datas) ){
			$v = array_merge($v,$datas);
		}

		// to db
		$this->ci->db->insert('c_user', $v);
		$userid = $this->ci->db->insert_id();
		//test: dump($this->ci->db->last_query());
		if ($userid < 1) {
			$code = 2;
			$ret['descr'] = $this->getRegError($code);
			return $ret;
		}
		$user = $v;

		// user infos
		$v = array();
		$v['userid'] = $userid;
		$v['email'] = $user['email'];
		$v['mobile'] = $user['mobile'];
		$v['type'] = $user['type'];
		$v['status'] = $user['status'];
		$v['lasttime'] = $tm;
		$v['lastip'] = $ip;
		$this->ci->db->insert('c_user_info', $v);

		// >>> todo
		// follows someone ??
		// set rank, credit, experience
		// return values
		$ret['status'] = 1;
		$ret['descr'] = $this->getRegError(0);
		$ret['id'] = $userid;
		return $ret;
	}

	public function userLogin($name, $password) {
		// return values
		$ret = array();
		$ret['status'] = -1;
		$ret['descr'] = "登录失败";

		// parameters
		$name = trim($name);
		$password = trim($password);

		$tm = time();
		$ip = $this->ci->input->ip_address();

 
		// format checks
		if (empty($name) || empty($password) || $name != addslashes($name) || $password != addslashes($password)) {
			$ret['descr'] = "非法输入";
			return $ret;
		}

		// query user
		$us = null;
		if ($this->check_email($name)) {
			$us = $this->getUserByEmail($name);
		} else if ($this->check_mobile($name)) {
			$name = $this->formatMobile($name);
			$us = $this->getUserByMobile($name);
		} else {
			$us = $this->getUserByName($name);
		}

		// not exists
		if ( count($us)<1 ) {
			$ret['descr'] = "用户不存在";
			return $ret;
		}

		// wrong name && pwd
		foreach ($us as $v) {
			$pwd =  md5( md5(trim($password)) . $v['salt']);
			if ($v['password'] == $pwd && $v['status'] > 0) {
				$this->ci->db->set('logincount', 'logincount+1', FALSE);
				$this->ci->db->set('lasttime', $tm);
				$this->ci->db->set('lastip', $ip);
				$this->ci->db->where(array('id' => $v['id']))->update('c_user');

				$ret['status'] = 1;
				$ret['descr'] = "登录成功";
				$ret['user'] = $v;
				return $ret;
			}
		}
		$ret['descr'] = '账号密码不匹配';
		return $ret;
	}

	public function saveUserInfo($userid, $post) {
		// return values
		$ret = array();
		$ret['id'] = 0;
		$ret['name'] = '';
		$ret['status'] = -1;
		$ret['descr'] = '保存失败';

		// parameters
		$userid = intval($userid);

		// check parameters
		if ($userid < 1 || !$post || count($post)<1) {
			$ret['descr'] = '参数错误';
			return $ret;
		}

		$this->ci->load->database();
		$tm = time();
		$ip = $this->ci->input->ip_address();
		$rc = 0;

		// update user
		$v = array();
		$snames = array('mobile','nickname','coverpath','covername','domain','sno', );
		$inames = array('type','status','clientid', );
		foreach( $snames as $name ){
			if( isset($post[$name]) ){
				$v[$name] = $post[$name];
			}
		}
		foreach( $inames as $name ){
			if( isset($post[$name]) ){
				$v[$name] = $post[$name];
			}
		}
		if(count($v)>0){
			$v['lasttime'] = $tm;
			$v['lastip'] = $ip;
			$this->ci->db->where(array('id' => $userid))->update('c_user', $v);
			$rc = $this->ci->db->affected_rows();
			if($rc<1){
				$ret['descr'] = '操作错误';
				return $ret;
			}
		}

		// update user info
		$v = array();
		$snames = array('realname', 'sign','favorite','birthday','remark','industry','degree','folk','blood','star','zodiac','physique','area');
		$inames = array('gender','residecity','height','weight');
		foreach( $snames as $name ){
			if( isset($post[$name]) ){
				$v[$name] = $post[$name];
			}
		}
		foreach( $inames as $name ){
			if( isset($post[$name]) ){
				$v[$name] = $post[$name];
			}
		}
		if (isset($post['birthday'])) {
			$v['birthday'] = strtotime($post['birthday']);
		}
		if ($post['area'] && strlen(trim($post['area'])) > 0) {
			$as = split(",", $post['area']);
			if (count($as) > 0) {
				$v['resideprovince'] = trim($as[0]);
			}
			if (count($as) > 1) {
				$v['residecity'] = trim($as[1]);
			}
		}

		if (count($v)>0) {
			$v['lasttime'] = $tm;
			$v['lastip'] = $ip;
			$this->ci->db->where(array('userid' => $userid))->update('c_user_info', $v);
			$rc += $this->ci->db->affected_rows();
		}

		$ret['id'] = $userid;
		$ret['status'] = 1;
		$ret['descr'] = '保存成功';
		return $ret;
	}

	// password
	public function resetPassword($userid, $oldpassword, $newpassword, $needoldpwd=true, $post=null) {
		// return values
		$ret = array();
		$ret['id'] = 0;
		$ret['name'] = '';
		$ret['status'] = -1;
		$ret['descr'] = '保存失败';

		// parameters
		$userid = intval($userid);
		$oldpassword = trim($oldpassword);
		$newpassword = trim($newpassword);

		// check parameters
		if ( $userid < 1 || !$newpassword || $newpassword != addslashes($newpassword) ) {
			$ret['descr'] = '参数错误';
			return $ret;
		}

		// logical check
		$user = $this->getUser($userid);
		if (!$user || count($user) < 1) { // not exist
			$ret['descr'] = "用户不存在";
			return $ret;
		}

		if($needoldpwd){
			$pwd =  md5( md5(trim($oldpassword)) . $user['salt']);
			if ($pwd != $user['password']) {
				$ret['descr'] = "旧密码不正确";
				return $ret;
			}
		}

		$v = array();
		$v['salt'] = rand(100000000, 900000000);
		$v['password'] = md5(md5(trim($newpassword)) . $v['salt']);
		if(count($post)>0){
			$v = array_merge($v, $post);
		}

		$this->ci->load->database();
		$this->ci->db->where(array('id' => $userid))->update('c_user', $v);
		$retid = $this->ci->db->affected_rows();
		if ($retid > 0) {
			$ret['status'] = 1;
			$ret['descr'] = '保存成功';
		}
		$ret['id'] = $userid;
		return $ret;
	}

	/* ------------------------------- app users ------------------------------------------- */

	public function getAppToken($appname, $id, $userid=-1, $openid=null, $status=-1) {
		$vs = array();
		$where = array();
		if ($id >= 0) {
			$where['id'] = $id;
		}
		if ($userid >= 0) {
			$where['userid'] = $userid;
		}
		if (strlen($openid) > 0) {
			$where['openid'] = $openid;
		}
		if ($status >= 0) {
			$where['status'] = $status;
		}

		if (strlen($appname) < 1 || count($where) < 1) {
			return $vs;
		}

		$this->ci->load->database();
		$query = $this->ci->db->get_where('c_user_' . $appname, $where);
		$vs = $query->result_array();
		//test: dump($this->ci->db->last_query());
		return $vs;
	}

	public function saveAppToken($appname, $id, $openid, $post=null) {
		// return values
		$ret = array();
		$ret['id'] = 0;
		$ret['name'] = '';
		$ret['status'] = -1;
		$ret['descr'] = '保存失败';

		// parameters
		$appname = trim($appname);
		$openid = trim($openid);

		// check parameters
		if ( strlen($appname)<1 || strlen($openid)<1) {
			$ret['descr'] = '参数错误';
			return $ret;
		}

		$v = array();
		$names = array('userid', 'token', 'secret', 'tokentype', 'scope', 'nickname', 'rtime', 'datafield', 'clientid', 'status');
		if( null != $post && count($post)>0){
			foreach ($names as $name) {
				if (isset($post[$name])) {
					$v[$name] = $post[$name];
				}
			}
		}
		if ( count($v)<1 ) {
			$ret['descr'] = '参数错误';
			return $ret;
		}
		$v['openid'] = $openid;
		$tm = time();
		$ip = $this->ci->input->ip_address();

		$this->ci->load->database();
		if ($id < 1) {  		// add
			$v['userid'] = 0;
			$v['addtime'] = $v['lasttime'] = $tm;
			$v['addip'] = $v['lastip'] = $ip;
			$this->ci->db->insert('c_user_' . $appname, $v);
			$retid = $this->ci->db->insert_id();
		} else {
			$v['lasttime'] = $tm;
			$v['lastip'] = $ip;
			$this->ci->db->where(array('id' => $id))->update('c_user_' . $appname, $v);
			$retid = $this->ci->db->affected_rows();
			if ($retid > 0) {
				$retid = $id;
			}
		}
		if ($retid > 0) {
			$ret['status'] = 1;
		} else {
			$ret['status'] = 0;
		}
		$ret['id'] = $retid;
		$ret['descr'] = '保存成功';
		return $ret;
	}

	public function userAppLogin($appname, $userid) {
		$this->ci->load->database();
		// return values
		$ret = array();
		$ret['id'] = 0;
		$ret['name'] = '';
		$ret['status'] = -1;
		$ret['descr'] = "登录失败";

		// parameters
		$appname = trim($appname);
		$userid = intval($userid);

		// format checks
		if (strlen($appname) < 1 || $userid < 1) {
			$ret['descr'] = "非法输入";
			return $ret;
		}

		$v = $this->getUser($userid);
		// not exists
		if (count($v) < 1) {
			$ret['descr'] = "用户不存在";
			return $ret;
		}
		$this->ci->db->set('logincount', 'logincount+1', FALSE);
		$this->ci->db->set('lasttime', time());
		$this->ci->db->set('lastip', $this->ci->input->ip_address());
		$this->ci->db->where(array('id' => $v['id']))->update('c_user');

		$ret['id'] = $v['id'];
		$ret['user'] = $v;
		$ret['status'] = 1;
		$ret['descr'] = "登录成功";

		return $ret;
	}

	/*  ----------------------- misc -------------------------- */

	// save user's request code
	public function saveRequestCode($mobile, $email, $code, $ssid='', $befrom='register', $post=null){
		// return values
		$ret = array();
		$ret['status'] = -1;
		$ret['descr'] = '保存失败';

		if( (strlen($mobile)<1 && strlen($email)<1) || strlen($code)<1  ){
			$ret['descr'] = '参数错误';
			return $ret;
		}

		// parameters
		$tm = time();
		$ip = $this->ci->input->ip_address();

		$v = array();
		$v['mobile'] = $mobile;
		$v['email'] = $email;
		$v['code'] = $code;
		$v['ssid'] = $ssid;
		$v['befrom'] = $befrom;
		$v['addtime'] = $tm;
		$v['addip'] = $ip;

		$snames = array('longitude', 'latitude', );
		$inames = array('userid', 'status', 'clientid', );
		foreach( $snames as $name ){
			if( isset($post[$name]) ){
				$v[$name] = $post[$name];
			}
		}
		foreach( $inames as $name ){
			if( isset($post[$name]) ){
				$v[$name] = intval($post[$name]);
			}
		}
		if( isset($v['longitude'])){
			$v['longitude'] = floatval($v['longitude']);
			$v['latitude'] = floatval($v['latitude']);
		}

		// to db
		$this->ci->load->database();
		$this->ci->db->insert('user_code', $v);
		$retid = $this->ci->db->insert_id();
		if($retid>0){
			$ret['id'] = $retid;
			$ret['code'] = $code;
			$ret['status'] = 1;
			$ret['descr'] = 'success';
		}
		return $ret;
	}

	public function feedback($userid, $note, $post=null){
		// return values
		$ret = array();
		$ret['status'] = -1;
		$ret['descr'] = '保存失败';

		// parameters
		$userid = intval($userid);
		$note = trim($note);
		$tm = time();
		$ip = $this->ci->input->ip_address();

		// check parameters
		if (strlen($note) < 1) {
			$ret['descr'] = '参数错误';
			return $ret;
		}

		$v = array();
		$v['userid'] = $userid;
		$v['note'] = $note;
		$v['type'] = 0;
		$v['addtime'] = $tm;
		$v['addip'] = $ip;

		// other parameters
		$snames = array('sid', 'email', 'username','location','longitude','latitude');
		$inames = array('type','status','client');
		foreach( $snames as $name ){
			if( isset($post[$name]) ){
				$v[$name] = $post[$name];
			}
		}
		foreach( $inames as $name ){
			if( isset($post[$name]) ){
				$v[$name] = intval($post[$name]);
			}
		}
		if( isset($v['longitude'])){
			$v['longitude'] = floatval($v['longitude']);
			$v['latitude'] = floatval($v['latitude']);
		}
		$this->ci->load->database();
		$this->ci->db->insert('c_feedback', $v);
		$retid = $this->ci->db->insert_id();
		if($retid < 0){
			return $ret;
		}

		$ret['id'] = $retid;
		$ret['status'] = 1;
		$ret['descr'] = '保存成功';
		return $ret;
	}

	public function groupChat($userid, $groupid, $chattype = 0, $note = null, $post = null){
		// return values
		$ret = array();
		$ret['status'] = -1;
		$ret['descr'] = '保存失败';

		// parameters
		$userid = intval($userid);
		$groupid = intval($groupid);
		$chattype = intval($chattype);

		// check parameters
		if ($userid < 1 || $groupid < 1) {
			$ret['descr'] = '参数错误';
			return $ret;
		}
		if( strlen($note)<1 && count($post)< 1){
			$ret['descr'] = '参数错误';
			return $ret;
		}

		$v['userid'] = $userid;
		$v['groupid'] = $groupid;
		$v['chattype'] = $chattype;
		$v['note'] = $note;
		$v['addtime'] = time();
		$v['addip'] = $this->ci->input->ip_address();

		if( !is_array($post) || count($post)<1){
			$v['objecttype'] = 'text';
		}else{
			$snames = array('objecttype','filetype', 'remark', 'savepath', 'savename', 'longitude','latitude');
			$inames = array('objectid', 'filesize', 'dateline','width','height','type');
			foreach( $snames as $name ){
				if( isset($post[$name]) ){
					$v[$name] = $post[$name];
				}
			}
			foreach( $inames as $name ){
				if( isset($post[$name]) ){
					$v[$name] = intval($post[$name]);
				}
			}
			if( isset($v['longitude'])){
				$v['longitude'] = floatval($v['longitude']);
				$v['latitude'] = floatval($v['latitude']);
			}
		}

		$this->ci->load->database();
		$this->ci->db->insert('group_chat', $v);
		$retid = $this->ci->db->insert_id();
		if ($retid < 1) {
			return $ret;
		}

		$ret['id'] = $retid;
		$ret['status'] = 1;
		$ret['descr'] = '保存成功';
		return $ret;
	}

	public function getGroupChat($id, $chattype=0){
		$vs = array();
		if ($id < 1) {
			return $vs;
		}
		$this->ci->load->database();
		$query = $this->ci->db->get_where('group_chat', array('id' => $id,'chattype'=>$chattype));
		$vs = $query->row_array();
		return $vs;
	}

	/*  ----------------------- util ------------------------------ */

	public function check_username($username) {
		$guestexp = '\xA1\xA1|^Guest|^\xD3\xCE\xBF\xCD|\xB9\x43\xAB\xC8';
		$len = strlen($username);
		if ($len > 40 || $len < 3 || preg_match("/\s+|^c:\\con\\con|[%,\*\"\s\<\>\&]|$guestexp/is", $username)) {
			return FALSE;
		} else {
			return TRUE;
		}
	}

	public function check_email($email) {
		return strlen($email) > 6 && preg_match("/^[\w\-\.]+@[\w\-\.]+(\.\w+)+$/", $email);
	}

	public function formatMobile($mobile){
		if( strlen($mobile)>0 ){
			return preg_replace( array("/^[0\+]86/","/[ \-+()]/si"), "",$mobile);
		}
		return null;
	}

	public function check_mobile($mobile) {
		//return preg_match("/^13[0-9]{9}|15[0|1|2|3|5|6|7|8|9]\d{8}|18[0|5|6|7|8|9]\d{8}$/");
		if(strlen($mobile)>0){
			$mobile = $this->formatMobile($mobile);
		}
		return preg_match("/^(\s?\d{2,4}[\-]?)?1[3|4|5|6|7|8]\d{9}$/", $mobile);
	}

	function getRegError($code) {
		$errors = array("0" => '注册成功',
            "1" => '输入的验证码不符，请重新确认',
            "2" => '系统错误，请稍后重试',
            "3" => '两次输入的密码不一致',
            "4" => '密码空或包含非法字符，请重新填写',
            "5" => '同一个IP在1小时内只能注册一个账号',
            "11" => '用户名不合法',
            "12" => '用户名包含不允许注册的词语',
            "13" => '用户名已经存在',
            "14" => '电子邮箱格式有误',
            "15" => '电子邮箱不允许注册',
            "16" => '电子邮箱已经被注册',
            "17" => '注册失败');
		$s = $errors[$code];
		if (empty($s)) {
			$s = '注册失败';
		}
		return $s;
	}

	function getUserLocation($post = null) {
		$this->ci->load->database();
		$ret = array();
		if (!$post || count($post) < 1) {
			$ip = $this->ci->input->ip_address();
			$vs = split("\.", $ip);
			$ip_num = $vs[0] * 255 * 255 * 255 + $vs[1] * 255 * 255 + $vs[2] * 255 + $vs[3];

			$query = $this->ci->db->get_where('ip', array('ip_num_start <=' => $ip_num, 'ip_num_end >=' => $ip_num));
			$ret = $query->row_array();
			//dump($this->ci->db->last_query());
		}
		return $ret;
	}


} //cls

