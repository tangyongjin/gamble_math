<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class AMoneyreb
{
    public function __construct()
    {
        $this->ci = &get_instance();
        $CI       = &get_instance();

    }

    public function debugDiv($arr)
    {

        echo "<pre><div  style='margin-top:50px;border:1px solid red;clear:both;'>Debug->";
        print_r($arr);
        echo "</pre></div>";

    }

    public function extract_users($a_group_score)
    {
        $ret = array();
        foreach ($a_group_score as $userid => $user_score) {
            $ret[] = array(
                'uid' => $userid,
            );
        }
        return $ret;
    }

    public function get_payers($a_or_b, $par, $x_group_score, $worst_payall)
    {

        $player_pair = array_keys($x_group_score);

        if ('noduty' == $worst_payall) {
            $this->ci->xlog->getInstance()->log("无包洞配置,{$a_or_b}方不包洞/XMoneyreb", 3);
            return $player_pair;
        }

        if (count($x_group_score) == 1) {
            return $player_pair;
        }

        $player_1_id    = $player_pair[0];
        $player_2_id    = $player_pair[1];
        $player_1_score = $x_group_score[$player_1_id];
        $player_2_score = $x_group_score[$player_2_id];

        if (strlen($worst_payall) > 0) {

            $worst_base_line = $this->getWorstBaseLine($par, $worst_payall);
            $payers          = $player_pair;

            if (($player_1_score >= $worst_base_line) && ($player_2_score < $worst_base_line)) {
                $this->ci->xlog->getInstance()->log("如果{$a_or_b}方输了,包洞ID:  {$player_1_id}/XMoneyreb", 3);
                $payers = array(
                    $player_1_id,
                );
            }

            if (($player_2_score >= $worst_base_line) && ($player_1_score < $worst_base_line)) {
                $this->ci->xlog->getInstance()->log("如果{$a_or_b}方输了包洞ID:  {$player_2_id}/XMoneyreb", 3);
                $payers = array(
                    $player_2_id,
                );
            }

            if (($player_2_score >= $worst_base_line) && ($player_1_score >= $worst_base_line)) {
                $this->ci->xlog->getInstance()->log("如果{$a_or_b}方输了,两人都达到了包洞标准,谁都不包/XMoneyreb", 3);
            }

        } else {
            $this->ci->xlog->getInstance()->log("如果{$a_or_b}方输了,不包/XMoneyreb", 3);
            $payers = $player_pair;
        }

        return $payers;
    }


    //  public function get_payers_8421($a_or_b, $par, $x_group_score, $duty_8421,$detail_8421)
    // {

    //     //duty_all|duty_negative_points|noduty
    //     $player_pair = array_keys($x_group_score);

    //     if ('noduty' == $duty_8421) {
    //         $this->ci->xlog->getInstance()->log("无8421包分配置,{$a_or_b}方不包洞/XMoneyreb", 3);
    //         return $player_pair;
    //     }

    //     $detail_for_check=$detail_8421[$a_or_b];
        
    //     $player_1_id    = $detail_for_check[0]['user'];
    //     $player_2_id    = $detail_for_check[1]['user'];
    //     $player_1_scoreget = $detail_for_check[0]['scoreget'];
    //     $player_2_scoreget = $detail_for_check[1]['scoreget'];
        
    
    //     //只有一个人负责的情况 
    //     //条件: duty_all 并且:
    //     // 一个正分,一个负分.
    //     if( $duty_8421 == 'duty_all'){
    //         if (($player_1_scoreget >0 )&&( $player_2_scoreget<0)){
    //              $this->ci->xlog->getInstance()->log(" $player_2_id 全包", 3);
    //              return  array($player_2_id);

    //          }

    //          if (($player_2_scoreget >0 )&&( $player_1_scoreget<0)){
    //              $this->ci->xlog->getInstance()->log(" $player_1_id 全包", 3);
    //              return  array($player_1_id);
    //          }
    //     }
    //     return $player_pair;
    // }






    public function getWorstBaseLine($par, $worst_payall)
    {

        $worst_top_indicator = $worst_payall;

        if ('doublepar' == $worst_top_indicator) {
            $worst_top_number = 2 * $par;
        }

        if ('par+3' == $worst_top_indicator) {
            $worst_top_number = $par + 3;
        }

        if ('par+4' == $worst_top_indicator) {
            $worst_top_number = $par + 4;
        }

        if ('doublepar+1' == $worst_top_indicator) {
            $worst_top_number = 2 * $par + 1;
        }
        if ('doublepar+2' == $worst_top_indicator) {
            $worst_top_number = 2 * $par + 2;
        }
        if ('doublepar+3' == $worst_top_indicator) {
            $worst_top_number = 2 * $par + 3;
        }
        if ('doublepar+4' == $worst_top_indicator) {
            $worst_top_number = 2 * $par + 4;
        }
        return $worst_top_number;
    }

    public function set_winner_failer_money($para, $index, $total_money_payed)
    {

        $kpi_summary = $para['gamedata'][$index]['kpi_summary'];
        $AB          = $para['gamedata'][$index]['AB'];

        $a_payers_if_fail = $kpi_summary['a_payers_if_fail'];
        $b_payers_if_fail = $kpi_summary['b_payers_if_fail'];
        $winner           = $kpi_summary['winner_after_summary'];
        $winners          = $kpi_summary['winners'];
        $failers          = $kpi_summary['failers'];

        $winner_count = count($winners);
        $failer_count = count($failers);

        if ('-' == $winner) {
            $winner_each_money = 0;
            $failer_each_money = 0;

        } else {

            $winner_each_money = $total_money_payed / $winner_count;
            $failer_each_money = $total_money_payed / $failer_count;

        }

        for ($i = 0; $i < count($winners); $i++) {
            $winners[$i]['money'] = $winner_each_money;
        }

        for ($i = 0; $i < count($failers); $i++) {
            $failers[$i]['money'] = $failer_each_money * -1;
        }

        if ('A' == $winner) {
            if (count($b_payers_if_fail) !== count($AB['B'])) {
                //1个人负责所有

                $payer_uid = $b_payers_if_fail[0];
                for ($i = 0; $i < count($failers); $i++) {
                    if ($failers[$i]['uid'] == $payer_uid) {
                        $failers[$i]['money'] = $total_money_payed * -1;
                    } else {
                        $failers[$i]['money'] = 0;
                    }
                }
            }
        }

        if ('B' == $winner) {
            if (count($a_payers_if_fail) !== count($AB['A'])) {
                $payer_uid = $a_payers_if_fail[0];
                for ($i = 0; $i < count($failers); $i++) {
                    if ($failers[$i]['uid'] == $payer_uid) {
                        $failers[$i]['money'] = $total_money_payed * -1;

                    } else {
                        $failers[$i]['money'] = 0;
                    }
                }
            }
        }

        return array(
            'winners' => $winners,
            'failers' => $failers,
        );
    }

    public function set_winner_failer_money_meat($para, $index, $total_money_payed)
    {

        $kpi_summary = $para['gamedata'][$index]['kpi_summary'];

        $winners = $kpi_summary['winners'];
        $failers = $kpi_summary['failers'];

        $winner_each_money = $total_money_payed / count($winners);
        $failer_each_money = $total_money_payed / count($failers);

        for ($i = 0; $i < count($winners); $i++) {
            $winners[$i]['money'] = $winner_each_money;
        }

        for ($i = 0; $i < count($failers); $i++) {
            $failers[$i]['money'] = $failer_each_money * -1;
        }

        $w_f_info = array(
            'winners' => $winners,
            'failers' => $failers,
        );

        return $w_f_info;
    }

    public function user_by_user_pay_info($pay_info, $valid)
    {

        $winners_with_money = $pay_info['winners'];
        $failers_with_money = $pay_info['failers'];

        $list = array_merge($winners_with_money, $failers_with_money);

        foreach ($list as $key => $value) {
            $money[$key] = $value['money'];
            $uid[$key]   = $value['uid'];
        }

        if (count($list) > 0) {
            array_multisort($uid, $money, $list);
        }
        foreach ($list as $key => $one_value) {
            $list[$key]['order'] = $key;
            $list[$key]['valid'] = $valid;
        }

        return $list;
    }
 
    public function merge_base_and_meat_money_log_with_initrank(&$para)
    {

        $rank_init = $para['rank_init'];

        foreach ($para['gamedata'] as $key => $one) {
            $merged_money = array();

            $money_list_1                             = $this->reorderArray($para['rank_init'], $one['base_money_log']);
            $para['gamedata'][$key]['base_money_log'] = $money_list_1;
            $money_list_2                             = $this->reorderArray($para['rank_init'], $one['meat_money_log']);
            $para['gamedata'][$key]['meat_money_log'] = $money_list_2;

            foreach ($rank_init as $k => $one_uid) {

                $tmp = array(
                    'uid'        => $one_uid,
                    'title'      => 'uinone',
                    'valid'      => $money_list_1[$k]['valid'],
                    'money_hole' => $money_list_1[$k]['money'],
                    'money_meat' => $money_list_2[$k]['money'],
                    'money'      => $money_list_1[$k]['money'] + $money_list_2[$k]['money'],
                );

                $merged_money[] = $tmp;
            }
            $para['gamedata'][$key]['base_money_log'] = $merged_money;
        }
    }

    public function reorderArray($rank_init, $money_log)
    {
        $ret = array();
        $n   = count($rank_init);
        for ($i = 0; $i < $n; $i++) {
            $uid_f = $rank_init[$i];

            for ($j = 0; $j < $n; $j++) {

                if ($money_log[$j]['uid'] == $uid_f) {
                    $ret[] = $money_log[$j];
                    break;
                }
            }
        }
        return $ret;
    }
}
