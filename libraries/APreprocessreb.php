<?php

/* 
 成绩预处理
让杆:如果让完杆以后小于封顶后的,是否处理?

*/

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class APreprocessreb
{
    public function __construct()
    {
        $this->ci =& get_instance();
        $CI =& get_instance();
        

    }
    
      //跟par有关的术语翻译
 

      function item_trans($item,$par){

            $item=strtolower($item);  

            $value=-1;


            if ($item == 'doublebogey') {
                $value = $par+2 ;
            }
            
            if ($item == 'bogey') {
                $value = $par+1 ;
            }

        
            if ($item == 'par') {
                $value = $par;
            }

            if ($item == 'birdie') {
                $value = $par -1;
            }



            if ($item == 'eagle') {
                $value = $par -2;
            }

            if ($item == 'hio') {
                $value = 1;
            }


            if ($item == 'par+2') {
                $value = $par+2;
            }

            if ($item == 'par+3') {
                $value = $par+3;
            }

            if ($item == 'par+4') {
                $value = $par+4;
            }


            if ($item == 'par+5') {
                $value = $par+5;
            }

            if ($item == 'par+6') {
                $value = $par+6;
            }


           if ($item == 'doublepar') {
                $value = 2 * $par;
            }
            
            if ($item == 'doublepar+1') {
                $value = 2 * $par + 1;
            }
            
            if ($item == 'doublepar+2') {
                $value = 2 * $par + 2;
            }
            
            if ($item == 'doublepar+3') {
                $value = 2 * $par + 3;
            }
            
            if ($item == 'doublepar+4') {
                $value = 2 * $par + 4;
            }

            if(  $value==-1){
                $this->ci->xlog->getInstance()->log("术语翻译出错/XPreprocessreb.php",4);
                die;
            }
            
            return  $value;

      }

      function preprocss_with_weak_and_max(&$para)
    {
         
        $weaker_cfg = $para['weaker_cfg'];
      
        $max_option = $para['max_option'];
        
        array_walk($para['gamedata'], array(
            $this,
            'score_onely_max'
        ), array(
            'max_option' => $max_option
        ));

        array_walk($para['gamedata'], array(
            $this,
            'score_max_and_weak'
        ), $weaker_cfg);
        
         
    }
    
      function score_onely_max(&$one_hole, $key, $params)
    {
        $par        = $one_hole['par'];
        $raw_scores = $one_hole['raw_scores'];
        $max_option = $params['max_option'];
        
        
        if ($max_option == 'nomax') {
            $one_hole['after_max'] = $raw_scores;
            return;
        }
        
        
        $score_after_max = array();
        if (strlen($max_option) > 1) {
            
            foreach ($raw_scores as $uid => $score) {
 
                $max_score=$this->item_trans($max_option,$par);
                
                if ($score <= $max_score) {
                    $score_after_max[$uid] = $score;
                } else {
                    $score_after_max[$uid] = $max_score;
                }
            }
            
            $one_hole['after_max'] = $score_after_max;
        } else {
            $one_hole['after_max'] = $raw_scores;
        }
    }
    
    

      function score_max_and_weak(&$one_hole, $key, $weaker_cfg)
    {
        $par                        = $one_hole['par'];
        $score_after_max_and_weak   = $this->score_weak_process($one_hole['after_max'], $par, $weaker_cfg);
        $one_hole['after_max_weak'] = $score_after_max_and_weak;
    }

     
    //预处理让杆 //['weaker_cfg']['rule']
    function score_weak_process($scores, $par, $weaker_cfg)
    {

       //没有让杆情况.
        if($weaker_cfg['rule']=='n'){
     
            return  $scores;
        }
 
        $real_weakers       = $weaker_cfg['real_weakers'];

        $score_weaked = array();
        
        foreach ($scores as $uid => $score) {
            if ( in_array($uid, $real_weakers)    ) {
                    if ($par == 3) {
                        $score_after_weak = $score - abs($weaker_cfg['weak_par3_num']);
                    }
                    if ($par == 4) {
                        
                        $score_after_weak = $score - abs($weaker_cfg['weak_par4_num']);
                    }
                    
                    if ($par == 5) {
                        $score_after_weak = $score - abs($weaker_cfg['weak_par5_num']);
                    }
                    
                    if ($par > 5) {
                        $score_after_weak = $score - abs($weaker_cfg['weak_par5_num']);
                    }
                    if ($score_after_weak < 0) {
                        $score_after_weak = 0;
                    }
            } else {
                $score_after_weak = $score;
            }
            
            $score_weaked[$uid] = $score_after_weak;
        }
        return $score_weaked;
    }
    
        
   

     function get_group_scores($game_data, $index, $score_type)
    {
        
        $hole_info     = $game_data[$index];
        $a_group_score = array();
        $b_group_score = array();
        
        foreach ($hole_info['AB']['A'] as $userid_in_a) {
            
            $a_group_score[$userid_in_a] = $hole_info[$score_type][$userid_in_a];
        }
        
        foreach ($hole_info['AB']['B'] as $userid_in_b) {
            $b_group_score[$userid_in_b] = $hole_info[$score_type][$userid_in_b];
        }
        
        $ab_group_score = array(
            'A' => $a_group_score,
            'B' => $b_group_score,
            'index' => $index
        );
        return $ab_group_score;
    }
     
}

?> 
