<?php

/*
取得最好成绩对应的奖励规则. 
*/

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class ABonusreb
{
    public function __construct()
    {
        $this->ci =& get_instance();
        $CI =& get_instance();
        $CI->load->library('APreprocessreb');
        $this->xpreprocess = $CI->apreprocessreb;
    }


      function get_score_name($score,$par){
          
            $score_name='';

            if ( $score == $par+2 ) {
                $score_name = 'DoubleBogey';
            }

            
            if ( $score == $par+1) {
                $score_name = 'Bogey';
            }

        
            if ($score == $par) {
                $score_name = 'Par';
            }

            if ($score == $par -1) {
                $score_name = 'Birdie';
            }


            if ($score == $par -2 ) {
                $score_name = 'Eagle';
            }

            if ($score == $par -3 ) {
                $score_name = 'Albatross';
            }
            

            if ($score == 1) {
                $score_name = 'HIO';
            }

            return  $score_name;

      }


      function get_twin_name($score_array,$par){
        
        //{"Par+Par":2,"Birdie+par":5,"Birdie+Birdie":10,"Eagle+Par":15,"Eagle+Birdie":20,"Eagle+Eagle":30}


        $pair_score=array_values($score_array);

        if(  count($pair_score)==1 ){
            //防止出现1个用户也寻找双奖
          
          die('1个用户也寻找双奖?');

        }

        $score1=$pair_score[0];
        $score2=$pair_score[1];
        
        $twin='';

        $name1=$this->get_score_name($score1,$par);
        $name2=$this->get_score_name($score2,$par);
          
        if(($name1=='Par')&&($name2=='Par')){
            $twin='Par+Par';
        }

        if( (($name1=='Birdie')&&($name2=='Par'))||(($name2=='Birdie')&&($name1=='Par'))  ){
            $twin='Birdie+Par';
        }



       if( ($name1=='Birdie')&&($name2=='Birdie')){
            $twin='Birdie+Birdie';
       
        }


       if( (($name1=='Eagle')&&($name2=='Par'))||(($name2=='Eagle')&&($name1=='Par'))  ){
            $twin='Eagle+Par';
        }


        if( (($name1=='Eagle')&&($name2=='Birdie'))||(($name2=='Eagle')&&($name1=='Birdie'))  ){
            $twin='Eagle+Birdie';
        }


       if( ($name1=='Eagle')&&($name2=='Eagle')){
            $twin='Eagle+Eagle';
       
        }

        if( ($name1=='Eagle')&&($score2< $par -2)){
            $twin='Eagle+Eagle';
       
        }
        
        if( ($name2=='Eagle')&&($score1< $par -2)){
            $twin='Eagle+Eagle';
       
        }
        return $twin;

       }
    


    function get_score_for_bonus($algorithm_cfg,$kpi_name,$Indicator,$winner,$ab_group_score_max_and_weak){
     
        $winnerside_best_score  = min(   $ab_group_score_max_and_weak[$winner]);
        $indicator_side='indicator_'.strtolower($winner); // indicator_a or indicator_b
             
        if( in_array($kpi_name, array('kpi_best','kpi_total_add','kpi_total_plus'))){
          $score_for_bonus=$winnerside_best_score;
        }
        
        //比A比K,以决定输赢的成绩来取奖励/或者以最好成绩
        if( in_array($kpi_name, array('kpi_best_ak'))){
            if($algorithm_cfg['kpi_ak_bonus_score_src']=='score_best'){
                $score_for_bonus=$winnerside_best_score;
            }else
            {
               $score_for_bonus=$Indicator[$indicator_side];  
            }
         }

        //比最差成绩,以决定输赢的成绩来取奖励/或者以最好成绩
         if( in_array($kpi_name, array('kpi_worst'))){
            if( $algorithm_cfg['kpi_worst_bonus_score_src']=='score_used' ){
                     $score_for_bonus=$Indicator[$indicator_side];  
            }else
            {
                     $score_for_bonus=$winnerside_best_score;
            }
         }
           return $score_for_bonus;
    }
    

     function get_bonus_by_score($bonus_option, $par, $score_used,$winner_pair_score)
    {
       

       $test=intval($score_used);

       if($test==0){
         die('score is 0');
       }


       $bonus_single_or_double= $bonus_option['bonus_single_or_double']=='double'?'双奖':'单奖';
       $score_name=$this->get_score_name($score_used,$par);  //单奖励
       
       if($bonus_option['bonus_single_or_double']=='double'){
           $twin_name=$this->get_twin_name($winner_pair_score,$par);
       }else
       {
          $twin_name='never_exists';
       }


        
         if( array_key_exists($twin_name, $bonus_option)){
           
            if(($bonus_option['bonus_operator']=='plus')&&
                  (  intval($bonus_option[$twin_name]) ==0)){
                 return array(
                    'bonus_operator' => 'none',
                    'bonus_factor' => 0
                  );

            }
            $this->ci->xlog->getInstance()->log("使用双奖:$twin_name  {$bonus_option[$twin_name]} " , 2, null);
            return array(
                'bonus_operator' => $bonus_option['bonus_operator'],
                'bonus_factor' =>   $bonus_option[$twin_name]
            );

        } // twin_name/双奖找到.

       //双奖没找到,寻找单奖 
       if( array_key_exists($score_name, $bonus_option)){
        
            if(($bonus_option['bonus_operator']=='plus')&&
                  (  intval($bonus_option[$score_name]) ==0)){
                 return array(
                    'bonus_operator' => 'none',
                    'bonus_factor' => 0
                  );

            }

            $this->ci->xlog->getInstance()->log("使用单奖:$score_name $bonus_option[$score_name]/ {$bonus_option['bonus_operator']} " , 2, null);
            
            return array(
                'bonus_operator' => $bonus_option['bonus_operator'],
                'bonus_factor' =>   $bonus_option[$score_name]
            );

        }
        else
        {
            return array(
                'bonus_operator' => 'none',
                'bonus_factor' => 0
            );

        }
    }

     function get_public_bonus($para, $index, $winner)
    {
        
        if ($winner == '-') {
            return array(
                'bonus_operator' => 'none',
                'bonus_factor' => 0
            );
        }
    
        $choosen=$para['algorithm_cfg']['public_bonus_score_src']; 
        $par                = $para['gamedata'][$index]['par'];
        $ab_group_score_selected = $this->xpreprocess->get_group_scores($para['gamedata'], $index,  $choosen);
        
        $winner_score       = $ab_group_score_selected[$winner];
        
        $ab_group_score_max_and_weak = $this->xpreprocess->get_group_scores($para['gamedata'], $index, 'after_max_weak');
        $winner_pair_score=$ab_group_score_max_and_weak[$winner];

        //4人拉丝-比A比K游戏.是用A来算奖励,还是用K算?
        if($para['kpi_cfg'][0]=='kpi_best_ak' ){   

             $this_hole_k_used=$para['ak_k_use_list'][$index];
             $ak_bonus_selector= $para['algorithm_cfg']['kpi_ak_bonus_score_src']; 
             
             if( ($this_hole_k_used=='y')&&($ak_bonus_selector=='score_used') ){
                       return $this->get_bonus_by_score($para['bonus_option'], $par, max($winner_score),$winner_pair_score);
             }else
             {
                       return $this->get_bonus_by_score($para['bonus_option'], $par, min($winner_score),$winner_pair_score);
             }
        }

        $bonus_found        = $this->get_bonus_by_score($para['bonus_option'], $par, min($winner_score),$winner_pair_score);
        return $bonus_found;
    }
}

?> 
