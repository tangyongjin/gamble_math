 <?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}


class XMath
{
    private $ci = null;
    
    public function __construct()
    {
        $this->ci =& get_instance();
        $this->listeners = array();
        $CI =& get_instance();
      


    }



    public function debug($r)
    {
        echo "<pre>";
        print_r($r);
        echo "</pre>";
    }
    
    //计算用户等级
    public function math_user_rank($experience, $user_rank)
    {
         log_model_call(); 
        $sys_rank_array = array(
            '1' => array(
                '0',
                '300'
            ),
            '2' => array(
                '300',
                '900'
            ),
            '3' => array(
                '900',
                '2700'
            ),
            '4' => array(
                '2700',
                '5000'
            ),
            '5' => array(
                '5000',
                '7800'
            ),
            '6' => array(
                '7800',
                '11500'
            ),
            '7' => array(
                '11500',
                '15500'
            ),
            '8' => array(
                '15500',
                '20000'
            ),
            '9' => array(
                '20000',
                '25500'
            ),
            '10' => array(
                '25500',
                '32000'
            ),
            '11' => array(
                '32000',
                '40000'
            ),
            '12' => array(
                '40000',
                '48500'
            ),
            '13' => array(
                '48500',
                '60000'
            ),
            '14' => array(
                '60000',
                '72000'
            ),
            '15' => array(
                '72000'
            )
        );
        $count          = count($sys_rank_array);
        foreach ($sys_rank_array as $key => $value) {
            if ($key != $count) {
                if (($value[0] <= $experience) && ($value[1] > $experience)) {
                    $user_rank = $key;
                }
            } else {
                if ($user_info['experience'] >= $value[0]) {
                    $user_rank = $key;
                }
            }
        }
        return $user_rank;
    }
    
    public function math_user_game_plus_experience($user_play_game_info, $userid, $game_end_time, $play_game_usernum, $gametype)
    {
         log_model_call(); 
        $plus_experience = 0;
        $end_time        = $game_end_time;
        $this->ci->load->database();
        $user_game_lasttime = date('Y-m-d', strtotime($user_play_game_info['user_game_lasttime']));
        $user_game_lasttime = strtotime($user_game_lasttime . ' 00:00:00');
        $game_end_time      = date('Y-m-d', $game_end_time);
        $game_end_time      = strtotime($game_end_time . ' 00:00:00');
        $username           = $user_play_game_info['nickname'];
        
        
        if ($gametype == GAME_TYPE_INNER) {
            if ($game_end_time - strtotime($user_play_game_info['team_game_lasttime']) > 604800) {
                $user_experience = $user_play_game_info['experience'] + 50;
                $plus_experience = 50;
                $this->ci->db->where('id', $userid);
                $this->ci->db->update('t_user', array(
                    'experience' => $user_experience,
                    'team_game_lasttime' => date('Y-m-d H:i:s', $end_time)
                ));
                $this->ci->db->insert('t_user_event_log', array(
                    'userid' => $userid,
                    'nickname' => $username,
                    'event' => '进行球队比赛',
                    'experience_added' => $plus_experience,
                    'addtime' => date('Y-m-d H:i:s')
                ));
                $id = $this->ci->db->insert_id();
                $this->experience_conversion_credit($userid, $id);
                return $plus_experience;
            } else {
                
                return $plus_experience;
            }
        }
        if ($play_game_usernum == 1) {
            if ($game_end_time - $user_game_lasttime == 0) {
                if ($user_play_game_info['user_game_one_player'] == 2) {
                    
                    return $plus_experience;
                }
                $user_game_one_two_player = $user_play_game_info['user_game_one_player'] + 1;
                $this->ci->db->where('id', $userid);
                $user_experience = $user_play_game_info['experience'] + 3;
                $plus_experience = 3;
                $this->ci->db->update('t_user', array(
                    'user_game_one_player' => $user_game_one_two_player,
                    'experience' => $user_experience,
                    'user_game_lasttime' => date('Y-m-d H:i:s', $end_time)
                ));
                $this->ci->db->insert('t_user_event_log', array(
                    'userid' => $userid,
                    'nickname' => $username,
                    'event' => '进行一场1个人的比赛',
                    'experience_added' => $plus_experience,
                    'addtime' => date('Y-m-d H:i:s')
                ));
                $id = $this->ci->db->insert_id();
            } else {
                $user_experience = $user_play_game_info['experience'] + 3;
                $plus_experience = 3;
                $this->ci->db->where('id', $userid);
                $this->ci->db->update('t_user', array(
                    'user_game_one_player' => 1,
                    'experience' => $user_experience,
                    'user_game_lasttime' => date('Y-m-d H:i:s', $end_time)
                ));
                $this->ci->db->insert('t_user_event_log', array(
                    'userid' => $userid,
                    'nickname' => $username,
                    'event' => '进行一场1个人的比赛',
                    'experience_added' => $plus_experience,
                    'addtime' => date('Y-m-d H:i:s')
                ));
                $id = $this->ci->db->insert_id();
            }
        }
        if ($play_game_usernum == 2) {
            if ($game_end_time - $user_game_lasttime == 0) {
                if ($user_play_game_info['user_game_two_player'] == 2) {
                    
                    return $plus_experience;
                }
                $user_game_one_two_player = $user_play_game_info['user_game_two_player'] + 1;
                $this->ci->db->where('id', $userid);
                $user_experience = $user_play_game_info['experience'] + 5;
                $plus_experience = 5;
                $this->ci->db->update('t_user', array(
                    'user_game_two_player' => $user_game_one_two_player,
                    'experience' => $user_experience,
                    'user_game_lasttime' => date('Y-m-d H:i:s', $end_time)
                ));
                $this->ci->db->insert('t_user_event_log', array(
                    'userid' => $userid,
                    'nickname' => $username,
                    'event' => '进行一场2个人的比赛',
                    'experience_added' => $plus_experience,
                    'addtime' => date('Y-m-d H:i:s')
                ));
                $id = $this->ci->db->insert_id();
            } else {
                $user_experience = $user_play_game_info['experience'] + 5;
                $plus_experience = 5;
                $this->ci->db->where('id', $userid);
                $this->ci->db->update('t_user', array(
                    'user_game_two_player' => 1,
                    'experience' => $user_experience,
                    'user_game_lasttime' => date('Y-m-d H:i:s', $end_time)
                ));
                $this->ci->db->insert('t_user_event_log', array(
                    'userid' => $userid,
                    'nickname' => $username,
                    'event' => '进行一场2个人的比赛',
                    'experience_added' => $plus_experience,
                    'addtime' => date('Y-m-d H:i:s')
                ));
                $id = $this->ci->db->insert_id();
            }
        }
        if ($play_game_usernum == 3) {
            if ($game_end_time - $user_game_lasttime == 0) {
                if ($user_play_game_info['user_game_three_player'] == 1) {
                    
                    return $plus_experience;
                }
                $this->ci->db->where('id', $userid);
                $user_experience = $user_play_game_info['experience'] + 15;
                $plus_experience = 15;
                $this->ci->db->update('t_user', array(
                    'user_game_three_player' => 1,
                    'experience' => $user_experience,
                    'user_game_lasttime' => date('Y-m-d H:i:s', $end_time)
                ));
                $this->ci->db->insert('t_user_event_log', array(
                    'userid' => $userid,
                    'nickname' => $username,
                    'event' => '进行一场3个人的比赛',
                    'experience_added' => $plus_experience,
                    'addtime' => date('Y-m-d H:i:s')
                ));
                $id = $this->ci->db->insert_id();
            } else {
                $this->ci->db->where('id', $userid);
                $user_experience = $user_play_game_info['experience'] + 15;
                $plus_experience = 15;
                $this->ci->db->update('t_user', array(
                    'user_game_three_player' => 1,
                    'experience' => $user_experience,
                    'user_game_lasttime' => date('Y-m-d H:i:s', $end_time)
                ));
                $this->ci->db->insert('t_user_event_log', array(
                    'userid' => $userid,
                    'nickname' => $username,
                    'event' => '进行一场3个人的比赛',
                    'experience_added' => $plus_experience,
                    'addtime' => date('Y-m-d H:i:s')
                ));
                $id = $this->ci->db->insert_id();
            }
        }
        if ($play_game_usernum == 4) {
            if ($game_end_time - $user_game_lasttime == 0) {
                if ($user_play_game_info['user_game_four_player'] != 1) {
                    
                    return $plus_experience;
                }
                $this->ci->db->where('id', $userid);
                $user_experience = $user_play_game_info['experience'] + 20;
                $plus_experience = 20;
                $this->ci->db->update('t_user', array(
                    'user_game_four_player' => 1,
                    'experience' => $user_experience,
                    'user_game_lasttime' => date('Y-m-d H:i:s', $end_time)
                ));
                $this->ci->db->insert('t_user_event_log', array(
                    'userid' => $userid,
                    'nickname' => $username,
                    'event' => '进行一场4个人的比赛',
                    'experience_added' => $plus_experience,
                    'addtime' => date('Y-m-d H:i:s')
                ));
                $id = $this->ci->db->insert_id();
            } else {
                $this->ci->db->where('id', $userid);
                $user_experience = $user_play_game_info['experience'] + 20;
                $plus_experience = 20;
                $this->ci->db->update('t_user', array(
                    'user_game_four_player' => 1,
                    'experience' => $user_experience,
                    'user_game_lasttime' => date('Y-m-d H:i:s', $end_time)
                ));
                $this->ci->db->insert('t_user_event_log', array(
                    'userid' => $userid,
                    'nickname' => $username,
                    'event' => '进行一场4个人的比赛',
                    'experience_added' => $plus_experience,
                    'addtime' => date('Y-m-d H:i:s')
                ));
                $id = $this->ci->db->insert_id();
            }
        }
        $this->experience_conversion_credit($userid, $id);
        return $plus_experience;
    }
    
    
    
    public function data_analyze($data, $key)
    {
         log_model_call(); 
        if (!$data) {
            
            return '';
        }
        $gross              = array();
        $par3               = 0;
        $par4               = 0;
        $par5               = 0;
        $middle             = 0;
        $left               = 0;
        $right              = 0;
        $par_num            = array();
        $gross_num          = array();
        $gross_eagle        = 0; //老鹰
        $gross_birdle       = 0; //博蒂
        $gross_par          = 0; //标准杆
        $gross_bogey        = 0; //柏忌
        $gross_double_bogey = 0; //双柏忌
        $hole_num           = count($data);
        
        $gameid_num        = array();
        $sandball_num      = array();
        $penalty_num       = array();
        $rescue_ball_ok    = 0; //救球成功
        $bunker_num        = 0; //掉沙坑次数
        $tangent_sphere_ok = 0; //切球成功
        $tangent_sphere_no = 0; //切球不成功
        foreach ($data as $k => $v) {
            $par_num[] = $v['par'];
            if ($key == 0) {
                if (!$v['gross']) {
                    continue;
                }
            }
            if ($v['par'] == 3) {
                $par3              = $par3 + 1;
                $gross['gross3'][] = $v['gross'];
                if (($v['gross'] - $v['par']) == 0) {
                    
                    $gross_par = $gross_par + 1;
                    if ($v['push'] == 0 || $v['push'] == 1) {
                        $tangent_sphere_ok = $tangent_sphere_ok + 1;
                    }
                }
                if (($v['gross'] - $v['par']) <= -2) {
                    
                    $gross_eagle = $vgross_eagle + 1;
                }
                if (($v['gross'] - $v['par']) == -1) {
                    
                    $gross_birdle = $gross_birdle + 1;
                    if ($v['push'] == 0) {
                        $tangent_sphere_ok = $tangent_sphere_ok + 1;
                    }
                }
                if (($v['gross'] - $v['par']) == 1) {
                    
                    $gross_bogey = $gross_bogey + 1;
                    if ($v['push'] == 0 || $v['push'] == 1) {
                        $tangent_sphere_no = $tangent_sphere_no + 1;
                    }
                }
                if (($v['gross'] - $v['par']) >= 2) {
                    
                    $gross_double_bogey = $gross_double_bogey + 1;
                    if ($v['push'] == 0 || $v['push'] == 1) {
                        $tangent_sphere_no = $tangent_sphere_no + 1;
                    }
                }
            }
            
            
            if ($v['par'] == 4) {
                $par4              = $par4 + 1;
                $gross['gross4'][] = $v['gross'];
                if (($v['gross'] - $v['par']) == 0) {
                    
                    $gross_par = $gross_par + 1;
                    if ($v['push'] == 0 || $v['push'] == 1) {
                        $tangent_sphere_ok = $tangent_sphere_ok + 1;
                    }
                }
                if (($v['gross'] - $v['par']) <= -2) {
                    
                    $gross_eagle = $vgross_eagle + 1;
                }
                if (($v['gross'] - $v['par']) == -1) {
                    
                    $gross_birdle = $gross_birdle + 1;
                    if ($v['push'] == 0) {
                        $tangent_sphere_ok = $tangent_sphere_ok + 1;
                    }
                }
                if (($v['gross'] - $v['par']) == 1) {
                    
                    $gross_bogey = $gross_bogey + 1;
                    if ($v['push'] == 0 || $v['push'] == 1) {
                        $tangent_sphere_no = $tangent_sphere_no + 1;
                    }
                }
                if (($v['gross'] - $v['par']) >= 2) {
                    
                    $gross_double_bogey = $gross_double_bogey + 1;
                    if ($v['push'] == 0 || $v['push'] == 1) {
                        $tangent_sphere_no = $tangent_sphere_no + 1;
                    }
                }
                if ($v['direction'] == DIRECTION_MIDDLE) {
                    $middle = $middle + 1;
                }
                if ($v['direction'] == DIRECTION_LEFT) {
                    $left = $left + 1;
                }
                if ($v['direction'] == DIRECTION_RIGHT) {
                    $right = $right + 1;
                }
            }
            if ($v['par'] == 5) {
                $par5              = $par5 + 1;
                $gross['gross5'][] = $v['gross'];
                if (($v['gross'] - $v['par']) == 0) {
                    
                    $gross_par = $gross_par + 1;
                    if ($v['push'] == 0 || $v['push'] == 1) {
                        $tangent_sphere_ok = $tangent_sphere_ok + 1;
                    }
                }
                if (($v['gross'] - $v['par']) <= -2) {
                    
                    $gross_eagle = $vgross_eagle + 1;
                }
                if (($v['gross'] - $v['par']) == -1) {
                    
                    $gross_birdle = $gross_birdle + 1;
                    if ($v['push'] == 0) {
                        $tangent_sphere_ok = $tangent_sphere_ok + 1;
                    }
                }
                if (($v['gross'] - $v['par']) == 1) {
                    
                    $gross_bogey = $gross_bogey + 1;
                    if ($v['push'] == 0 || $v['push'] == 1) {
                        $tangent_sphere_no = $tangent_sphere_no + 1;
                    }
                }
                if (($v['gross'] - $v['par']) >= 2) {
                    
                    $gross_double_bogey = $gross_double_bogey + 1;
                    if ($v['push'] == 0 || $v['push'] == 1) {
                        $tangent_sphere_no = $tangent_sphere_no + 1;
                    }
                }
                if ($v['direction'] == DIRECTION_MIDDLE) {
                    $middle = $middle + 1;
                }
                if ($v['direction'] == DIRECTION_LEFT) {
                    $left = $left + 1;
                }
                if ($v['direction'] == DIRECTION_RIGHT) {
                    $right = $right + 1;
                }
            }
            if ($v['sandball']) {
                $bunker_num = $bunker_num + 1;
                if ($v['gross'] == $v['par'] || $v['gross'] < $v['par']) {
                    $rescue_ball_ok = $rescue_ball_ok + 1;
                }
            }
            $gross_num[]    = $v['gross'];
            $gameid_num[]   = $v['gameid'];
            $sandball_num[] = $v['sandball'];
            $penalty_num[]  = $v['penalty'];
        }
        $vs                    = array();
        $vs['par4_par5']       = $par4 + $par5;
        $vs['gameidnum']       = count($gameid_num);
        $vs['gameidnum']       = $gameidnum;
        $vs['averaging_gross'] = array_sum($gross_num);
        $vs['par_num']         = array_sum($par_num);
        $vs['par3_num']        = $par3;
        $vs['par4_num']        = $par4;
        $vs['par5_num']        = $par5;
        $vs['par3']            = array_sum($gross['gross3']);
        $vs['par4']            = array_sum($gross['gross4']);
        $vs['par5']            = array_sum($gross['gross5']);
        
        $vs['gross_eagle']        = $gross_eagle;
        $vs['gross_birdle']       = $gross_birdle;
        $vs['gross_par']          = $gross_par;
        $vs['gross_bogey']        = $gross_bogey;
        $vs['gross_double_bogey'] = $gross_double_bogey;
        $vs['hole_num']           = $hole_num;
        
        $vs['tangent_sphere'] = round($tangent_sphere_ok / ($tangent_sphere_ok + $tangent_sphere_no), 2);
        $vs['middle']         = $middle;
        $vs['left']           = $left;
        $vs['right']          = $right;
        $vs['sandball']       = array_sum($sandball_num);
        $vs['penalty']        = array_sum($penalty_num);
        $vs['bunker_num']     = $bunker_num;
        $vs['rescue_ball']    = $rescue_ball_ok;
        
        return $vs;
        
    }
    
    
    
    public function player_kpi($data, $userid, $game_num)
    {
         log_model_call(); 
        $par4_par5 = array();
        
        $par_num            = array();
        $averaging_gross    = array();
        $par3               = array();
        $par4               = array();
        $par5               = array();
        $par3_sum           = array();
        $par4_sum           = array();
        $par5_sum           = array();
        $gross_eagle        = array();
        $gross_birdle       = array();
        $gross_par          = array();
        $gross_bogey        = array();
        $gross_double_bogey = array();
        $middle             = array();
        $left               = array();
        $right              = array();
        $sandball_num       = array();
        $penalty_num        = array();
        $bunker_num         = array();
        $rescue_ball        = array();
        $hole_num           = array();
        foreach ($data as $k => $v) {
            foreach ($v as $k1 => $v1) {
                if ($k1 == 'par4_par5') {
                    $par4_par5[] = $v1;
                }
                if ($k1 == 'par_num') {
                    $par_num[] = $v1;
                }
                if ($k1 == 'averaging_gross') {
                    $averaging_gross[] = $v1;
                }
                if ($k1 == 'par3') {
                    $par3[] = $v1;
                }
                if ($k1 == 'par4') {
                    $par4[] = $v1;
                }
                if ($k1 == 'par5') {
                    $par5[] = $v1;
                }
                if ($k1 == 'par3_num') {
                    $par3_num[] = $v1;
                }
                if ($k1 == 'par4_num') {
                    $par4_num[] = $v1;
                }
                if ($k1 == 'par5_num') {
                    $par5_num[] = $v1;
                }
                if ($k1 == 'gross_eagle') {
                    $gross_eagle[] = $v1;
                }
                if ($k1 == 'gross_birdle') {
                    $gross_birdle[] = $v1;
                }
                if ($k1 == 'gross_par') {
                    $gross_par[] = $v1;
                }
                if ($k1 == 'gross_bogey') {
                    $gross_bogey[] = $v1;
                }
                if ($k1 == 'gross_double_bogey') {
                    $gross_double_bogey[] = $v1;
                }
                if ($k1 == 'middle') {
                    $middle[] = $v1;
                }
                if ($k1 == 'left') {
                    $left[] = $v1;
                }
                if ($k1 == 'right') {
                    $right[] = $v1;
                }
                if ($k1 == 'sandball') {
                    $sandball_num[] = $v1;
                }
                if ($k1 == 'penalty') {
                    $penalty_num[] = $v1;
                }
                if ($k1 == 'bunker_num') {
                    $bunker_num[] = $v1;
                }
                if ($k1 == 'rescue_ball') {
                    $rescue_ball[] = $v1;
                }
                if ($k1 == 'hole_num') {
                    $hole_num[] = $v1;
                }
                if ($k1 == 'tangent_sphere') {
                    $tangent_sphere = $v1;
                }
            }
        }
        
        $vs                    = array();
        $par4_par5             = array_sum($par4_par5);
        $par_num               = array_sum($par_num);
        $bunker_num            = array_sum($bunker_num);
        $vs['gameidnum']       = $game_num;
        $vs['userid']          = $userid;
        $vs['averaging_gross'] = (string) round(array_sum($averaging_gross) / $game_num, 2);
        $vs['par3']            = (string) round(array_sum($par3) / array_sum($par3_num), 2);
        $vs['par4']            = (string) round(array_sum($par4) / array_sum($par4_num), 2);
        $vs['par5']            = (string) round(array_sum($par5) / array_sum($par5_num), 2);
        
        $vs['gross_eagle']        = (string) (round(array_sum($gross_eagle) / array_sum($hole_num), 2) * 100) . '%';
        $vs['gross_birdle']       = (string) (round(array_sum($gross_birdle) / array_sum($hole_num), 2) * 100) . '%';
        $vs['gross_par']          = (string) (round(array_sum($gross_par) / array_sum($hole_num), 2) * 100) . '%';
        $vs['gross_bogey']        = (string) (round(array_sum($gross_bogey) / array_sum($hole_num), 2) * 100) . '%';
        $vs['gross_double_bogey'] = (string) (round(array_sum($gross_double_bogey) / array_sum($hole_num), 2) * 100) . '%';
        
        $vs['middle']         = (string) (round(array_sum($middle) / $par4_par5, 2) * 100) . '%';
        $vs['left']           = (string) (round(array_sum($left) / $par4_par5, 2) * 100) . '%';
        $vs['right']          = (string) (round(array_sum($right) / $par4_par5, 2) * 100) . '%';
        $vs['tangent_sphere'] = (string) ($tangent_sphere * 100) . '%';
        $vs['penalty']        = (string) (round(array_sum($penalty_num) / $game_num, 2));
        $vs['rescue_ball']    = (string) (round(array_sum($rescue_ball) / $bunker_num, 2) * 100) . '%';
        return $vs;
    }
    
    
    //积分换算
    public function experience_conversion_credit($userid, $id)
    {
         log_model_call(); 
        $this->ci->load->database();
        $sql             = "select experience,credit from t_user where id=$userid";
        $user_experience = $this->ci->db->query($sql)->row_array();
        $experience      = $user_experience['experience'];
        $credit          = $user_experience['credit'];
        $user_credit     = $experience * 1.6;
        $user_credit     = round($user_credit);
        if ($user_credit - $credit == 0) {
            return;
        }
        $user_credit = ($user_credit - $credit) + $credit;
        $this->ci->db->where('id', $userid);
        $this->ci->db->update('t_user', array(
            'credit' => $user_credit
        ));
        $this->ci->db->where('id', $id);
        $this->ci->db->update('t_user_event_log', array(
            'credit_added' => $credit
        ));
        $this->user_rank($userid);
        return;
    }
    
    public function computing_par_gancha($one_score)
    {
         log_model_call(); 
        $albatross    = 0; //信天翁
        $eagle        = 0; //老鹰
        $birds        = 0; //小鸟
        $par_par      = 0; //标准杆
        $bogey        = 0; //柏忌
        $double_bogey = 0; //双柏忌
        foreach ($one_score as $value) {
            $gross   = $value['gross'];
            $par     = $value['par'];
            $gan_cha = $gross - $par;
            if ($gross < 1) {
                continue;
            }
            
            if ($gan_cha < -3) {
                $albatross++;
            }
            if ($gan_cha == -2) {
                $eagle++;
            }
            if ($gan_cha == -1) {
                $birds++;
            }
            if ($gan_cha == 0) {
                $par_par++;
            }
            if ($gan_cha == 1) {
                $bogey++;
            }
            if ($gan_cha >= 2) {
                $double_bogey++;
            }
            
        }
        $data = array(
            'albatross' => $albatross,
            'eagle' => $eagle,
            'bird' => $birds,
            'par' => $par_par,
            'bogey' => $bogey,
            'double_bogey' => $double_bogey
        );
        return $data;
    }


    //计算某个人一场比赛中的数据
    public function computing_user_one_game_score($one_game_score)
    {
         log_model_call(); 
        $gross_sum      = 0;
        $push_sum       = 0;
        $gan_cha_sum    = 0;
        $rescue_ball_ok = 0;
        $hole_num       = 0;
        $bunker_num     = 0;
        $par_sum        = 0;
        $hit_fairway    = 0;
        $penalty_num    = 0;
        $On_num         = 0;
        $score_array    = array();
        $user_sandball_num = 0;
        $index          = 0;
        $direction_hole_num = 0;
        foreach ($one_game_score as $one_score) {
            $par = $one_score['par'];
            
            $par_sum                      = $par_sum + $par;
            $gross                        = $one_score['gross'];
            $score_array[$index]['par']   = $par;
            $score_array[$index]['gross'] = $gross;
            $score_array[$index]['holeid'] = $one_score['holeid'];
            $score_array[$index]['courtno'] = $one_score['courtno'];
            $index++;
            if ($gross < 1) {
                continue;
            }
            $biao_par   = $par - 2;
            $push       = $one_score['push'];
            $gross_push = $gross - $push;
            if ($gross_push <= $biao_par) {
                $On_num++;
            }
            $hole_num++;
            $direction   = $one_score['direction'];
            $sandball    = $one_score['sandball'];
            $penalty     = $one_score['penalty'];
            $gan_cha     = $gross - $par;
            $gan_cha_sum = $gan_cha_sum + $gan_cha;
            $gross_sum   = $gross + $gross_sum;
            $push_sum    = $push + $push_sum;
            $penalty_num = $penalty_num + $penalty;
            if($par != 3){
               $direction_hole_num++;
                if ($direction == DIRECTION_MIDDLE) {
                    $hit_fairway++;
                }
            }
            
            if($sandball > 0){
                $user_sandball_num = $user_sandball_num+$sandball;
            }
            
        }
        
        $data                      = $this->computing_par_gancha($one_game_score);
        $data['grosssum']          = $gross_sum;
        $data['pushsum']           = $push_sum;
        $data['gan_cha_sum']       = $gan_cha_sum;
        $data['hole_num']          = $hole_num;
        $data['user_game_penalty'] = $penalty_num;
        if ($hole_num > 0) {
            $On_rate    = $On_num / $hole_num * 100;
            $data['On'] = round($On_rate, 2);
        }
        
        $data['user_game_sandball'] = $user_sandball_num;
        $data['par_sum'] = $par_sum;
        if ($hit_fairway > 0) {
            $fairway             = $hit_fairway / $direction_hole_num * 100;
            $data['hit_fairway'] = round($fairway, 2);
        } else {
            $data['hit_fairway'] = 0;
        }
        $data['score_array'] = $score_array;
        return $data;
    }
    
    //计算某个人一场比赛中的数据
    public function computing_user_one_game_score_log($one_game_score)
    {
         log_model_call(); 
        $gross_sum      = 0;
        $push_sum       = 0;
        $gan_cha_sum    = 0;
        $rescue_ball_ok = 0;
        $hole_num       = 0;
        $bunker_num     = 0;
        $par_sum        = 0;
        $hit_fairway    = 0;
        $penalty_num    = 0;
        $On_num         = 0;
        $score_array    = array();
        $user_sandball_num = 0;
        $index          = 0;
        foreach ($one_game_score as $one_score) {
            $par = $one_score['par'];
            
            $par_sum                      = $par_sum + $par;
            $gross                        = $one_score['gross'];
            $score_array[$index]['par']   = $par;
            $score_array[$index]['gross'] = $gross;
            $index++;
            if ($gross < 1) {
                continue;
            }
            $biao_par   = $par - 2;
            $push       = $one_score['push'];
            $gross_push = $gross - $push;
            if ($gross_push <= $biao_par) {
                $On_num++;
            }
            $hole_num++;
            $direction   = $one_score['direction'];
            $sandball    = $one_score['sandball'];
            $penalty     = $one_score['penalty'];
            $gan_cha     = $gross - $par;
            $gan_cha_sum = $gan_cha_sum + $gan_cha;
            $gross_sum   = $gross + $gross_sum;
            $push_sum    = $push + $push_sum;
            $penalty_num = $penalty_num + $penalty;
            if ($direction == DIRECTION_MIDDLE) {
                $hit_fairway++;
            }

            if($sandball > 0){
                $user_sandball_num = $user_sandball_num+$sandball;
            }
            
        }
        
        $data                      = $this->computing_par_gancha($one_game_score);
        $data['grosssum']          = $gross_sum;
        $data['pushsum']           = $push_sum;
        $data['gan_cha_sum']       = $gan_cha_sum;
        $data['hole_num']          = $hole_num;
        $data['user_game_penalty'] = $penalty_num;
        if ($hole_num > 0) {
            $On_rate    = $On_num / $hole_num * 100;
            $data['On'] = round($On_rate, 2);
        }
        
        $data['user_game_sandball'] = $user_sandball_num;
        $data['par_sum'] = $par_sum;
        if ($hit_fairway > 0) {
            $fairway             = $hit_fairway / $hole_num * 100;
            $data['hit_fairway'] = round($fairway, 2);
        } else {
            $data['hit_fairway'] = 0;
        }
        $data['score_array'] = $score_array;
        return $data;
    }
    
    
    
    public function one_game_all_user_score($gameid)
    {
         log_model_call(); 
        $users_data = array();
        $this->ci->load->database();
        $sql              = "select userid from t_game_group_user where gameid=$gameid";
        $game_group_users = $this->ci->db->query($sql)->result_array();
        foreach ($game_group_users as $one_user) {
            $userid              = $one_user['userid'];
            $sql                 = "select * from t_game_score where gameid=$gameid and userid=$userid";
            $one_user_game_score = $this->ci->db->query($sql)->result_array();
            $users_data[]        = $this->computing_user_one_game_score($one_user_game_score);
        }
        
        return $users_data;
    }
    
    public function sorting($data, $sorting_rule, $field)
    {
         log_model_call(); 
        $sort = array(
            'direction' => $sorting_rule, //排序顺序标志 SORT_DESC 降序；SORT_ASC 升序  
            'field' => $field //排序字段  
        );
        
        $arrSort = array();
        foreach ($data AS $uniqid => $row) {
            foreach ($row AS $key => $value) {
                $arrSort[$key][$uniqid] = $value;
            }
        }
        if ($sort['direction']) {
            array_multisort($arrSort[$sort['field']], constant($sort['direction']), $data);
        }
        return $data;
    }
    

    public function math_user_one_game_handicap($par_array,$gross_array){
        $par_sum = array_sum($par_array);
        $gross_sum = array_sum($gross_array);
        $handicap = ($gross_sum*1.5-$par_sum)*0.8;
        return $handicap;
    }
    
}

?>
