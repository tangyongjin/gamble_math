<?php

if (!defined('BASEPATH'))
exit('No direct script access allowed');

/**
 * 	XPlayer
 *
 * @author		Nick Louis
 * @copyright	Copyright (c) 2013 Nick Louis
 * @since		Version 1.0
 *
 */
class XPlayers {

	private $ci = null;

	public function __construct() {
		$this->ci = & get_instance();
	}

	/* ------------------------------- user query ------------------------- */

	public function getUser($id) {
		$vs = array();
		if ($id < 1) {
			return $vs;
		}
		$this->ci->load->database();
		$query = $this->ci->db->get_where('user', array('id' => $id));
		$vs = $query->row_array();
		return $vs;
	}

	public function getUserByName($name) {
		$this->ci->load->database();

		$query = $this->ci->db->get_where('user', array('username' => $name));
		return $query->result_array($query);
	}

	public function getUserByEmail($email) {
		$this->ci->load->database();

		$query = $this->ci->db->get_where('user', array('email' => $email));
		return $query->result_array($query);
	}

	public function getUserByMobile($mobile) {
		$this->ci->load->database();

		$query = $this->ci->db->get_where('user', array('mobile' => $mobile));
		return $query->result_array($query);
	}

	public function getUsers($type = -1, $status = -1, $pageidx = 1, $pagesize = 15, $orderby = null) {
		$where = array();
		$start = 0;
		$end = 15;
		if ($type >= 0) {
			$where['type'] = $type;
		}
		if ($status >= 0) {
			$where['status'] = $status;
		}
		if ($pagesize < 1) {
			$pagesize = 15;
		}
		if ($pageidx <= 0) {
			$pageidx = 1;
			$start = 0;
			$end = $pagesize;
		}
		$start = ($pageidx - 1) * $pagesize;

		$this->ci->load->database();
		if (count($where) > 0) {
			$this->ci->db->where($where);
		}
		$count = $this->ci->db->from('user')->count_all_results();
		if( $count < 1 ){
			return array('pageidx' => $pageidx, 'pagesize' => $pagesize, 'count' => $count, 'data' => array());
		}

		if (count($where) > 0) {
			$this->ci->db->where($where);
		}
		if ( $orderby ) {
			if(is_array($orderby) && count($orderby) > 0){
				foreach ($orderby as $k => $order) {
					$this->ci->db->order_by($order);
				}
			}else if( strlen($orderby)>0 ){
				$this->ci->db->order_by($orderby);
			}
		}
		$query = $this->ci->db->from('user')->limit($pagesize, $start)->get();
		$vs = $query->result_array();
		//dump($this->ci->db->last_query());
		return array('pageidx' => $pageidx, 'pagesize' => $pagesize, 'count' => $count, 'data' => $vs);
	}

	public function getUserCount($type = -1, $status = -1) {
		$where = array();
		if (is_array($type) && count($type) > 0) {
			foreach ($type as $k => $v) {
				$where[$k] = $v;
			}
		} else if ($type >= 0) {
			$where['type'] = $type;
		}
		if (is_array($status) && count($status) > 0) {
			foreach ($status as $k => $v) {
				$where[$k] = $v;
			}
		} else if ($status >= 0) {
			$where['status'] = $status;
		}
		$this->ci->load->database();
		if (count($where) > 0) {
			$this->ci->db->where($where);
		}
		$count = $this->ci->db->from('user')->count_all_results();
		//dump($this->ci->db->last_query());
		return $count;
	}

	public function getUserInfo($userid) {
		$vs = array();
		if ($userid < 1) {
			return $vs;
		}
		$this->ci->load->database();

		$query = $this->ci->db->get_where('user_info', array('userid' => $userid));
		//dump($this->ci->db->last_query());
		$vs = $query->row_array();

		return $vs;
	}

	public function searchUsers($key = '', $ktype = 0, $type = -1, $status = -1, $pageidx = 1, $pagesize = 15, $orderby = null) {
		$where = array();
		$start = 0;
		$end = 15;
		if ($type >= 0) {
			$where['type'] = $type;
		}
		if ($status >= 0) {
			$where['status'] = $status;
		}
		if ($pagesize < 1) {
			$pagesize = 15;
		}
		if ($pageidx <= 0) {
			$pageidx = 1;
			$start = 0;
			$end = $pagesize;
		}
		$start = ($pageidx - 1) * $pagesize;

		$this->ci->load->database();
		// count
		if (count($where) > 0) {
			$this->ci->db->where($where);
		}
		if (strlen($key) > 0) {
			if (0 == $ktype) {
				$this->ci->db->like('nickname', $key);
			} else if (1 == $ktype) {
				$this->ci->db->like('username', $key);
			} else if (2 == $ktype) {
				$this->ci->db->like('email', $key);
			} else if (3 == $ktype){
				$this->ci->db->like('mobile', $key);
			}
		}
		$count = $this->ci->db->from('user')->count_all_results();
		if( $count < 1 ){
			return array('pageidx' => $pageidx, 'pagesize' => $pagesize, 'count' => $count, 'data' => array());
		}

		// list
		if (count($where) > 0) {
			$this->ci->db->where($where);
		}
		if (strlen($key) > 0) {
			if (0 == $ktype) {
				$this->ci->db->like('nickname', $key);
			} else if (1 == $ktype) {
				$this->ci->db->like('username', $key);
			} else if (2 == $ktype) {
				$this->ci->db->like('email', $key);
			}else if (3 == $ktype){
				$this->ci->db->like('mobile', $key);
			}
		}
		if ( $orderby ) {
			if(is_array($orderby) && count($orderby) > 0){
				foreach ($orderby as $k => $order) {
					$this->ci->db->order_by($order);
				}
			}else if( strlen($orderby)>0 ){
				$this->ci->db->order_by($orderby);
			}
		}
		$query = $this->ci->db->from('user')->limit($pagesize, $start)->get();
		$vs = $query->result_array();	//test: dump($this->ci->db->last_query());

		return array('pageidx' => $pageidx, 'pagesize' => $pagesize , 'count' => $count, 'data' => $vs);
	}

	/* ------------------------------- users ------------------------------- */

	// save user's request code
	public function saveRequestCode($mobile, $email, $code, $ssid='', $befrom='register', $post=null){
		// return values
		$ret = array();
		$ret['status'] = -1;
		$ret['descr'] = '保存失败';

		if( (strlen($mobile)<1 && strlen($email)<1) || strlen($code)<1  ){
			$ret['descr'] = '参数错误';
			return $ret;
		}

		// parameters
		$tm = time();
		$ip = $this->ci->input->ip_address();

		$v = array();
		$v['mobile'] = $mobile;
		$v['email'] = $email;
		$v['code'] = $code;
		$v['ssid'] = $ssid;
		$v['befrom'] = $befrom;
		$v['addtime'] = $tm;
		$v['addip'] = $ip;

		$snames = array('longitude', 'latitude', );
		$inames = array('userid', 'status', 'clientid', );
		foreach( $snames as $name ){
			if( isset($post[$name]) ){
				$v[$name] = $post[$name];
			}
		}
		foreach( $inames as $name ){
			if( isset($post[$name]) ){
				$v[$name] = intval($post[$name]);
			}
		}
		if( isset($v['longitude'])){
			$v['longitude'] = floatval($v['longitude']);
			$v['latitude'] = floatval($v['latitude']);
		}

		// to db
		$this->ci->load->database();
		$this->ci->db->insert('user_code', $v);
		$retid = $this->ci->db->insert_id();
		if($retid>0){
			$ret['id'] = $retid;
			$ret['code'] = $code;
			$ret['status'] = 1;
			$ret['descr'] = 'success';
		}
		return $ret;
	}

	// save user's request register code
	public function registerCode($mobile, $code, $sid='', $post=null){
		// return values
		$ret = array();
		$ret['status'] = -1;
		$ret['descr'] = '保存失败';

		if( null == $mobile || strlen($mobile)<1 || null == $code){
			return $ret;
		}

		// parameters
		$tm = time();
		$ip = $this->ci->input->ip_address();

		$v = array();
		$v['sid'] = $sid;
		$v['mobile'] = $mobile;
		$v['code'] = $code;
		$v['addtime'] = $tm;
		$v['addip'] = $ip;

		$snames = array('longitude','latitude');
		$inames = array('status');
		foreach( $snames as $name ){
			if( isset($post[$name]) ){
				$v[$name] = $post[$name];
			}
		}
		foreach( $inames as $name ){
			if( isset($post[$name]) ){
				$v[$name] = intval($post[$name]);
			}
		}
		if( isset($v['longitude'])){
			$v['longitude'] = floatval($v['longitude']);
			$v['latitude'] = floatval($v['latitude']);
		}

		// to db
		$this->ci->load->database();
		$this->ci->db->insert('user_regcode', $v);
		$retid = $this->ci->db->insert_id();
		if($retid>0){
			$ret['id'] = $retid;
			$ret['code'] = $code;
			$ret['status'] = 1;
			$ret['descr'] = 'success';
		}
		return $ret;
	}

	// user register
	public function registerByEmail($email, $password, $nickname, $_datas = null) {
		// return values
		$ret = array();
		$ret['status'] = -1;
		$ret['descr'] = '注册失败';

		// parameters
		$email = trim($email);
		$password = trim($password);
		$nickname = trim($nickname);

		// format checks
		if (empty($nickname) || empty($password) || empty($email)) {
			$ret['descr'] = '用户名/密码/邮箱为空';
			return $ret;
		}
		if (!$nickname || $nickname != addslashes($nickname) || !($this->check_username($nickname))) {
			$code = 11;
			$ret['descr'] = $this->getRegError($code);
			return $ret;
		}
		if (!$password || $password != addslashes($password)) {
			$code = 4;
			$ret['descr'] = $this->getRegError($code);
			return $ret;
		}
		if (!$email || !($this->check_email($email))) {
			$code = 14;
			$ret['descr'] = $this->getRegError($code);
			return $ret;
		}

		// logical check
		$us = $this->getUserByEmail($email);
		if (count($us) > 0) {
			$code = 16;
			$ret['descr'] = $this->getRegError($code);
			return $ret;
		}

		// format data
		$v = array();
		$v['nickname'] = $nickname;
		$v['email'] = $email;
		$v['password'] = $password;

		return $this->userRegister($v,$_datas);
	}

	// user register
	public function registerByMobile($mobile, $password, $nickname, $_datas = null) {
		// return values
		$ret = array();
		$ret['status'] = -1;
		$ret['descr'] = '注册失败';

		// parameters
		$mobile = trim($mobile);
		$password = trim($password);
		$nickname = trim($nickname);

		// format checks
		if (empty($nickname) || empty($password) || empty($mobile)) {
			$ret['descr'] = '用户名/密码/手机号为空';
			return $ret;
		}
		if (!$nickname || $nickname != addslashes($nickname) || !($this->check_username($nickname))) {
			$code = 11;
			$ret['descr'] = $this->getRegError($code);
			return $ret;
		}
		if (!$password || $password != addslashes($password)) {
			$code = 4;
			$ret['descr'] = $this->getRegError($code);
			return $ret;
		}
		if (!$mobile || !($this->check_mobile($mobile)) ) {
			$ret['descr'] = "手机号有误";
			return $ret;
		}

		// logical check
		$us = $this->getUserByMobile($mobile);
		if (count($us) > 0) {
			$ret['descr'] = "手机号已被注册";
			return $ret;
		}
		$mobile = preg_replace( array("/^[0\+]86/","/[ \-+()]/si"), "",$mobile);

		// format data
		$v = array();
		$v['nickname'] = $nickname;
		$v['mobile'] = $mobile;
		$v['password'] = $password;

		return $this->userRegister($v,$_datas);
	}

	public function userRegister($user, $datas = null, $type=1, $status=1){
		// return values
		$ret = array();
		$ret['status'] = -1;
		$ret['descr'] = '注册失败';

		if( null == $user || !is_array($user) || count($user)< 1){
			return $ret;
		}

		// parameters
		$tm = time();
		$ip = $this->ci->input->ip_address();

		// password
		$user['salt'] = rand(100000000, 900000000);
		$user['password'] = md5( md5(trim($user['password'])) . $user['salt']);

		// constants
		$user['type'] = $type;
		$user['status'] = $status;
		$user['logincount'] = 1;
		$user['addtime'] = $user['lasttime'] = $tm;
		$user['addip'] = $user['lastip'] = $ip;

		// extra fields
		if( null != $datas && is_array($datas)){
			$user = array_merge($user,$datas);
		}

		// to db
		$this->ci->load->database();
		$this->ci->db->insert('user', $user);
		$userid = $this->ci->db->insert_id();
		if ($userid < 1) {
			$code = 2;
			$ret['descr'] = $this->getRegError($code);
			return $ret;
		}
		$user['id'] = $userid;

		// user infos
		$v = array();
		$v['userid'] = $userid;
		$v['email'] = $user['email'];
		$v['mobile'] = $user['mobile'];
		$v['type'] = $user['type'];
		$v['status'] = $user['status'];
		$v['lasttime'] = $tm;
		$v['lastip'] = $ip;
		$this->ci->db->insert('user_info', $v);

		// >>> todo
		// follows someone ??
		// set rank, credit, experience
		// return values
		$ret['status'] = 1;
		$ret['descr'] = $this->getRegError(0);
		$ret['_v'] = $user;
		return $ret;
	}

	public function userRegisterTmp($uid, $nickname, $mobile="", $type=2){
		// return values
		$ret = array();
		$ret['status'] = -1;
		$ret['descr'] = '注册失败';

		// parameters
		$tm = time();
		$ip = $this->ci->input->ip_address();

		if($uid < 1){
			return $ret;
		}
		if (!$nickname || $nickname != addslashes($nickname) || !($this->check_username($nickname))) {
			$code = 11;
			$ret['descr'] = $this->getRegError($code);
			return $ret;
		}

		//$u = $this->getUser($uid);
		$this->ci->load->database();
		$sql = "select * from t_c_user where id=$uid";
		$u = $this->ci->db->query($sql)->row_array();
		if(count($u)<1 || $uid < 1){
			$ret['descr'] = '用户不存在';
			return $ret;
		}

		if(strlen($mobile)>0){
			$mobile = preg_replace(array("/^[0\+]86/","/[ \-+()]/"), "",$mobile);
		}
		$user = array();
		$user['nickname'] = $nickname;
		$user['email'] = "";
		$user['mobile'] = $mobile;
		$randpwd = "111111"; //"".rand(100000, 1000000);
		$user['password'] = $randpwd;

		// password
		$user['salt'] = $randpwd;       // rand(100000000, 900000000);
		$user['password'] = md5( md5(trim($user['password'])) . $user['salt'] );

		// constants
		$user['type'] = $type;
		$user['status'] = 1;
		$user['logincount'] = 0;
		$user['addtime'] = $user['lasttime'] = $tm;
		$user['addip'] = $user['lastip'] = $ip;

		// default avatar
		$user['coverpath'] = 'user';
		$user['covername'] = 'holder_user_cover.png';

		if($this->check_mobile($mobile)){
			$us = $this->getUserByMobile($mobile);

			// already registered?
			if ( $us && count($us) > 0) {
				$us = $us[0];
				$ret['status'] = 0;
				$ret['descr'] = "success";
				$ret['id'] = $us['id'];
				$ret['_v'] = $us;

				$userid = $us['id'];
				$this->followUser($uid, $userid, $nickname, "", 1 , 2);
				$this->followUser($userid, $uid, "", "", 1 , 2);

				if($us['logincount'] < 1){
					$ret['pwd'] = $us['salt'];
					$ret['status'] = 2;
				}
				return $ret;
			}else{
				$user['type'] = 1;
			}
		} // if mobile

		// to db
		$this->ci->load->database();
		$this->ci->db->insert('user', $user);
		$userid = $this->ci->db->insert_id();
		if ($userid < 1) {
			$code = 2;
			$ret['descr'] = $this->getRegError($code);
			return $ret;
		}

		// user infos
		$v = array();
		$v['userid'] = $userid;
		$v['email'] = $user['email'];
		$v['mobile'] = $user['mobile'];
		$v['type'] = $user['type'];
		$v['status'] = $user['status'];
		$v['lasttime'] = $tm;
		$v['lastip'] = $ip;
		$this->ci->db->insert('user_info', $v);

		// >>> todo
		// follows someone ??
		// set rank, credit, experience
		// return values
		$this->followUser($uid, $userid, $nickname, "", 1 , 2);
		$this->followUser($userid, $uid, "", "", 1 , 2);

		$ret['status'] = 1;
		$ret['descr'] = "注册成功";

		$user['id'] = $userid;
		$ret['_v'] = $user;
		$ret['id'] = $userid;
		$ret['pwd'] = $randpwd;
		return $ret;
	}

	// user login
	public function userLogin($name, $password) {
		// return values
		$ret = array();
		$ret['status'] = -1;
		$ret['descr'] = "登录失败";

		// parameters
		$name = trim($name);
		$password = trim($password);

		$tm = time();
		$ip = $this->ci->input->ip_address();

		// format checks
		if (empty($name) || empty($password) || $name != addslashes($name) || $password != addslashes($password)) {
			$ret['descr'] = "非法输入";
			return $ret;
		}

		// query user
		$us = null;
		if ($this->check_email($name)) {
			$us = $this->getUserByEmail($name);
		} else if ($this->check_mobile($name)) {
			$us = $this->getUserByMobile($name);
		} else {
			$us = $this->getUserByName($name);
		}

		// not exists
		if ($us == null || count($us) < 1) {
			$ret['descr'] = "用户不存在";
			return $ret;
		}

		// wrong name && pwd
		foreach ($us as $v) {
			$pwd =  md5( md5(trim($password)) . $v['salt']);
			if ($v['password'] == $pwd && $v['status'] > 0) {
				if($v['type'] == 2){
					$this->ci->db->set('type', 1);
				}
				$this->ci->db->set('logincount', 'logincount+1', FALSE);
				$this->ci->db->set('lasttime', $tm);
				$this->ci->db->set('lastip', $ip);
				$this->ci->db->where(array('id' => $v['id']))->update('user');

				$ret['status'] = 1;
				$ret['descr'] = "登录成功";
				$ret['_v'] = $v;
				return $ret;
			}
		}
		$ret['descr'] = '账号密码不匹配';
		return $ret;
	}

	public function findPasswordByEmail($email, $password){
		// return values
		$ret = array();
		$ret['status'] = -1;
		$ret['descr'] = '保存失败';

		// parameters
		$email = trim($email);
		$password = trim($password);

		// check parameters
		if ( strlen($password)< 3 || strlen($email)<3 || $password != addslashes($password) || !($this->check_email($email)) ) {
			$ret['descr'] = '参数错误';
			return $ret;
		}

		$v = array();
		$v['salt'] = rand(100000000, 900000000);
		$v['password'] = md5(md5(trim($password)) . $v['salt']);

		$this->ci->load->database();
		$this->ci->db->where(array('email' => $email))->update('user', $v);
		$retid = $this->ci->db->affected_rows();
		if ($retid >= 0) {
			$ret['status'] = 1;
			$ret['descr'] = '保存成功';
		}
		return $ret;
	}

	public function findPasswordByMobile($mobile, $password){
		// return values
		$ret = array();
		$ret['status'] = -1;
		$ret['descr'] = '保存失败';

		// parameters
		$mobile = trim($mobile);
		$password = trim($password);

		// check parameters
		if ( strlen($password)< 3 || strlen($mobile)<3 || $password != addslashes($password) || !($this->check_mobile($mobile)) ) {
			$ret['descr'] = '参数错误';
			return $ret;
		}

		$v = array();
		$v['salt'] = rand(100000000, 900000000);
		$v['password'] = md5(md5(trim($password)) . $v['salt']);

		$this->ci->load->database();
		$this->ci->db->where(array('mobile' => $mobile))->update('user', $v);
		$retid = $this->ci->db->affected_rows();
		if ($retid >= 0) {
			$ret['status'] = 1;
			$ret['descr'] = '保存成功';
		}
		return $ret;
	}

	// password
	public function resetPassword($userid, $oldpassword, $newpassword) {
		// return values
		$ret = array();
		$ret['id'] = 0;
		$ret['name'] = '';
		$ret['status'] = -1;
		$ret['descr'] = '保存失败';

		// parameters
		$userid = intval($userid);
		$oldpassword = trim($oldpassword);
		$newpassword = trim($newpassword);

		// check parameters
		if ($userid < 1 || !$oldpassword || $oldpassword != addslashes($oldpassword) || !$newpassword || $newpassword != addslashes($newpassword)) {
			$ret['descr'] = '参数错误';
			return $ret;
		}

		// logical check
		$user = $this->getUser($userid);
		if (!$user || count($user) < 1) { // not exist
			$ret['descr'] = "用户不存在";
			return $ret;
		}

		$pwd =  md5( md5(trim($oldpassword)) . $user['salt']);
		if ($pwd != $user['password']) {
			$ret['descr'] = "旧密码不正确";
			return $ret;
		}

		$v = array();
		$v['salt'] = rand(100000000, 900000000);
		$v['password'] = md5(md5(trim($newpassword)) . $v['salt']);

		$this->ci->load->database();
		$this->ci->db->where(array('id' => $userid))->update('user', $v);
		$retid = $this->ci->db->affected_rows();
		if ($retid > 0) {
			$ret['status'] = 1;
			$ret['descr'] = '保存成功';
		}
		$ret['id'] = $userid;
		return $ret;
	}

	public function resetAvatar($userid, $coverpath, $covername) {
		// return values
		$ret = array();
		$ret['status'] = -1;
		$ret['descr'] = '保存失败';

		// parameters
		$userid = intval($userid);
		$coverpath = trim($coverpath);
		$covername = trim($covername);

		// check parameters
		if ($userid < 1 || !$covername || strlen($covername) < 1) {
			$ret['descr'] = '参数错误';
			return $ret;
		}

		$v = array();
		$v['coverpath'] = $coverpath;
		$v['covername'] = $covername;

		$this->ci->load->database();
		$this->ci->db->where(array('id' => $userid))->update('user', $v);
		$retid = $this->ci->db->affected_rows();
		if ($retid > 0) {
			$ret['status'] = 1;
			$ret['descr'] = '保存成功';
		}
		$ret['id'] = $userid;
		return $ret;
	}

	public function resetCover($userid, $avatarpath, $avatarname) {
		// return values
		$ret = array();
		$ret['id'] = 0;
		$ret['name'] = '';
		$ret['status'] = -1;
		$ret['descr'] = '保存失败';

		// parameters
		$userid = intval($userid);
		$avatarpath = trim($avatarpath);
		$avatarname = trim($avatarname);

		// check parameters
		if ($userid < 1 || !$avatarname || strlen($avatarname) < 1) {
			$ret['descr'] = '参数错误';
			return $ret;
		}

		$v = array();
		$v['coverpath'] = $avatarpath;
		$v['covername'] = $avatarname;

		$this->ci->load->database();
		$this->ci->db->where(array('userid' => $userid))->update('user_info', $v);
		$retid = $this->ci->db->affected_rows();
		if ($retid > 0) {
			$ret['status'] = 1;
			$ret['descr'] = '保存成功';
		}
		$ret['id'] = $userid;
		return $ret;
	}

	public function saveUserInfo($userid, $post) {
		// return values
		$ret = array();
		$ret['id'] = 0;
		$ret['name'] = '';
		$ret['status'] = -1;
		$ret['descr'] = '保存失败';

		// parameters
		$userid = intval($userid);

		// check parameters
		if ($userid < 1 || !$post || count($post) < 1) {
			$ret['descr'] = '参数错误';
			return $ret;
		}

		$this->ci->load->database();
		$tm = time();
		$ip = $this->ci->input->ip_address();
		$rc = 0;

		// update user
		$v = array();
		$snames = array('mobile','nickname','coverpath','covername', );
		$inames = array('type','status','clientid', );
		foreach( $snames as $name ){
			if( isset($post[$name]) ){
				$v[$name] = $post[$name];
			}
		}
		foreach( $inames as $name ){
			if( isset($post[$name]) ){
				$v[$name] = $post[$name];
			}
		}
		if(count($v)>0){
			$v['lasttime'] = $tm;
			$v['lastip'] = $ip;
			$this->ci->db->where(array('id' => $userid))->update('user', $v);
			$rc = $this->ci->db->affected_rows();
			if($rc<1){
				$ret['descr'] = '操作错误';
				return $ret;
			}
		}

		// update user info
		$v = array();
		$snames = array('sign','favorite','birthday','remark','industry','degree','folk','blood','star','zodiac','physique','area');
		$inames = array('gender','residecity','height','weight');
		$ps = array();
		foreach( $snames as $name ){
			if( isset($post[$name]) ){
				$v[$name] = $post[$name];
			}
		}
		foreach( $inames as $name ){
			if( isset($post[$name]) ){
				$v[$name] = $post[$name];
			}
		}

		if (isset($post['birthday'])) {
			$v['birthday'] = strtotime($post['birthday']);
		}
		if ($post['area'] && strlen(trim($post['area'])) > 0) {
			$as = split(",", $post['area']);
			if (count($as) > 0) {
				$v['resideprovince'] = trim($as[0]);
			}
			if (count($as) > 1) {
				$v['residecity'] = trim($as[1]);
			}
		}
		if (count($v)>0) {
			$v['lasttime'] = $tm;
			$v['lastip'] = $ip;
			$this->ci->db->where(array('userid' => $userid))->update('user_info', $v);
			$rc += $this->ci->db->affected_rows();
		}

		$ret['id'] = $userid;
		$ret['status'] = $rc;
		$ret['descr'] = '保存成功';
		return $ret;
	}

	/* ------------------------------- app users ------------------------------------------- */

	public function getThirdAccounts($userid, $appname, $status = -1) {
		$vs = array();
		$where = array();
		if ($userid >= 0) {
			$where['userid'] = $userid;
		}
		if ($appname && strlen($appname) > 0) {
			$where['appname'] = $appname;
		}
		if ($status >= 0) {
			$where['status'] = $status;
		}

		if (count($where) < 1) {
			return $vs;
		}

		$this->ci->load->database();
		$query = $this->ci->db->order_by('id asc')->get_where('app_user', $where);
		$vs = $query->result_array();
		//dump($this->ci->db->last_query());
		return $vs;
	}

	public function getThirdAccountToken($appname, $id, $userid = -1, $openid = null, $status = -1) {
		$vs = array();
		$where = array();
		if ($id >= 0) {
			$where['id'] = $id;
		}
		if ($userid >= 0) {
			$where['userid'] = $userid;
		}
		if ($openid && strlen($openid) > 0) {
			$where['openid'] = $openid;
		}
		if ($status >= 0) {
			$where['status'] = $status;
		}

		if (!$appname || strlen($appname) < 1 || count($where) < 1) {
			return $vs;
		}

		$this->ci->load->database();
		$query = $this->ci->db->get_where('app_' . $appname, $where);
		$vs = $query->result_array();
		//dump($this->ci->db->last_query());
		return $vs;
	}

	public function saveThirdAccountToken($appname, $id, $openid, $post = null) {
		// return values
		$ret = array();
		$ret['id'] = 0;
		$ret['name'] = '';
		$ret['status'] = -1;
		$ret['descr'] = '保存失败';

		// parameters
		$appname = trim($appname);
		$openid = trim($openid);

		// check parameters
		if (!$appname || strlen($appname) < 1 || !$openid || strlen($openid) < 1) {
			$ret['descr'] = '参数错误';
			return $ret;
		}

		$v = array();
		$names = array('userid', 'token', 'secret', 'tokentype', 'scope', 'nickname', 'rtime', 'datafield', 'clientid', 'status');
		if( null != $post && count($post)>0){
			foreach ($names as $name) {
				if (isset($post[$name])) {
					$v[$name] = $post[$name];
				}
			}
		}
		if (count($v) < 1) {
			$ret['status'] = 0;
			$ret['descr'] = '参数错误';
			return $ret;
		}

		$this->ci->load->database();
		$tm = time();
		$ip = $this->ci->input->ip_address();
		$v['openid'] = $openid;
		if ($id < 1) {  		// add
			$v['userid'] = 0;
			$v['addtime'] = $v['lasttime'] = $tm;
			$v['addip'] = $v['lastip'] = $ip;

			$this->ci->db->insert('app_' . $appname, $v);
			$retid = $this->ci->db->insert_id();
		} else {
			$v['lasttime'] = $tm;
			$v['lastip'] = $ip;

			$this->ci->db->where(array('id' => $id))->update('app_' . $appname, $v);
			$retid = $this->ci->db->affected_rows();
			if ($retid > 0) {
				$retid = $id;
			}
		}
		if ($retid > 0) {
			$ret['status'] = 1;
		} else {
			$ret['status'] = 0;
		}
		$ret['id'] = $retid;
		$ret['descr'] = '保存成功';
		return $ret;
	}

	public function userAppLogin($appname, $userid) {
		$this->ci->load->database();
		// return values
		$ret = array();
		$ret['id'] = 0;
		$ret['name'] = '';
		$ret['status'] = -1;
		$ret['descr'] = "登录失败";

		// parameters
		$appname = trim($appname);
		$userid = intval($userid);

		// format checks
		if (strlen($appname) < 1 || $userid < 1) {
			$ret['descr'] = "非法输入";
			return $ret;
		}

		$v = $this->getUser($userid);
		// not exists
		if ($v == null || count($v) < 1) {
			$ret['descr'] = "用户不存在";
			return $ret;
		}
		$this->ci->db->set('logincount', 'logincount+1', FALSE);
		$this->ci->db->set('lasttime', time());
		$this->ci->db->set('lastip', $this->ci->input->ip_address());
		$this->ci->db->where(array('id' => $v['id']))->update('user');

		$ret['id'] = $v['id'];
		$ret['name'] = $v['username'];
		$ret['status'] = 1;
		$ret['descr'] = "登录成功";
		$ret['_v'] = $v;
		return $ret;
	}

	public function userAppBindLogin($name, $password, $appname, $appid, $nickname = "") {
		$this->ci->load->database();
		// return values
		$ret = array();
		$ret['id'] = 0;
		$ret['name'] = '';
		$ret['status'] = -1;
		$ret['descr'] = "保存失败";

		// parameters
		$appname = trim($appname);
		$appid = intval($appid);

		// format checks
		if (empty($appname) || $appid < 1) {
			$ret['descr'] = "非法输入";
			return $ret;
		}
		// login
		$r = $this->userLogin($name, $password);
		if ($r['status'] < 1) {
			$ret['descr'] = $r['descr'];
			return $ret;
		}

		$tm = time();
		$ip = $this->ci->input->ip_address();
		$this->ci->db->where(array('id' => $appid))->update('app_' . $appname, array('userid' => $r['id']));
		$retid = $this->ci->db->affected_rows();
		if ($retid > 0) {
			$v = array();
			$v['userid'] = $r['id'];
			$v['appid'] = $appid;
			$v['appname'] = $appname;
			$v['nickname'] = $nickname;
			$v['addtime'] = $v['lasttime'] = $tm;
			$v['addip'] = $v['lastip'] = $ip;

			$this->ci->db->insert('app_user', $v);
			$retid = $this->ci->db->insert_id();
			if ($retid > 0) {
				$ret['id'] = $retid;
				$ret['status'] = 1;
				$ret['_v'] = $r['_v'];
				$ret['descr'] = '保存成功';
			}
		}
		return $ret;
	}

	public function userAppBindNew($name, $password, $email, $appname, $appid, $nickname = "") {
		$this->ci->load->database();
		// return values
		$ret = array();
		$ret['id'] = 0;
		$ret['name'] = '';
		$ret['status'] = -1;
		$ret['descr'] = "保存失败";

		// parameters
		$appname = trim($appname);
		$appid = intval($appid);

		// format checks
		if (empty($appname) || $appid < 1) {
			$ret['descr'] = "非法输入";
			return $ret;
		}
		// login
		$r = $this->userRegister($name, $password, $email);
		if ($r['status'] < 1) {
			$ret['descr'] = $r['descr'];
			return $ret;
		}

		$tm = time();
		$ip = $this->ci->input->ip_address();
		$this->ci->db->where(array('id' => $appid))->update('app_' . $appname, array('userid' => $r['id']));
		$retid = $this->ci->db->affected_rows();
		if ($retid > 0) {
			$v = array();
			$v['userid'] = $r['id'];
			$v['appid'] = $appid;
			$v['appname'] = $appname;
			$v['nickname'] = $nickname;
			$v['addtime'] = $v['lasttime'] = $tm;
			$v['addip'] = $v['lastip'] = $ip;

			$this->ci->db->insert('app_user', $v);
			$retid = $this->ci->db->insert_id();
			if ($retid > 0) {
				$ret['id'] = $retid;
				$ret['status'] = 1;
				$ret['_v'] = $r['_v'];
				$ret['descr'] = '保存成功';
			}
		}
		return $ret;
	}

	/* ------------------------------- friendship ------------------------------------- */

	public function followUser($userid, $fuserid, $nickname = null, $remark = null, $status = 0, $type=0) {
		// return values
		$ret = array();
		$ret['status'] = -1;
		$ret['descr'] = '保存失败';

		// parameters
		$userid = intval($userid);
		$fuserid = intval($fuserid);
		$tm = time();
		$ip = $this->ci->input->ip_address();

		// check parameters
		if ($userid < 1 || $fuserid < 1 || $userid == $fuserid) {
			$ret['descr'] = '参数错误';
			return $ret;
		}

		$v = array();
		$v['userid'] = $userid;
		$v['fuserid'] = $fuserid;
		$v['nickname'] = $nickname;
		$v['remark'] = $remark;
		$v['status'] = $status;
		$v['type'] = $type;
		$v['addtime'] = $v['lasttime'] = $tm;
		$v['addip'] = $v['lastip'] = $ip;

		// check if added sometimes ago?
		$cnt = $this->getFriendsCount($fuserid, $userid, -1, -1);
		if($cnt > 0){               // is friend now
			$v['status'] = 1;
			$ret['status'] = 2;
		}

		$this->ci->load->database();
		$this->ci->db->insert('friend', $v);
		$retid = $this->ci->db->insert_id();
		if ($retid > 0) {               // add
			$ret['id'] = $retid;
			$ret['status'] = 1;

		}else{                          // update
			$ret['status'] = 0;

			$ps = array();
			$ps['nickname'] = $nickname;
			$ps['remark'] = $remark;
			$ps['status'] = $v['status'];
			$ps['type'] = $type;
			$ps['addtime'] = $ps['lasttime'] = $tm;
			$ps['addip'] = $ps['lastip'] = $ip;
			$retid = $this->ci->db->where(array('userid' => $userid, 'fuserid' => $fuserid))->update('friend', $ps);
		}
		if($v['status']>0 && $retid>0){  // be friends
			$this->ci->db->where(array('userid' => $fuserid, 'fuserid' => $userid))->update('friend', array("status"=>1));
		}

		$ret['descr'] = '保存成功';
		return $ret;
	}

	public function unfollowUser($userid, $fuserid) {
		// return values
		$ret = array();
		$ret['status'] = -1;
		$ret['descr'] = '保存失败';

		// parameters
		$userid = intval($userid);
		$fuserid = intval($fuserid);

		// check parameters
		if ($userid < 1 || $fuserid < 1 || $userid == $fuserid) {
			$ret['descr'] = '参数错误';
			return $ret;
		}

		$v = array();
		$v['userid'] = $userid;
		$v['fuserid'] = $fuserid;

		$this->ci->load->database();
		$this->ci->db->delete('friend', $v);
		//$this->ci->db->where(array('userid' => $userid, 'fuserid' => $fuserid))->update('friend',array('status'=>0));
		$retid = $this->ci->db->affected_rows();

		if ($retid >= 0) {
			$this->ci->db->where(array('userid' => $fuserid, 'fuserid' => $userid))->update('friend',
			array('status'=>0));
		}

		$ret['id'] = $userid;
		$ret['status'] = $retid;
		$ret['descr'] = '保存成功';
		return $ret;
	}

	public function saveFriendInfo($userid, $fuserid, $post) {
		// return values
		$ret = array();
		$ret['status'] = -1;
		$ret['descr'] = '保存失败';

		// parameters
		$userid = intval($userid);
		$fuserid = intval($fuserid);

		// check parameters
		if ($userid < 1 || $fuserid < 1 || $userid == $fuserid) {
			$ret['descr'] = '参数错误';
			return $ret;
		}

		$v = array();  	//$v['userid'] = $userid;  $v['fuserid'] = $fuserid;
		$snames = array('nickname', 'remark', 'befrom',);
		$inames = array('pvrpost', 'pvspost', 'pvmsg', 'pvblock', 'type', 'status',);
		foreach( $snames as $name ){
			if( isset($post[$name]) ){
				$v[$name] = $post[$name];
			}
		}
		foreach( $inames as $name ){
			if( isset($post[$name]) ){
				$v[$name] = intval($post[$name]);
			}
		}
		if (isset($post['status'])) {
			$v['lasttime'] = time();
			$v['lastip'] = $this->ci->input->ip_address();
		}
		if (count($v) < 1) {
			$ret['descr'] = '参数错误';
			return $ret;
		}

		$this->ci->load->database();
		$this->ci->db->where(array('userid' => $userid, 'fuserid' => $fuserid))->update('friend', $v);
		$retid = $this->ci->db->affected_rows();

		$ret['id'] = $userid;
		$ret['status'] = $retid;
		$ret['descr'] = '保存成功';
		return $ret;
	}

	// add by awen 获取用户所有有效好友的id数组
	public function getFriendsId($userid){
		$fids = array();
		$this->ci->load->database();
		$this->ci->db->select('fuserid')->where(array(
                    'userid'=>$userid,
                    'status'=>1
		));
		$query = $this->ci->db->from('friend')->get();
		$ret = $query->result_array();

		// dump($this->ci->db->last_query());
		if (count($ret)>0) {
			foreach ($ret as $fid) {
				$fids[] = $fid['fuserid'];
			}
		}
		return $fids;
	}

	public function getFriends($userid, $fuserid = -1, $type = -1, $status = -1, $pageidx = 1, $pagesize = 15, $orderby = null) {
		$where = array();
		$start = 0;
		$end = 15;
		if ($userid >= 0) {
			$where['userid'] = $userid;
		}
		if ($fuserid >= 0) {
			$where['fuserid'] = $fuserid;
		}
		if ($type >= 0) {
			$where['type'] = $type;
		}
		if ($status >= 0) {
			$where['status'] = $status;
		}
		if ($pagesize < 1) {
			$pagesize = 15;
		}
		if ($pageidx <= 0) {
			$pageidx = 1;
			$start = 0;
			$end = $pagesize;
		}
		$start = ($pageidx - 1) * $pagesize;

		$this->ci->load->database();
		// count
		if (count($where) > 0) {
			$this->ci->db->where($where);
		}
		$count = $this->ci->db->from('friend')->count_all_results();
		if( $count < 1 ){
			return array('pageidx' => $pageidx, 'pagesize' => $pagesize, 'count' => $count, 'data' => array());
		}

		// datas
		if (count($where) > 0) {
			$this->ci->db->where($where);
		}
		if ( $orderby ) {
			if(is_array($orderby) && count($orderby) > 0){
				foreach ($orderby as $k => $order) {
					$this->ci->db->order_by($order);
				}
			}else if( strlen($orderby)>0 ){
				$this->ci->db->order_by($orderby);
			}
		}
		$query = $this->ci->db->from('friend')->limit($pagesize, $start)->get();
		$vs = $query->result_array();

		return array('pageidx' => $pageidx, 'pagesize' => $pagesize, 'count' => $count, 'data' => $vs);
	}

	public function getFriendsCount($userid, $fuserid = -1, $type = -1, $status = -1){
		$where = array();
		if ($userid >= 0) {
			$where['userid'] = $userid;
		}
		if ($fuserid >= 0) {
			$where['fuserid'] = $fuserid;
		}
		if ($type >= 0) {
			$where['type'] = $type;
		}
		if ($status >= 0) {
			$where['status'] = $status;
		}

		$this->ci->load->database();
		// count
		if (count($where) > 0) {
			$this->ci->db->where($where);
		}
		$count = $this->ci->db->from('friend')->count_all_results();
		return $count;
	}

	/*  ------------------------------ user chat -------------------------------------------- */

	public function getUserChat($id){
		$vs = array();
		if ($id < 1) {
			return $vs;
		}
		$this->ci->load->database();
		$query = $this->ci->db->get_where('chat', array('id' => $id));
		$vs = $query->row_array();
		return $vs;
	}

	public function getGroupChat($id, $chattype=0){
		$vs = array();
		if ($id < 1) {
			return $vs;
		}
		$this->ci->load->database();
		$query = $this->ci->db->get_where('group_chat', array('id' => $id,'chattype'=>$chattype));
		$vs = $query->row_array();
		return $vs;
	}

	public function userChat($userid, $fuserid, $note = null, $post = null){
		// return values
		$ret = array();
		$ret['status'] = -1;
		$ret['descr'] = '保存失败';

		// parameters
		$userid = intval($userid);
		$fuserid = intval($fuserid);

		// check parameters
		if ($userid < 1 || $fuserid < 1) {
			$ret['descr'] = '参数错误';
			return $ret;
		}
		if( strlen($note)<1 && count($post)< 1){
			$ret['descr'] = '参数错误';
			return $ret;
		}

		$v = array();
		$v['sid'] = 's_'.$userid."_".$fuserid;
		if($userid > $fuserid){
			$v['sid'] = 's_'.$fuserid."_".$userid;
		}
		$v['userid'] = $userid;
		$v['fuserid'] = $fuserid;
		$v['note'] = $note;
		$v['addtime'] = time();
		$v['addip'] = $this->ci->input->ip_address();

		if( !is_array($post) || count($post)<1){
			$v['objecttype'] = 'text';
		}else{
			$snames = array('atids', 'objecttype','filetype', 'remark', 'savepath', 'savename', 'longitude','latitude');
			$inames = array('objectid', 'filesize', 'dateline','width','height','type');
			foreach( $snames as $name ){
				if( isset($post[$name]) ){
					$v[$name] = $post[$name];
				}
			}
			foreach( $inames as $name ){
				if( isset($post[$name]) ){
					$v[$name] = intval($post[$name]);
				}
			}
			if( isset($v['longitude'])){
				$v['longitude'] = floatval($v['longitude']);
				$v['latitude'] = floatval($v['latitude']);
			}
		}

		$this->ci->load->database();
		$this->ci->db->insert('chat', $v);
		$retid = $this->ci->db->insert_id();
		if ($retid < 1) {
			return $ret;
		}

		$ret['id'] = $retid;
		$ret['status'] = 1;
		$ret['descr'] = '保存成功';
		return $ret;
	}

	public function groupChat($userid, $groupid, $chattype = 0, $note = null, $post = null){
		// return values
		$ret = array();
		$ret['status'] = -1;
		$ret['descr'] = '保存失败';

		// parameters
		$userid = intval($userid);
		$groupid = intval($groupid);
		$chattype = intval($chattype);

		// check parameters
		if ($userid < 1 || $groupid < 1) {
			$ret['descr'] = '参数错误';
			return $ret;
		}
		if( strlen($note)<1 && count($post)< 1){
			$ret['descr'] = '参数错误';
			return $ret;
		}

		$v['userid'] = $userid;
		$v['groupid'] = $groupid;
		$v['chattype'] = $chattype;
		$v['note'] = $note;
		$v['addtime'] = time();
		$v['addip'] = $this->ci->input->ip_address();

		if( !is_array($post) || count($post)<1){
			$v['objecttype'] = 'text';
		}else{
			$snames = array('atids', 'objecttype','filetype', 'remark', 'savepath', 'savename', 'longitude','latitude');
			$inames = array('objectid', 'filesize', 'dateline','width','height','type');
			foreach( $snames as $name ){
				if( isset($post[$name]) ){
					$v[$name] = $post[$name];
				}
			}
			foreach( $inames as $name ){
				if( isset($post[$name]) ){
					$v[$name] = intval($post[$name]);
				}
			}
			if( isset($v['longitude'])){
				$v['longitude'] = floatval($v['longitude']);
				$v['latitude'] = floatval($v['latitude']);
			}
		}

		$this->ci->load->database();
		$this->ci->db->insert('group_chat', $v);
		$retid = $this->ci->db->insert_id();
		if ($retid < 1) {
			return $ret;
		}

		$ret['id'] = $retid;
		$ret['status'] = 1;
		$ret['descr'] = '保存成功';
		return $ret;
	}

	public function userChatHistory($userid, $fuserid = -1, $type = -1, $status = -1, $startid = 0, $pagesize = 15, $orderby=null) {
		$where = array();
		$sid = 's_'.$userid."_".$fuserid;
		if($userid > $fuserid){
			$sid = 's_'.$fuserid."_".$userid;
		}
		$where['sid'] = $sid;
		if ($type >= 0) {
			$where['type'] = $type;
		}
		if ($status >= 0) {
			$where['status'] = $status;
		}
		if ($pagesize < 1) {
			$pagesize = 15;
		}
		$where['id >'] = $startid;

		$this->ci->load->database();
		// count   ( no need? )
		$this->ci->db->where($where);
		$count = $this->ci->db->from('chat')->count_all_results();

		// datas
		$this->ci->db->where($where);
		if ( $orderby ) {
			if(is_array($orderby) && count($orderby) > 0){
				foreach ($orderby as $k => $order) {
					$this->ci->db->order_by($order);
				}
			}else if( strlen($orderby)>0 ){
				$this->ci->db->order_by($orderby);
			}
		}
		$query = $this->ci->db->from('chat')->limit($pagesize)->get();
		$vs = $query->result_array();

		return array('startid' => $startid, 'pagesize' => $pagesize, 'count' => $count, 'data' => $vs);
	}

	public function groupChatHistory($userid = -1, $groupid = -1, $chattype = 0, $type = -1, $status = -1,
	$startid = -1, $endid = -1, $pagesize = 15, $orderby=null) {
		$where = array();
		if ($groupid >= 0) {
			$where['groupid'] = $groupid;
		}
		if ($userid >= 0) {
			$where['userid'] = $userid;
		}
		if($chattype>=0){
			$where['chattype'] = $chattype;
		}
		if ($type >= 0) {
			$where['type'] = $type;
		}
		if ($status >= 0) {
			$where['status'] = $status;
		}
		if($startid >=0){
			$where['id >'] = $startid;
		}
		if($endid >=0){
			$where['id <'] = $endid;
		}
		if ($pagesize < 1) {
			$pagesize = 15;
		}
		$this->ci->load->database();
		// count   ( no need? )
		$this->ci->db->where($where);
		$count = $this->ci->db->from('group_chat')->count_all_results();

		// datas
		$this->ci->db->where($where);
		if ( $orderby ) {
			if(is_array($orderby) && count($orderby) > 0){
				foreach ($orderby as $k => $order) {
					$this->ci->db->order_by($order);
				}
			}else if( strlen($orderby)>0 ){
				$this->ci->db->order_by($orderby);
			}
		}
		$query = $this->ci->db->from('group_chat')->limit($pagesize)->get();
		$vs = $query->result_array();
		//dump($this->ci->db->last_query());
		return array('startid' => $startid, 'pagesize' => $pagesize, 'count' => $count, 'data' => $vs);
	}


	/*  ------------------------------ user group ------------------------------------------- */

	public function getGroup($id){
		$vs = array();
		if ($id < 1) {
			return $vs;
		}
		$this->ci->load->database();
		$query = $this->ci->db->get_where('group', array('id' => $id));
		$vs = $query->row_array();
		return $vs;
	}

	public function getGroups($userid = -1, $type = -1, $status = -1, $rank = -1, $pageidx = 1, $pagesize = 15, $orderby=null){
		$where = array();
		$start = 0;
		$end = 15;
		if ($userid >= 0) {
			$where['userid'] = $userid;
		}
		if ($type >= 0) {
			$where['type'] = $type;
		}
		if ($status >= 0) {
			$where['status'] = $status;
		}
		if ($rank >= 0) {
			$where['rank'] = $rank;
		}
		if ($pagesize < 1) {
			$pagesize = 15;
		}
		if ($pageidx <= 0) {
			$pageidx = 1;
			$start = 0;
			$end = $pagesize;
		}
		$start = ($pageidx - 1) * $pagesize;

		$this->ci->load->database();
		// count
		if (count($where) > 0) {
			$this->ci->db->where($where);
		}
		$count = $this->ci->db->from('group')->count_all_results();
		if( $count < 1 ){
			return array('pageidx' => $pageidx, 'pagesize' => $pagesize, 'count' => $count, 'data' => array());
		}

		// datas
		if (count($where) > 0) {
			$this->ci->db->where($where);
		}
		if ( $orderby ) {
			if(is_array($orderby) && count($orderby) > 0){
				foreach ($orderby as $k => $order) {
					$this->ci->db->order_by($order);
				}
			}else if( strlen($orderby)>0 ){
				$this->ci->db->order_by($orderby);
			}
		}
		$query = $this->ci->db->from('group')->limit($pagesize, $start)->get();
		$vs = $query->result_array();

		return array('pageidx' => $pageidx, 'pagesize' => $pagesize, 'count' => $count, 'data' => $vs);
	}

	public function addGroup($userid, $ids, $name = null, $remark = null, $post = null){
		// return values
		$ret = array();
		$ret['status'] = -1;
		$ret['descr'] = '保存失败';

		// parameters
		$userid = intval($userid);
		$ids = trim($ids);
		$tm = time();
		$ip = $this->ci->input->ip_address();

		// check parameters
		if ($userid < 1 || strlen($ids) < 1) {
			$ret['descr'] = '参数错误';
			return $ret;
		}

		$ids = trim($ids, ",");
		$ids = explode("," ,$ids);
		if( !$ids ){
			$ret['descr'] = '参数错误';
			return $ret;
		}

		// sort/unique userids
		array_push($ids, $userid);
		$ids = array_map(intval,$ids);
		$ids = array_unique($ids);
		sort($ids);
		if(count($ids)>0){
			if( 0 == $ids[0]){
				unset($ids[0]);
			}
		}

		// only yourself ?
		if( count($ids) < 2 ){
			$ret['descr'] = '参数错误';
			return $ret;
		}
		// should check ids is valid ???
		$user_names = array();
		$users = array();
		foreach ($ids as $uid){
			$u = $this->getUser($uid);
			if(null != $u && count($u)>0){
				$user_names[] = $u['nickname'];
				$users['u_'.$uid] = $u;
			}
		}
		if( count($ids) != count($user_names)){
			$ret['descr'] = '用户不存在!';
			return $ret;
		}

		// 2 db
		$v = array();
		$v['userid'] = $userid;
		$v['name'] = $name;
		$v['remark'] = $remark;
		$v['users'] = serialize($ids);
		$v['usernum'] = count($ids);
		$v['type'] = 0;
		$v['addtime'] = $v['lasttime'] = $tm;
		$v['addip'] = $v['lastip'] = $ip;
		$v['cover'] = $this->genGroupPic($users);

		// other parameters
		$snames = array('descr','tips', 'avatar', 'password', 'longitude','latitude');
		$inames = array('rank', 'limits', 'privacy','type','status');
		foreach( $snames as $name ){
			if( isset($post[$name]) ){
				$v[$name] = $post[$name];
			}
		}
		foreach( $inames as $name ){
			if( isset($post[$name]) ){
				$v[$name] = intval($post[$name]);
			}
		}
		if( isset($v['longitude'])){
			$v['longitude'] = floatval($v['longitude']);
			$v['latitude'] = floatval($v['latitude']);
		}
		//$v['unqhash'] = md5($v['type'].'-'.$v['users']);
		$v['unqhash'] = md5($v['users']);

		// tips
		if( null == $v['tips'] || strlen($v['tips'])<1 ){
			$usernames = implode(",",$user_names);
			if( mb_strlen($usernames)>100 ){
				$v['tips'] = mb_substr($usernames, 0, 100)."...";
			}else{
				$v['tips'] = $usernames;
			}
		}

		$this->ci->load->database();
		$this->ci->db->insert('group', $v);
		$retid = $this->ci->db->insert_id();
		if($retid>0){
			$ret['status'] = 1;
			$ret['descr'] = '保存成功';

			foreach ($ids as $id){
				$u = $users['u_'.$id] ;
				$this->saveUserGroupInfo($id, $retid, array('addid'=>$userid,'nickname'=>$u['nickname']));
			}
		}
		/*
		 if($retid < 0){
			return $ret;
			}else if( 0 == $retid){		// already existed
			$ret['status'] = 0;
			$query = $this->ci->db->get_where('group', array('unqhash' => $v['unqhash']));
			$us = $query->row_array();

			$retid = $us['id'] ;
			$v['id'] = $us['id'];
			$v['name'] = $us['name'];
			$v['cover'] = $us['cover'];
			}else{
			$ret['status'] = 1;
			foreach ($ids as $id){
			$u = $users['u_'.$id] ;
			$this->saveUserGroupInfo($id, $retid, array('addid'=>$userid,'nickname'=>$u['nickname']));
			}
			}*/

		$v['id'] = $retid;
		$ret['data'] = $v;
		$ret['users'] = $users;

		return $ret;
	}

	public function saveGroup($groupid, $ids = null, $post = null){
		// return values
		$ret = array();
		$ret['status'] = -1;
		$ret['descr'] = '保存失败';

		// parameters
		$groupid = intval($groupid);
		$ids = trim($ids);
		$tm = time();
		$ip = $this->ci->input->ip_address();

		// check parameters
		if ($groupid < 1 || (strlen($ids) < 1 && count($post)< 1) ) {
			$ret['descr'] = '参数错误';
			return $ret;
		}
		$us = $this->getGroup($groupid);
		if( null == $us || count($us) < 1){
			$ret['descr'] = '参数错误';
			return $ret;
		}
		$userid = $us['userid'];

		$v = array();
		$v['lasttime'] = $tm;
		$v['lastip'] = $ip;
		$snames = array('name', 'remark', 'descr','tips', 'cover', 'password',);
		$inames = array('rank', 'limits', 'privacy','type','status');
		foreach( $snames as $name ){
			if( isset($post[$name]) ){
				$v[$name] = $post[$name];
			}
		}
		foreach( $inames as $name ){
			if( isset($post[$name]) ){
				$v[$name] = intval($post[$name]);
			}
		}

		if( strlen($ids)>0 ){
			// should check ids is valid ???
			$ids = trim($ids,",");
			$ids = explode(",", $ids);
			if( !$ids ){
				$ret['descr'] = '参数错误';
				return $ret;
			}

			// sort/unique userids
			array_push($ids, $userid);
			$ids = array_map(intval,$ids);
			$ids = array_unique($ids);
			sort($ids);
			if(count($ids)>0){
				if( 0 == $ids[0]){
					unset($ids[0]);
				}
			}
			if( count($ids) < 1 ){
				$ret['descr'] = '参数错误';
				return $ret;
			}

			$v['users'] = serialize($ids);
			$v['usernum'] = count($ids);
			//$v['unqhash'] = md5($v['type'].'-'.$v['users']);
			$v['unqhash'] = md5($v['users']);
		}

		$this->ci->load->database();
		$this->ci->db->where(array('id' => $groupid ))->update('group', $v);
		$retid = $this->ci->db->affected_rows();
		if($retid <= 0){
			return $ret;
		}else{
			$oids = unserialize($us['users']);
			$addids = array_diff($ids, $oids);
			$delids = array_diff($oids, $ids);
			if( $addids && count($addids)> 0){
				foreach ($addids as $id){
					$this->saveUserGroupInfo($id, $groupid, array('addid'=>$userid) );
				}
			}
			if( $delids && count($delids)> 0){
				foreach ($delids as $id){
					$this->delUserGroupInfo($id, $groupid);
				}
			}
			$ret['addids'] = $addids;
			$ret['delids'] = $delids;
		}

		$ret['id'] = $retid;
		$ret['status'] = 1;
		$ret['descr'] = '保存成功';
		return $ret;
	}

	public function saveGroupInfo($groupid, $post = null){
		// return values
		$ret = array();
		$ret['status'] = -1;
		$ret['descr'] = '保存失败';

		// parameters
		$groupid = intval($groupid);
		$tm = time();
		$ip = $this->ci->input->ip_address();

		// check parameters
		if ($groupid < 1 || count($post)< 1 ) {
			$ret['descr'] = '参数错误';
			return $ret;
		}
		$us = $this->getGroup($groupid);
		if( null == $us || count($us) < 1){
			$ret['descr'] = '参数错误';
			return $ret;
		}
		$userid = $us['userid'];

		$v = array();
		$v['lasttime'] = $tm;
		$v['lastip'] = $ip;
		$snames = array('name', 'remark', 'descr','tips', 'cover', 'password',);
		$inames = array('rank', 'limits', 'privacy','type','status');
		foreach( $snames as $name ){
			if( isset($post[$name]) ){
				$v[$name] = $post[$name];
			}
		}
		foreach( $inames as $name ){
			if( isset($post[$name]) ){
				$v[$name] = intval($post[$name]);
			}
		}

		$this->ci->load->database();
		$this->ci->db->where(array('id' => $groupid ))->update('group', $v);
		$retid = $this->ci->db->affected_rows();
		if($retid >= 0){
			$ret['status'] = $retid;
			$ret['descr'] = '保存成功';
		}
		return $ret;
	}

	public function updateGroupUsers($groupid){
		// return values
		$ret = 0;

		$ids = array();
		$user_names = array();
		$us = $this->getUserGroupInfo(-1, $groupid, -1, -1, 1, 400, "userid asc");
		if( $us && $us['count']>0){
			$data = $us['data'];
			foreach ($data as $gu){
				$ids[] = $gu['userid'];
				$user_names[] = $gu['nickname'];
			}
		}

		$v = array();
		$v['users'] = serialize($ids);
		$v['unqhash'] = md5($v['users']);
		$v['usernum'] = count($ids);
		$usernames = implode(",",$user_names);
		if( mb_strlen($usernames)>100 ){
			$v['tips'] = mb_substr($usernames, 0, 100)."...";
		}else{
			$v['tips'] = $usernames;
		}

		$this->ci->load->database();
		$this->ci->db->where(array('id' => $groupid))->update('group', $v);
		$ret = $this->ci->db->affected_rows();
		return $ret;
	}

	public function getUserGroupInfo($userid, $groupid = -1, $type = -1, $status = -1, $pageidx = 1, $pagesize = 15, $orderby = null){
		$where = array();
		$start = 0;
		$end = 15;
		if ($userid >= 0) {
			$where['userid'] = $userid;
		}
		if ($groupid >= 0) {
			$where['groupid'] = $groupid;
		}
		if ($type >= 0) {
			$where['type'] = $type;
		}
		if ($status >= 0) {
			$where['status'] = $status;
		}
		if ($pagesize < 1) {
			$pagesize = 15;
		}
		if ($pageidx <= 0) {
			$pageidx = 1;
			$start = 0;
			$end = $pagesize;
		}
		$start = ($pageidx - 1) * $pagesize;

		$this->ci->load->database();
		// count
		if (count($where) > 0) {
			$this->ci->db->where($where);
		}
		$count = $this->ci->db->from('group_user')->count_all_results();
		if( $count < 1 ){
			return array('pageidx' => $pageidx, 'pagesize' => $pagesize, 'count' => $count, 'data' => array());
		}

		// datas
		if (count($where) > 0) {
			$this->ci->db->where($where);
		}
		if ( $orderby ) {
			if(is_array($orderby) && count($orderby) > 0){
				foreach ($orderby as $k => $order) {
					$this->ci->db->order_by($order);
				}
			}else if( strlen($orderby)>0 ){
				$this->ci->db->order_by($orderby);
			}
		}
		$query = $this->ci->db->from('group_user')->limit($pagesize, $start)->get();
		$vs = $query->result_array();
		//dump($this->ci->db->last_query());
		return array('pageidx' => $pageidx, 'pagesize' => $pagesize, 'count' => $count, 'data' => $vs);
	}

	public function saveUserGroupInfo($userid, $groupid, $post = null){
		// return values
		$ret = array();
		$ret['status'] = -1;
		$ret['descr'] = '保存失败';

		// parameters
		$userid = intval($userid);
		$groupid = intval($groupid);

		// check parameters
		if ($userid < 1 || $groupid < 1) {
			$ret['descr'] = '参数错误';
			return $ret;
		}

		$v = array();
		$v['userid'] = $userid;
		$v['groupid'] = $groupid;

		$snames = array('nickname', 'remark', );
		$inames = array('appid', 'addid', 'type', 'status', 'pvmsg', 'pvblock',);
		foreach( $snames as $name ){
			if( isset($post[$name]) ){
				$v[$name] = $post[$name];
			}
		}
		foreach( $inames as $name ){
			if( isset($post[$name]) ){
				$v[$name] = intval($post[$name]);
			}
		}
		$v['addtime'] = $v['lasttime'] = time();
		$v['addip'] = $v['lastip'] = $this->ci->input->ip_address();

		$this->ci->load->database();
		$this->ci->db->insert('group_user', $v);
		$retid = $this->ci->db->insert_id();
		if ($retid < 0) {
			return $ret;
		}else if( 0 == $retid){
			$ret['status'] = 0;
			// alreay existed ? update
			$us = $this->getUserGroupInfo($userid, $groupid);
			if( $us['data'] && count($us['data'])>0 ){
				$u = $us['data'][0];
				$retid = $u['id'];

				unset($v['addtime'], $v['addip']);
				$this->ci->db->where(array('id' => $retid))->update('group_user', $v);
			}
		}else{
			$ret['status'] = 1;
			// ok
		}
		$ret['id'] = $retid;
		$ret['descr'] = '保存成功';
		return $ret;
	}

	public function delUserGroupInfo($userid, $groupid){
		// return values
		$ret = -2 ;

		// parameters
		$userid = intval($userid);
		$groupid = intval($groupid);

		// check parameters
		if ($userid < 1 || $groupid < 1) {
			return $ret;
		}

		$this->ci->load->database();
		$this->ci->db->where(array('userid' => $userid, 'groupid'=>$groupid))->delete('group_user');
		$ret = $this->ci->db->affected_rows();
		return $ret;
	}

	/*  ----------------------- feedback -------------------------- */

	public function feedback($userid, $note, $post=null){
		// return values
		$ret = array();
		$ret['status'] = -1;
		$ret['descr'] = '保存失败';

		// parameters
		$userid = intval($userid);
		$note = trim($note);
		$tm = time();
		$ip = $this->ci->input->ip_address();

		// check parameters
		if (strlen($note) < 1) {
			$ret['descr'] = '参数错误';
			return $ret;
		}

		$v = array();
		$v['userid'] = $userid;
		$v['note'] = $note;
		$v['type'] = 0;
		$v['addtime'] = $tm;
		$v['addip'] = $ip;

		// other parameters
		$snames = array('sid', 'email', 'username','location','longitude','latitude');
		$inames = array('type','status','client');
		foreach( $snames as $name ){
			if( isset($post[$name]) ){
				$v[$name] = $post[$name];
			}
		}
		foreach( $inames as $name ){
			if( isset($post[$name]) ){
				$v[$name] = intval($post[$name]);
			}
		}
		if( isset($v['longitude'])){
			$v['longitude'] = floatval($v['longitude']);
			$v['latitude'] = floatval($v['latitude']);
		}
		$this->ci->load->database();
		$this->ci->db->insert('feedback', $v);
		$retid = $this->ci->db->insert_id();
		if($retid < 0){
			return $ret;
		}else{
			$ret['id'] = $retid;
			$ret['status'] = 1;
			$ret['descr'] = '保存成功';
		}
		return $ret;
	}


	/*  ----------------------- util ------------------------------ */

	public function check_username($username) {
		$guestexp = '\xA1\xA1|^Guest|^\xD3\xCE\xBF\xCD|\xB9\x43\xAB\xC8';
		$len = strlen($username);
		if ($len > 40 || $len < 3 || preg_match("/\s+|^c:\\con\\con|[%,\*\"\s\<\>\&]|$guestexp/is", $username)) {
			return FALSE;
		} else {
			return TRUE;
		}
	}

	public function check_email($email) {
		return strlen($email) > 6 && preg_match("/^[\w\-\.]+@[\w\-\.]+(\.\w+)+$/", $email);
	}

	public function check_mobile($mobile) {
		//return preg_match("/^13[0-9]{9}|15[0|1|2|3|5|6|7|8|9]\d{8}|18[0|5|6|7|8|9]\d{8}$/");
		if(strlen($mobile)>0){
			$mobile = preg_replace( array("/^[0\+]86/","/[ \-+()]/si"), "",$mobile);
		}
		return preg_match("/^(\s?\d{2,4}[\-]?)?1[3|4|5|6|7|8]\d{9}$/", $mobile);
	}

	function getRegError($code) {
		$errors = array("0" => '注册成功',
            "1" => '输入的验证码不符，请重新确认',
            "2" => '系统错误，请稍后重试',
            "3" => '两次输入的密码不一致',
            "4" => '密码空或包含非法字符，请重新填写',
            "5" => '同一个IP在1小时内只能注册一个账号',
            "11" => '用户名不合法',
            "12" => '用户名包含不允许注册的词语',
            "13" => '用户名已经存在',
            "14" => '电子邮箱格式有误',
            "15" => '电子邮箱不允许注册',
            "16" => '电子邮箱已经被注册',
            "17" => '注册失败');
		$s = $errors[$code];
		if (empty($s)) {
			$s = '注册失败';
		}
		return $s;
	}

	function getUserLocation($post = null) {
		$this->ci->load->database();
		$ret = array();
		if (!$post || count($post) < 1) {
			$ip = $this->ci->input->ip_address();
			$vs = split("\.", $ip);
			$ip_num = $vs[0] * 255 * 255 * 255 + $vs[1] * 255 * 255 + $vs[2] * 255 + $vs[3];

			$query = $this->ci->db->get_where('ip', array('ip_num_start <=' => $ip_num, 'ip_num_end >=' => $ip_num));
			$ret = $query->row_array();
			//dump($this->ci->db->last_query());
		}
		return $ret;
	}

	function genUserPassword($password){
		$ret = array();
		if(null == $password){
			$password = "";
		}

		$salt = rand(100000000, 900000000);
		$ret['password'] = md5(md5(trim($password)) . $salt);
		$ret['salt'] = $salt;
		return $ret;
	}

	function  genGroupPic($users) {
		if( !is_array($users) ||  count($users)< 1){
			return "group/holder_group_cover.png";;
		}

		$group_cfg = array(
			'g1' => array(
		array('x' => 64, 'y' => 64, 'resize' => 92)
		),
			'g2' => array(
		array('x' => 12, 'y' => 64, 'resize' => 92),
		array('x' => 116, 'y' => 64, 'resize' => 92)
		),
			'g3' => array(
		array('x' => 64, 'y' => 12, 'resize' => 92),
		array('x' => 12, 'y' => 116, 'resize' => 92),
		array('x' => 116, 'y' => 116, 'resize' => 92)
		),
			'g4' => array(
		array('x' => 12, 'y' => 12, 'resize' => 92),
		array('x' => 116, 'y' => 12, 'resize' => 92),
		array('x' => 12, 'y' => 116, 'resize' => 92),
		array('x' => 116, 'y' => 116, 'resize' => 92)
		),

			'g5' => array(
		array('x' => 12, 'y' => 42, 'resize' => 60),
		array('x' => 80, 'y' => 42, 'resize' => 60),
		array('x' => 148, 'y' => 42, 'resize' => 60),
		array('x' => 50, 'y' => 110, 'resize' => 60),
		array('x' => 118, 'y' => 110, 'resize' => 60)
		),
			'g6' => array(
		array('x' => 12, 'y' => 42, 'resize' => 60),
		array('x' => 80, 'y' => 42, 'resize' => 60),
		array('x' => 148, 'y' => 42, 'resize' => 60),
		array('x' => 12, 'y' => 110, 'resize' => 60),
		array('x' => 80, 'y' => 110, 'resize' => 60),
		array('x' => 148, 'y' => 110, 'resize' => 60)
		),
			'g7' => array(
		array('x' => 80, 'y' => 12, 'resize' => 60),
		array('x' => 12, 'y' => 80, 'resize' => 60),
		array('x' => 80, 'y' => 80, 'resize' => 60),
		array('x' => 148, 'y' => 80, 'resize' => 60),
		array('x' => 12, 'y' => 148, 'resize' => 60),
		array('x' => 80, 'y' => 148, 'resize' => 60),
		array('x' => 148, 'y' => 148, 'resize' => 60)
		),
			'g8' => array(
		array('x' => 12, 'y' => 12, 'resize' => 60),
		array('x' => 80, 'y' => 12, 'resize' => 60),
		array('x' => 148, 'y' => 12, 'resize' => 60),
		array('x' => 12, 'y' => 80, 'resize' => 60),
		array('x' => 80, 'y' => 80, 'resize' => 60),
		array('x' => 148, 'y' => 80, 'resize' => 60),
		array('x' => 12, 'y' => 148, 'resize' => 60),
		array('x' => 80, 'y' => 148, 'resize' => 60)
		),
			'g9' => array(
		array('x' => 12, 'y' => 12, 'resize' => 60),
		array('x' => 80, 'y' => 12, 'resize' => 60),
		array('x' => 148, 'y' => 12, 'resize' => 60),
		array('x' => 12, 'y' => 80, 'resize' => 60),
		array('x' => 80, 'y' => 80, 'resize' => 60),
		array('x' => 148, 'y' => 80, 'resize' => 60),
		array('x' => 12, 'y' => 148, 'resize' => 60),
		array('x' => 80, 'y' => 148, 'resize' => 60),
		array('x' => 148, 'y' => 148, 'resize' => 60)
		)
		);

		$files = array();
		foreach ($users as $us) {
			if(strlen($us['coverpath'])>0){
				$files[] = ATTACH_PATH.'/'. $us['coverpath'] . '/c240_' . $us['covername'];
			}else{
				$files[] = ATTACH_PATH ."/group/holder_user_cover.jpg";
			}
		}

		$copy_option = 100;
		$image = imagecreatetruecolor(220, 220);
		$bg    = imagecolorallocate($image, 235, 235, 235);//定义背景颜色
		imagefill($image, 0, 0, $bg);
		$items = count($files);
		if($items > 9){
			$items = 9;
		}
		$index = 0;
		foreach ($files as $filename) {
			$small                = imagecreatefromjpeg($filename);
			list($width, $height) = getimagesize($filename);
			$grp                  = 'g' . $items;
			$newwidth             = $group_cfg[$grp][$index]['resize'];
			$x                    = $group_cfg[$grp][$index]['x'];
			$y                    = $group_cfg[$grp][$index]['y'];
			$newheight            = $newwidth;
			$thumb                = imagecreatetruecolor($newwidth, $newheight);
			imagecopyresized($thumb, $small, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);
			//imagecopymerge($image, $thumb, $x, $y, 0, 0, 92, 92, $copy_option);
			imagecopymerge($image, $thumb, $x, $y, 0, 0, $newwidth, $newwidth, $copy_option);
			$index++;
		}

		$opath = "group/". date("Y") . '/' . date("m") . '/' .date("d");
		mt_srand();
		$oname = md5(uniqid(mt_rand())).".png";

		$p = ATTACH_PATH ."/". $opath;
		$array_dir = explode("/",$p);
		for($i=0; $i<count($array_dir); $i++){
			$path .= $array_dir[$i]."/";
			if(!file_exists($path)){
				mkdir($path);
			}
		}

		$outfilename = $p ."/". $oname;
		imagepng($image, $outfilename);
		imagedestroy($image);
		imagedestroy($small);

		return $opath. "/" . $oname;
	}
}

//cls

