<?php
/*
计算指标,按照玩家数量来进行
 */

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class AIndicatorreb
{
    public function __construct()
    {
        $this->ci = &get_instance();
        $CI       = &get_instance();
    }


     public function get_indicator($para, $kpi_name, $ab_group_score_max_and_weak, $par)
    {

        /*
        'kpi_best',    //最好成绩
        'kpi_best_ak', //kpi_best变种,比A比K, 如果双方最好成绩打平,则比较第二名.
        'kpi_worst',   //最差成绩
        'kpi_total_add', //加法总和
        'kpi_total_plus', //乘法总和
        'kpi_case_8421', //8421
        'kpi_gross'   //2人比杆
        */
        
        $division_type = $para['division_type'];
        if ('4_follow' == $division_type) {
            //地主婆模式

            $Indicator= $this->get_indicator_follow_mode($para, $kpi_name, $ab_group_score_max_and_weak, $par);
            $this->ci->xlog->getInstance()->logIndicator($Indicator);
            return  $Indicator;

        } else {
            //非地主婆模式
            $Indicator= $this->get_indicator_non_follow_mode($para, $kpi_name, $ab_group_score_max_and_weak, $par);
            $this->ci->xlog->getInstance()->logIndicator($Indicator);
            return  $Indicator;
        }

    }


    public function get_indicator_non_follow_mode($para, $kpi_name, $ab_group_score_max_and_weak, $par)
    {

        /*
        'kpi_best',    //最好成绩
        'kpi_best_ak', //kpi_best变种,比A比K, 如果双方最好成绩打平,则比较第二名.
        'kpi_worst',   //最差成绩
        'kpi_total_add', //加法总和
        'kpi_total_plus', //乘法总和
        'kpi_case_8421', //8421
        'kpi_gross'   //2人比杆
         */

         switch ($kpi_name) {
             case 'kpi_best':
                 return $this->get_indicator_non_follow_mode_kpi_best($para, $kpi_name, $ab_group_score_max_and_weak, $par);
                 break;

         case 'kpi_gross':
                 return $this->get_indicator_non_follow_mode_kpi_gross($para, $kpi_name, $ab_group_score_max_and_weak, $par);
                 break;

         case 'kpi_best_ak':
                 return $this->get_indicator_non_follow_mode_kpi_best_ak($para, $kpi_name, $ab_group_score_max_and_weak, $par);
                 break;

         case 'kpi_worst':
                 return $this->get_indicator_non_follow_mode_kpi_worst($para, $kpi_name, $ab_group_score_max_and_weak, $par);
                 break;

         case 'kpi_total_add':
                 return $this->get_indicator_non_follow_mode_kpi_total_add($para, $kpi_name, $ab_group_score_max_and_weak, $par);
                 break;

         case 'kpi_total_plus':
                 return $this->get_indicator_non_follow_mode_kpi_total_plus($para, $kpi_name, $ab_group_score_max_and_weak, $par);
                 break;

         case 'kpi_case_8421':
                 return $this->get_indicator_non_follow_mode_8421($para, $kpi_name, $ab_group_score_max_and_weak, $par);
                 break;
         }

    }


     public function get_indicator_non_follow_mode_kpi_best($para, $kpi_name, $ab_group_score_max_and_weak, $par)
    {


        $a_group_score_max_and_weak = $ab_group_score_max_and_weak['A'];
        $b_group_score_max_and_weak = $ab_group_score_max_and_weak['B'];

        return array(
            'indicator_a' => min($a_group_score_max_and_weak),
            'indicator_b' => min($b_group_score_max_and_weak),
            'indicator_type'=>'score',
            'kpi_name'=>$kpi_name ,
            'ak_k_used' => 'n'
        );
    }

     public function get_indicator_non_follow_mode_kpi_gross($para, $kpi_name, $ab_group_score_max_and_weak, $par)
    {

        //'kpi_gross'   2人比杆

        $a_group_score_max_and_weak = $ab_group_score_max_and_weak['A'];
        $b_group_score_max_and_weak = $ab_group_score_max_and_weak['B'];
    
        return array(
            'indicator_a' => min($a_group_score_max_and_weak),
            'indicator_b' => min($b_group_score_max_and_weak),
            'indicator_type'=>'score',
            'kpi_name'=>$kpi_name,
            'ak_k_used' =>'n'
        );

        
    }

     public function get_indicator_non_follow_mode_kpi_best_ak($para, $kpi_name, $ab_group_score_max_and_weak, $par)
    {

        $ak_k_used='n';

        $a_group_score_max_and_weak = $ab_group_score_max_and_weak['A'];
        $b_group_score_max_and_weak = $ab_group_score_max_and_weak['B'];

    
        $this->ci->xlog->getInstance()->log("比A比K模式/XIndicatorreb.php", 4);
        $indicator_type='score';

        $indicator_a_A = min($a_group_score_max_and_weak); //最好成绩
        $indicator_a_K = max($a_group_score_max_and_weak); //最差成绩(第二好成绩)

        $indicator_b_A = min($b_group_score_max_and_weak); //最好成绩
        $indicator_b_K = max($b_group_score_max_and_weak); //最差成绩(第二好成绩)

        if ($indicator_a_A == $indicator_b_A) {
            $this->ci->xlog->getInstance()->log("使用K/XIndicatorreb.php", 4);

            $indicator_a = $indicator_a_K;
            $indicator_b = $indicator_b_K;
            $ak_k_used='y';


        } else {
            $indicator_a = $indicator_a_A;
            $indicator_b = $indicator_b_A;
        }
     

        return array(
            'indicator_a' => $indicator_a,
            'indicator_b' => $indicator_b,
            'kpi_name'=>$kpi_name,
            'indicator_type'=>$indicator_type,
            'ak_k_used'=> $ak_k_used 
        );

    }

     public function get_indicator_non_follow_mode_kpi_worst($para, $kpi_name, $ab_group_score_max_and_weak, $par)
    {

        $a_group_score_max_and_weak = $ab_group_score_max_and_weak['A'];
        $b_group_score_max_and_weak = $ab_group_score_max_and_weak['B'];

      
        //4人拉丝,比最差成绩,不让杆
        $indicator_a = max($a_group_score_max_and_weak);
        $indicator_b = max($b_group_score_max_and_weak);
        $indicator_type='score';
    
        return array(
            'indicator_a' => $indicator_a,
            'indicator_b' => $indicator_b,
            'kpi_name'=>$kpi_name,
            'indicator_type'=>$indicator_type,
            'ak_k_used'=>'n'

        );

    }

     public function get_indicator_non_follow_mode_kpi_total_add($para, $kpi_name, $ab_group_score_max_and_weak, $par)
    {


        $a_group_score_max_and_weak = $ab_group_score_max_and_weak['A'];
        $b_group_score_max_and_weak = $ab_group_score_max_and_weak['B'];
    
            //只有一个选手的加法总和
        if (count($a_group_score_max_and_weak) == 1) {
            $indicator_a = 2 * array_sum($a_group_score_max_and_weak);
        } else {
            //两个选手
            $indicator_a = array_sum($a_group_score_max_and_weak);
        }

        if (count($b_group_score_max_and_weak) == 1) {
            $indicator_b = 2 * array_sum($b_group_score_max_and_weak);
        } else {
            $indicator_b = array_sum($b_group_score_max_and_weak);
        }
        
        $indicator_type='add';
        return array(
            'indicator_a' => $indicator_a,
            'indicator_b' => $indicator_b,
            'kpi_name'=>$kpi_name,
            'indicator_type'=>$indicator_type,
            'ak_k_used'=> 'n' 

        );

    }

     public function get_indicator_non_follow_mode_kpi_total_plus($para, $kpi_name, $ab_group_score_max_and_weak, $par)
    { 

        $a_group_score_max_and_weak = $ab_group_score_max_and_weak['A'];
        $b_group_score_max_and_weak = $ab_group_score_max_and_weak['B'];

         
    
        $indicator_a = 1;
        $indicator_b = 1;
        $indicator_type='plus';

        if (count($a_group_score_max_and_weak) == 1) {
            //1个选手,自己乘自己
            $indicator_a = array_sum($a_group_score_max_and_weak) * array_sum($a_group_score_max_and_weak);
        } else {
            //2个选手,互相乘.

            foreach ($a_group_score_max_and_weak as $key => $one_gross) {
                $indicator_a = $indicator_a * $one_gross;
            }
        }

        if (count($b_group_score_max_and_weak) == 1) {
            $indicator_b = array_sum($b_group_score_max_and_weak) * array_sum($b_group_score_max_and_weak);
        } else {

            foreach ($b_group_score_max_and_weak as $key => $one_gross) {
                $indicator_b = $indicator_b * $one_gross;
            }
        }
    
        return array(
            'indicator_a' => $indicator_a,
            'indicator_b' => $indicator_b,
            'kpi_name'=>$kpi_name,
            'indicator_type'=>$indicator_type,
            'ak_k_used'=>'n' 

        );

    }

     public function get_indicator_non_follow_mode_8421($para, $kpi_name, $ab_group_score_max_and_weak, $par)
    {
        $a_group_score_max_and_weak = $ab_group_score_max_and_weak['A'];
        $b_group_score_max_and_weak = $ab_group_score_max_and_weak['B'];


        $sum_a       = 0;
        $sum_b       = 0;
        $indicator_a = 0;
        $indicator_b = 0;
        $indicator_type='8421';
            //循环两次

        
        // debug($a_group_score_max_and_weak);

        foreach ($a_group_score_max_and_weak as $user => $one_gross) {
            $indicator_a = $indicator_a + $this->sum8421($user,$one_gross, $par,$para['value_8421']);
        }

        foreach ($b_group_score_max_and_weak as $user => $one_gross) {
            $indicator_b = $indicator_b + $this->sum8421($user,$one_gross, $par,$para['value_8421']);
        }
    
        return array(
            'indicator_a' => $indicator_a,
            'indicator_b' => $indicator_b,
            'kpi_name'=>$kpi_name,
            'indicator_type'=>$indicator_type,
            'ak_k_used'=>'n' 
        );
    }



    
    /*
    地主婆模式:
     算法:
         指标计算方法:
         
         加法总和:地主婆所在的一方,两人的最好成绩+2,作为指标
         乘法总和:地主婆所在的一方,两人的最好成绩*两人的最好成绩,作为指标

    实际含义:
         地主婆总跟随最好成绩的人,以最好人的成绩作为自己的成绩(如果地主婆的成绩不好,队友的成绩好)

    */

    public function get_indicator_follow_mode($para, $kpi_name, $ab_group_score_max_and_weak, $par)
    {

        $a_group_score_max_and_weak = $ab_group_score_max_and_weak['A'];
        $b_group_score_max_and_weak = $ab_group_score_max_and_weak['B'];

        if ('kpi_best' == $kpi_name) {
            return array(
                'indicator_a' => min($a_group_score_max_and_weak),
                'indicator_b' => min($b_group_score_max_and_weak),
                'kpi_name'=>$kpi_name,
                'indicator_type'=>'score'
            );
        }

        $a_users = array_keys($a_group_score_max_and_weak);
        $b_users = array_keys($b_group_score_max_and_weak);

        //加法总和
        if ('kpi_total_add' == $kpi_name) {
            $indicator_type='add';
            if (in_array($para['follower'], $a_users)) {
                $indicator_a = 2 * min($a_group_score_max_and_weak);
                $indicator_b = array_sum($b_group_score_max_and_weak);

            } else {
                $indicator_a = array_sum($a_group_score_max_and_weak);
                $indicator_b = 2 * min($b_group_score_max_and_weak);

            }
                  
        }

        if ('kpi_total_plus' == $kpi_name) {
             $indicator_type='plus';
            if (in_array($para['follower'], $a_users)) {

                $indicator_a = min($a_group_score_max_and_weak) * min($a_group_score_max_and_weak);
                $b_scores    = array_values($b_group_score_max_and_weak);
                $indicator_b = $b_scores[0] * $b_scores[1];

            } else {
                $a_scores    = array_values($a_group_score_max_and_weak);
                $indicator_a = $a_scores[0] * $b_scores[1];
                $indicator_b = min($b_group_score_max_and_weak) * min($b_group_score_max_and_weak);

            }
        }
       
         
        $Indicator= array(
            'indicator_a' => $indicator_a,
            'indicator_b' => $indicator_b,
            'indicator_type'=>$indicator_type,
            'kpi_name'=>$kpi_name
        );
      
        return  $Indicator;
    }



    public function rebuild_8421($v8421){
     
         
         
         $v8421_arr= (array) json_decode($v8421) ;
        
         $v8421_tab=array();

         $deduction_based='';
         
         foreach ($v8421_arr as $key => $one_table) {
          
           $one_vt=(array)$one_table;
           
           if( array_key_exists('clash', $one_vt)  ){
                    $conflict=    $one_vt['clash'];
                    unset($one_vt['clash']);
           }else
           {
                    $conflict=    'both_hand';
           }

           arsort($one_vt,1);  // sort desc by numberic
            
           $add_arr=array();
           $deduction_arr=array();
           
           foreach ($one_vt as $score => $value) {
                
                if( intval($value) >=0){
                   $add_arr[$score]=$value;
                }
                else
                {
                    $deduction_arr[$score]=$value;

                    if (strpos($score, 'Double') !== false) {
                       $deduction_based='doublepar_based';
                    }else
                    {
                       $deduction_based='par_based';
                    }
                }
           }

           //$v8421_tab[$key]['vtable']=$one_vt;
           $v8421_tab[$key]['add_arr']=$add_arr;
           $v8421_tab[$key]['deduction_arr']=$deduction_arr;
           
           
           $v8421_tab[$key]['conflict']= $conflict;
           $v8421_tab[$key]['deduction_based']= $deduction_based;




         }

         
          return $v8421_tab;

    }


       function trans_score_name_for_8421_add($score,$par){
          
          /*
          TripleBogey
          DoubleBogey
          Bogey
          Par
          Birdie
          Eagle
          
          */
 

            if ( $score == $par+3 ) {
                $score_name = 'TripleBogey';
            }


            if ( $score == $par+2 ) {
                $score_name = 'DoubleBogey';
            }

            
            if ( $score == $par+1) {
                $score_name = 'Bogey';
            }

        
            if ($score == $par) {
                $score_name = 'Par';
            }

            if ($score == $par -1) {
                $score_name = 'Birdie';
            }


            if ($score == $par -2 ) {
                $score_name = 'Eagle';
            }

            if( $score <= $par -3 ){
                $score_name='Eagle' ;
            }

            if( $score >= $par +4 ){
                $score_name='notfound' ;
            }

            return  $score_name;
      }


      //基于doublepar
      public function trans_score_name_for_8421_deduction_doublepar_based($score,$par){
         /*
          "DoublePar":"-1",
          "DoublePar+1":"-2",
         */
      
          $score_name='notfound';
          
          if( $score == 2* $par){
            $score_name='DoublePar';
          }

          if( $score == (2 * $par) +1){
            $score_name='DoublePar+1';
          }

          if( $score > (2 * $par) +1){
            $score_name='DoublePar+1';
          }
          return $score_name;
      }

       //基于par
        public function trans_score_name_for_8421_deduction_par_based($score,$par){
         
          $score_name='notfound';

          if( $score == $par + 4){
            $score_name='Par+4';
          }

          if( $score == $par +5){
            $score_name='Par+5';
          }

          if( $score > $par +5){
            $score_name='Par+5';
          }

          return $score_name;

      }



    public function sum8421($user,$score, $par,$cfg_8421)
    {

        

        $vt=$this->rebuild_8421($cfg_8421);
        $player_vt=$vt[$user];
        $added='no';
        $deleted='no';
        $conflict=$player_vt['conflict'];
        $based=$player_vt['deduction_based'];
        $name_for_add=$this->trans_score_name_for_8421_add($score,$par);
        
        if($based=='doublepar_based'){
            $name_for_deduction=$this->trans_score_name_for_8421_deduction_doublepar_based($score,$par);
        }else
        {
            $name_for_deduction=$this->trans_score_name_for_8421_deduction_par_based($score,$par);
        }

        
        if($name_for_add=='notfound'){
            $sub_add=0;
            $added='no';
        }else{
            $sub_add=abs($player_vt['add_arr'][$name_for_add]);
            if($sub_add >0){
                 $added='yes';
            }else
            {
                 $added='no';
            }
            
        }


        $this->ci->xlog->getInstance()->log("basd:$based user/$user/$score/add:$name_for_add/$sub_add/delete:$name_for_deduction", 4);
        
        if($name_for_deduction=='notfound'){
            $sub_del=0;
            $deleted='no';
        }else{
            $sub_del=abs($player_vt['deduction_arr'][$name_for_deduction]);
            if($sub_del >0){
                 $deleted='yes';
            }else
            {
                 $deleted='no';
            }

            $deleted='yes';
        }
        
          $this->ci->xlog->getInstance()->log("delete:$sub_del", 4);
      

         if(  ($added == 'yes')  && ($deleted == 'yes') ) 
         {

          $this->ci->xlog->getInstance()->log("加和减分同时出现 $user", 4);
           //  we have conflict !
             if( $conflict=='add_first'){
                $num_get=$sub_add;
             }

             if( $conflict=='deduction_first'){
                $num_get= -1 * $sub_del;
             }

             if( $conflict=='both_hand'){
                $num_get = $sub_add - $sub_del;
             }
         }
         else
         {

            if($added=='yes'){
                  $num_get=$sub_add ;

            }

            if($deleted=='yes'){
                  $num_get= -1 * $sub_del ;
            }
         }

        return $num_get;
    }

   

    public function get_winner_by_indicator($Indicator, $kpi_name)
    {

        //只有8421,指标大者赢.
        if ('kpi_case_8421' == $kpi_name) {

            $indicator_a = -1 * $Indicator['indicator_a'];
            $indicator_b = -1 * $Indicator['indicator_b'];

        } else {

            $indicator_a = $Indicator['indicator_a'];
            $indicator_b = $Indicator['indicator_b'];
        }

        if ($indicator_a > $indicator_b) {
            $winner = 'B';
        }

        if ($indicator_a < $indicator_b) {
            $winner = 'A';
        }

        if ($indicator_a == $indicator_b) {
            $winner = '-';
        }
        $this->ci->xlog->getInstance()->log("{$kpi_name}赢方=: $winner", 4);
        return $winner;
    }

}
