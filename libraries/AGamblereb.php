<?php



/*
暂时不考虑的字段:
meat_eat_condition(吃肉的条件)
bonus_switch:头N总M的奖励开关:总赢才奖(zong_win)|总平或以上(zong_equal_or_up)|与总无关(zong_ignore)

*/

error_reporting(E_STRICT);

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}


function terminate_missing_variables($errno, $errstr, $errfile, $errline)
{
    if (($errno == E_NOTICE) and (strstr($errstr, "Undefined variable")))
        die("$errstr in $errfile line $errline");
    
    return false; // Let the PHP error handler handle all the rest  
}

$old_error_handler = set_error_handler("terminate_missing_variables");


class AGamblereb
{
    public function __construct()
    {
        $this->ci =& get_instance();
        $CI =& get_instance();
        $CI->load->library('AIndicatorreb');
        $this->xindicator = $CI->aindicatorreb;
        $CI->load->library('AAbrankreb');
        $this->xabrank = $CI->aabrankreb;
        $CI->load->library('ATitlereb');
        $this->xtitle = $CI->atitlereb;
        $CI->load->library('APreprocessreb');
        $this->xpreprocess = $CI->apreprocessreb;
        $CI->load->library('ABonusreb');
        $this->xbonus = $CI->abonusreb;
        $CI->load->library('ABasepointreb');
        $this->xbasepoint = $CI->abasepointreb;
        $CI->load->library('AMeatreb');
        $this->xmeat = $CI->ameatreb;
        $CI->load->library('AMoneyreb');
        $this->xmoney = $CI->amoneyreb;
        $CI->load->library('ADrawreb');
        $this->xdraw = $CI->adrawreb;
        $CI->load->library('AP8421reb');
        $this->x8421 = $CI->ap8421reb;
        $CI->load->library('AGuoreb');
        $this->xguo = $CI->aguoreb;
        
    }
    
    
    function errorhandle($errtext)
    {
        $ret['errtext'] = $errtext;
        echo json_encode($ret);
        die;
    }
    
    
    function check_hole_valid($para, $index)
    {
        if ($para['gamedata'][$index]['selected'] == 'n') {
            return false;
        }
        $ab_group_score_max_and_weak = $this->xpreprocess->get_group_scores($para['gamedata'], $index, 'after_max_weak');
        $a_group_score               = $ab_group_score_max_and_weak['A'];
        $b_group_score               = $ab_group_score_max_and_weak['B'];
        $valid                       = true;
        if ((min($a_group_score) == 0) || (min($b_group_score) == 0)) {
            $valid = false; //无效成绩,不能计算钱,
        }
        return $valid;
    }
    
    
     
    
    function get_winner_failer_paylist($para, $index, $kpi_summary, $detail_8421)
    {

        /*
           不需要考虑8421的情况,因为8421后面还会根据情况进行处理:
             包负分.
             全包

        */
        
        // assume A is the winner.
        
        $par                         = $para['gamedata'][$index]['par'];
        $ab_group_score_max_and_weak = $this->xpreprocess->get_group_scores($para['gamedata'], $index, 'after_max_weak');
        $ab_group_score_raw          = $this->xpreprocess->get_group_scores($para['gamedata'], $index, 'raw_scores');
        $ab_group_score_after_max    = $this->xpreprocess->get_group_scores($para['gamedata'], $index, 'after_max');
        
        
        if ($kpi_summary['winner_after_summary'] == 'A') {
            $winner  = 'A';
            $winners = $this->xmoney->extract_users($ab_group_score_max_and_weak['A']);
            $failers = $this->xmoney->extract_users($ab_group_score_max_and_weak['B']);
            
        }
        
        if ($kpi_summary['winner_after_summary'] == 'B') {
            $winner  = 'B';
            $winners = $this->xmoney->extract_users($ab_group_score_max_and_weak['B']);
            $failers = $this->xmoney->extract_users($ab_group_score_max_and_weak['A']);
        }
        
        if ($kpi_summary['winner_after_summary'] == '-') {
            $winner  = '-';
            $winners = $this->xmoney->extract_users($ab_group_score_max_and_weak['A']);
            $failers = $this->xmoney->extract_users($ab_group_score_max_and_weak['B']);
        }


            $a_payers_if_fail = $this->xmoney->get_payers('A', $par, $ab_group_score_raw['A'], $para['dutp_option']);
            $b_payers_if_fail = $this->xmoney->get_payers('B', $par, $ab_group_score_raw['B'], $para['dutp_option']);
            

        return array(
            'winner' => $winner,
            'w' => $winners,
            'f' => $failers,
            'apayers' => $a_payers_if_fail,
            'bpayers' => $b_payers_if_fail
        );
    }
    
    
    
    
    
    function process_kpis(&$para, $index, $valid)
    {
        
        $gambleid                    = $para['gambleid'];
        $ab_group_score_max_and_weak = $this->xpreprocess->get_group_scores($para['gamedata'], $index, 'after_max_weak');
        $ab_group_score_raw          = $this->xpreprocess->get_group_scores($para['gamedata'], $index, 'raw_scores');
        $ab_group_score_after_max    = $this->xpreprocess->get_group_scores($para['gamedata'], $index, 'after_max');
        
        $par = $para['gamedata'][$index]['par'];
        
        $kpis       = $para['kpi_cfg'];
        $kpi_result = array();
        
        foreach ($kpis as $one_kpi) {
            $this->ci->xlog->getInstance()->log("<br/>处理指标: $one_kpi", 4);
            $_one_kpi_ret = $this->process_one_kpi($valid, $para, $one_kpi, $index);
            
            //ZZZ记录是否使用了K指标.--如果以后ak指标与其他指标组合,会冲掉.maybe fixed later.
            
            $para['ak_k_use_list'][$index] = $_one_kpi_ret['ak_k_used'];
            $this->ci->xlog->getInstance()->log("无奖励情况下赢的点数: {$_one_kpi_ret['K_point']}  ", 4, 'red');
            $kpi_result[$one_kpi] = $_one_kpi_ret;
        }
        
        $kpi_summary = $this->summary_points($para, $gambleid, $index, $kpi_result);
        
        if (!$valid) {
            $drawinfo = array(
                'draw' => false,
                'draw_option'=>$para['draw_option'],
                'valid' => $valid,
                'eaten' => false
            );
            
        } else {
            $drawinfo = array(
                'draw' => $kpi_summary['draw'],
                'draw_option'=>$para['draw_option'],
                'valid' => $valid,
                'eaten' => false
            );
        }


        $detail_8421 = $this->x8421->get8421detail($ab_group_score_max_and_weak, $par, $para);
        $w_f_pay_list = $this->get_winner_failer_paylist($para, $index, $kpi_summary, $detail_8421);
        
        $K_winner_side_point_after_bonus = $this->execute_plus_bonus($para, $index, $kpi_summary);
        
        $holesum = array(
            'kpi_result' => $kpi_result,
            'winner_after_summary' => $kpi_summary['winner_after_summary'],
            'K_winner_side_point' => $kpi_summary['K_winner_side_point'],
            'K_winner_side_point_after_bonus' => $K_winner_side_point_after_bonus,
            'sum_base_raw_point' => $kpi_summary['sum_base_raw_point'],
            'drawinfo' => $drawinfo,
            'drawdetail'=>$this->xdraw->getDrawDetail($kpi_result),
            'winners' => $w_f_pay_list['w'],
            'failers' => $w_f_pay_list['f'],
            'a_payers_if_fail' => $w_f_pay_list['apayers'],
            'b_payers_if_fail' => $w_f_pay_list['bpayers'],
            'detail_8421'=>$detail_8421
        );
        
         $para['gamedata'][$index]['kpi_summary'] = $holesum;
    }
    



    
    
    //进行乘法奖励.
    function execute_plus_bonus($para, $index, $kpi_summary)
    {
        
        $bonus_info     = $this->xbonus->get_public_bonus($para, $index, $kpi_summary['winner_after_summary']);
        $point          = $kpi_summary['K_winner_side_point'];
        $bonus_operator = $bonus_info['bonus_operator'];
        $bonus_factor   = $bonus_info['bonus_factor'];
        if ($kpi_summary['winner_after_summary'] == '-') {
            return 0;
        }
        
        if ($bonus_operator == 'add') //已经加过一次.
            {
            $point_after_bonus = $point;
        }
        
        if ($bonus_operator == 'plus') {
            $point_after_bonus = $point * $bonus_factor;
            $this->ci->xlog->getInstance()->log("进行乘法奖励,奖励系数{$bonus_factor},奖励完:{$point_after_bonus}", 4, 'red');
        }
        
        if ($bonus_operator == 'none') {
            $point_after_bonus = $point;
        }
        return $point_after_bonus;
    }
    
    
    //无效洞
    function get_not_valid_result($kpi_name)
    {
        $kpi_tmp = array(
            'kpi_name' => $kpi_name,
            'winner' => '-',
            'valid' => false,
            'K_point' => 0,
            'K_point_after_add_bonus' => 0,
            'ak_k_used' => 'n'
            
        );
        $this->ci->xlog->getInstance()->log("此洞无效", 4, 'red');
        return $kpi_tmp;
    }
    
    //打平
    function get_euqal_result($kpi_name, $Indicator)
    {
        $kpi_tmp = array(
            'kpi_name' => $kpi_name,
            'winner' => '-',
            'valid' => true,
            'K_point' => 0,
            'K_point_after_add_bonus' => 0,
            'ak_k_used' => $Indicator['ak_k_used']
        );
        $this->ci->xlog->getInstance()->log("此洞打平,不需要考虑奖励.", 4, 'red');
        return $kpi_tmp;
    }
    
    
    //如果是加法奖励,计算奖励完以后的点数
    function get_if_added_point($index, $para, $kpi_name, $winner, $x_sub_point, $Indicator)
    {
        
        
        $par                         = $para['gamedata'][$index]['par'];
        $ab_group_score_max_and_weak = $this->xpreprocess->get_group_scores($para['gamedata'], $index, 'after_max_weak');
        
        $K_point_after_add_bonus = $x_sub_point['K_point'];
        $x_bonus_operator        = 'none';
        if ($para['bonus_option']['bonus_used'] == 'y') {
            $x_bonus_operator = $para['bonus_option']['bonus_operator'];
        }
        
        if ($x_bonus_operator == 'add') {
            
            //拉丝2/3点,并且包含合计指标时候,合计指标是否进行加法奖励
            
            $algorithm_cfg = $para['algorithm_cfg'];
            $still_add     = $algorithm_cfg['total_kpi_will_add_in_lasi23'];
            if (($still_add == 'n') && ($para['is_lasi'] == 'y') && ($para['kpi_num'] >= 2) && ($kpi_name == 'kpi_total_add' || $kpi_name == 'kpi_total_plus')) {
                
            } else {
                $score_for_bonus         = $this->xbonus->get_score_for_bonus($algorithm_cfg, $kpi_name, $Indicator, $winner, $ab_group_score_max_and_weak);
                $item_bonus              = $this->xbonus->get_bonus_by_score($para['bonus_option'], $par, $score_for_bonus, $ab_group_score_max_and_weak[$winner]);
                $this_kpi_bonus_factor   = $item_bonus['bonus_factor'];
                $K_point_after_add_bonus = $x_sub_point['K_point'] + $this_kpi_bonus_factor;
                $this->ci->xlog->getInstance()->log("加法奖励 {$this_kpi_bonus_factor}点,加完后:{$K_point_after_add_bonus} ", 4);
            }
        }
        return $K_point_after_add_bonus;
    }
    
    
    function process_one_kpi($valid, $para, $kpi_name, $index)
    {
        
        if (!$valid) {
            return $this->get_not_valid_result($kpi_name);
        }
        
        $par                         = $para['gamedata'][$index]['par'];
        $ab_group_score_max_and_weak = $this->xpreprocess->get_group_scores($para['gamedata'], $index, 'after_max_weak');
        
        //取某些kpi使用的指标(最好成绩(包括AK),最差成绩,加法总计杆数,乘法总计杆数,8421,)
        $Indicator = $this->xindicator->get_indicator($para, $kpi_name, $ab_group_score_max_and_weak, $par);
        
        //判断单项指标的输赢
        $winner = $this->xindicator->get_winner_by_indicator($Indicator, $kpi_name);
        
        if ($winner == '-') {
            return $this->get_euqal_result($kpi_name, $Indicator);
        }
        
        //计算这个指标赢的点数,先不考虑奖励
        $kpi_base_point = $this->xbasepoint->get_base_point($winner, $Indicator, $para, $kpi_name);
        
        //计算如果是加法奖励,计算奖励完的点数.
        $K_point_after_add_bonus = $this->get_if_added_point($index, $para, $kpi_name, $winner, $kpi_base_point, $Indicator);
        
        $kpi_tmp = array(
            'kpi_name' => $kpi_name,
            'winner' => $winner,
            'valid' => true,
            'K_point' => $kpi_base_point['K_point'], //无奖励时候的点数.
            'K_point_after_add_bonus' => $K_point_after_add_bonus, //如果是加法奖励,奖励完的点数.
            'ak_k_used' => $Indicator['ak_k_used']
        );
        
        if ($this->x8421->checkif8421($para)) {
            $kpi_tmp['Indicator_8421'] = $Indicator;
        }
        
        return $kpi_tmp;
    }
    
    
    //合计各项,判断输赢
    function summary_points($para, $gambleid, $index, $kpi_result)
    {
        
        $winner_after_summary = '-';
        $A_subtotal           = 0;
        $B_subtotal           = 0;
        
        $A_win_counter = 0;
        $B_win_counter = 0;
        $raw_point     = 0;
        $draw          = false;
        
        
        $bonus_type = $para['bonus_option']['bonus_used'] == 'y' ? $para['bonus_option']['bonus_operator'] : 'none';
        $equal_arr  = array(); //各指标顶洞情况.
        
        foreach ($kpi_result as $one_kpi_res) {
            
            if ($bonus_type == 'add') {
                $K_point_for_judge_win_or_fail = $one_kpi_res['K_point_after_add_bonus'];
            } else {
                $K_point_for_judge_win_or_fail = $one_kpi_res['K_point'];
            }
            
            if ($one_kpi_res['winner'] == 'A') {
                $equal_arr[$one_kpi_res['kpi_name']] = 'no';
                $A_win_counter++;
                $A_subtotal = $A_subtotal + $K_point_for_judge_win_or_fail;
                $B_subtotal = $B_subtotal - $K_point_for_judge_win_or_fail;
            }
            
            if ($one_kpi_res['winner'] == 'B') {
                $equal_arr[$one_kpi_res['kpi_name']] = 'no';
                $B_win_counter++;
                $A_subtotal = $A_subtotal - $K_point_for_judge_win_or_fail;
                $B_subtotal = $B_subtotal + $K_point_for_judge_win_or_fail;
            }
            
            if ($one_kpi_res['winner'] == '-') {
                $equal_arr[$one_kpi_res['kpi_name']] = 'yes';
            }
        }
        
        //顶洞配置:是否顶洞?  //4_xy_best_worst
        
        $raw_point = abs($A_win_counter - $B_win_counter);
        
        if ($A_subtotal > $B_subtotal) {
            $winner_after_summary = 'A';
            $draw                 = false;
        }
        
        
        //不发生输赢为顶:
        if ($A_subtotal == $B_subtotal) {
            $winner_after_summary = '-';
            $raw_point            = 0;
            $draw                 = true;
        }
        
        if ($A_subtotal < $B_subtotal) {
            $winner_after_summary = 'B';
            $draw                 = false;
        }
        


        
        $points_info = array(
            'draw' => $draw,
            'winner_after_summary' => $winner_after_summary,
            'A_side_point' => $A_subtotal,
            'B_side_point' => $B_subtotal,
            'K_winner_side_point' => max($A_subtotal, $B_subtotal),
            'sum_base_raw_point' => max($A_subtotal, $B_subtotal)
        );
        
        
        //A_subtotal +  B_subtotal 永远为0
        
        $tmp = max($A_subtotal, $B_subtotal);
        
        $this->ci->xlog->getInstance()->log("subtotal:A_subtotal:$A_subtotal  ,B_subtotal:$B_subtotal");
        $this->ci->xlog->getInstance()->log("总体赢方: $winner_after_summary,赢了 {$tmp}点", 3, 'subwinner');
        return $points_info;
    }
    
    
    
    function get_hole_money_info($if8421, $para, $index)
    {
        
        $unit    = $para['unit'];
        $kpi_tmp = $para['gamedata'][$index]['kpi_summary'];
        
        
        $winner_count                  = count($kpi_tmp['winners']);
        $failer_count                  = count($kpi_tmp['failers']);
        $winner_side_point_after_bonus = $kpi_tmp['K_winner_side_point_after_bonus'];
        
        $this_hole_actual_money = max($unit * $winner_side_point_after_bonus * $winner_count, $unit * $winner_side_point_after_bonus * $failer_count);
        
        if ($if8421) {
            $this_hole_meat_money = 0;
        } else {
            $this_hole_meat_money = $this->xmeat->get_hole_meat_money($para, $index);
        }
        
        
        $holeinfo = array(
            'winner_side_point_after_bonus' => $winner_side_point_after_bonus,
            'winner_count' => $winner_count,
            'failer_count' => $failer_count,
            'total_money_payed' => $this_hole_actual_money,
            'this_hole_meat_money' => $this_hole_meat_money //如果吃肉,本洞的"肉值"
        );
        
        return $holeinfo;
    }
    
    
    public function getNicknameByUid($uid)
    {
        $sql      = "select nickname from t_user where id=$uid";
        $row      = $this->ci->db->query($sql)->row_array();
        $nickname = $row['nickname'];
        return $nickname;
    }
    
    
    
  
    //计算一个游戏的结果
    function compute_one($para)
    {

        $if8421 = $this->x8421->checkif8421($para);
        
        //根据成绩排名
        $this->xabrank->set_init_rank($para);
        $this->xpreprocess->preprocss_with_weak_and_max($para);


        // debug($para);
        // die;

        $attender_num    = $para['playernum'];
        

        //$index是球洞数据索引,0为第一个洞数据.
        $index = 0;
        foreach ($para['gamedata'] as $one_hole_base_data) {
            
            //根据上洞成绩和上洞排名,决定本洞的选手排名
            $hole_name = $para['gamedata'][$index]['holename'];
            $par       = $para['gamedata'][$index]['par'];
            $this->ci->xlog->getInstance()->log("<br/>第" . ($index + 1) . "洞 $hole_name PAR:$par", 0, 'hole');
            
            $this->xabrank->get_rank_by_prev_score($para, $index);
            $this->xabrank->set_ab_by_rank(&$para,$index);

 

            $this->ci->xlog->getInstance()->logRedBlueGroup($para, $index, $para['gamedata'][$index]);
            $valid = $this->check_hole_valid($para, $index);
 

            //这里需要提前设置是否顶洞,以及顶住的项
            $this->process_kpis($para, $index, $valid);

            $this->ci->xlog->getInstance()->log_kpidone($para, $index);
            $base_money_info = $this->get_hole_money_info($if8421, $para, $index);
            
            $hole_should_pay_nomeat                     = $base_money_info['total_money_payed'];
            $base_money_pay_info                        = $this->xmoney->set_winner_failer_money($para, $index, $hole_should_pay_nomeat);
 


            //8421包洞处理.
            if($if8421&&$valid){
                $tuned_base_money_pay_info=$this->x8421->duty8421_process($para,$index,$base_money_pay_info);
            }else{
                $tuned_base_money_pay_info=$base_money_pay_info;
            }



            $list                                       = $this->xmoney->user_by_user_pay_info($tuned_base_money_pay_info, $valid);
            $para['gamedata'][$index]['base_money_log'] = $list;
            
            
            //ZZZ:计算吃肉时候,用什么成绩?(封顶完毕?还是封顶再让杆?)
            $ab_group_score_after_max = $this->xpreprocess->get_group_scores($para['gamedata'], $index, 'after_max');
            

            //8421顶洞处理:
            if($if8421){
                $this->x8421->raise8421price(&$para,$index);
            }


            //得到被吃掉的洞列表,以及被吃了几个.
            $money_eat_summary = $this->xmeat->get_money_eate_summary($para, $index, $valid, $ab_group_score_after_max);
            $eated_holes       = $money_eat_summary['eated_holes'];
            
            $number_eated = $money_eat_summary['number_eated'];
            $meat_money   = $number_eated * $base_money_info['this_hole_meat_money'];


            
            $this->xmeat->set_eaten($para, $eated_holes);
            $meat_money_pay_info                        = $this->xmoney->set_winner_failer_money_meat($para, $index, $meat_money);
            
            
            $eat_money_log                              = $this->xmoney->user_by_user_pay_info($meat_money_pay_info, null);
            $para['gamedata'][$index]['meat_money_log'] = $eat_money_log;


            //捐锅
             $tax_info=$this->xguo->getguoinfo($para,$index);


             $para['gamedata'][$index]['guo_log'] = array('guo_money'=>$tax_info['guo_money']);
             $para['gamedata'][$index]['base_money_log'] =$tax_info['base_money_log'];
             $para['gamedata'][$index]['meat_money_log'] =$tax_info['meat_money_log'];
           
            $index++;
        }
        
        $this->xmoney->merge_base_and_meat_money_log_with_initrank($para);
        $this->xtitle->reset_title($para);
        return $para;
    }


 
    
    
}

?> 