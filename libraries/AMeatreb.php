<?php


 /*
        meat_value_option:
        without_bonus (本洞输赢,不含奖励)
        with_bonus    (本洞输赢,含奖励)
        kpi_separate  (头吃头,尾吃尾)
*/



if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class AMeatreb
{
    public function __construct()
    {
        $this->ci =& get_instance();
        $CI =& get_instance();
        
        $CI->load->library('ABonusreb');
        $this->xbonus = $CI->abonusreb;
    }

 

   // 能否吃肉
    function can_eat_meat(){


    }


    //可以吃几块肉
    function find_number_can_eat($par, $winner, $ab_group_score, $meat_hole_num_option)
    {
        $winner_score = min($ab_group_score[$winner]); //ZZZZ 是否考虑让杆.
        $score_name=$this->xbonus->get_score_name($winner_score,$par);
        if( $score_name =='DoubleBogey'){
            $score_name ='Bogey+';   //因为配置的数据不一致.
        }        
        
        if( array_key_exists($score_name, $meat_hole_num_option) ){
            $number_to_eat=intval($meat_hole_num_option[$score_name]);
        }
        else
        {
            $number_to_eat = 0;
        }
       
       $this->ci->xlog->getInstance()->log("可以肉数量: $number_to_eat ", 3, 'subwinner');
       return $number_to_eat;
    }
   

    //取某洞的肉发生值(跟用户数相关)   
     function get_hole_meat_money($para, $index)
    {
        $unit            = $para['unit'];
        $meat_value_option = $para['meat_option']['meat_value_option'];
        
        $bonus_type = $para['bonus_option']['bonus_used'] == 'y' ? $para['bonus_option']['bonus_operator'] : 'none';
        
        $kpi_tmp      = $para['gamedata'][$index]['kpi_summary'];
        $winner_count = count($kpi_tmp['winners']);
        $failer_count = count($kpi_tmp['failers']);

        $weight_cfg= $para['weight_cfg'];
        $weight_array=explode(',',trim($weight_cfg));
        
        $winner_side_point_after_bonus = $kpi_tmp['K_winner_side_point_after_bonus'];
        $this_hole_actual_money        = max($unit * $winner_side_point_after_bonus * $winner_count, $unit * $winner_side_point_after_bonus * $failer_count);
        $this_hole_base_money          = max($unit * $kpi_tmp['sum_base_raw_point'] * $winner_count, $unit * $kpi_tmp['sum_base_raw_point'] * $failer_count);
          
        $this_hole_meat_money=0;

        if ($meat_value_option == 'without_bonus') {
             $this_hole_meat_money = $this_hole_base_money;
        }

        if ($meat_value_option == 'with_bonus') {
            $this_hole_meat_money = $this_hole_actual_money;
        }


        if ($meat_value_option == 'meat_as_tou') {

            $tou_money = $weight_array[0];
            $this_hole_meat_money=max($unit*$tou_money*$winner_count,$unit*$tou_money*$failer_count);

        }
        

        if ($meat_value_option == 'meat_as_wei') {
            $wei_money = $weight_array[1];
            $this_hole_meat_money=max($unit*$wei_money*$winner_count,$unit*$wei_money*$failer_count);
        }
        

        if ($meat_value_option == 'meat_as_zong') {
             $zong_money = $weight_array[1];
             $this_hole_meat_money=max($unit*$zong_money*$winner_count,$unit*$zong_money*$failer_count);
        }
         


        $this->ci->xlog->getInstance()->log("肉的发生的值: $this_hole_meat_money ", 3, 'subwinner');
        return $this_hole_meat_money;
    }
    
 


    
    function get_money_eate_summary($para, $index, $valid, $ab_group_score_max)
    {
        //计算吃几个洞,并返回吃调的洞的列表
        
        $kpi_summary    = $para['gamedata'][$index]['kpi_summary'];
        $this_hole_draw = $kpi_summary['drawinfo']['draw'];
        
        $meat_hole_num_option = $para['meat_option']['meat_hole_num_option'];
        $x_par           = $para['gamedata'][$index]['par'];
        
        if ($this_hole_draw) {
            $number_eated = 0;
            $eated_holes  = array();
            
        } else {
            $hole_num_to_eat = $this->find_number_can_eat($x_par, $kpi_summary['winner_after_summary'], $ab_group_score_max, $meat_hole_num_option);
            $eat_info     = $this->get_number_to_eat($para, $index, $hole_num_to_eat, $valid);
            $number_eated = $eat_info['number_eated'];
            $eated_holes  = $eat_info['eated_holes'];
        }
       
        return array(
            'eated_holes' => $eated_holes,
            'number_eated' => $number_eated
        );
    }
    
    
  
    
    function get_number_to_eat($para, $index, $number_to_eat, $valid)
    {
        
        if ((!$valid) || ($index == 0)) {
            return array(
                'number_eated' => 0,
                'eated_holes' => array()
            );
        }
         
        
        $eated_holes  = array();
        $number_eated = 0;
        for ($i = 0; $i < $index; $i++) {
            if (($para['gamedata'][$i]['kpi_summary']['drawinfo']['draw'] == true) && ($para['gamedata'][$i]['kpi_summary']['drawinfo']['eaten'] == false) && ($para['gamedata'][$i]['kpi_summary']['drawinfo']['valid'] == true) && ($number_eated < $number_to_eat)) {
                $para['gamedata'][$i]['kpi_summary']['drawinfo']['eaten'] = true;
                $eated_holes[]                                        = $i;
                $number_eated++;
            }
        }
        if ($number_eated > 0) {
            
            $this->ci->xlog->getInstance()->log("<span class=meat>吃了:$number_eated 块肉</span>", 3);
        }
        
        
        return array(
            'number_eated' => $number_eated,
            'eated_holes' => $eated_holes
        );
    }
    
    
    
    function set_eaten(&$para, $eated_holes)
    {
        foreach ($eated_holes as $key => $i) {
            $para['gamedata'][$i]['kpi_summary']['drawinfo']['eaten'] = true;
        }
    }
    
    
    
}

?> 