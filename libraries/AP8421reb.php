<?php


 /*
        8421特别处理
*/



if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class AP8421reb
{
    public function __construct()
    {
        $this->ci =& get_instance();
        $CI =& get_instance();
        
        $this->xindicator = $CI->aindicatorreb;
        $CI->load->library('AAbrankreb');
      
         
    }
 
  

  function checkif8421($para)
    {
        
        $if8421 = false;
        if ($para['kpi_cfg'][0] == 'kpi_case_8421') {
            $if8421 = true;
        }
        return $if8421;
    }
    
    
   function   duty8421_process($para,$index,$base_money_pay_info){

            //duty_all|duty_negative_points|noduty
               $winner=$para['gamedata'][$index]['kpi_summary']['winner_after_summary'];
               
                if($winner=='-'){
                    return $base_money_pay_info;
                 }

                 if($para['duty_8421'] == 'noduty'){
                   return $base_money_pay_info;
                 }

 
                 $detail_8421= $para['gamedata'][$index]['kpi_summary']['detail_8421'];
             
                 $failer =($winner=='A') ? 'B':'A';
               
                 $failer_money_pair=$base_money_pay_info['failers'];
                
 

                 $winner_money_pair=$base_money_pay_info['winners'];
 
                 $failer_8421=$detail_8421[$failer];

                 $player_1_id    = $failer_8421[0]['user'];
                 $player_2_id    = $failer_8421[1]['user'];
                 $player_1_scoreget = $failer_8421[0]['scoreget'];
                 $player_2_scoreget = $failer_8421[1]['scoreget'];

                 $be_punished_id=-1;

                 if (($player_1_scoreget >0 )&&( $player_2_scoreget<0)){
                   $be_punished_id=$player_2_id;
                   $good_one=$player_1_id;
                   $be_punished_score=$player_2_scoreget;

                 }

                 if (($player_2_scoreget >0 )&&( $player_1_scoreget<0)){
                   $be_punished_id=$player_1_id;
                   $good_one=$player_2_id;
                   $be_punished_score=$player_1_scoreget;
                 }

                 if($be_punished_id==-1){
                    return $base_money_pay_info;
                 }

                 
                 $public_failer_money=$failer_money_pair[0]['money'];
                 
                 if( $para['duty_8421'] == 'duty_all' ){
                    $new_failer_info=array(
                           array('uid'=>$good_one,'money'=>0),
                           array('uid'=>$be_punished_id,'money'=>2*$public_failer_money)
                        );
                 }

                 if( $para['duty_8421'] == 'duty_negative_points' ){
                    $new_failer_info=array(
                           array('uid'=>$good_one,'money'=> $public_failer_money +abs($be_punished_score) ),
                           array('uid'=>$be_punished_id,'money'=>$public_failer_money - abs($be_punished_score) )
                        );
                 }
                 $new_win_failer_info=array('winners'=>$winner_money_pair,'failers'=>$new_failer_info);
                 return  $new_win_failer_info;
   }


    function  raise8421price($para,$index){
 
        if (  (!$para['gamedata'][$index]['kpi_summary']['drawinfo']['draw']) && 
               ($para['gamedata'][$index]['kpi_summary']['drawinfo']['valid']) && 
               ($index > 0) && 
               (!($para['draw_8421'] == 'draw_ignore'))
            ) {
                
                $pre_raw_num = $this->find_previous_draw_num($para['gamedata'], $index);
            
                $factor8421=0;

                if ($pre_raw_num > 0) {
                    
                    if ($para['draw_8421'] == 'draw_addadd') {
                        $factor8421 = ($pre_raw_num + 1);
                    } else {
                        $factor8421 = pow(2, $pre_raw_num);
                    }
                }

                $this->ci->xlog->getInstance()->log("8421翻倍:前面打平$pre_raw_num 个洞,factor8421=>$factor8421", 2);
                foreach ($para['gamedata'][$index]['base_money_log'] as $k => $user_m) {
                    $user_m['money'] = $user_m['money'] * $factor8421;
                    $para['gamedata'][$index]['base_money_log'][$k] = $user_m;
                }
            }
    }


       public function find_previous_draw_num($holes, $index)
    {
        
        
        $pre_draw_num = 0;
        $i            = 0;
        for ($i = 1; $i <= $index; $i++) {
            
            $preindex = $index - $i;
            
            if ($holes[$preindex]['kpi_summary']['drawinfo']['draw']) {
                $pre_draw_num++;
            } else {
                break;
            }
        }
        return $pre_draw_num;
    }
    
    
    
    function get8421detail($ab_group_score_max_and_weak, $par, $para)
    {

        $if8421 = $this->checkif8421($para);
        
        if( !$if8421){
           return null;
        }

        $vt=$para['value_8421'];

        $a_group_score_max_and_weak = $ab_group_score_max_and_weak['A'];
        $b_group_score_max_and_weak = $ab_group_score_max_and_weak['B'];
        
        $a_detail = array();
        $b_detail = array();
        
        foreach ($a_group_score_max_and_weak as $user => $one_gross) {
            $tmp_8421   = $this->xindicator->sum8421($user, $one_gross, $par, $vt);
            $a_detail[] = array(
                'user' => $user,
                'scoreget' => $tmp_8421
            );
        }
        
        foreach ($b_group_score_max_and_weak as $user => $one_gross) {
            $tmp_8421   = $this->xindicator->sum8421($user, $one_gross, $par, $vt);
            $b_detail[] = array(
                'user' => $user,
                'scoreget' => $tmp_8421
            );
        }
        return array(
            'A' => $a_detail,
            'B' => $b_detail
        );
    }
    
    
    
}

?> 