<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}


class XGame  
{
    private $ci = null;
   
    public function __construct()
    {   
        $this->ci =& get_instance();
        $CI =& get_instance();
        include_once('/opt/space/webroot/ugf/api/web/controllers/gamefuncs.php');
    }
    
    
    
    public function debug($r)
    {   
        
        echo "<pre>";
        print_r($r);
        echo "</pre>";
    }
    
    public function relationship($me, $other)
    {   
        
        $relationship = 'NO_RELATIONSHIP';
        $sql          = "select status  from t_friend  where  userid=$me and fuserid=$other  ";
        $row          = $this->db->query($sql)->row_array();
        if (!$row) {
            $relationship = 'NO_RELATIONSHIP';
        } else {
            if ($row['status'] == 1) {
                $relationship = 'IS_FRIEND';
            }
            
            if ($row['status'] == 0) {
                $relationship = 'I_ASK_OTHER';
            }
        }
        $sql = "select status  from t_friend  where  userid=$other and fuserid=$me  and status=0 ";
        $row = $this->db->query($sql)->row_array();
        $row = $this->db->query($sql)->row_array();
        if ($row) {
            $relationship = 'OTHER_ASK_ME';
        }
        return $relationship;
    }
    
    public function get_non_inner_game_groupid($gameid) {
        
               $this->ci->load->database();
        $sql                = "select groupid from t_game_group  where gameid=$gameid limit 1 ";
        $game_group_summary = $this->ci->db->query($sql)->row_array();
        $groupid            = $game_group_summary['groupid'];
        return $groupid;
    }
    
    
    public function get_game_play_list($gameinfo, $agent_userid)
    {   
        
        $palyer_list = array();
        $gameid      = $gameinfo['gameid'];
        $gametype    = $gameinfo['gametype'];
        $gamestate   = $gameinfo['gamestate'];
        
        if ($gamestate == GAME_REGISTER_ENDED || $gamestate == GAME_INITED) {

            $palyer_list = $this->get_game_applyer_info($gameid);
        } 
        else 
        {
            $sql     = "select userid  from t_game_group_user where gameid=$gameid";

            $palyers = $this->ci->db->query($sql)->result_array();
            foreach ($palyers as $one_palyer) {
                $userid        = intval($one_palyer['userid']);
                $palyer_list[] = $this->user_info($userid);
            }
        }
        
        array_walk($palyer_list, array(
            $this,
            'setting_alt_nickname'
        ), array(
            'gameid' => $gameid,
            'userid' => $agent_userid
        ));
       

        return $palyer_list;
    }

    
    public function game_summary($gameid)
    {   
        
        $sql          = "select t_game.*,t_course.name as course_name from t_game,t_course where gameid=$gameid and t_course.courseid=t_game.courseid";
        // echo $sql;
        // die;
        
        $game_summary = $this->ci->db->query($sql)->row_array();
        // pre_starttime  apply_stoptime  pre_startdate pre_starttime apply_stopdate  apply_stoptime


        $pre_starttime=$game_summary['pre_starttime'];
        $apply_stoptime=$game_summary['apply_stoptime'];
        $aa=explode(" ",$pre_starttime);
        $game_summary['pre_starttime_d']=$aa[0];
        $game_summary['pre_starttime_t']=$aa[1];
        
        $aa=explode(" ",$apply_stoptime);
        $game_summary['apply_stoptime_d']=$aa[0];
        $game_summary['apply_stoptime_t']=$aa[1];



      
        return $game_summary;
    }
    
    
    public function user_info($userid)
    {   
        
        $sys_path = SERVER_PIC_DIR;
        $this->ci->load->database();
        $sql = "select id as userid,nickname,mobile,credit,experience,rank,concat($sys_path,coverpath,'/c240_',covername) as user_picurl from t_user where id=$userid";
        $user_info = $this->ci->db->query($sql)->row_array();
        return $user_info;
    }

    public function caddie_info($userid)
    {   
        
        $sys_path = SERVER_PIC_DIR;
        $this->ci->load->database();
        $sql = "select id as userid,nickname,concat($sys_path,coverpath,'/c240_',covername) as user_picurl from t_c_user where id=$userid";
        $caddie_info = $this->ci->db->query($sql)->row_array();
        return $caddie_info;
    }
    
    
    public function is_team_admin($team_id, $userid)
    {   
        
     
        $sql = "select userid from t_team_admins where teamid=$team_id and userid=$userid";
        $row = $this->ci->db->query($sql)->row_array();
        return count($row);
    }
    
    
    public function is_userid_in_players($palyer_list, $userid)
    {   
        
        $haveme      = false;
        $players_ids = array_retrieve($palyer_list, 'userid');
        if (in_array($userid, $players_ids)) {
            $haveme = true;
        }
        return $haveme;
    }
    
    public function get_game_players_all_text($group_info, $gametype, $team_name)
    {   
        
        if ($gametype == GAME_TYPE_INNER) {
            $players = $team_name;
            
        } else {
            $players         = '';
            $group_nicknames = array();
            foreach ($group_info as $one_group) {
                $group_users = $one_group['group_user'];
                foreach ($group_users as $one_play_info) {
                    $players .= $one_play_info['nickname'] . ",";
                }
            }
            $players = rtrim($players, ',');
        }
        return $players;
    }
    
    
    public function get_mask_groupid($gametype, $gameid)
    {   
        
        if ($gametype == GAME_TYPE_INNER) {
            $mask_groupid = -1;
        } else {
            $sql                = "select groupid from t_game_group  where gameid=$gameid limit 1 ";
            $game_group_summary = $this->ci->db->query($sql)->row_array();
            $mask_groupid       = $game_group_summary['groupid'];
        }
        return $mask_groupid;
    }
    
    public function get_game_pic($gameinfo, $team_info)
    {   
        
        $sys_path = str_replace("'", "", SERVER_PIC_DIR);
        if ($gameinfo['gametype'] == GAME_TYPE_INNER) {
            $game_pic = $sys_path . $team_info['coverpath'] . '/' . $team_info['covername'];
        } else {
            $game_pic = $sys_path . $gameinfo['game_group_coverpath'] . '/' . $gameinfo['game_group_covername'];
        }
        return $game_pic;
    }
    

    public function get_game_court_info($gameid){
        $sql="select t_game_court.courtid,courtname,court_key  from t_course_court,t_game_court ";
        $sql.=" where gameid=$gameid and  t_game_court.courtid=t_course_court.courtid ";
        $court_info              = $this->ci->db->query($sql)->result_array();
        return $court_info;
    }



   
    
     
    
    public function create_game_and_init_group($para)
    {   
        
        $this->ci->load->database();
        $random_no ='weixin';  //????  weixin?
        $pre_starttime = $para['pre_starttime'];
        $apply_stoptime = $para['apply_stoptime'];

        if(strlen($apply_stoptime) < 1){
            $apply_stoptime = $pre_starttime;
        }
        
        if($para['userid']==0)
        {
            return;
        }

        $game = array(
            'name' => $para['name'],
            'courseid' => $para['courseid'],
            'privacy' => $para['privacy'],
            'cost' => $para['cost'],
            'remark' => $para['remark'],
            'slongitude' => $para['slongitude'],
            'slatitude' => $para['slatitude'],
            'teamid' => $para['teamid'],
            'pre_starttime' => $pre_starttime,
            'apply_stoptime' => $apply_stoptime,
            'gamestate' => 1,  //define("GAME_REGISTER_ENDED", 1); //报名完成
            'gametype' => 1,
            'gamerule' => $para['gamerule'],
            'create_ip' => $para['agent_ip'],
            'creatorid' => $para['userid'],
            'adminid' => $para['userid'],
            'creat_time' => date('Y-m-d H:i:s'),
            'offline_gameid' => $para['offline_gameid'],
            'invite_caddie_mode'=>$para['invite_caddie_mode'],
            'reward' => $para['reward'],
            'playrule' => $para['playrule'],
            'mobile' => $para['mobile'],
            'random_no' => $random_no,
        );
        
        $this->ci->db->insert('t_game', $game);
        $gameid = mysql_insert_id();

        $court_setting=array('gameid'=> $gameid ,'court_info'=>$para['court_info']);
        $this->set_game_court($court_setting);

        $newgame = array(
            'gameid' => $gameid,
            'random_no' => $random_no
        );
        return $newgame;
    }
    
    public function set_game_court($para)
    {   
        
        $this->ci->load->database();
        $gameid    = $para['gameid'];
       
        if ((array_key_exists('court_info', $para)) && (strlen($para['court_info']) > 0)) {
            $court_info = $para['court_info'];
            $court_info = array_filter(explode(",", $court_info));
            $this->ci->db->delete('t_game_court', array(
                'gameid' => $gameid
            ));
            $t_game_court = array();
            foreach ($court_info as $key => $one_court) {
                $index          = $key + 1;
                $t_game_court[] = array(
                    'gameid' => $gameid,
                    'courtid' => $one_court,
                    'court_key' => $index
                );
            }
            $this->ci->db->insert_batch('t_game_court', $t_game_court);
        }
    }
    
    public function set_game_attenders($para)
    {   
        
        $this->ci->load->database();
        $gameid    = $para['gameid'];
        $attenders = trim($para['attenders']);
        if($gameid < 1){
            return;
        }
        $sql = "delete from t_game_score where gameid=$gameid";
        $this->ci->db->query($sql);

        $sql = "delete from t_game_group_user where gameid=$gameid  ";
        $this->ci->db->query($sql);
        
        $sql = "delete from t_game_group where gameid=$gameid  ";
        $this->ci->db->query($sql);
        
        $group_users_array = explode('|', $attenders);
        foreach ($group_users_array as $one_group) {
            $arr = array(
                'gameid' => $gameid,
                'groupid' => $groupid,
                'random_no' => $para['random_no'],
                'group_name' => '',
                'group_create_time' => date('Y-m-d H:i:s')
            );
            
            $this->ci->db->insert('t_game_group', $arr);
            $inserted_group_id = $this->ci->db->insert_id();
            $users_array       = explode(',', $one_group);
            foreach ($users_array as $one_group_user) {
                $arr = array(
                    'gameid' => $gameid,
                    'groupid' => $inserted_group_id,
                    'userid' => $one_group_user,
                    'random_no' => $para['random_no'],
                    'addtime' => date('Y-m-d H:i:s')
                );
                $this->ci->db->insert('t_game_group_user', $arr);
            }
        }
        
    }
    
    public function set_game_attender_tland($para)
    {   
        
        $this->ci->load->database();
        $attender_tland      = trim($para['attender_tland']);
        $attender_tland_list = array_filter(explode("|", $attender_tland));
        foreach ($attender_tland_list as $one) {
            $detail = array_filter(explode(",", $one));
            $this->ci->db->where(array(
                'gameid' => $para['gameid'],
                'userid' => $detail[0]
            ));
            $this->ci->db->update('t_game_group_user', array(
                'tland_chose' => $detail[1]
            ));
            $this->ci->db->where(array(
                'gameid' => $para['gameid'],
                'userid' => $detail[0]
            ));
            $this->ci->db->update('t_game_score', array(
                'tland' => $detail[1]
            ));
        }
    }
    
    public function m_get_group_info($gameid, $userid)
    {   
        
        $this->ci->load->database();
        $sys_path  = SERVER_PIC_DIR;
        $sql_group = "";
        $sql_group .= "select groupid,group_name,group_start_status,group_slogan ";
        $sql_group .= " from t_game_group";
        $sql_group .= " where gameid=$gameid";
        $groups = $this->ci->db->query($sql_group)->result_array();
        $index  = 0;
        foreach ($groups as $one_group) {
            $groupid        = $one_group['groupid'];
            $sql_group_user = "";
            
            $sql_group_user = "select  userid,nickname as username,nickname, ";
            $sql_group_user .= "concat($sys_path,t_user.coverpath,'/c240_',t_user.covername) as user_picurl,  ";
            $sql_group_user .= " confirmed,confirmed_time,admin";
            $sql_group_user .= " from t_game_group_user,t_user";
            $sql_group_user .= " where  t_game_group_user.groupid=$groupid";
            $sql_group_user .= "   and t_user.id=t_game_group_user.userid";
            $group_user = $this->ci->db->query($sql_group_user)->result_array();
            
            array_walk($group_user, array(
                $this,
                'setting_alt_nickname'
            ), array(
                'gameid' => $gameid,
                'userid' => $userid
            ));
            $groups[$index]['group_user'] = $group_user;
            $index++;
        }
       // $groups = $this->check_caddie_is_ingroup($groups,$userid);

        return $groups;
    }
    
    
    public function setting_alt_nickname(&$one_player, $key, $arr)
    {   
        
        $alter_userid           = array_key_exists('userid', $one_player) ? $one_player['userid'] : $one_player['id'];
        $player_new_nickname    = $this->judge_one_player_nickname($alter_userid, $one_player['nickname'], $arr['userid'], $arr['gameid']);
        $one_player['nickname'] = $player_new_nickname;
    }
    
    
    
    public function judge_one_player_nickname($playerid, $player_nickname, $userid, $gameid)
    {   
        
        
        $game_summary = $this->game_summary($gameid);
        $gametype     = $game_summary['gametype'];
        $teamid       = $game_summary['teamid'];
        if (intval($teamid) == 0) {
            $teamid = -1;
        }
        
        $player_new_nickname = "";
        $is_team_user        = $this->is_team_user($teamid, $userid);
        $applyer_nickname    = $this->get_game_applyer_nickname($gameid, $playerid);
        $friend_nickname     = $this->get_friend_nickname($userid, $playerid);
        $team_user_nickname  = $this->get_team_user_nickname($teamid, $playerid);
        
        
        if (strlen($friend_nickname) > 0) {
            $player_new_nickname = $friend_nickname;
        }
        
        if ($is_team_user) 
          {
                if (strlen($team_user_nickname) > 0) {
                    $player_new_nickname = $team_user_nickname;
                }
          }

            if (strlen($applyer_nickname) > 0) {
                $player_new_nickname = $applyer_nickname;
            }
        
         
        
        if (strlen(trim($player_new_nickname)) == 0) {
            $player_new_nickname = $player_nickname;
        }

        

        $sql="select realname from t_user_info where userid=$playerid" ;

        $row       = $this->ci->db->query($sql)->row_array();
        $realname=$row['realname'];

        if( strlen($realname)>0){
            $player_new_nickname=$realname ;
        }

        if (strlen(trim($applyer_nickname))>0) {
             $player_new_nickname = $applyer_nickname;
        }

        return $player_new_nickname;
 
    }
    
    
    
     



    public function m_get_my_friends_public_games_by_gamestate($userid, $type,$gamestate)
    {   
        
     
        $GAME_PUBLIC         = GAME_PUBLIC;
        $GAME_NOTPUBLIC      = GAME_NOTPUBLIC;
        $friend_status       = IS_GAME_FRIEND;
        $friend_str          = $this->m_friends($userid);
        $this->ci->load->database();
        
        $sql1 = "select starttime,gametype, t_game.gameid,t_game.name from t_game where gamestate=$gamestate";
        $sql1 .= " and privacy=$GAME_PUBLIC and  gameid in ";
        $sql1 .= "(select gameid from  t_game_group_user where userid in ($friend_str))";
        $sql1 .= " and gameid not in (select gameid from  t_game_group_user where userid=$userid)";
        
        $sql2 = "select starttime,gametype,t_game.gameid,t_game.name from t_game where gamestate=$gamestate";
        $sql2 .= " and privacy<>$GAME_NOTPUBLIC and  gameid in ";
        $sql2 .= "(select gameid from  t_game_group_user where userid=$userid )";
        
        
        $GAME_ATTENTION_PUBLIC = GAME_ATTENTION_PUBLIC;
        $friend_status         = IS_GAME_ATTENTION;
        $this->ci->load->database();
        $sql3 = "select starttime,gametype,t_game.gameid,t_game.name from t_game where gamestate=$gamestate";
        $sql3 .= " and privacy=$GAME_ATTENTION_PUBLIC and  gameid in ";
        $sql3 .= "(select gameid from  t_game_group_user where userid in ( select fuserid from t_friend where userid=$userid and status=$friend_status))";
        $sql3 .= " and gameid not in (select gameid from  t_game_group_user where userid=$userid)";
        $sql3 .= "order by  starttime desc, gametype";
        
        
        
        if ($type == 'all') {
            if (strlen($friend_str) > 0) {
                $sql = $sql1 . " union " . $sql2 . " union " . $sql3;
            } else {
                $sql = $sql1 . " union " . $sql2;
            }
        } else {
            if (strlen($friend_str) > 0) {
                $sql = $sql1 . " union " . $sql2;
            } else {
                $sql = $sql2;
            }
        }
        $rows       = $this->ci->db->query($sql)->result_array();
        $rows_fixed = $this->fix_game_detail($rows, $userid);
        return $rows_fixed;
    }

 
     

    
    public function push_route($receiver, $action, $uid, $note, $markname, $id = -1)
    {   
        
        
        include_once('/opt/space/webroot/ugf/api/web/controllers/libs/push/xgpush.php');
        $pushcfg = array(
            'add' => array(
                'memo' => '创建球队',
                'title' => '创建了球队',
                'module' => 'team',
                '_uid' => 'teamid'
                
            ),
            
            //'MODTG' => array(
            'editgame' => array(
                'memo' => '修改队内比赛',
                'title' => '修改队内比赛',
                'module' => 'team',
                '_uid' => 'gameid'
            ),
            
            'ADDYQ' => array(
                'memo' => '创建约球',
                'title' => '约球了',
                'module' => 'user',
                'module' => 'user',
                '_uid' => 'gameid'
            ),
            
            'CANCELG' => array(
                'memo' => '取消比赛',
                'title' => '比赛取消了',
                'module' => 'user',
                'module' => 'user',
                '_uid' => 'gameid'
            ),
            
            'INSTG' => array(
                'memo' => '立即比赛',
                'title' => '立即比赛',
                'module' => 'user',
                '_uid' => 'gameid'
            ),
            
            'MODSCORE' => array(
                'memo' => '成绩修改了',
                'title' => '成绩修改了',
                'module' => 'user',
                '_uid' => 'gameid'
            ),
            
            'gamechat' => array(
                'memo' => '修改了成绩',
                'title' => '修改了成绩',
                'module' => 'user',
                '_uid' => 'gameid'
            ),
            
            'MODIYQ' => array(
                'memo' => '修改约球',
                'title' => '修改了约球',
                'module' => 'user',
                '_uid' => 'gameid'
            ),
            
            //'ADDTG' => array(
            'addgame' => array(
                'memo' => '创建对内比赛',
                'title' => '创建了队内比赛',
                'module' => 'team',
                '_uid' => 'teamid',
                'id' => 'gameid'
            )
            
        );
        
        // $num = func_num_args();
        $pc = array();
        $pc['action'] = $action;
        $pc['uid']    = $uid;
        $pc['module'] = $pushcfg[$action]['module'];
        if ($id > 0) {
            $pc['id'] = $id;
        }
        
        if (!empty($note)) {
            $title = $note['note'];
            unset($note['desc']);
            $pc['content'] = json_encode($note);
        } else {
            $title = "创建一场比赛";
        }
        
        $pc['title'] = $markname;
        $pc['descr'] = $title;
        $pret = pushSingleUserMsg($receiver, $markname . ':' . $title, json_encode($pc));
        
    }
     
  
    public function get_team_game_by_status($teamid,$userid, $status)
    {   
        
        $GAME_STATUS = $status;
        $this->ci->load->database();
        $sql        = "select t_game.gameid,t_game.name from t_game where gametype=1 and gamestate=$GAME_STATUS  and teamid=$teamid order by t_game.gameid desc";
        $rows       = $this->ci->db->query($sql)->result_array();
        $rows_fixed = $this->fix_game_detail($rows,$userid);
        return $rows_fixed;
    }


      public function game_detail($gameid, $userid = -1)
    {

        $this->ci->load->database();
        $ret = array();
        $game_info = $this->game_summary($gameid);
        $team_id  = intval($game_info['teamid']);
        $gametype = intval($game_info['gametype']);
        $game_info['isadmin'] = $this->is_team_admin($team_id, $userid);
        $palyer_list = $this->get_game_play_list($game_info, $userid);

        $game_info['court_info'] = $this->get_game_court_info($gameid);
        $group_info              = $this->m_get_group_info($gameid, $userid);
      
        if ($team_id > 0) {
            $sys_path = str_replace("'", "", SERVER_PIC_DIR);
            $sql       = "select * from t_team where teamid=$team_id";
            $team_info = $this->ci->db->query($sql)->row_array();
            $team_info['team_picurl'] = $sys_path . $team_info['coverpath'] .'/'.$team_info['covername'];
            $team_name = $team_info['team_name'];
            $sql = "select id from t_team_admins where teamid=$team_id and userid=$userid";
            $id = $this->ci->db->query($sql)->row_array();
            if(isset($id['id'])){
                $ret['is_team_admin'] = true;
            }else{
                $ret['is_team_admin'] = false;
            }
        } else {
            $team_info = null;
            $team_name = '';
        }

        $sql = "select count(id) as cont_squad from t_game_squad where gameid=$gameid";
        $game_squads = $this->ci->db->query($sql)->row_array();

        if(intval($game_squads['cont_squad']) > 0){
            $result = $this->get_game_squad($gameid,$userid);
            $ret['game_squads']          = $result['game_squads'];
            $ret['game_squad_users']          = $result['game_squad_users'];
        }

        $creator_info = $this->user_info($game_info['creatorid']);
        
        $applyer_list = $this->get_game_applyers($gameid,$userid);
        $players_text = $this->get_game_players_all_text($group_info, $gametype, $team_name);
        $mask_groupid = $this->get_mask_groupid($gametype, $gameid);
        
        $game_info['picurl'] = $this->get_game_pic($game_info, $team_info);
        $ret['error_code']           = 1;
        $ret['error_descr']          = 'success';
        $ret['game_info']            = $game_info;
        $ret['creator_info']         = $creator_info;
        $ret['player_num']           = count($palyer_list);
        $ret['group_info']           = $group_info;
        $ret['team_id']              = $team_id;
        $ret['team_info']            = $team_info;
        $ret['gametype']             = intval($game_info['gametype']);
        $ret['game_info']['players'] = $players_text;
        $ret['mask_groupid']         = $mask_groupid;
        $ret['palyer_list']          = $palyer_list;
        $ret['applyer_list']          = $applyer_list;
        $ret['haveme']               = $this->is_caddie_in_game_caddies($gameid, $userid);
        if(isset($ret['game_squad_users'])){
            $ret['no_checkin_user_list'] = $this->get_squad_no_checkin_user_list($ret['game_squad_users']);
        }
        
        return $ret;
        
    }

    public function get_game_applyers($gameid,$userid){
        $sys_path = SERVER_PIC_DIR;
        $this->ci->load->database();

        $sql=" select t_user.id as userid, t_user_info.mobile as info_m, t_user.mobile as user_m, nickname,realname,concat($sys_path,t_user.coverpath,'/c240_',t_user.covername) as user_picurl,t_user_info.gender,handicap,t_game_applyer.checkin_status,t_game_applyer.pay_status ";
        $sql.= " from t_user,t_user_info,t_game_applyer where gameid=$gameid and t_user_info.userid=t_user.id  ";
        $sql.=" and t_game_applyer.userid=t_user.id order by t_game_applyer.id asc " ;
        $game_applyers = $this->ci->db->query($sql)->result_array();
        


        foreach ($game_applyers as $key => $one ) {
              
              $mobile='';  
              if( strlen($one['user_m']>0) ){
                  $mobile=$one['user_m'];
                }
                


                if( strlen($one['info_m']>0) ){
                  $mobile=$one['info_m'];
                }
                
                $game_applyers[$key]['mobile']=$mobile;

                if( strlen($one['realname'])>0 ){
                  $game_applyers[$key]['nickname']=$one['realname'];
                }
               
        }
        
         


        array_walk($game_applyers, array(
            $this,
            'setting_alt_nickname'
        ), array(
            'gameid' => $gameid,
            'userid' => $userid
        ));

        return $game_applyers;
    }


    public function get_game_squad($gameid,$userid){
        $sys_path = SERVER_PIC_DIR;
        $game_squad_user_array = array();
        $this->ci->load->database();
        $sql = "select * from t_game_squad where gameid=$gameid";
        $game_squads = $this->ci->db->query($sql)->result_array();
        foreach ($game_squads as $key => $one_squad) {
            $squadid = intval($one_squad['id']);
            $squad_name = trim($one_squad['squad_name']);
            $squad_picurl = '';
            if(isset($one_squad['teamid'])){
                $teamid = $one_squad['teamid'];
                $sql = "select team_name,concat($sys_path,coverpath,'/',covername) as team_picurl from t_team where teamid=$teamid";
                $team_info = $this->ci->db->query($sql)->row_array();
                $squad_name = trim($team_info['team_name']);
                $squad_picurl = trim($team_info['team_picurl']);
            }
            $game_squads[$key]['squad_picurl'] = $squad_picurl;
            $game_squad_user_array[$key]['squad_name'] = $squad_name;
            $game_squad_user_array[$key]['squad_picurl'] = $squad_picurl;
            $game_squad_user_array[$key]['squad_user_list'] = $this->get_game_squad_users($gameid,$squadid,$userid);
        }
        $not_in_squad_users = $this->get_not_in_game_squad_players($gameid,$userid);
        $guest_squad = array('squad_name'=>'嘉宾','squad_user_list'=>$not_in_squad_users,'squad_picurl'=>'');
        $game_squad_user_array[] = $guest_squad;
        return array('game_squads'=>$game_squads,'game_squad_users'=>$game_squad_user_array);
    }



    public function get_not_in_game_squad_players($gameid,$userid){
        $sys_path = SERVER_PIC_DIR;
        $this->ci->load->database();

        $sql = "select t_user.id as userid, t_user_info.mobile as info_m, t_user.mobile as user_m, nickname,realname,concat($sys_path,t_user.coverpath,'/c240_',t_user.covername) as user_picurl,t_user_info.gender,handicap ";
        $sql .= " from t_user,t_user_info,t_game_applyer where gameid=$gameid and t_user_info.userid=t_user.id  and t_game_applyer.userid not in (select userid from t_game_squad_user where gameid=$gameid) ";
        $sql .= " and t_game_applyer.userid=t_user.id order by t_game_applyer.id asc  ";
        $game_applyers = $this->ci->db->query($sql)->result_array();
        


        foreach ($game_applyers as $key => $one ) {
              $playerid = intval($one['userid']);
              $sql = "select checkin_status,pay_status from t_game_applyer where gameid=$gameid and userid=$playerid ";
              $player_checkin_pay_status = $this->ci->db->query($sql)->row_array();
              $game_applyers[$key]['checkin_status'] = $player_checkin_pay_status['checkin_status'];
              $game_applyers[$key]['pay_status'] = $player_checkin_pay_status['pay_status'];
              $mobile='';  
              if( strlen($one['user_m']>0) ){
                  $mobile=$one['user_m'];
                }
                


                if( strlen($one['info_m']>0) ){
                  $mobile=$one['info_m'];
                }
                
                $game_applyers[$key]['mobile']=$mobile;

                if( strlen($one['realname'])>0 ){
                  $game_applyers[$key]['nickname']=$one['realname'];
                }
               
        }
        
         


        array_walk($game_applyers, array(
            $this,
            'setting_alt_nickname'
        ), array(
            'gameid' => $gameid,
            'userid' => $userid
        ));

        return $game_applyers;
    }




    public function get_game_squad_users($gameid,$squadid,$userid){
        $sys_path = SERVER_PIC_DIR;
        $this->ci->load->database();

        $sql = "select t_user.id as userid, t_user_info.mobile as info_m, t_user.mobile as user_m, nickname,realname,concat($sys_path,t_user.coverpath,'/c240_',t_user.covername) as user_picurl,t_user_info.gender,handicap  ";
        $sql .= " from t_user,t_user_info,t_game_squad_user "; 
        $sql .= " where gameid=$gameid and t_user_info.userid=t_user.id  and t_game_squad_user.userid=t_user.id and  t_game_squad_user.squadid=$squadid order by t_game_squad_user.addtime asc ";
        $game_applyers = $this->ci->db->query($sql)->result_array();
        


        foreach ($game_applyers as $key => $one ) {
              $playerid = intval($one['userid']);
              $sql = "select checkin_status,pay_status from t_game_applyer where gameid=$gameid and userid=$playerid ";
              $player_checkin_pay_status = $this->ci->db->query($sql)->row_array();
              $game_applyers[$key]['checkin_status'] = $player_checkin_pay_status['checkin_status'];
              $game_applyers[$key]['pay_status'] = $player_checkin_pay_status['pay_status'];
              $mobile='';  
              if( strlen($one['user_m']>0) ){
                  $mobile=$one['user_m'];
                }
                


                if( strlen($one['info_m']>0) ){
                  $mobile=$one['info_m'];
                }
                
                $game_applyers[$key]['mobile']=$mobile;

                if( strlen($one['realname'])>0 ){
                  $game_applyers[$key]['nickname']=$one['realname'];
                }
               
        }
         


        array_walk($game_applyers, array(
            $this,
            'setting_alt_nickname'
        ), array(
            'gameid' => $gameid,
            'userid' => $userid
        ));

        return $game_applyers;
    }



    public function is_caddie_in_game_caddies($gameid,$caddieid){
          $this->ci->load->database();
          $caddie_record_score_status = CADDIE_ACCEPT;
          $sql = "select id from t_c_caddie_game where caddieid=$caddieid and status=$caddie_record_score_status and gameid=$gameid";
          $id = $this->ci->db->query($sql)->row_array();
          if(intval($id['id']) == 0){
                return false;
          }else{
                return true;
          }
    }
     

     public function fix_game_detail($rows, $userid)
    {   
       
        $ret = array();
        for ($i = 0; $i < count($rows); $i++) {
            $one_game  = $rows[$i];
            $gameid    = $one_game['gameid'];
            $game_info = $this->game_detail($gameid, $userid);
            $ret[]     = $game_info;
        }
        return $ret;
    }
 
     
    
    //差点
    public function handicap($userid)
    {
        
        $this->ci->load->database();
        
        $user_game_info = $this->get_handicap_and_game_score_by_user($userid);

        usort($user_game_info, array($this,'user_game_score_sort'));

        $game_count = count($user_game_info);
        $configures = array('1'=>array(5,6),
                           '2'=>array(7,8),
                           '3'=>array(9,10),
                           '4'=>array(11,12),
                           '5'=>array(13,14),
                           '6'=>array(15,16),
                           '7'=>array(17),
                           '8'=>array(18),
                           '9'=>array(19),
                           '10'=>array(20)
                          );
        if(($game_count) < 5 && ($game_count != 0)){
            $game_num = 1;
        }else{
            $game_num = 0;
            foreach ($configures as $key => $one_configure) {
                if(in_array($game_count, $one_configure)){
                    $game_num = $key;
                    break;
                }
            }
        }
        $sumgross = 0;
        $sumpar = 0;
        $gameidnum      = 0;
        foreach ($user_game_info as $key => $one_game) {
            if($gameidnum == $game_num){
                break;
            }
          
            $sumpar = $sumpar+intval($one_game['sumpar']);
            $sumgross = intval($one_game['user_game_score'])+$sumgross;
            $gameidnum++;
        }


        if (count($user_game_info) < 1) {
            $handicap = -6;
        } else {
             $total_gancha = $sumgross-$sumpar;
             $handicap = ($total_gancha/$gameidnum)*0.96;
             $handicap_array = explode('.', $handicap);
             if(count($handicap_array) > 1){
                $last_handicap_array = $handicap_array[1];
                $last_handicap_value = substr($last_handicap_array,0,1);
                $handicap = $handicap_array[0].'.'.$last_handicap_value;
              }else{
                $handicap = $handicap_array[0].'.0';
              }
        }

        $this->ci->db->where('id', $userid);
        
        
        if ($handicap < -5) {
            $this->ci->db->update('t_user', array(
                'handicap' => null
            ));
        } else {
            $this->ci->db->update('t_user', array(
                'handicap' => $handicap
            ));
        }
    }


    public function get_handicap_and_game_score_by_user($userid)
    {

        $this->ci->load->database();
        // $sql = "select t_game_score.gameid,sum(t_game_score.gross) as grossnum,min(t_game_score.gross) as grossmin,sum(t_game_score.par) as sumpar,count(t_game_score.par) as holenum,t_game.courseid,t_game.starttime,t_course.name ";
        // $sql .= " from t_game_score,t_game,t_course ";
        // $sql .= " where t_game_score.userid=$userid and t_game_score.score_status=1 and t_game.gameid in (t_game_score.gameid) ";
        // $sql .= " and t_game.gamestate=4 and t_course.courseid in (t_game.courseid) group by t_game_score.gameid order by t_game.starttime desc ";
        
        $gamestate = GAME_ENDED;
        $sql = "select sum(gs.gross) as user_game_score,min(gs.gross) as grossmin,count(gs.holeid) as hole_num,sum(gs.par) as sumpar   ";
        $sql .= "from t_game_group_user as gu,t_game as g,t_game_score as gs ";
        $sql .= "where gu.userid=$userid and g.gameid=gu.gameid and g.gamestate=4 and gs.gameid=g.gameid and gs.userid=$userid group by gu.gameid having grossmin<>0 and hole_num=18  order by g.starttime desc limit 20 ";


        $user_game_info = $this->ci->db->query($sql)->result_array();
        return $user_game_info;
    }
     

    public function m_get_my_team_game($userid)
    {   
        
        $gametype_yueqiu             = GAME_TYPE_YUEQIU;
        $gametype_team               = GAME_TYPE_INNER;
        $ret                         = array();
        
        $in_gamestate     = GAME_INITED . ',' . GAME_REGISTER_ENDED . ',' . GAME_GROUPED;
       
        $this->ci->load->database();
        $sql        = "select gameid  from t_game where gameid in (select gameid from t_game_applyer where userid=$userid) ";
        $sql .= " and gametype in ($gametype_yueqiu,$gametype_team) and gamestate   in ($in_gamestate) order by UNIX_TIMESTAMP(pre_starttime) desc ";
        $rows       = $this->ci->db->query($sql)->result_array();
        $rows_fixed = $this->fix_game_detail($rows, $userid);
        return $rows_fixed;
    }


    
   
    
    public function m_friends($userid)
    {   
        
        $friend_status = IS_GAME_FRIEND;
        $m_friends_str = "";
        $this->ci->load->database();
        //ZZZ
        //$sql = "select id from t_user where id in  (  select fuserid from t_friend where userid=$userid and status=$friend_status ) and type <>2";
        $sql = "select id from t_user where id in  (  select fuserid from t_friend where userid=$userid and status=$friend_status ) and type <>2";
        
        $fuserids = $this->ci->db->query($sql)->result_array();
        if (count($fuserids) == 0) {
            return '-1';
        }
        $friend_str = array_to_string($fuserids, 'id');
        return $friend_str;
    }

 

    
    public function get_team_user_nickname($teamid, $userid)
    {   
        
        $this->ci->load->database();
        $sql = "select team_user_nickname from t_team_user where teamid=$teamid and userid=$userid";
        
        $team_user_nickname = $this->ci->db->query($sql)->row_array();
        $user_team_nickname = trim($team_user_nickname['team_user_nickname']);
        return $user_team_nickname;
    }
    
    public function get_game_applyer_nickname($gameid, $userid)
    {   
        
        $this->ci->load->database();
        $sql = "select username from t_game_applyer where gameid=$gameid and userid=$userid";
        
        
        $game_user_nickname = $this->ci->db->query($sql)->row_array();
        $user_game_nickname = trim($game_user_nickname['username']);
        return $user_game_nickname;
    }
    
    public function get_friend_nickname($userid, $fuserid)
    {   
        
        $this->ci->load->database();
        $sql             = "select nickname from t_friend where userid=$userid and fuserid=$fuserid";
        $friend_nickname = $this->ci->db->query($sql)->row_array();
        $name            = trim($friend_nickname['nickname']);
        return $name;
    }
    
    public function is_team_user($teamid, $userid)
    {   
        
        $this->ci->load->database();
        $sql          = "select id from t_team_user where teamid=$teamid and userid=$userid";
        $is_team_user = false;
        $ids          = $this->ci->db->query($sql)->row_array();
        if (count($ids) >= 1) {
            $is_team_user = true;
        }
        return $is_team_user;
    }
    
     
    
    public function have_by_game_group($gameid, $groupid, $userid)
    {   
        
        $caddie_record_score_status = CADDIE_ACCEPT;
        $sql = "select id from t_c_caddie_game where gameid=$gameid and caddieid=$userid and status=$caddie_record_score_status";
        $this->ci->load->database();
        $users = $this->ci->db->query($sql)->result_array();
        if (count($users) > 0) {
            return ture;
        } else {
            return false;
        }
        
    }
    
     
    
     
    public function get_team_info($teamid)
    {   
        
        $sys_path = SERVER_PIC_DIR;
        $this->ci->load->database();
        $sql       = "select teamid,team_name,concat($sys_path,coverpath,'/c240_',covername) as team_picurl from t_team where teamid=$teamid";
        $team_info = $this->ci->db->query($sql)->row_array();
        return $team_info;
    }
    
    public function get_team_user_list($teamid)
    {   
        
        $this->ci->load->database();
        $sql            = "select userid from t_team_user where teamid=$teamid";
        $team_user_list = $this->ci->db->query($sql)->result_array();
        return $team_user_list;
    }
    
    public function get_game_applyer_info($gameid)
    {   
        
        $this->ci->load->database();
       
         $sql = "select userid as id from t_game_applyer where gameid=$gameid";
        
        
        $users     = $this->ci->db->query($sql)->result_array();
        $user_info = array();
            foreach ($users as $key => $value) {
                $userid      = intval($value['id']);
                $one=  $this->user_info($userid);

                if( !( $one==null) ){
                   $user_info[] = $one;
                }
            }
        return $user_info;
    }
    
             

     
    public function get_course_info($courseid = 0){
        
        if($courseid < 1){
            return array();
        }
        $sys_path            = SERVER_PIC_DIR;
        $this->ci->load->database();
        $sql = "select $courseid as courseid,t_course.pid,t_course.lat,t_course.lgt,t_course.name,t_course.tnum,t_prov_list.prov_name,concat($sys_path,t_course.coverpath,'/',t_course.covername) as coverpath from t_course,t_prov_list where t_course.courseid=$courseid and t_prov_list.prov_code=t_course.pid ";
        $course_info = $this->ci->db->query($sql)->row_array();
        return $course_info;
    }

      

    public function get_me_create_team_games($userid){
        $team_game_type = GAME_TYPE_INNER;
        $game_status = 
        $query = $this->ci->db->get_where('game',array('creatorid' => $userid,'gametype'=>0,'gamestate'));
        $games = $query->result_array();
        return $games;
    }

    
    
    public function user_is_in_applyer($gameid,$userid){
        $sql = "select id from t_game_applyer where gameid=$gameid and userid=$userid";
        $one_applyer = $this->ci->db->query($sql)->row_array();
        if(intval($one_applyer['id']) > 1){
            return true;
        }
        return false;
    }

    public function user_game_apply($gameid,$userid,$nickname = "",$squadid = 0){
        $data = array('gameid'=>$gameid,
                      'userid'=>$userid,
                      'create_time'=>date('Y-m-d H:i:s'));
        if(strlen($nickname) > 0){
            $data['username'] = $nickname;
        }
        if(XSession::get('frm_state')){
           $data['checkin_status'] = 1;
        }
        $this->ci->db->insert('game_applyer',$data);
        $id = $this->ci->db->insert_id();

        if($id > 0){
            if(strlen($nickname) > 0){
                $this->ci->db->where("userid",$userid);
                $this->ci->db->update("t_user_info",array("realname"=>$nickname));
            }
        }

        if($squadid != 0){
            $this->ci->db->insert('t_game_squad_user',array('gameid'=>$gameid,'userid'=>$userid,'squadid'=>$squadid,'addtime'=>time()));
            $this->ci->db->set('usernum', "usernum+1", FALSE);
            $this->ci->db->where(array('id' =>$squadid))->update('game_squad');
        }
        return $id;
    }

    public function cancel_apply($gameid,$userid){
        $this->ci->db->delete('game_applyer', array('gameid' =>$gameid,'userid'=>$userid));
        $this->ci->db->delete('game_group_user', array('gameid' =>$gameid,'userid'=>$userid));
        $this->cancel_squad_user($gameid,$userid);
        return;
    }


    public function cancel_squad_user($gameid,$userid){
        $this->ci->load->database();
        $sql = "select * from t_game_squad_user where gameid=$gameid and userid=$userid";
        $squad_user = $this->ci->db->query($sql)->row_array();
        if($squad_user || count($squad_user) > 1){
            $squadid = intval($squad_user['squadid']);
            $this->ci->db->where('gameid',$gameid);
            $this->ci->db->where('userid',$userid);
            $this->ci->db->where('squadid',$squadid);
            $this->ci->db->delete('t_game_squad_user');
            $this->ci->db->set('usernum', "usernum-1", FALSE);
            $this->ci->db->where(array('id' =>$squadid))->update('game_squad');
        }
        return;
    }


    public function update_game_apply_help_userid($id,$userid){
        $this->ci->db->where(array('id' => $id))->update('game_applyer', array('help_by_me_id'=>$userid));
        return;
    }

    public function check_team_admin($userid,$gameid){
        $this->ci->db->select('teamid,gametype ');
        $this->ci->db->where('gameid',$gameid);
        $query = $this->ci->db->get('t_game');
        $game_info = $query->row_array();
        if($game_info['gametype'] != GAME_TYPE_INNER){
            return false;
        }
        $team_admins = $this->get_team_admins($game_info['teamid']);
        foreach ($team_admins as $one_team_admin) {
            if($one_team_admin['userid'] == $userid){
                return true;
                break;
            }
        }
        return false;
    }


    public function get_team_admins($teamid){
        $team_user_list = array();
        if($teamid < 1){
            return $team_user_list;
        }
        $this->ci->load->database();
        $query = $this->ci->db->get_where('team_admins',array('teamid' => $teamid));
        $team_user_list = $query->result_array();
        return $team_user_list;
   }


   public function get_squad_no_checkin_user_list($game_squad_users){
        $squads = array();
        foreach ($game_squad_users as $key => $one_squad) {
            $users = array();
            $squad_user = $one_squad['squad_user_list'];
            foreach ($squad_user as $key => $one_user) {
                if($one_user['checkin_status'] == 0){
                    $users[] = $one_user;
                }
            }
            $squads[] = array('squad_name'=>$one_squad['squad_name'],'user_list'=>$users);
        }
        return $squads;
   }



   public function user_merge_game($original_user,$changer){
        $this->ci->load->database();
        $sql = "select ggu.gameid,g.gamestate from t_game_group_user as ggu,t_game as g where ggu.userid=$original_user and g.gameid=ggu.gameid";
        $games = $this->ci->db->query($sql)->result_array();
        foreach ($games as $key => $one_game) {
            $gameid = intval($one_game['gameid']);
            $sql = "select groupid from t_game_group_user where gameid=$gameid and userid=$changer ";
            $game_groupid = $this->ci->db->query($sql)->row_array();
            if(count($game_groupid) == 1){
                continue;
            }
            $gamestate = intval($one_game['gamestate']);
            if($gamestate == 3){//3为正在进行的比赛  从user调取可能取不到常量
                $data = array(
                                  'gameid'=>$gameid,
                                  'old_userid'=>$original_user,
                                  'new_userid'=>$changer,
                                  'addtime'=>time(),
                                  'status'=>2,//2为改变比赛打球人比赛结束后替换 从user调取可能取不到常量
                                  'approver'=>$changer,
                                  'approve_time'=>time(),
                                  'action'=>'merge'
                              );
                $this->ci->db->insert('t_game_change_player',$data);
            }else{
                $this->change_game_player_wxcheckin($gameid,$original_user,$changer);
            }
        }
        $this->handicap($changer);
        return;
    }



    public function change_game_player_wxcheckin($gameid,$original_user,$changer){

        $this->ci->load->database();
        $where = array('gameid'=>$gameid,'userid'=>$original_user);
        $this->ci->db->where($where);
        $this->ci->db->update('t_game_group_user',array('userid'=>$changer));
        $this->ci->db->where($where);
        $this->ci->db->update('t_game_score',array('userid'=>$changer));
        $this->ci->db->where($where);
        $this->ci->db->update('t_game_applyer',array('userid'=>$changer));
        $this->ci->db->where($where);
        $this->ci->db->update('t_game_squad_user',array('userid'=>$changer));
        $this->change_gamble_game($gameid,$original_user,$changer);
        return;
    }



   public function change_game_player($original_user,$changer){
      $this->ci->load->database();
      $sql = "select gameid,gamestate from t_game where gameid in (select gameid from t_game_group_user where userid=$original_user) and gamestate not in (3,5,6,7) ";
      $games = $this->ci->db->query($sql)->result_array();
      $tables = array('t_game_group_user','t_game_score','t_game_applyer');
      foreach ($games as $key => $one_game) {
         $gameid = intval($one_game['gameid']);
         $gamestate = intval($one_game['gamestate']);
         $this->change_gamble_game($gameid,$original_user,$changer);
         $this->ci->db->where('gameid',$gameid);
         $this->ci->db->where('userid',$original_user);
         $this->ci->db->update($tables,array('userid'=>$changer));
      }
      return;
   }


   public function change_gamble_game($gameid,$original_user,$changer){

      return ;
      $this->ci->load->database();
      $sql = "select * from t_gamble_game where gameid=$gameid";
      $infos = $this->ci->db->query($sql)->result_array();
      if(count($infos) < 1){
        return;
      }
      foreach ($infos as $key => $one_gamble) {
          if($one_gamble['gamblecreator'] == $original_user){
             $this->ci->db->where('gamblecreator',$original_user);
             $this->ci->db->where('gambleid',$one_gamble['gambleid']);
             $this->ci->db->update('t_gamble_game',array('gamblecreator'=>$changer));
          }
          $players = trim($one_gamble['players']);
          $new_players = str_replace($original_user,$changer,$players);
          $initorder = trim($one_gamble['initorder']);
          $new_initorder = str_replace($original_user,$changer,$initorder);
          $agroup = trim($one_gamble['agroup']);
          $new_agroup = str_replace($original_user,$changer,$agroup);
          $bgroup = trim($one_gamble['bgroup']);
          $new_bgroup = str_replace($original_user,$changer,$bgroup);

          $combo_cfg = trim($one_gamble['combo_cfg']);
          $new_combo_cfg = str_replace($original_user,$changer,$combo_cfg);

          $this->ci->db->where('gambleid',$one_gamble['gambleid']);
          $this->ci->db->update('t_gamble_game',array('players'=>$new_players,'initorder'=>$new_initorder,'agroup'=>$new_agroup,'bgroup'=>$new_bgroup,'combo_cfg'=>$new_combo_cfg));


          $weaker = intval($one_gamble['weaker']);
          if($weaker == $original_user){
             $this->ci->db->where('gambleid',$one_gamble['gambleid']);
             $this->ci->db->update('t_gamble_game',array('weaker'=>$changer));
          }
          
          $fixed_best_player = intval($one_gamble['fixed_best_player']);
          if($fixed_best_player == $original_user){
             $this->ci->db->where('gambleid',$one_gamble['gambleid']);
             $this->ci->db->update('t_gamble_game',array('fixed_best_player'=>$changer));
          }
          
      }
      return;

   }



   public function get_game_group_info($gameid, $userid,$groupid = 0,$version = 'all')
    {
        
        $this->ci->load->database();
        $sys_path  = SERVER_PIC_DIR;
        $sql_group = "";
        $sql_group .= "select groupid,group_name,group_start_status ";
        $sql_group .= " from t_game_group";
        $sql_group .= " where gameid=$gameid";
        $groups = $this->ci->db->query($sql_group)->result_array();
       
        if($version == 'simple'){
            // if($groupid > 0){
            //     $sql = "select groupid,group_name,group_start_status ";
            //     $sql .= " from t_game_group";
            //     $sql .= " where gameid=$gameid and groupid=$groupid";
            //     $groups = $this->ci->db->query($sql_group)->result_array();
            // }else{
                 return $groups;
            // }
        }
        $index  = 0;
        foreach ($groups as $one_group) {
            $groupid        = $one_group['groupid'];
            $sql_group_user = "select  userid,nickname as username,nickname, ";
            $sql_group_user .= "concat($sys_path,t_user.coverpath,'/c240_',t_user.covername) as user_picurl,t_user.type,  ";
            $sql_group_user .= " confirmed,confirmed_time,admin";
            $sql_group_user .= " from t_game_group_user,t_user";
            $sql_group_user .= " where  t_game_group_user.groupid=$groupid";
            $sql_group_user .= "   and t_user.id=t_game_group_user.userid order by t_game_group_user.id asc";

            $group_user = $this->ci->db->query($sql_group_user)->result_array();
           
            array_walk($group_user, array(
                $this,
                'setting_alt_nickname'
            ), array(
                'gameid' => $gameid,
                'userid' => $userid
            ));

            $groups[$index]['group_user'] = $group_user;
            $index++;
        }
        return $groups;
    }
    
    
}
?>