<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}


class XCloud
{
	private $ci = null;
   
    public function __construct()
    {   
        $this->ci =& get_instance();
        $CI =& get_instance();
        include_once('/opt/space/webroot/ugf/api/web/controllers/gamefuncs.php');
    }

    public function get_cloud_course_data($courseid,$userid){
    	$this->ci->load->database();
    	$this->ci->db->select("name,coverpath,covername");
        $this->ci->db->from("t_course");
        $this->ci->db->where("courseid",$courseid);
        $course_info = $this->ci->db->get()->row_array();
        $course_score_data = $this->get_course_score_data($courseid);
        foreach ($course_score_data as $key => $one_hole) {
            $holeid = intval($one_hole['holeid']);
            $hole_info = $this->get_one_hole_info($holeid);
            $course_score_data[$key]['par'] = $hole_info['par'];
            $course_score_data[$key]['holename'] = $hole_info['holename'];
            $one_hole['par'] = $hole_info['par'];
            $one_hole['holename'] = $hole_info['holename'];
            $course_score_data[$key]['black'] = $hole_info['black'];
            $course_score_data[$key]['gold'] = $hole_info['gold'];
            $course_score_data[$key]['blue'] = $hole_info['blue'];
            $course_score_data[$key]['white'] = $hole_info['white'];
            $course_score_data[$key]['red'] = $hole_info['red'];
            $hole_average_gross = $this->get_one_hole_average_gross($one_hole);
            $user_average_gross = $this->get_user_one_hole_score($userid,$holeid,$courseid,$hole_info['par']);
            $course_score_data[$key]['hole_average_gross'] = $hole_average_gross;
            $course_score_data[$key]['user_average_gross'] = $user_average_gross;
            $this->ci->db->from("t_course_comment");
            $this->ci->db->where("objectid",$holeid);
            $this->ci->db->where("courseid",$courseid);
            $this->ci->db->where("objecttype","hole");
            $course_score_data[$key]['hole_comment_num'] = $this->ci->db->count_all_results();
            $course_score_data[$key]['is_devil_bird'] = 0;//0为普通  1为魔鬼 2为鸟
        }
        if(strlen($course_info['coverpath']) < 1 || strlen($course_info['covername']) < 1){
            $course_pic = "http://t1.golf-brother.com/data/images/default_course_pic.png";
        }elseif($course_info['covername'] == "2405.jpg"){
            $course_pic = "http://t1.golf-brother.com/data/images/default_course_pic.png";
        }else{
            $course_pic = SERVER_PIC_DIR_SPELL.$course_info['coverpath']."/".$course_info['covername'];
        }

        return array("course_name"=>$course_info['name'],"course_pic"=>$course_pic,"course_score_data"=>$course_score_data);
    }


    public function get_hole_score_data($courseid,$holeid,$userid){
    	$this->ci->load->database();
        $this->ci->db->select("name");
        $this->ci->db->from("t_course");
        $this->ci->db->where("courseid",$courseid);
        $course_name = $this->ci->db->get()->row_array();
        $hole_info = $this->get_one_hole_info($holeid);

        $this->ci->db->select("id,courseid,courtid,holeid,sum_gross,play_num");
        $this->ci->db->from("t_course_hole_score_data");
        $this->ci->db->where("courseid",$courseid);
        $this->ci->db->where("holeid",$holeid);
        $result = $this->ci->db->get()->row_array();
        $hole_info['id'] = $result['id'];
        $hole_info['courseid'] = $courseid;
        $hole_info['courtid'] = $result['courtid'];
        $hole_info['holeid'] = $holeid;
        $hole_info['sum_gross'] = $result['sum_gross'];
        $hole_info['play_num'] = $result['play_num'];
        $hole_average_gross = $this->get_one_hole_average_gross($hole_info);
        $user_average_gross = $this->get_user_one_hole_score($userid,$holeid,$courseid,$hole_info['par']);

        return array("course_name"=>$course_name,"hole_info"=>$hole_info,'hole_average_gross'=>$hole_average_gross,"user_average_gross"=>$user_average_gross);
    }


    public function get_one_hole_info($holeid){
    	$this->ci->load->database();
    	$this->ci->db->select("holename,par,black,gold,blue,white,red");
    	$this->ci->db->from("t_court_hole");
    	$this->ci->db->where("holeid",$holeid);
    	$info = $this->ci->db->get()->row_array();
    	return $info;

    }


    private function get_course_score_data($courseid){
    	$this->ci->db->select("id,courseid,courtid,holeid,sum_gross,play_num");
        $this->ci->db->from("t_course_hole_score_data");
        $this->ci->db->where("courseid",$courseid);
        $result = $this->ci->db->get()->result_array();
        return $result;
    }


    public function get_one_hole_average_gross($hole_data){
        $par = intval($hole_data['par']);
        $sum_gross = intval($hole_data['sum_gross']);
        $play_num = intval($hole_data['play_num']);
        if($play_num == 0){
            return 0;
        }
        $hole_average_gross = ($sum_gross/$play_num)-$par;

        return round($hole_average_gross,2);
    }


    private function get_user_one_hole_score($userid,$holeid,$courseid,$par){
        $sql = "select g.gameid from t_game_score as gs,t_game as g where gs.userid=$userid and gs.holeid=$holeid and g.courseid=$courseid and g.gameid=gs.gameid and g.gamestate=4 ";
        $gameids = $this->ci->db->query($sql)->result_array();
        
        foreach ($gameids as $key => $one_game) {
            $gameid = intval($one_game['gameid']);
            $this->ci->db->from("t_game_court");
            $this->ci->db->where("gameid",$gameid);
            if($this->ci->db->count_all_results() < 2){
                unset($gameids[$key]);
            }

            $this->ci->db->select("min(gross) as min_gross");
            $this->ci->db->from("t_game_score");
            $this->ci->db->where("gameid",$gameid);
            $this->ci->db->where("userid",$userid);
            $min_gross = $this->ci->db->get()->row_array();
            if(intval($min_gross['min_gross']) == 0){
                unset($gameids[$key]);
            }
        }

        $num = count($gameids);
        if($num == 0){
            return 0;
        }
        $game_str = array_to_string($gameids,'gameid');
        $game_array = explode(',', $game_str);
        $this->ci->db->select("sum(gross) as sum_gross");
        $this->ci->db->from("t_game_score");
        $this->ci->db->where_in("gameid",$game_array);
        $this->ci->db->where("userid",$userid);
        $this->ci->db->where("holeid",$holeid);
        $sum_gross = $this->ci->db->get()->row_array();

        $average_gross = (intval($sum_gross['sum_gross'])/$num)-$par;
        return round($average_gross,2);
    }


    public function cloud_course_reply($data,$commentid){
    	$this->ci->load->database();
    	$this->ci->db->insert("t_course_comment",$data);
        $id = $this->ci->db->insert_id();
        if($id > 0){
            $this->ci->db->set('comment_num', 'comment_num+1', FALSE);
            $this->ci->db->where('id', $commentid);
            $this->ci->db->update('t_course_comment');
        }
        return $id;
    }


    public function change_comment_like_status($commentid,$userid){
    	$this->ci->load->database();
    	$this->ci->db->from("t_course_like");
        $this->ci->db->where("objectid",$commentid);
        $this->ci->db->where("userid",$userid);
        if(($this->ci->db->count_all_results()) > 0){
            $this->ci->db->where("objectid",$commentid);
            $this->ci->db->where("userid",$userid);
            $this->ci->db->delete("t_course_like");
            $this->ci->db->set('like_num', 'like_num-1', FALSE);
            $this->ci->db->where('id', $commentid);
            $this->ci->db->update('t_course_comment');
            $operation = "delete";
        }else{
            $data = array("objectid"=>$commentid,"userid"=>$userid,"addtime"=>time());
            $this->ci->db->insert("t_course_like",$data);
            $this->ci->db->set('like_num', 'like_num+1', FALSE);
            $this->ci->db->where('id', $commentid);
            $this->ci->db->update('t_course_comment');

            $this->ci->db->select("userid,courseid");
            $this->ci->db->from("t_course_comment");
            $this->ci->db->where("id",$commentid);
            $info = $this->ci->db->get()->row_array();
            $authorid = intval($info['userid']);
            $courseid = intval($info['courseid']);

            $news_data = array("pid"=>$commentid,"objectid"=>$authorid,"objecttype"=>"like","courseid"=>$courseid,"userid"=>$userid,"addtime"=>time());
            $this->ci->db->insert("t_cloud_course_news",$news_data);
            $operation = "add";
        }
        return $operation;
    }
   
}
?>