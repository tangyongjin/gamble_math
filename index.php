<?php
if (defined('STDIN')){
	chdir(dirname(__FILE__));
}

$syspath = '../../zxn';
$apppath = './' ;

if(realpath($syspath)){
	$syspath = realpath($syspath);
}else{
	exit("<center><h1>You have lost!</h1></center>");
}
if(realpath($apppath)){
	$apppath = realpath($apppath);
}else{
	exit("<center><h1>Be patient!</h1></center>");
}

define('SELF',pathinfo(__FILE__, PATHINFO_BASENAME));
define('ROOT_PATH', realpath($syspath.'/../'));
define('BASE_PATH', $syspath);
define('APP_PATH', $apppath);

define('ROOTPATH', realpath($syspath.'/../').'/');
define('BASEPATH', $syspath."/");
define('APPPATH', $apppath."/");

define('ATTACH_REL', 'data/attach');
define('STATIC_REL', 'data/static');

define('ATTACH_PATH', ROOT_PATH.'/'.ATTACH_REL);
define('STATIC_PATH', ROOT_PATH.'/'.STATIC_REL);

//define('API_HOST','http://112.126.69.217:10000');
//define('RES_HOST','http://112.126.69.217:10001');
define('API_HOST','http://api.golf-brother.com');
define('RES_HOST','http://s1.golf-brother.com');

define('ATTACH_HOST', RES_HOST.'/'.ATTACH_REL);
define('STATIC_HOST', RES_HOST.'/'.STATIC_REL);

define('ENVIRONMENT', 'dev');
if(ENVIRONMENT == 'dev'){
	error_reporting(E_ALL);
}else{
	error_reporting(0);
}
require_once BASE_PATH.'/core/CodeIgniter.php';


