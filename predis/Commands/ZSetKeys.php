<?php

namespace Predis\Commands;

use Predis\Iterators\MultiBulkResponseTuple;

class ZSetKeys extends Command {
    public function getId() {
        return 'ZKEYS';
    }
}
