<html>
<head>
 <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
 <meta http-equiv="Content-Language" content="zh-CN" />
 <meta name="roots" content="" />
 <meta name="Keywords" content="" />
 <meta name="Description" content="" />
 <meta http-equiv="refresh" content="300" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <link rel='stylesheet' href="http://s1.golf-brother.com/data/attach/css/log.css" />
 <script src="http://s1.golf-brother.com/data/attach/js/jquery.js" type="text/javascript" charset="utf-8"></script>
 <title>高尔夫江湖－－用户表</title>
 <style type="text/css">
	body{
		 background-color:#f5f5f5;
		 font: normal 100% Helvetica, Arial, sans-serif;
		 margin: 0;
		 padding:0;
	}
	.status_div{
		width: 50%;
		margin-bottom: 3%;
		margin-left:3%; 
	}
	.money_div{
		float: right;
		width: 50%;
		margin-bottom: 3%;
		margin-top:-10%;
		margin-right: -30%;
	}
	.content_div{
		width: 100%;
		padding-right: 2%;
		
	}


	ul li {
		list-style-type: none;
		font-size: 100%;
	}
	img{
		width: 50%;
		margin-left: 35%; 
	}
	hr{
		margin-right: 3%;
		margin-left: 3%;
	}
	#no_record{
		width: 100%;
	}
	#no_uses{
		margin-top:20%;
		width: 30%;

	}
	#text_class{
		margin-top: 10%;
		margin-left: 30%;
	}
	#div1{
		background-color:#fff;
	}
	.kong_div{
		height: 0.5%;
	}
	</style>
	<script type="text/javascript">
  		function jump(id){
  			window.location.href = 'http://test.golf-brother.com/web/caddie/withdraws_log/?id='+id;
  		}
  </script>
</head>
<body>
<div class="kong_div"></div>
<?php if(count($caddie_withdraws) > 0 && $caddie_withdraws){ ?>
	<div id="div1">
<?php foreach ($caddie_withdraws as $key => $value) { ?>
	<?php if($value['status'] == 0){?>
	<div class="content_div" onclick="jump(<?php echo $value['id'];?>)">
		<div class="status_div">
			<font>待发放</font><br />
			<font><?php echo date('Y年m月d日',$value['addtime'])?></font>
		</div>
		<div class="money_div">
			<font>-<?php echo $value['amount'];?></font>
	 	</div>
		</div>
		<hr style="height:1px;border:none;border-top:1px solid #f5f5f5;"/>
		<?php continue; ?>
		<?php }?>

			<?php if($value['status'] == 1){?>
	<div class="content_div" onclick="jump(<?php echo $value['id'];?>)">
		<div class="status_div">
			<font>待领取</font><br />
			<font><?php echo date('Y年m月d日',$value['addtime'])?></font>
		</div>
		<div class="money_div">
			<font>-<?php echo $value['amount'];?></font>
	 	</div>
		</div>
		<hr style="height:1px;border:none;border-top:1px solid #f5f5f5;"/>
		<?php continue; ?>
		<?php }?>

			<?php if($value['status'] == 2){?>
	<div class="content_div" onclick="jump(<?php echo $value['id'];?>)">
		<div class="status_div">
			<font>已领取</font><br />
			<font><?php echo date('Y年m月d日',$value['addtime'])?></font>
		</div>
		<div class="money_div">
			<font>-<?php echo $value['amount'];?></font>
	 	</div>
		</div>
		<hr style="height:1px;border:none;border-top:1px solid #f5f5f5;"/>
		<?php continue; ?>
		<?php }?>
		
			<?php if($value['status'] == 3){?>
	<div class="content_div" onclick="jump(<?php echo $value['id'];?>)">
		<div class="status_div">
			<font>已退回</font><br />
			<font><?php echo date('Y年m月d日',$value['addtime'])?></font>
		</div>
		<div class="money_div">
			<font>+<?php echo $value['amount'];?></font>
	 	</div>
		</div>
		<hr style="height:1px;border:none;border-top:1px solid #f5f5f5;"/>
		<?php continue; ?>
		<?php }?>
	
		
<?php } ?>
</div>
<?php }else{?>
	<div id='no_record'>
		<img id='no_uses' src="http://t1.golf-brother.com/data/images/no_record.png" /><br />
		<div id="text_class"><font>您当前还没有使用纪录</font></div>
	</div>
<?php }?>

</body>
</html>