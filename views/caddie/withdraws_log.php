<html>
<head>
 <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
 <meta http-equiv="Content-Language" content="zh-CN" />
 <meta name="roots" content="" />
 <meta name="Keywords" content="" />
 <meta name="Description" content="" />
 <meta http-equiv="refresh" content="300" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <link rel='stylesheet' href="http://s1.golf-brother.com/data/attach/css/log.css" />
 <script src="http://s1.golf-brother.com/data/attach/js/jquery.js" type="text/javascript" charset="utf-8"></script>
 <title>高尔夫江湖－－用户表</title>
 <style type="text/css">
	body{
		 background-color:#f5f5f5;
		 font: normal 100% Helvetica, Arial, sans-serif;
		 margin: 0;
		 padding:0;
	}
	.top_kong_div{
		height: 0.5%;
	}
	#div1{
		background-color:#fff;
	}
	#withdraw_info{
		width: 100%;
		height: 10%;
		background-color:#fff;
	}
	#withdraw_text{
		margin-left: 2%;
		width: 50%;
	}
	#withdraw{
		text-align: right;
		margin-left: 56%; 
		margin-top: -8%;
		width: 40%;
	}
	.withdraw_complete{
		margin-left: 20%;
		margin-top: 10%;
	}
	.withdraw_has_been_issued{
		margin-left: 20%;
		margin-top: 10%;
	}
	.withdraw_accept{
		margin-left: 20%;
		margin-top: 10%;
	}
	.withdraw_failed{
		margin-left: 20%;
		margin-top: 10%;
	}
	.kong_div{
		height: 2%;
	}
	</style>
</head>
<body>
	<div class="top_kong_div"></div>
	<div id="withdraw_info">
		<div id="withdraw_text">
		<font><b>提现金额</b></font><br />
		<font color="#666"><?php echo date('Y年m月d日',$caddie_withdraw_info['addtime']);?></font>
		</div>
		<div id="withdraw">
			<font size="5%"><b><?php echo $caddie_withdraw_info['amount'];?></b></font>
		</div>
	</div>
<?php foreach ($caddie_withdraws_log as $one_log) { ?>
	<?php if($one_log['status'] == 2) {?>
		<div class="withdraw_complete">
			<font color="#666"><?php echo date('Y年m月d日',$one_log['addtime']);?></font><br />
			<div class="kong_div"></div>
			<font><b>微信红包已经领取,提现交易完成</b></font>
		</div>
	<?php } ?>
	<?php if($one_log['status'] == 1) {?>
		<div class="withdraw_has_been_issued">
			<font color="#666"><?php echo date('Y年m月d日',$one_log['addtime']);?></font><br />
			<div class="kong_div"></div>
			<font><b>微信红包已发放,请注意查收</b></font>
		</div>
	<?php } ?>
	<?php if($one_log['status'] == 0) {?>
		<div class="withdraw_accept">
			<font color="#666"><?php echo date('Y年m月d日',$one_log['addtime']);?></font><br />
			<div class="kong_div"></div>
			<font><b>提现申请已接受,将以微信红包的方式发放到您的微信,请耐心等待</b></font>
		</div>
	<?php } ?>

<?php }?>
<?php if(count($caddie_withdraws_failed_log) > 0){?>
		<div class="withdraw_failed">
			<font color="#666"><?php echo date('Y年m月d日',$one_log['addtime']);?></font><br />
			<div class="kong_div"></div>
			<font><b>微信红包未领取,提现交易失败,提现金额已返还您的球童账号,请核实</b></font>
		</div>
<?php } ?>
</body>
</html>