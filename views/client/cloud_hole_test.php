<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=0;" name="viewport"/>
<title>云端球场</title>
<link rel="stylesheet" type="text/css" href="<?php echo STATIC_HOST;?>/res/css/amazeui.min.css" />
 <script src="http://s1.golf-brother.com/data/attach/js/jquery.js" type="text/javascript" charset="utf-8"></script>
<script src="<?php echo STATIC_HOST;?>/res/js/amazeui.min.js" type="text/javascript" charset="utf-8"></script>
<style>
body{
	margin:0;
	padding: 0;
	width: 100%;
	height: 100%;
	text-align: center;
	font-family: Helvetica Neue,Helvetica,Arial,sans-serif;
	font-size: 17px;
}
.hole_data{
	width: 100%;
	background-color:#333;

}
.course_name{
	background-color: #333;
	width: 100%;
	text-align: left;
	padding-top:20px;
	padding-bottom: 20px;
	color:#f5f5f5;
}
.hole_info{
	text-align: center;
	border-bottom:1px solid #000;
	margin-left: 5%;
	margin-right: 5%;
	padding-bottom: 20px;
}

.hole_name{
	background-color: #fff;
	font-weight: bold;
	color: #333;
	border-radius: 50%;
	font-size: 40px;
	padding: 20px;
	position: relative;
}
.tland_chose{
 width:100%;
 text-align: center;
 margin-top: 20px;
 line-height: 50px;
}

.tland_distance{
	text-align: center;
	font-size: 15px;
	margin-top: -10px;
	color: #fff;
}

.hole_par{
	color:#f5f5f5;
}
.li_black{
	color:black;
	font-size: 20px;
	/*height: 10px;
	width: 10px;
	border-radius:50%;*/
	margin-top: 2px;
}


.li_gold{
	color:orange;
	font-size: 20px;
	/*height: 10px;
	width: 10px;
	border-radius:50%;*/
	margin-top: 2px;
}

.li_blue{
	color:#00ccff;
	font-size: 20px;
	/*height: 10px;
	width: 10px;
	border-radius:50%;*/
	margin-top: 2px;
}

.li_white{
	color:white;
	font-size: 20px;
	/*height: 10px;
	width: 10px;
	border-radius:50%;*/
	margin-top: 2px;
}

.li_red{
	color:red;
	font-size: 20px;
	/*height: 10px;
	width: 10px;
	border-radius:50%;*/
	margin-top: 2px;
}


#comments {
	background-color: #fff;
	margin: 0;
}
#comments img{
	margin-left: 20px;
	margin-right: 20px;
	height: 40px;
}
.like{
	margin-left: 5px;
	padding-top: 15px;
	padding-bottom: 5px;
	color: #333;
}
.text_class{
	margin-left: 10px;
}
.user_nickname{
 /*margin-top: 100px;*/
 padding-top: 8px;
}
hr{
	
	border:none;
	border-top:1px solid #f5f5f5;
}
.comm_info{
	margin-top: 5px;
	margin-left: 80px;
	margin-bottom: 5px;
	margin-right: 5px;
	width: 70%;
	word-break:break-all; 
	text-align: left;
}
.one_reply{
	margin-left: 14%;
}

.comm_reply_list{
	background-color: #f5f5f5;
	width: 100%;
	padding-top: 20px;
	padding-bottom: 20px;
}
.no_like_back{
	background-image: url("http://t1.golf-brother.com/data/images/like.png");
	background-repeat: no-repeat;
	padding-left: 22px;
	/*padding-bottom: 10px;*/
	background-size:70%,70%;
}
.yes_like_back{
	background-image: url("http://t1.golf-brother.com/data/images/like_touched.png");
	background-repeat: no-repeat;
	padding-left: 22px;
	/*padding-bottom: 10px;*/
	background-size:70%,70%;
}
.like_position{
	position: absolute;
	right: 40px;
	top:0;
}
.time{
	padding-bottom: 0;
	/*margin-top: 15px;*/
	margin-bottom: 0;
}
.one_commenter{
	margin-top: 25px;
	/*margin-bottom: 30px;*/
}
</style>
<script>
var courseid = 0;
var userid = 0;
var holeid = 0;
var is_show_code = 0;
<?php 
	echo "courseid=".$courseid.";";
	echo "userid=".$userid.";";
	echo "holeid=".$holeid.";";
	echo "is_show_code=".$is_show_code.";";

?>

function reply(commentid,id,nickname){
	if(is_show_code == 1){
		return;
	}
	var url = "reply://?commentid="+commentid+"&userid="+id+"&nickname="+nickname;
	document.location = url;
}
function on_heat(){
	$("#heat").addClass("am-btn-primary");
	$("#heat").removeClass("am-btn-default");
	$("#new").addClass("am-btn-default");
	$("#new").removeClass("am-btn-primary");
	var order_by = "heat";
	$.post("http://test.golf-brother.com/web/webpage/course_comment_list/",
	  {
	    courseid:courseid,
	    userid:userid,
	    order_by:order_by,
	    holeid:holeid
	  },
	  function(data,status){
	  	if(data.error_code == 1){
	  		$(".one_commenter").empty();
	  		$("hr").remove();
	  		$('#hidden_div').after(data.html_str);
	  	}else{
	  		console.log(data.error_descr);
	  	}
	});
}
function on_new(){
	$("#new").addClass("am-btn-primary");
	$("#new").removeClass("am-btn-default");
	$("#heat").addClass("am-btn-default");
	$("#heat").removeClass("am-btn-primary");
	var order_by = "new";
	$.post("http://test.golf-brother.com/web/webpage/course_comment_list/",
	  {
	    courseid:courseid,
	    userid:userid,
	    order_by:order_by,
	    holeid:holeid
	  },
	  function(data,status){
	  	if(data.error_code == 1){
	  		$(".one_commenter").empty();
	  		$("hr").remove();
	  		$('#hidden_div').after(data.html_str);
	  	}else{
	  		console.log(data.error_descr);
	  	}
	});
}
function like(btn,id,like_status){	
	if(is_show_code == 1){
		return;
	}
	$.post("http://test.golf-brother.com/web/webpage/course_comment_like/",
	  {
	    course_comment_id:id,
	    userid:userid
	  },
	  function(data,status){
	  	if(data.error_code == 1){
	  		if(data.operation == "delete"){
	  			var span = $(btn).children();
		  		var num = $(span).text();
		  		num = parseInt(num);
		  		var new_num = num-1;
		  		$(span).text(new_num);
		  		$(span).removeClass("yes_like_back");
		  		$(span).addClass("no_like_back");
	  		}
	  		if(data.operation == "add"){
	  			var span = $(btn).children();
		  		var num = $(span).text();
		  		num = parseInt(num);
		  		var new_num = num+1;
		  		$(span).text(new_num);
		  		$(span).addClass("yes_like_back");
		  		$(span).removeClass("no_like_back");
	  		}
	  	}else{
	  		console.log(data.error_descr);
	  	}
	  });
}


function client_request(note){
	var phone_type = "";
	var u = navigator.userAgent;
	if (u.indexOf('Android') > -1 || u.indexOf('Linux') > -1) {//安卓手机
		phone_type = "Android";
	} else if (u.indexOf('iPhone') > -1) {//苹果手机
		phone_type = "IOS";
	} else if (u.indexOf('Windows Phone') > -1) {//winphone手机
		phone_type = "Windows";
	}

	if(is_show_code == 1){
		return;
	}
	if(note.length < 1){
		 if(phone_type == "Android"){
			window.CommentCallback.onCommentFinish(false);
		}else if(phone_type == "IOS"){
			var url = "status="+false;
			document.location = url;
		}
	}
	$.ajax({
		  url:'http://test.golf-brother.com/web/webpage/course_comments/', 
     	  data:{courseid:courseid,userid:userid,content:note,holeid:holeid},  
    	  type:'post',  
          cache:false,
          async:true,
          dataType:'json',  
          success:function(data) {
          	var course_comment_num = $("#course_comment_num").text();
          	var new_comment_num = parseInt(course_comment_num)+1;
          	$("#course_comment_num").html(new_comment_num);
          	if(data.order_by == "new"){
	     		var first_hr = $("#comments_hr").first();
		     	$(data.html_str).insertBefore(first_hr);
	     	}

	     	if(data.order_by == "heat"){
	     		var last_hr = $("#comments hr").last();
		     	$(last_hr).after(data.html_str);
	     	}
	     	if(phone_type == "Android"){
				window.CommentCallback.onCommentFinish(true);
			}else if(phone_type == "IOS"){
				var url = "status="+true;
				document.location = url;
			}
          },  
          error : function() {  
          	 if(phone_type == "Android"){
				window.CommentCallback.onCommentFinish(false);
			}else if(phone_type == "IOS"){
				var url = "status="+false;
				document.location = url;
			}
          }
	});
}


function client_request_reply(commentid,reply_userid,note){
	var phone_type = "";
	var u = navigator.userAgent;
	if (u.indexOf('Android') > -1 || u.indexOf('Linux') > -1) {//安卓手机
		phone_type = "Android";
	} else if (u.indexOf('iPhone') > -1) {//苹果手机
		phone_type = "IOS";
	} else if (u.indexOf('Windows Phone') > -1) {//winphone手机
		phone_type = "Windows";
	}

	if(is_show_code == 1){
		return;
	}
	if(note.length < 1){
		if(phone_type == "Android"){
			window.CommentCallback.onCommentFinish(false);
		}else if(phone_type == "IOS"){
			var url = "status="+false;
			document.location = url;
		}
	}
	$.ajax({
		  url:'http://test.golf-brother.com/web/webpage/add_reply/', 
     	  data:{courseid:courseid,userid:userid,content:note,reply_userid:reply_userid,commentid:commentid},  
    	  type:'post',  
          cache:false,
          async:true,
          dataType:'json',  
          success:function(data) {
          	var commentid = data.commentid;
	  		if($("#"+commentid+" .one_reply").length > 0){
	  			var last_reply = $("#"+commentid+" .one_reply").first();
	  			$(last_reply).before(data.html_str);
	  		}else{
				var last_reply = $("#"+commentid+" .comm_info");
				$(last_reply).after("<div class='comm_reply_list'>"+data.html_str+"</div>");
	  		}
	     	$('html, body').animate({  
	            scrollTop: $("#"+commentid).offset().top  
	        }, 1000);
	         if(phone_type == "Android"){
				window.CommentCallback.onCommentFinish(true);
			}else if(phone_type == "IOS"){
				var url = "status="+true;
				document.location = url;
			}
          },  
          error : function() {
          	if(phone_type == "Android"){
				window.CommentCallback.onCommentFinish(false);
			}else if(phone_type == "IOS"){
				var url = "status="+false;
				document.location = url;
			}
          }
	});	
}

$(document).ready(function(){
	$("#course_comments_btn").click(function(){
			$("#comments_content").val(" ");
	})
	$("#comments_submit").click(function(){
		var content = $("#comments_content").val();
		if((content.length) < 1){
			alert("没有内容");
			return;
		}
	    $.post("http://test.golf-brother.com/web/webpage/course_comments/",
		  {
		    courseid:courseid,
		    userid:userid,
		    content:content,
		    holeid:holeid
		  },
		  function(data,status){
		     if(data.error_code == 1){
		     	if(data.order_by == "new"){
		     		var first_hr = $("#comments_hr").first();
			     	$(data.html_str).insertBefore(first_hr);
			     	console.log("这里是最新");
		     	}

		     	if(data.order_by == "heat"){
		     		var last_hr = $("#comments hr").last();
			     	$(last_hr).after(data.html_str);
			     	console.log("这里是热度");
		     	}
		     	$("#comments_window").modal('close');
		     	
		     }else{
				console.log("失败");
		     }
		  });
	})

	$("#reply_submit").click(function(){
		var content = $("#reply_content").val();
		if((content.length) < 1){
			alert("没有内容");
			return;
		}
		var reply_userid = $("#userid").val();
		var commentid = $("#commentid").val();
	    $.post("http://test.golf-brother.com/web/webpage/add_reply/",
		  {
		    courseid:courseid,
		    userid:userid,
		    content:content,
		    reply_userid:reply_userid,
		    commentid:commentid
		  },
		  function(data,status){
		  	if(data.error_code == 1){
		  		var commentid = data.commentid;
		  		if($("#"+commentid+" .one_reply").length > 0){
		  			var last_reply = $("#"+commentid+" .one_reply").first();
		  			$(last_reply).before(data.html_str);
		  			console.log("有内容");
		  		}else{
					var last_reply = $("#"+commentid+" .comm_info");
					$(last_reply).after(data.html_str);
					console.log("没有内容");
		  		}
		  		
			    
			    $("#reply_window").modal('close');
		  	}
		  });
	})


});
</script>
</head>

<body>
<div class="hole_data">
	<div class="course_name"><span style="margin-left:10px;"><?php echo $course_name; ?></span></div>
	<div class="hole_info" >
		<div style="width:100%;margin-top:20px;text-align: center;margin-bottom: 20px;"><span class="hole_name "><?php echo $hole_info['holename']; ?></span></div>
		<div style="margin-top:0px;margin-left:0px;width:100%;text-align: center;"><span class="hole_par"><b>par </b><font><b><?php echo $hole_info['par']; ?></b></font></span></div>
		<div>
			<?php 
				if($hole_info['black'] != 0){//&bull;
					echo "<span class='tland_distance'><i class='li_black'>&bull;</i>&nbsp;&nbsp;".$hole_info['black']."</span>&nbsp;&nbsp;";
				}

				if($hole_info['gold'] != 0){
					echo "<span class='tland_distance'><i class='li_gold'>&bull;</i>&nbsp;&nbsp;".$hole_info['gold']."</span>&nbsp;&nbsp;";
				}

				if($hole_info['blue'] != 0){
					echo "<span class='tland_distance'><i class='li_blue'>&bull;</i>&nbsp;&nbsp;".$hole_info['blue']."</span>&nbsp;&nbsp;";
				}

				if($hole_info['white'] != 0){
					echo "<span class='tland_distance'><i class='li_white'>&bull;</i>&nbsp;&nbsp;".$hole_info['white']."</span>&nbsp;&nbsp;";
				}

				if($hole_info['red'] != 0){
					echo "<span class='tland_distance'><i class='li_red'>&bull;</i>&nbsp;&nbsp;".$hole_info['red']."</span>&nbsp;&nbsp;";
				}
			?>
		</div>
			
	</div>
	<div style="text-align: center;padding-bottom: 20px; border-top-width: 20px;">
		<div style="width:100%;text-align:center;">
			<div style="color:#f5f5f5;font-size:50px;width:30%;float:left;text-align:right;margin-top:20px;">｛</div>
			<div style="width:40%;float:left;margin-top:35px;">
				<div style="font-size:18px;color:#f5f5f5;">难度系数</div>
				<div>
					<span style="color:orange;font-size:15px;">
						<?php 
							for ($i=0; $i < $grades; $i++) { 
					    		echo "★";
							}
							if((5-$grades) > 0){
								$cha = 5-$grades;
								for ($i=0; $i < $cha; $i++) { 
									echo "<font color='#e5e5e5'>★</font>";
								}
							}
						?>
					</span>
				</div>
			</div>
			<div style="color:#f5f5f5;font-size:50px;width:30%;float:right;text-align:left;margin-top:20px;">｝</div>
		</div>
		<div style='clear: both; '></div>
		<div style="color:#f5f5f5;font-size:14px;">
			本洞所有人平均成绩：<?php echo $hole_average_gross; ?>
		</div>
		<div style="color:#f5f5f5;font-size:14px;">
			我的平均成绩：<?php echo $user_average_gross; ?>
		</div>
	</div>
</div>
<div style="background-color:#f5f5f5;width:100%;height:30px;"></div>
<div class="am-text-middle" style="border-top:1px solid #e5e5e5;border-bottom:1px solid #e5e5e5;width:100%;padding-top:15px;padding-bottom:15px;padding-left:20px;background-color:#fff;text-align:left;">
			球场评论(<font id="course_comment_num"><?php echo $course_comments_num; ?></font>)
</div>

<div id="comments" style="width:100%;">
	<div id="hidden_div" style="display:none"></div>
	<hr id="comments_hr" style="display:none" />
	<?php foreach ($course_comments as $key => $one_comment) { ?>
		<div class='one_commenter' id="<?php echo $one_comment['id'];?>">
			<img style="border-radius:50%;" align='left' src="<?php echo $one_comment['user_info']['user_picurl'];?>" />
			<div  class='text_class' style="text-align:left;position:relative;">
				<div onclick="reply(<?php echo $one_comment['id']; ?>,<?php echo $one_comment['userid']; ?>,'<?php echo $one_comment['user_info']['nickname'];?>')" class='user_nickname'><font style="font-size:13px;" color='#333'><?php echo $one_comment['user_info']['nickname'];?></font></div>
				<div onclick="reply(<?php echo $one_comment['id']; ?>,<?php echo $one_comment['userid']; ?>,'<?php echo $one_comment['user_info']['nickname'];?>')" class='time'><font style="font-size:12px;" color='#c9c9c9'><?php echo $one_comment['addtime'];?></font></div>
				<div onclick="like(this,<?php echo $one_comment['id']?>,<?php echo $one_comment['like_status'];?>)" class="like_position">
					<span class="<?php if($one_comment['like_status'] == 0){echo 'no_like_back';}else{echo 'yes_like_back';}?>"><?php echo $one_comment['like_num'];?><span>
				</div>
			</div>
			<div class='comm_info' style="margin-bottom:30px;"><font style="font-size:15px;" color='#333'><?php echo $one_comment['note']; ?></font></div>
			<?php if(count($one_comment['comments']) > 0){?>
			<div class="comm_reply_list">
			<?php foreach ($one_comment['comments'] as $row) {  ?>
				<div class="one_reply" >
					<img style="border-radius:50%;" align='left' src="<?php echo $row['user_info']['user_picurl'];?>" />
					<div class='text_class' style="text-align:left;">
						<div onclick="reply(<?php echo $one_comment['id']; ?>,<?php echo $row['userid']; ?>,'<?php echo $row['user_info']['nickname'];?>')" class='user_nickname'><font style="font-size:13px;" color='#333'><?php echo $row['user_info']['nickname'];?></font></div>
						<div onclick="reply(<?php echo $one_comment['id']; ?>,<?php echo $row['userid']; ?>,'<?php echo $row['user_info']['nickname'];?>')" class='time'><font style="font-size:12px;" color='#c9c9c9'><?php echo $row['addtime'];?></font></div>
					</div>
					<div class='comm_info' style=""><font style="font-size:15px;" color='#999'><?php echo "回复 ".$row['pid_info']['nickname']."：</font><font style='font-size:15px;' color='#333'>".$row['note']; ?></font></div>
				</div>
			<?php } ?>
			</div>
			<?php } ?>
		</div>
		<hr id="comments_hr" style="background-color:#e5e5e5;height:1px;border:none;margin-top: 0px;" />
	<?php } ?>
</div>

</body>
</html>