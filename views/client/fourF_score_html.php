<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv=content-type content="text/html; charset=utf-8">
	<title>高尔夫成绩卡</title>
	<style type="text/css">
  body{
     background-color:#f2f2f2;
     font-family:Arial, sans-serif;
  }
  table{
    width: 100%;
    text-align: center;
    margin: 0;
    padding: 0;
    border-collapse:collapse;
  }
  tr{
    padding: 0;
    margin: 0;
  }
  td {
    margin: 0;
    padding: 0;
  }
  .first_tr {
    background-color: #333;
      height: 44px;
      margin: 0px;
      padding: 0px;
  }
  .first_tr td{
    /*background-color: #000;*/
    color:#fff;
  }
	.blue_backcolor,.blue_color{
		background-color: blue;
    color:#fff;
  
	}
  .blue_backcolor .red_backcolor{
    height: 60px;
  }
	.red_color,.red_backcolor{
		background-color: red;
    color:#fff;

	}
  .team_div_red{
    width: 50%;
    background-color: red;
    color: #fff;
    float: left;
    text-align: center;
    line-height: 4;
    font-size: 20px;
    position:relative;
  }
  .team_div_blue{
    width: 50%;
    background-color: blue;
    color: #fff;
    float: right;
    text-align: center;
    line-height: 4;
    font-size: 20px;
    position:relative;
  }
  .score_red{
    position: absolute;
    right: 30px;
    top: 10%;
  }
  .score_blue{
    position: absolute;
    left: 20px;
    top: 10%;
  }
  .team_div{
    width: 100%;
  }
  #triangle-down {
    width: 0;
    height: 0;
    border-left: 50px solid transparent;
    border-right: 50px solid transparent;
    border-top: 60px solid #f2f2f2;
    position: absolute;
    right: 0;
    left: -10.5%;
    top: -30%;

  }
	</style>

<script>

</script>

</head>
<body>
  <div style="width:100%;">
    <span style="font-size:30px;">渝泰博杯2016重庆高尔夫球队联赛</span>
    <div style="float: right;">
      <img src="http://s1.golf-brother.com/data/images/logo.png" height="30px" />
      <span style="font-size:30px;">四人四球成绩卡</span>
    </div>
  </div>
  <div style="width:100%;">
    <p style="font-size:30px;border-left:10px solid #000;">简介 <font size="4" color="red">INTRO</font></p>
    <p style="font-size:20px;"><?php echo $game_info['course_name'];?></p>
    <p style="font-size:20px;"><?php echo $game_info['game_name'];?> <?php echo $game_info['pre_starttime'];?></p>
    <br />
    <p style="font-size:30px;border-left:10px solid #000;">赛况 <font size="4" color="red">SCHEDULE</font></p>
  </div>
  <br /><br />
  <div class="team_div">
    
  <?php foreach ($game_teams as $key => $one_team) { ?>
        <?php if($key != 0){ ?>
          <div class="team_div_<?php echo $one_team['color']; ?>">
            <span class="score_<?php echo $one_team['color']; ?>">
              <?php echo $one_team['score']; ?>
            </span>
            <?php echo trim($one_team['team_name']);?>
            <div id="triangle-down"></div>
         </div>


        <?php }else{ ?>

          <div class="team_div_<?php echo $one_team['color']; ?>">
            <?php echo trim($one_team['team_name']);?>
            <span class="score_<?php echo $one_team['color']; ?>">
              <?php echo $one_team['score']; ?>
            </span>
         </div>

        <?php } ?>
         
  <?php } ?>
</div>
  <table>
  	 <tr class="first_tr">
  	 	<td style="border-right:#fff solid 1px;">COURSE RAR</td>
  	 	<?php 
        $par_index = 0;
  	 		foreach ($pars as $key => $one_par) {
          $par_index++;
        
          if((count($pars) - $key) > 1){
            if($par_index == 3){
              $par_index = 0;
              echo "<td style='border-right:#fff solid 1px;'>".$one_par['par']."</td>";
            }else{
              echo "<td>".$one_par['par']."</td>";
            }
              
          }
  	 			
  	 			
  	 		}
  	 	?>
  	 </tr>
  	 <?php 
  	 	foreach ($game_group_score as $key => $one_group) {
  	 		$num = $key+1;
  	 		echo "<tr style='background-color:#f3f9fd;'>";
  	 		echo "<td style='height:40px;'>Match $num </td>";
  	 		foreach ($one_group['group_outcome_result'] as $one_hole_outcome) {
          
            if(count($one_hole_outcome) > 1){
            
              echo "<td class='".$one_hole_outcome[1]."_color'>";
            }else{
              echo "<td>";
            }
  	 			
  	 			echo $one_hole_outcome[0];


  	 			echo "</td>";
  	 		}
  	 		echo "</tr>";
  	 		echo "<tr style='border-bottom:1px solid rgba(51,51,51,0.1);'>";

  	 		echo "<td class='".$one_group[0]['color']."_backcolor'>";
  	 		foreach ($one_group[0]['nickname'] as $index => $one_nickname) {
          if($index == 0){
            echo $one_nickname.'<br />';
          }else{
            echo $one_nickname;
          }
  	 			
  	 		}
  	 		echo "</td>";
        $score_index = 0;
  	 		foreach ($one_group[0]['score'] as $hole_index => $one_score) {
          $score_index++;
          if($score_index == 3){
            $score_index = 0;
              if($one_score == 0){
                echo "<td style='border-right:rgba(51,51,51,0.1) solid 1px;'>-</td>";
              }else{
                 if($one_score < $pars[$hole_index]['par']){
                    echo "<td style='color:red;border-right:rgba(51,51,51,0.1) solid 1px;'>".$one_score."</td>";
                 }elseif($one_score == $pars[$hole_index]['par']){
                   echo "<td style='color:blue;border-right:rgba(51,51,51,0.1) solid 1px;'>".$one_score."</td>";
                 }else{
                    echo "<td style='border-right:rgba(51,51,51,0.1) solid 1px;'>".$one_score."</td>";
                 }
                
              }

          }else{
            if($one_score == 0){
              echo "<td>-</td>";
            }else{
               if($one_score < $pars[$hole_index]['par']){
                  echo "<td style='color:red;'>".$one_score."</td>";
               }elseif($one_score == $pars[$hole_index]['par']){
                 echo "<td style='color:blue;'>".$one_score."</td>";
               }else{
                  echo "<td>".$one_score."</td>";
               }
              
            }

          }

  	 			
  	 		}

  	 		echo "</tr>";

  	 		echo "<tr>";

  	 		echo "<td class='".$one_group[1]['color']."_backcolor'>";
  	 		foreach ($one_group[1]['nickname'] as $index => $one_nickname) {
           if($index == 0){
            echo $one_nickname.'<br />';
          }else{
            echo $one_nickname;
          }
  	 		}
  	 		echo "</td>";
        $score_index = 0;
  	 		foreach ($one_group[1]['score'] as $hole_index => $one_score) {
          $score_index++;
          if($score_index == 3){
            $score_index = 0;
            if($one_score == 0){
              echo "<td style='border-right:rgba(51,51,51,0.1) solid 1px;'>-</td>";
            }else{
               if($one_score < $pars[$hole_index]['par']){
                  echo "<td style='color:red;border-right:rgba(51,51,51,0.1) solid 1px;'>".$one_score."</td>";
               }elseif($one_score == $pars[$hole_index]['par']){
                 echo "<td style='color:blue;border-right:rgba(51,51,51,0.1) solid 1px;'>".$one_score."</td>";
               }else{
                  echo "<td style='border-right:rgba(51,51,51,0.1) solid 1px;'>".$one_score."</td>";
               }
              
            }
          }else{
            if($one_score == 0){
              echo "<td>-</td>";
            }else{
               if($one_score < $pars[$hole_index]['par']){
                  echo "<td style='color:red;'>".$one_score."</td>";
               }elseif($one_score == $pars[$hole_index]['par']){
                 echo "<td style='color:blue;'>".$one_score."</td>";
               }else{
                  echo "<td>".$one_score."</td>";
               }
              
            }
          }
          
  	 			
  	 		}

  	 		echo "</tr>";

  	 		
  	 	}
  	 // dump($game_group_score[0]);

  	 ?>
  </table>
  <div style="height:30px;width:100%;background-color:#333;"></div>
</body>
</html>
