<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv=content-type content="text/html; charset=utf-8">
<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=0;" name="viewport"/>
<link rel="stylesheet" type="text/css" href="<?php echo STATIC_HOST;?>/res/css/amazeui.min.css" />
<script src="http://s1.golf-brother.com/data/attach/js/jquery.js" type="text/javascript" charset="utf-8"></script>
<script src="<?php echo STATIC_HOST;?>/res/js/amazeui.min.js" type="text/javascript" charset="utf-8"></script>
	<title>高尔夫成绩卡</title>
	<style type="text/css">
  body{
     background-color:#f2f2f2;
     font-family:Arial, sans-serif;
     height: 100%;
     width: 100%;
  }
  table{
    width: 100%;
    text-align: center;
    margin: 0;
    padding: 0;
    border-collapse:collapse;
  }
  tr{
    padding: 0;
    margin: 0;
  }
  td {
    margin: 0;
    padding: 0;
  }
  .first_tr {
    background-color: #333;
      height: 44px;
      margin: 0px;
      padding: 0px;
  }
  .first_tr td{
    /*background-color: #000;*/
    color:#fff;
  }
	.blue_backcolor,.blue_color{
		background-color: blue;
    color:#fff;
  
	}
  .blue_backcolor .red_backcolor{
    height: 60px;
  }
	.red_color,.red_backcolor{
		background-color: red;
    color:#fff;

	}
  .team_div_red{
    width: 50%;
    background-color: red;
    color: #fff;
    float: left;
    text-align: center;
    line-height: 4;
    font-size: 15px;
    position:relative;
    margin-bottom:2px;
  }
  .team_div_blue{
    width: 50%;
    background-color: blue;
    color: #fff;
    float: right;
    text-align: center;
    line-height: 4;
    font-size: 15px;
    position:relative;
    margin-bottom:2px;
  }
  .score_red{
    position: absolute;
    right: 30px;
    top: 5%;
  }
  .score_blue{
    position: absolute;
    left: 20px;
    top: 5%;
  }
  .team_div{
    width: 100%;
    /*position: relative;*/
  }
  #triangle-down {
    clear:both;
    width: 0;
    height: 0;
    border-left: 30px solid transparent;
    border-right: 30px solid transparent;
    border-top: 30px solid #f2f2f2;
    position: absolute;
    right: 0;
    left: 41.6%;
    /*margin: 0 auto;*/
    /*background-color: red;*/
  }
  .squad_class{
    margin-bottom: 2.5px;
  }
  .score_class{
    margin-top: 2.5px;
  }
  .one_group_class{
    text-align: center;
    float: left;
    width:45%;
    font-size: 15px;
    line-height:40px;
  }
  .score_right{
    
    margin-right: 10px;
  }
 .score_left{
    margin-left: 10px;
  }
  .squad_score{
    margin-bottom: 90px;
  }
	</style>

<script>
function show_squad_score(btn){
  $('div[name="squad_score"]').hide();
  
  document.getElementById(btn).style.display="inline";

}

$(document).ready(function(){
  $('div[name="squad_score"]').hide();
}); 
</script>

</head>
<body>
  <div style="width:100%;padding-left: 10px;padding-right: 10px;margin-bottom:10px;">
  <img src="http://s1.golf-brother.com/data/images/logo.png" align="right" height="20px" style="margin-top:10px;" /><br/>
  <?php
    echo '<span style="font-size:20px;"><font color="#333"><b>';
    echo $team_game_name;
    echo '</b></font></span><br />';
  ?>

  <span style="font-size:15px;"><font color="#333"><?php echo $coursename;?></font></span><br />
  <span id="time" style="font-size:15px;"><font color="#999"><?php echo $time;?></font></span><br />
  
  </div>
  <div style="padding-left: 10px; padding-right: 10px;">
    <?php foreach ($game_group_score as $num => $row) { ?>
        <div class="squad_class">
          <div onclick="show_squad_score('score_<?php echo $num; ?>')" class="team_div">
              <?php foreach ($row['squads'] as $key => $one_squad) { ?>
                   
                   <div class="team_div_<?php echo $one_squad['color']; ?>">
                      <?php 
                         if(strlen($one_squad['team_picurl']) > 2){ ?>
                         <img class="am-img-thumbnail am-circle" width="40px" src="<?php echo $one_squad['team_picurl']; ?>" />
                      <?php 
                       }
                       ?>
                      <?php echo trim($one_squad['squad_name']);?>
                      <span class="score_<?php echo $one_squad['color']; ?>">
                        <?php echo $one_squad['score']; ?>
                      </span>
                   </div>
                   
              <?php } ?>
              
              <!-- <div id="triangle-down"></div> -->
          </div>
          <div class="score_class" name="squad_score" id="score_<?php echo $num; ?>">
            <?php foreach ($row['user_score_data'] as $key => $one_group) {   ?>
                             
                              <?php 
                                if(count($one_group['group_outcome_result_one_hole']) > 1){
                                   if($one_group['group_outcome_result_one_hole'][1] == $one_group[0]['color']){ ?>
                                   <div class="one_group_class" style="color:#fff;background-color:<?php echo $one_group[0]['color'];?>;" >
                                   <?php 
                                      if(strlen($one_group[0]['nickname'][0]) > 9){
                                        $userid_0 = substr($one_group[0]['nickname'][0],0,9)."...";
                                      }else{
                                        $userid_0 = $one_group[0]['nickname'][0];
                                      }

                                      if(strlen($one_group[0]['nickname'][1]) > 9){
                                        $userid_1 = substr($one_group[0]['nickname'][1],0,9)."...";
                                      }else{
                                        $userid_1 = $one_group[0]['nickname'][1];
                                      }

                                      echo $userid_0.'/'.$userid_1; 
                                   ?>
                                   <span class="am-fr score_right"><?php echo $one_group['group_outcome_result_one_hole'][0];?></span>
                              <?php }else{ ?>
                                    <div class="one_group_class" style="" >
                                    <?php 
                                      if(strlen($one_group[0]['nickname'][0]) > 9){
                                        $userid_0 = substr($one_group[0]['nickname'][0],0,9)."...";
                                      }else{
                                        $userid_0 = $one_group[0]['nickname'][0];
                                      }

                                      if(strlen($one_group[0]['nickname'][1]) > 9){
                                        $userid_1 = substr($one_group[0]['nickname'][1],0,9)."...";
                                      }else{
                                        $userid_1 = $one_group[0]['nickname'][1];
                                      }

                                      echo $userid_0.'/'.$userid_1;
                                    ?>
                              <?php   }
                                }else{
                              ?>
                              <div  class="one_group_class" style="" >
                              <?php 
                                  if(strlen($one_group[0]['nickname'][0]) > 9){
                                    $userid_0 = substr($one_group[0]['nickname'][0],0,9)."...";
                                  }else{
                                    $userid_0 = $one_group[0]['nickname'][0];
                                  }

                                  if(strlen($one_group[0]['nickname'][1]) > 9){
                                    $userid_1 = substr($one_group[0]['nickname'][1],0,9)."...";
                                  }else{
                                    $userid_1 = $one_group[0]['nickname'][1];
                                  }

                                  echo $userid_0.'/'.$userid_1;
                              ?>
                              <span class="am-fr score_right">v/s</span>
                              <?php
                                }

                              ?>
                             </div>
                             <div style="line-height:40px;font-size:15px;text-align: center;float: left;width:10%;background-color:#000;color:#fff;">
                                <?php 
                                    if($one_group['hole_index'][0] == 0 && !$one_group['hole_index'][1]){
                                      echo "V";
                                    }elseif($one_group['hole_index'][0] == 17){
                                      echo "F";
                                    }else{
                                      echo $one_group['hole_index'][0]+1;
                                    }
                                ?>
                             </div>
                             
                              <?php 
                                if(count($one_group['group_outcome_result_one_hole']) > 1){
                                   if($one_group['group_outcome_result_one_hole'][1] == $one_group[1]['color']){ ?>
                                   <div class="one_group_class" style="color:#fff;background-color:<?php echo $one_group[1]['color'];?>">
                                   <span class="am-fl score_left" ><?php echo $one_group['group_outcome_result_one_hole'][0];?></span>
                              <?php }else{
                                      echo '<div class="one_group_class" style="">';
                                    }
                                }else{
                              ?>
                              <div class="one_group_class" style="">
                              <span class="am-fl score_left">v/s</span>
                              <?php
                                }

                              ?>
                              <?php
                                if(strlen($one_group[1]['nickname'][0]) > 9){
                                  $userid_0 = substr($one_group[1]['nickname'][0],0,9)."...";
                                }else{
                                  $userid_0 = $one_group[1]['nickname'][0];
                                }

                                if(strlen($one_group[1]['nickname'][1]) > 9){
                                  $userid_1 = substr($one_group[1]['nickname'][1],0,9)."...";
                                }else{
                                  $userid_1 = $one_group[1]['nickname'][1];
                                }

                                echo $userid_0.'/'.$userid_1;
                              ?>
                            </div>
                       <?php } ?>
          </div>
          <div style="clear:both;"></div>
        </div>
    <?php } ?>
  </div>
  <br/><br/><br/><br/><br/>
</body>
</html>
