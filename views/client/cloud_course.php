<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta content="width=device-width, initial-scale=0.5, maximum-scale=0.5, minimum-scale=0.5, user-scalable=0;" name="viewport"/>
<title>云端球场</title>
<link rel="stylesheet" type="text/css" href="<?php echo STATIC_HOST;?>/res/css/amazeui.min.css" />
 <script src="http://s1.golf-brother.com/data/attach/js/jquery.js" type="text/javascript" charset="utf-8"></script>
<script src="<?php echo STATIC_HOST;?>/res/js/amazeui.min.js" type="text/javascript" charset="utf-8"></script>
<style>
body{
	margin:0;
	padding: 0;
	width: 100%;
	height: 100%;
	text-align: center;
	font-family: Helvetica Neue,Helvetica,Arial,sans-serif;
	font-size: 32px;
}
.holename_class{
	width: 50px;
	height: 50px;
	border-radius: 50%;
	background-color: #333;
	color: #fff;
	font-size: 20px;
}
table{
	text-align: center;

}
.grades_color_1{
	color:orange;
}
.grades_color_2{
	color:orange;
}
.grades_color_3{
	color:orange;
}
.grades_color_4{
	color:orange;
}
.grades_color_5{
	color:orange;
}
td{
	font-size: 180%;
	font-weight: bold;
	height: 90px;
	line-height:90px;
	text-align: center;
	vertical-align:middle;
	color:#666;
}
.td_class{
	width: 6%;
}
.course_top{
	width: 100%;
	height: 40%;
	text-align: left;
	background-image:url("<?php echo $course_pic;?>");
	background-size: 100% 100%;
}
.course_name{
	margin-left: 10px;
	margin-bottom: 10px;
	font-size: 40px;
	color: #fff;

}

#comments {
	background-color: #fff;
	margin: 0;
	/*padding-top: 30px;*/
	/*margin-top:90%;*/
}
#comments img{
	margin-left: 20px;
	margin-right: 20px;
	height: 80px;
}
.like{
	margin-left: 10px;
	padding-top: 15px;
	padding-bottom: 5px;
	color: #333;
}
.text_class{
	margin-left: 10px;
}
.user_nickname{
 /*margin-top: 100px;*/
 padding-top: 8px;
}
hr{
	border:none;
	border-top:1px solid #f5f5f5;
}
.comm_info{
	margin-top: 10px;
	margin-left: 120px;
	margin-bottom: 10px;
	margin-right: 10px;
	width: 75%;
	word-break:break-all; 
	text-align: left;
}
.one_reply{
	margin-left: 14%;
}
.comm_reply_list{
	background-color: #f5f5f5;
	width: 100%;
	padding-top: 20px;
	padding-bottom: 20px;
}

.no_like_back{
	background-image: url("http://t1.golf-brother.com/data/images/like.png");
	background-repeat: no-repeat;
	padding-left: 50px;
	padding-top: 4px;
}
.yes_like_back{
	background-image: url("http://t1.golf-brother.com/data/images/like_touched.png");
	background-repeat: no-repeat;
	padding-left: 50px;
	padding-top: 4px;
}
.like_position{
	position: absolute;
	right: 40px;
	top:0;
}

.time{
	padding-bottom: 0;
	/*margin-top: 15px;*/
	margin-bottom: 0;
}
.one_commenter{
	margin-top: 50px;
	/*margin-bottom: 30px;*/
}
</style>
<script>
var courseid = 0;
var userid = 0;
var is_show_code = 0;
<?php 
	echo "courseid=".$courseid.";";
	echo "userid=".$userid.";";
	echo "is_show_code=".$is_show_code.";";

?>

function reply(commentid,id,nickname){
	if(is_show_code == 1){
		return;
	}
	var url = "reply://?commentid="+commentid+"&userid="+id+"&nickname="+nickname;
	document.location = url;
}
function on_heat(){
	$("#heat").addClass("am-btn-primary");
	$("#heat").removeClass("am-btn-default");
	$("#new").addClass("am-btn-default");
	$("#new").removeClass("am-btn-primary");
	var order_by = "heat";
	$.post("http://test.golf-brother.com/web/webpage/course_comment_list/",
	  {
	    courseid:courseid,
	    userid:userid,
	    order_by:order_by
	  },
	  function(data,status){
	  	if(data.error_code == 1){
	  		$(".one_commenter").empty();
	  		$("hr").remove();
	  		$('#hidden_div').after(data.html_str);
	  	}else{
	  		console.log(data.error_descr);
	  	}
	});
}
function on_new(){
	$("#new").addClass("am-btn-primary");
	$("#new").removeClass("am-btn-default");
	$("#heat").addClass("am-btn-default");
	$("#heat").removeClass("am-btn-primary");
	var order_by = "new";
	$.post("http://test.golf-brother.com/web/webpage/course_comment_list/",
	  {
	    courseid:courseid,
	    userid:userid,
	    order_by:order_by
	  },
	  function(data,status){
	  	if(data.error_code == 1){
	  		$(".one_commenter").empty();
	  		$("hr").remove();
	  		$('#hidden_div').after(data.html_str);
	  	}else{
	  		console.log(data.error_descr);
	  	}
	});
}
function like(btn,id,like_status){
	if(is_show_code == 1){
		return;
	}
	$.post("http://test.golf-brother.com/web/webpage/course_comment_like/",
	  {
	    course_comment_id:id,
	    userid:userid
	  },
	  function(data,status){
	  	if(data.error_code == 1){
	  		if(data.operation == "delete"){
	  			var span = $(btn).children();
		  		var num = $(span).text();
		  		num = parseInt(num);
		  		var new_num = num-1;
		  		$(span).text(new_num);
		  		$(span).removeClass("yes_like_back");
		  		$(span).addClass("no_like_back");
	  		}
	  		if(data.operation == "add"){
	  			var span = $(btn).children();
		  		var num = $(span).text();
		  		num = parseInt(num);
		  		var new_num = num+1;
		  		$(span).text(new_num);
		  		$(span).addClass("yes_like_back");
		  		$(span).removeClass("no_like_back");
	  		}
	  	}else{
	  		console.log(data.error_descr);
	  	}
	  });
}

function jump_hole_info(holeid,grades){
	window.location.href="http://test.golf-brother.com/web/webpage/cloud_hole_info/?courseid="+courseid+"&holeid="+holeid+"&userid="+userid+"&grades="+grades;
}

function jump_hole(holeid){
	var grades = $("#hole_"+holeid).attr("grades");
	console.log(grades);
	window.location.href="http://test.golf-brother.com/web/webpage/cloud_hole_info/?courseid="+courseid+"&holeid="+holeid+"&userid="+userid+"&grades="+grades;
}


function client_request(note){
	if(is_show_code == 1){
		return;
	}
	if(note.length < 1){
		return false;
	}
	var request_status = 0;
	var obj_id = 0;
	$.ajax({
		  url:'http://test.golf-brother.com/web/webpage/course_comments/', 
     	  data:{courseid:courseid,userid:userid,content:note},  
    	  type:'post',  
          cache:false,
          async:false,
          dataType:'json',  
          success:function(data) {
          	var course_comment_num = $("#course_comment_num").text();
          	var new_comment_num = parseInt(course_comment_num)+1;
          	$("#course_comment_num").html(new_comment_num);
          	if(data.order_by == "new"){
	     		var first_hr = $("#comments_hr").first();
		     	$(data.html_str).insertBefore(first_hr);
	     	}

	     	if(data.order_by == "heat"){
	     		var last_hr = $("#comments hr").last();
		     	$(last_hr).after(data.html_str);
	     	}
	     	request_status = 1;
	     	obj_id = data.id;
          },  
          error : function() {  
          	request_status = 0;
          }
	});

	if(request_status == 1){
        $('html, body').animate({  
            scrollTop: $("#"+obj_id).offset().top  
        }, 1000);
		return true;
	}else{
		return false;
	}	
}



function client_request_reply(commentid,reply_userid,note){
	if(is_show_code == 1){
		return;
	}
	if(note.length < 1){
		return false;
	}
	var request_status = 0;
	$.ajax({
		  url:'http://test.golf-brother.com/web/webpage/add_reply/', 
     	  data:{courseid:courseid,userid:userid,content:note,reply_userid:reply_userid,commentid:commentid},  
    	  type:'post',  
          cache:false,
          async:false,
          dataType:'json',  
          success:function(data) {
          	var commentid = data.commentid;
	  		if($("#"+commentid+" .one_reply").length > 0){
	  			var last_reply = $("#"+commentid+" .one_reply").first();
	  			$(last_reply).before(data.html_str);
	  		}else{
				var last_reply = $("#"+commentid+" .comm_info");
				$(last_reply).after("<div class='comm_reply_list'>"+data.html_str+"</div>");
	  		}
	     	request_status = 1;
          },  
          error : function() {
          	request_status = 0;
          }
	});
          	
	if(request_status == 1){
        $('html, body').animate({  
            scrollTop: $("#"+commentid).offset().top  
        }, 1000);
		return true;
	}else{
		return false;
	}	
}

function check_phone_type(){
	var u = navigator.userAgent;
	if (u.indexOf('Android') > -1 || u.indexOf('Linux') > -1) {//安卓手机
	alert("安卓手机");
	// window.location.href = "mobile/index.html";
	} else if (u.indexOf('iPhone') > -1) {//苹果手机
	// window.location.href = "mobile/index.html";
	alert("苹果手机");
	} else if (u.indexOf('Windows Phone') > -1) {//winphone手机
	alert("winphone手机");
	// window.location.href = "mobile/index.html";
	}
}



$(document).ready(function(){
	$("#course_comments_btn").click(function(){
		$("#comments_content").val(" ");
	})
	$("#comments_submit").click(function(){
		var content = $("#comments_content").val();
		if((content.length) < 1){
			alert("没有内容");
			return;
		}
	    $.post("http://test.golf-brother.com/web/webpage/course_comments/",
		  {
		    courseid:courseid,
		    userid:userid,
		    content:content
		  },
		  function(data,status){
		     if(data.error_code == 1){
		     	if(data.order_by == "new"){
		     		var first_hr = $("#comments_hr").first();
			     	$(data.html_str).insertBefore(first_hr);
			     	console.log("这里是最新");
		     	}

		     	if(data.order_by == "heat"){
		     		var last_hr = $("#comments hr").last();
			     	$(last_hr).after(data.html_str);
			     	console.log("这里是热度");
		     	}
		     	$("#comments_window").modal('close');
		     	
		     }else{
				console.log("失败");
		     }
		  });
	})

	$("#reply_submit").click(function(){
		var content = $("#reply_content").val();
		if((content.length) < 1){
			alert("没有内容");
			return;
		}
		var reply_userid = $("#userid").val();
		var commentid = $("#commentid").val();
	    $.post("http://test.golf-brother.com/web/webpage/add_reply/",
		  {
		    courseid:courseid,
		    userid:userid,
		    content:content,
		    reply_userid:reply_userid,
		    commentid:commentid
		  },
		  function(data,status){
		  	if(data.error_code == 1){
		  		var commentid = data.commentid;
		  		if($("#"+commentid+" .one_reply").length > 0){
		  			var last_reply = $("#"+commentid+" .one_reply").first();
		  			$(last_reply).before(data.html_str);
		  			console.log("有内容");
		  		}else{
					var last_reply = $("#"+commentid+" .comm_info");
					$(last_reply).after(data.html_str);
					console.log("没有内容");
		  		}
		  		
			    
			    $("#reply_window").modal('close');
		  	}
		  });
	})


});
</script>
</head>

<body>
	<input type="button" onclick="check_phone_type" value="检测系统类型" />
	<div class="course_top am-vertical-align">
		<div class="am-vertical-align-bottom course_name">
			<?php echo $course_name; ?>
		</div>
	</div>
	<div style="padding-bottom:40px;text-align:left;padding-left:10px;padding-top:60px;background-color:#f5f5f5;">
		<p>截止今日，江湖人士在本球场共记录<font style="color:red;"><?php echo $all_game_num_by_course['course_all_game_num'];?></font>场完整成绩，其中我有<font style="color:red;"><?php echo $all_game_num_by_course['user_all_game_by_course_num'];?></font>场成绩。</p>
		<p style="font-size:20px;">完整成绩：完整记录全场成绩并结合高尔夫江湖计算方法。</p>
	</div>
	<div class="am-tabs" id="doc-my-tabs">
	  <ul class="am-tabs-nav am-nav am-nav-tabs am-nav-justify">
	    <?php foreach ($courts as $key => $one_court) { ?>
	  		<li <?php if($key == 0){echo "class='am-active'";}?>><a href="#<?php echo $one_court['courtid'];?>"><b><?php echo $one_court['courtname'];?></b></a></li>
	  	<?php } ?>
	  </ul>
	  <div class="am-tabs-bd" style="padding-left:0px;">
	   <?php foreach ($courts as $key => $one_court) { $holes = $one_court['holes']; ?>
	  			<div class="am-scrollable-horizontal am-tab-panel  <?php if($key == 0){echo ' am-active';}?>" id="<?php echo $one_court['courtid'];?>">
					  <table class="am-table am-table-bordered am-table-striped am-text-nowrap">
					  	<tr>
					  		<td class="td_class am-text-middle"><font style="font-size:24px;" color="#333">球洞</font></td>
					  		<td class="td_class am-text-middle"><font style="font-size:24px;" color="#333">标准杆</font></td>
					  		<td class="td_class am-text-middle"><font style="font-size:24px;" color="#333">平均杆差</font></td>
					  		<td class="td_class am-text-middle"><font style="font-size:24px;" color="#333">我的平均杆差</font></td>
					  		<td class="td_class am-text-middle"><font style="font-size:24px;" color="#333">难易度</font></td>
					  		<td class="td_class am-text-middle"><font style="font-size:24px;" color="#333">评论</font></td>
					  	</tr>
				    <?php foreach ($holes as $one_hole) {  ?>
				    	<tr id="hole_<?php echo $one_hole['holeid']; ?>" grades="<?php echo $one_hole['grades']; ?>" onclick="jump_hole_info(<?php echo $one_hole['holeid'];?>,<?php echo $one_hole['grades'];?>)">
				    		<td class="td_class am-text-middle"><button type="button" class="am-btn holename_class"><b style="margin-left:-9px;"><?php echo $one_hole['holename'];?></b></button></td>
				    		<td class="td_class am-text-middle"><font color="#333"><?php echo $one_hole['par'];?></font></td>
				    		<td class="td_class am-text-middle"><?php echo $one_hole['hole_average_gross'];?></td>
				    		<td class="td_class am-text-middle"><?php echo $one_hole['user_average_gross'];?></td>
				    		<td class="grades_color_<?php echo $one_hole['grades']; ?> td_class am-text-middle">
				    			<?php 
				    				for ($i=0; $i < $one_hole['grades']; $i++) { 
				    				echo "★";
				    				}

				    				if((5-$one_hole['grades']) > 0){
										$cha = 5-$one_hole['grades'];
										for ($i=0; $i < $cha; $i++) { 
											echo "<font color='#e5e5e5'>★</font>";
										}
									}
				    			?>
				    		</td>
				    		<td class="td_class am-text-middle"><?php echo $one_hole['hole_comment_num'];?></td>
				    	</tr>
				    <?php } ?>
				  </table>
				</div>
	  	<?php } ?>
	  </div>
	</div>
<script>
  $(function() {
    $('#doc-my-tabs').tabs();
  })

  $('#doc-my-tabs').find('a').on('opened.tabs.amui', function(e) {
  console.log('[%s] 选项卡打开了', $(this).text());
})
</script>

<div style="background-color:#f5f5f5;width:100%;height:60px;"></div>
<div class="am-text-middle" style="border-top:1px solid #e5e5e5;border-bottom:1px solid #e5e5e5;width:100%;padding-top:30px;padding-bottom:30px;padding-left:20px;background-color:#fff;text-align:left;">
			球场评论(<font id="course_comment_num"><?php echo $course_comments_num; ?></font>)
</div>

<div id="comments">
	<div id="hidden_div" style="display:none"></div>
	<hr id="comments_hr" style="display:none" />
	<?php foreach ($course_comments as $key => $one_comment) {  ?>
		<div class='one_commenter' id="<?php echo $one_comment['id'];?>">
			<img style="border-radius:50%;" align='left' src="<?php echo $one_comment['user_info']['user_picurl'];?>" />
			<div  class='text_class' style="text-align:left;position:relative;">
				<div onclick="reply(<?php echo $one_comment['id']; ?>,<?php echo $one_comment['userid']; ?>,'<?php echo $one_comment['user_info']['nickname'];?>')" class='user_nickname'><font style="font-size:26px;" color='#333'><?php echo $one_comment['user_info']['nickname'];?></font></div>
				<div onclick="reply(<?php echo $one_comment['id']; ?>,<?php echo $one_comment['userid']; ?>,'<?php echo $one_comment['user_info']['nickname'];?>')" class='time'><font style="font-size:24px;" color='#c9c9c9'><?php echo $one_comment['addtime']; ?></font></div>
				<div onclick="like(this,<?php echo $one_comment['id']?>,<?php echo $one_comment['like_status'];?>)" class="like_position">
					<span class="<?php if($one_comment['like_status'] == 0){echo 'no_like_back';}else{echo 'yes_like_back';}?>"><?php echo $one_comment['like_num'];?><span>
				</div>
			</div>
			<div class='comm_info' style="margin-bottom:30px;"><font style="font-size:30px;" color='#333'><?php if($one_comment['objecttype'] == "hole"){ $holeid = $one_comment['objectid'];  echo "<a onclick='jump_hole($holeid)'>#".$one_comment['holename']."#</a>"; }?>  <?php echo $one_comment['note']; ?></font></div>
			<?php if(count($one_comment['comments']) > 0){?>
			<div class="comm_reply_list">
			<?php foreach ($one_comment['comments'] as $row) {  ?>
				<div class="one_reply" >
					<img style="border-radius:50%;" align='left' src="<?php echo $row['user_info']['user_picurl'];?>" />
					<div class='text_class' style="text-align:left;">
						<div onclick="reply(<?php echo $one_comment['id']; ?>,<?php echo $row['userid']; ?>,'<?php echo $row['user_info']['nickname'];?>')" class='user_nickname'><font style="font-size:26px;" color='#333'><?php echo $row['user_info']['nickname'];?></font></div>
						<div onclick="reply(<?php echo $one_comment['id']; ?>,<?php echo $row['userid']; ?>,'<?php echo $row['user_info']['nickname'];?>')" class='time'><font style="font-size:24px;" color='#c9c9c9'><?php echo $row['addtime']; ?></font></div>
					</div>
					<div class='comm_info' style=""><font style="font-size:30px;" color='#999'><?php echo "回复 ".$row['pid_info']['nickname']."：</font><font style='font-size:30px;' color='#333'>".$row['note']; ?></font></div>
				</div>
			<?php } ?>
			</div>
			<?php } ?>
		</div>
		<hr id="comments_hr" style="background-color:#e5e5e5;height:1px;border:none;margin-top: 0px;" />
	<?php } ?>
</div>

	<!-- <div class="am-modal am-modal-no-btn" tabindex="-1" id="comments_window">
	  <div class="am-modal-dialog">
	    <div class="am-modal-hd">
	    	<h3 style="font-size:30px;">对本球场发表评论</h3>
	      <a href="javascript: void(0)" class="am-close am-close-spin" style="font-size:60px;right: 24px;" data-am-modal-close>&times;</a>
	    </div>
	    <div class="am-modal-bd">
		      <label for="doc-ta-1">文本域</label>
		      <textarea class="" rows="5" id="comments_content"></textarea>
	    </div>
	     <p><button type="submit" id="comments_submit" style="width:100%;height:15%;font-size:30px;" class="am-btn am-btn-secondary">提交</button></p>
	  </div>

	</div>

	


	<div class="am-modal am-modal-no-btn" tabindex="-1" id="reply_window">
	  <div class="am-modal-dialog">
	    <div class="am-modal-hd">
	    	<h3 style="font-size:30px;">对本球场发表评论</h3>
	      <a href="javascript: void(0)" class="am-close am-close-spin" style="font-size:60px;right: 24px;" data-am-modal-close>&times;</a>
	    </div>
	    <div class="am-modal-bd">
	    	  <input type="hidden" id="commentid" name="commentid" value="" />
	    	  <input type="hidden" id="userid" name="userid" value="" />
		      <label for="doc-ta-1">文本域</label>
		      <textarea class="" rows="5" id="reply_content"></textarea>
	    </div>
	     <p><button type="submit" id="reply_submit" style="width:100%;height:15%;font-size:30px;" class="am-btn am-btn-secondary">提交</button></p>
	  </div>

	</div> -->


</body>
</html>