<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv=content-type content="text/html; charset=utf-8">
	<title>高尔夫统计</title>
	<style type="text/css">
	body{
		 background-color:#f2f2f2;
		 font-family:Arial, sans-serif;
	}
	 table tr td{
	 	margin: 0px;
	 	padding: 0px;
	 	border:0px;
	 }
	 table{
	 	border-collapse:collapse;
	 	margin-top:20px;
	 }
		.par{
			background-color: #333;
			height: 44px;
			margin: 0px;
			padding: 0px;
		}
		#score td{
			border-bottom:#e5e5e5 solid 1px;
		}
		#time{
			margin-top:30px;
		}
	.button_class{
		    background: none repeat scroll 0 0 #0099ff;
		    color: #fff;
		    font-size: 40px;
		    margin: 60px auto 60px;
		    position: relative;
		    width: 80%;
		    z-index: 2;
		    border-radius: 8px;
		    cursor: pointer;
		    display: block;
		    font-weight: 700;
		    padding: 40px 60px 39px;
		    text-align: center;
		    text-decoration: none;
	}
	.QRcode{
		color:#333;
		text-align: center;
		margin-bottom: 60px; 
		font-size: 36px; 
	}
	.qrcode_pic{
		margin:40px auto 20px;
		padding-bottom: 50px;
		padding-top: 30px;
	}
	.font_class{
		line-height: 60px;
	}
	</style>

	<script>
var _hmt = _hmt || [];
(function() {
  var hm = document.createElement("script");
  hm.src = "//hm.baidu.com/hm.js?42514b23f3dd908b8ffbd0fb40f621f4";
  var s = document.getElementsByTagName("script")[0]; 
  s.parentNode.insertBefore(hm, s);
})();
</script>

</head>
<body>
	<div style="width:100%;">
	<img src="http://s1.golf-brother.com/data/images/title_logo.png" align="right" height="40px" /><br /><br />
	<?php if(strlen($team_game_name) > 1) { ?>
	<span style="font-size:200%;"><font color="#333"><?php echo $team_game_name;?></font></span><br /><br />
	<?php }?>
	<span style="font-size:180%;"><font color="#333"><?php echo $coursename;?></font></span><br />
	<span id="time" style="font-size:120%;"><font color="#999"><?php echo $time;?></font></span><br />
	
	</div>
	<div style="background-color:#fff; width:100%;">
	<table style="width:100%;">
		<tr align="center" class="par">
			<td style="width:4%;"><font color="#fff">排名</font></td>
			<td style="width:4%;"><font color="#fff">球手</font></td>
			<td style="width:10%;"></td>
			<!--<td style="font-size:120%; border-left:#fff solid 1px;border-right:#fff solid 1px;" width="50"><font color="#fff">信天翁</font></td>-->
			<td style="font-size:120%; border-left:#fff solid 1px;border-right:#fff solid 1px;" width="50"><font color="#fff">老鹰</font></td>
			<td style="font-size:120%; border-left:#fff solid 1px;border-right:#fff solid 1px;" width="50"><font color="#fff">小鸟</font></td>
			<td style="font-size:120%; border-left:#fff solid 1px;border-right:#fff solid 1px;" width="50"><font color="#fff">标杆</font></td>
			<td style="font-size:120%; border-left:#fff solid 1px;border-right:#fff solid 1px;" width="50"><font color="#fff">柏忌</font></td>
			<td style="font-size:120%; border-left:#fff solid 1px;border-right:#fff solid 1px;" width="50"><font color="#fff">双柏忌+</font></td>
			<td style="font-size:120%; border-left:#fff solid 1px;border-right:#fff solid 1px;" width="50"><font color="#fff">标On率</font></td>
			<td style="font-size:120%; border-left:#fff solid 1px;border-right:#fff solid 1px;" width="50"><font color="#fff">推杆</font></td>
			<td style="font-size:120%; border-left:#fff solid 1px;border-right:#fff solid 1px;" width="50"><font color="#fff">命中球道</font></td>
			<td style="font-size:120%; border-left:#fff solid 1px;border-right:#fff solid 1px;" width="50"><font color="#fff">沙坑数</font></td>
			<td style="font-size:120%; border-left:#fff solid 1px;border-right:#fff solid 1px;" width="50"><font color="#fff">罚杆</font></td>
			<td style="font-size:120%; border-left:#fff solid 1px;border-right:#fff solid 1px;" width="50"><font color="#fff">总杆</font></td>
			
		</tr>
		<?php 

		     

			foreach ($user_game_score as $key => $value) {
				$out_gross = array();
				$in_gross = array();
				$gross = $value['gross'];
				$pushsum = intval($value['pushsum']);
				$On = trim($value['On']);
				
				$hit_fairway = $value['hit_fairway'];
				$user_game_sandball = intval($value['user_game_sandball']);
				$user_game_penalty = intval($value['user_game_penalty']);
				if($value['user_info']['tland'] == "BLUE"){
					$tland = "蓝T";
					$img = '<img src="http://s1.golf-brother.com/data/images/blue-t.png" />';
				}elseif ($value['user_info']['tland'] == "RED") {
					$tland = "红T";
					$img = '<img src="http://s1.golf-brother.com/data/images/red-t.png" />';
				}elseif ($value['user_info']['tland'] == "GOLD") {
					$tland = "金T";
					$img = '<img src="http://s1.golf-brother.com/data/images/gold-t.png" />';
				}elseif ($value['user_info']['tland'] == "WHITE") {
					$tland = "白T";
					$img = '<img src="http://s1.golf-brother.com/data/images/white-t.png" />';
				}elseif ($value['user_info']['tland'] == "BLACK") {
					$tland = "黑T";
					$img = '<img src="http://s1.golf-brother.com/data/images/black-t.png" />';
				}
				$grosssum = $value['grosssum'];
				$rank = $key+1;
				if($key != 0){
					if($grosssum == $user_game_score[$key-1]['grosssum']){
						if($user_rank > 0){
							$rank = $user_rank;
						}else{
							$rank = $key;
							$user_rank = $rank;
						}
						
					}else{
						$user_rank = -1;
					}
				}

				$index = $key+1;
				$yushu = $index%2;
				if($yushu > 0){
					$color = "#fff";
				}else{
					$color = "#f5f5f5";
				}
				if($using_plug){
				// echo '<tr bgcolor="'.$color.'" id="score" align="center" style="border-bottom:1px solid #f2f2f2;"><td align="center" style="font-size:150%; ">'.$rank.'</td><td style="padding-top:4px"><img height="54px" width="54px" src="'.$value['user_info']['user_picurl'].'"></td><td style="padding-left:10px;" align="left">'.$value['user_info']['nickname'].'<br />'.$img.$tland.'</td>';
				

				echo '<tr bgcolor="'.$color.'" id="score" align="center" style="'.$border.';"><td align="center" style="font-size:150%; ">'.$rank.'</td><td style="padding-top:4px"><img height="59px" width="59px" src="'.$value['user_info']['user_picurl'].'"></td><td style="padding-left:2px;padding-right:2px;" align="left">'.
				'<div style="color:#000;background-color:'.$value['user_info']['ucolor'].';">'.$value['user_info']['nickname'].'<br/><div style="font-size:85%;">'.$value['user_info']['utitle'].'<br /></div>'.$img.$tland.'</div></td>';

				}
				else
				{
				echo '<tr bgcolor="'.$color.'" id="score" align="center" style="border-bottom:1px solid #f2f2f2;"><td align="center" style="font-size:150%; ">'.$rank.'</td><td style="padding-top:4px"><img height="54px" width="54px" src="'.$value['user_info']['user_picurl'].'"></td><td style="padding-left:10px;" align="left">'.$value['user_info']['nickname'].'<br />'.$img.$tland.'</td>';
			
				}
			 	 
				$eagle = intval($value['eagle']);
				if($eagle == 0){
					echo '<td style="font-size:150%;"><font color="#d8d8d8">'.$eagle.'</font></td>';
				}else{
					echo '<td style="font-size:150%;">'.$eagle.'</td>';
				}
				$bird = intval($value['bird']);
				if($bird == 0){
					echo '<td style="font-size:150%;"><font color="#d8d8d8">'.$bird.'</font></td>';
				}else{
					echo '<td style="font-size:150%;">'.$bird.'</td>';
				}
				$par = intval($value['par']);
				if($par == 0){
					echo '<td style="font-size:150%;"><font color="#d8d8d8">'.$par.'</font></td>';
				}else{
					echo '<td style="font-size:150%;">'.$par.'</td>';
				}
				$bogey = intval($value['bogey']);
				if($bogey == 0){
					echo '<td style="font-size:150%;"><font color="#d8d8d8">'.$bogey.'</font></td>';
				}else{
					echo '<td style="font-size:150%;">'.$bogey.'</td>';
				}
				$double_bogey = intval($value['double_bogey']);
				if($double_bogey == 0){
					echo '<td style="font-size:150%;"><font color="#d8d8d8">'.$double_bogey.'</font></td>';
				}else{
					echo '<td style="font-size:150%;">'.$double_bogey.'</td>';
				}
				if($On == 0){
					if($grosssum > 0){
						echo '<td style="font-size:150%"><font color="#d8d8d8">'.$On.'%</font></td>';
					}else{
						echo '<td style="font-size:150%"><font color="#999">-</font></td>';
					}
				}else{
					echo '<td style="font-size:150%">'.$On.'%</td>';
				}
				if($pushsum == 0){
					echo '<td style="font-size:150%"><font color="#d8d8d8">'.$pushsum.'</font></td>';
				}else{
					echo '<td style="font-size:150%">'.$pushsum.'</td>';
				}
				if($hit_fairway > 0){
					echo '<td style="font-size:150%"><font color="#d8d8d8">'.$hit_fairway.'%</font></td>';
				}else{
					echo '<td style="font-size:150%"><font color="#999">-</font></td>';
				}
				if($user_game_sandball > 0){
					echo '<td style="font-size:150%"><font color="#d8d8d8">'.$user_game_sandball.'</font></td>';
				}else{
					echo '<td style="font-size:150%"><font color="#999">-</font></td>';
				}
				if($user_game_penalty == 0){
					echo '<td style="font-size:150%"><font color="#d8d8d8">'.$user_game_penalty.'</font></td>';
				}else{
					echo '<td style="font-size:150%">'.$user_game_penalty.'</td>';
				}
				echo '<td style="font-size:180%">'.$grosssum.'</td>';
				echo "</tr>";
			}
		?>
	</table>
</div>
<?php if($isShowCode == 1){?>
<div>
	
	<div class="QRcode">
	
	<img src="http://s1.golf-brother.com/data/images/brother_QRcode.jpeg" class="qrcode_pic" width="320" height="320" /><br />
	<div class="font_class">
		<b>关注公众号，更多精彩等着你！</b><br />
		<font color="#666">双击二维码后长按， 识别  添加或搜索微信号</font><br /><font color="#0099ff"> golfbrother</font>
	</div>
	</div>
	<a class="button_class" href="http://www.golf-brother.com/">精彩球事，下载高尔夫江湖</a>
</div>
<?php } ?>
</body>
</html>