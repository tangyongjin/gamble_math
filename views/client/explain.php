<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=0;" name="viewport"/>
<title>游戏规则说明</title>
<link rel="stylesheet" type="text/css" href="<?php echo STATIC_HOST;?>/res/css/amazeui.min.css" />
<script type="text/javascript" src="<?php echo STATIC_HOST;?>/res/js/jweixin-1.0.0.js"></script>
<script src="http://s1.golf-brother.com/data/attach/js/jquery.js" type="text/javascript" charset="utf-8"></script>
<script src="<?php echo STATIC_HOST;?>/res/js/amazeui.min.js" type="text/javascript" charset="utf-8"></script>
<style>
body{
	margin:0;
	padding: 0;
	width: 100%;
	height: 100%;
	text-align: center;
	font-family: Helvetica Neue,Helvetica,Arial,sans-serif;
	font-size: 16px;
}
.a_font_class{
	font-size: 18px;
	text-align: left;
}
.three_layer{
	margin-top: 0;
	padding-top: 0;
}
.no_border_li{
  border-style: hidden;
}
.no_border_li ul li a{
	margin-left: 30px;
}
/*.three_layer li a{
	margin-left: 9px;
	margin-right: 9px;
	border-style: none;
}*/
p{
	text-indent:2em;
	word-wrap:break-word; 
	word-break:normal;
	line-height:28px;
	text-align: left;
}
div{
	background-color: #f5f5f5;
	margin-top: 23px;
	margin-left: 10px;
	margin-right: 10px;
}
ul{
	background-color: #f5f5f5;
}

</style>
</head>
<body>
	<ul class="am-list admin-sidebar-list" id="collapase-nav-1">
	  <li class="am-panel">
	    <a class="a_font_class" data-am-collapse="{parent: '#collapase-nav-1', target: '#user-nav'}">
	        <i class="am-margin-left-sm"></i> 1、游戏简介
	    </a>
	  </li>
	  <div class="am-collapse admin-sidebar-sub" id="user-nav">
	       <p>游戏是按照目前高尔夫爱好者通行的游戏方式，通过配置一些参数，根据计分卡数据，由系统自动计算出结果的一个功能模块。</p>
	       <p>由于游戏种类在不同地域玩的方式千差万别，所以我们目前以及更远的将来可能都无法将所有的种类全部覆盖。</p>
	    </div>



	  <li class="am-panel">
	    <a class="a_font_class" data-am-collapse="{parent: '#collapase-nav-1', target: '#kinds'}">
	        <i class="am-margin-left-sm"></i> 2、游戏种类说明
	    </a>

	    <ul style="" class="am-list am-collapse admin-sidebar-sub" id="kinds">
	        <li><a class="a_font_class" data-am-collapse="{target: '#two_game'}"><i class="am-margin-left-sm"></i> 2.1&nbsp;&nbsp;二人游戏</a></li>
		    <div class="am-collapse admin-sidebar-sub" id="two_game">
		        	<p>2人游戏包括比杆和比洞两种。</p>
		    </div>
	        <li><a class="a_font_class" data-am-collapse="{target: '#game'}"><i class="am-margin-left-sm"></i> 2.2&nbsp;&nbsp;三人游戏</a></li>

	       <li class="am-panel no_border_li" style="border-top-width: 0px; border-bottom-width: 0px;">
		        <ul class="am-list am-collapse admin-sidebar-sub three_layer" id="game" style="margin-bottom:-20px;padding-bottom:20px;">
		        	<li ><a class="a_font_class" data-am-collapse="{target: '#lord_proprietor'}"><i class="am-margin-left-sm"></i> 2.2.1&nbsp;&nbsp;斗大地主</a></li>
		        	<div class="am-collapse admin-sidebar-sub" id="lord_proprietor">
		        		<p>即前洞第一名为本洞地主，第二、三名最好成绩与地主比较计算本洞输赢。</p>
		        	</div>
		        	<li><a class="a_font_class" data-am-collapse="{target: '#sublandlord'}"><i class="am-margin-left-sm"></i> 2.2.2&nbsp;&nbsp;斗二地主</a></li>
		        	<div class="am-collapse admin-sidebar-sub" id="sublandlord">
		        		<p>即前洞第二名为本洞地主，第一、三名的平均成绩与地主比较计算本洞输赢。</p>
		        	</div>
		        	<li><a class="a_font_class" data-am-collapse="{target: '#two_dozen'}"><i class="am-margin-left-sm"></i> 2.2.3&nbsp;&nbsp;二打一</a></li>
		        	<div class="am-collapse admin-sidebar-sub" id="two_dozen">
		        		<p>即在三人中固定某人做地主，无论前洞成绩如何，总是由另2人的最好成绩与地主比较计算本洞输赢。</p>
		        	</div>
		        </ul>
	    	</li>

	        <li><a class="a_font_class" data-am-collapse="{target: '#four_game'}"><i class="am-margin-left-sm"></i> 2.3&nbsp;&nbsp;四人游戏</a></li>
	        <li class="am-panel no_border_li" style="border-top-width: 0px; border-bottom-width: 0px;">
		        <ul class="am-list am-collapse admin-sidebar-sub three_layer" id="four_game" style="margin-bottom:-20px;padding-bottom:20px;">
		        	<li><a class="a_font_class" data-am-collapse="{target: '#wire_drawing'}"><i class="am-margin-left-sm"></i> 2.3.1&nbsp;&nbsp;拉丝</a></li>
		        	<div class="am-collapse admin-sidebar-sub" id="wire_drawing">
						<p>拉丝一般可以在三个点上进行比较，即头点、尾点和总成绩。头点即为Pk双方各自组合中最好成绩那一点，尾点即为最差成绩那一点，总成绩为各自组合的综合成绩那一点（综合成绩的计算方法包括乘法和加法两种配置方式。</p>
						<p>比如3、5对4、4，按加法总杆双方打平，但按乘法总杆则3*5小于4*4，杆数为3、5的组合获胜这一点。</p>
	       				<p>拉丝游戏根据上面三个点，可以自由配置拉丝一点（头点或总成绩）、拉丝两点（头点+尾点或者头点+总成绩）以及拉丝三点（头点+尾点+总成绩）。</p>
					</div>
		        	<li><a class="a_font_class" data-am-collapse="{target: '#four_fight_against_landlords'}"><i class="am-margin-left-sm"></i> 2.3.2&nbsp;&nbsp;四人斗地主</a></li>
		        	<div class="am-collapse admin-sidebar-sub" id="four_fight_against_landlords">
		        		<p>此游戏项下的规则几乎等同于3人游戏中的斗地主规则。唯一不同的是，4人中固定2人捆绑在一起，以这个组合的最好成绩与另外2人进行斗地主的游戏。</p>
		        	</div>
		        	<li><a class="a_font_class" data-am-collapse="{target: '#four_hit_one'}"><i class="am-margin-left-sm"></i> 2.3.3&nbsp;&nbsp;三打一</a></li>
		        	<div class="am-collapse admin-sidebar-sub" id="four_hit_one">
		        		<p>即在4人中固定某人做地主，无论前洞成绩如何，总是由其他3人最好成绩与地主比较计算本洞输赢。</p>
		        	</div>
		        </ul>
		    </li>
	    </ul>
	  </li>




	  <li class="am-panel">
	    <a class="a_font_class" data-am-collapse="{parent: '#collapase-nav-1', target: '#rules'}">
	        <i class="am-margin-left-sm"></i> 3、游戏规则详解 
	    </a>

	    <ul class="am-list am-collapse admin-sidebar-sub" id="rules" style="margin-bottom:-20px;padding-bottom:20px;">
	        <li><a class="a_font_class" data-am-collapse="{target: '#top_rule'}"><i class="am-margin-left-sm"></i>3.1&nbsp;&nbsp;封顶规则</a></li>
	        <div class="am-collapse admin-sidebar-sub" id="top_rule">
	        	<p>配置中涉及到的封顶规则指的是完成本洞的实际杆数的封顶值。</p>
	        	<p>如果设定了封顶值，那么无论本洞选手打得多差，也无论计分卡上记录的杆数为多少，系统在游戏结果计算时，顶多只按照封顶值来计算本洞游戏结果。</p>
	        	<p>举例说明：在某比杆游戏中设置Double Par封顶。</p>
	        	<p>在某4杆洞中，A选手用10杆完成本洞，B选手用8杆完成本洞，C选手用4杆完成本洞。那么系统在计算A、B两选手游戏结果时，将认为此洞双方打平，没有输赢。</p>
	        	<p>在计算A、C两选手游戏结果时，将认为C选手赢了A选手4分，而非10-4=6分。</p>
	        </div>
	        <li><a class="a_font_class" data-am-collapse="{target: '#top_hole_rule'}"><i class="am-margin-left-sm"></i>3.2&nbsp;&nbsp;顶洞规则</a></li>
	        <li class="am-panel no_border_li" style="border-top-width: 0px; border-bottom-width: 0px;">
		        <ul class="am-list am-collapse admin-sidebar-sub three_layer" id="top_hole_rule">
		        	<li><a class="a_font_class" data-am-collapse="{target: '#3_2_1'}"><i class="am-margin-left-sm"></i>3.2.1&nbsp;&nbsp;帕1鸟2鹰3</a></li>

		        	<div class="am-collapse admin-sidebar-sub" id="3_2_1">
		        		<p>这种配置指的当游戏双方的胜方中有人成绩为帕、鸟或者鹰及以上成绩时，对于之前的顶洞（肉），分别可以赢得1个、2个和3个（如果顶洞（肉）足够多的话）。胜方如果最好成绩为博忌或更差时，无法赢得顶洞（肉），顶洞（肉）数量仍旧保持不变。</p>
				        <p>在4人进行拉丝两点或拉丝三点时，还会遇到赢了的顶洞（肉）算几点的问题。系统给了两种解决方案。第一种是肉算一点。即无论本洞赢了几点，吃掉的每一个顶洞（肉）只算一点。第二种是本洞赢几点肉算几点。即本洞如果可以赢得之前的顶洞（肉），那么本洞赢了几点，那么吃掉的每一个顶洞（肉）就算几点，不过由于鸟炸得到的额外奖励不包含在内。</p>
				        <p>举例说明：</p>
				        <p>例一。在某拉丝三点的游戏中，AB两人一组与CD两人Pk。在四杆洞开始时，前面已经形成了3个顶洞（肉）。本洞ABCD四人分别用3、5、4、6杆完成比赛。</p>
				        <p>那么本洞AB组合赢得了头点尾点和总成绩三项，得3分。由于A得3杆为鸟，在选择鸟*2的规则下，本洞AB组合获得3*2=6分。</p>
				        <p>如果在顶洞规则中，选择的是帕1鸟2鹰3（肉算一点）的选项，那么，AB组合将吃掉之前3个顶洞（肉）中的2个，且这2个顶洞（肉）每个只算1点，所以，最终，AB组合每人应获得3*2+1+1=8分，而对应CD组合每人减8分。</p>
				        <p>如果在顶洞规则中，选择的是帕1鸟2鹰3（本洞赢几点肉算几点）的选项，那么，AB组合也是吃掉之前3个顶洞（肉）中的2个，但这2个顶洞（肉）每个都算3点（而非6点），所以，最终，AB组合每人应获得3*2+3+3=12分，而对应CD组合每人减12分。</p>
				        <p>例二。在某拉丝三点的游戏中，EF两人一组与GH两人Pk。在四杆洞开始时，前面已经形成了3个顶洞（肉）。本洞EFGH四人分别用5、6、6、7杆完成比赛。</p>
				        <p>那么本洞EF组合赢得了头点尾点和总成绩三项，得3分。由于选择了帕1鸟2鹰3规则，而胜方EF两人最好成绩为博忌，因此前面形成的3个顶洞（肉）不受影响，仍旧保持，即使下一洞组合发生了变动（EH对FG）。所以，最终，EF组合每人获得3分，对应GH组合每人减掉3分。</p>
		        	</div>
		        	<li><a class="a_font_class" data-am-collapse="{target: '#3_2_2'}"><i class="am-margin-left-sm"></i>3.2.2&nbsp;&nbsp;全收全炸</a></li>
		        	<div class="am-collapse admin-sidebar-sub" id="3_2_2">
			        	<p>此配置下，意味着无论之前有多少顶洞（肉），只要赢得本洞，那么之前所有的顶洞（肉）将全部被胜方获得，不需要胜方有帕或更好的成绩。而且，一旦胜方有鸟发生Double的情况，前面所有的顶洞（肉）也以此来计算！</p>
		         		<p>以3.2.1中例项说明。</p>
		         		<p>例一中，如果四人选择的是全收全炸配置项，那么，AB本洞赢得3分*2=6分，前面的3个顶洞（肉）均按照6分计算，因此本洞AB组合将获得3*2+3*2+3*2+3*2=24分！</p>
		         		<p>例二中，虽然EF组合最好成绩为博忌，但在全收全炸配置项下，EF组合也可将之前的3个顶洞（肉）揽入囊中，最终获得3+3+3+3=12分！</p>
	         		</div>
		        	<li><a class="a_font_class" data-am-collapse="{target: '#3_2_3'}"><i class="am-margin-left-sm"></i>3.2.3&nbsp;&nbsp;顶洞即过</a></li>
		        	<p class="am-list am-collapse admin-sidebar-sub" id="3_2_3">此配置项下，任何洞发生双方打平的情况即Pass，不累计。后洞无论何种情况都不与之前任何打平的情况发生关系。</p>
		        </ul>
		    </li>
	        <li><a class="a_font_class" data-am-collapse="{target: '#3_3'}"><i class="am-margin-left-sm"></i>3.3&nbsp;&nbsp;包洞规则</a></li>
	        <div class="am-collapse admin-sidebar-sub" id="3_3">
	        	<p>此规则是对3人或4人游戏中，对失败一方发挥较差的球手的惩罚规则，失败方发挥较差球手在达到包洞规则中约定的条件时，将单独承担本洞失分，而同组球手不失分。</p>
        		<p>当此规则配置为不包洞时，失败方较差球手无论用多少杆完成本洞，都将不会单独承担失败而导致的失分。</p>
         		<p>系统默认发生包洞情况时包洞选手只包本洞的失分，而不包顶洞（肉），此项目前是不可被配置的。</p>
         	</div>
         	<li><a class="a_font_class" data-am-collapse="{target: '#3_4'}"><i class="am-margin-left-sm"></i>3.4&nbsp;&nbsp;奖励规则</a></li>
         	<li class="am-panel no_border_li" style="border-top-width: 0px; border-bottom-width: 0px;">
		        <ul class="am-list am-collapse admin-sidebar-sub three_layer" id="3_4" style="margin-bottom:-20px;padding-bottom:20px;">
		        	<li><a class="a_font_class" data-am-collapse="{target: '#3_4_1'}"><i class="am-margin-left-sm"></i>3.4.1&nbsp;&nbsp;乘法奖励</a></li>
		        	<div class="am-collapse admin-sidebar-sub" id="3_4_1"> 
		        		<p>当选择乘法奖励的时候，系统将根据获胜方赢得的分数乘以配置中的倍数得到最终本洞的得分数，而失败方即使有鸟或以上成绩，奖励规则都将不发生作用。</p>
		        		<p>在拉丝游戏中，如果获胜方两人都得到鸟或更好成绩，系统将按照连续乘的原则来处理最终得分。</p>
		        		<p>比如配置鸟*2的规则，那么获胜方在两人都打鸟的情况下，将按照本洞基础赢得的分数*2*2来得到获胜方本洞最终赢得的分数。</p>
	        			<p>举例说明：</p>
	        			<p>例三。AB与CD四人在某4杆洞中进行拉丝三点游戏且奖励规则配置为鸟*2。</p>
	        			<p>四人本洞成绩分别为3、3、3、4，那么按照本洞基础得分原则（头点打平，尾点得1分，总成绩得1分），AB组合应得2分。再根据奖励规则中鸟*2的配置，由于AB两人都打到鸟，所以AB组合最终本洞每人得分为2分*2*2=8分。</p>
	        		</div>
	        		<li><a class="a_font_class" data-am-collapse="{target: '#3_4_2'}"><i class="am-margin-left-sm"></i>3.4.2&nbsp;&nbsp;加法奖励</a></li>
	        		<div class="am-collapse admin-sidebar-sub" id="3_4_2">
	        			<p>当选择加法奖励配置项时，系统将对参与游戏的所有球手中获得鸟或更好成绩的球手进行奖励，而无论此球手所在组合是否获胜。</p>
	        			<p>以3.4.1中例三说明。如果配置项为鸟+1时，AB组合对CD组合，头点双方都是鸟，各自奖励1分，所以仍旧打平。</p>
	        			<p>尾点获胜得1分，由于是鸟得到奖励1分，因此尾点得2分。总成绩获胜得1分。因此本洞最终AB组合获得2分+1分=3分。</p>
	        			<p>再如例四。EF组合与GH组合四人在某4杆洞中分别以3、7和4、4杆数完成本洞。EF组合赢得头点，但输了尾点和总成绩。</p>
	        			<p>由于头点为鸟要奖励1分，所以最终本洞双方打平。</p>
	        		</div>
		        </ul>
		    </li>
	        <li><a class="a_font_class" data-am-collapse="{target: '#3_5'}"><i class="am-margin-left-sm"></i>3.5&nbsp;&nbsp;让杆规则</a></li>
	        <div class="am-collapse admin-sidebar-sub" id="3_5">
	        	<p>此规则针对游戏参与者有明显差距时弱者受让而设计。</p>
	        	<p>此规则项下，系统以受让者实际完成本洞杆数-受让杆数的结果作为受让者参与游戏的计算杆数。</p>
	        	<p>特别指出，在4人进行拉丝游戏的时候，受让者的计算杆数仅仅只在总成绩这一点发生作用，而受让者在头点和尾点的游戏比较仍旧以他本洞实际完成杆数作为游戏计算杆数。</p>
	        </div>
	    </ul>
	  </li>



	  <li class="am-panel">
	    <a class="a_font_class" data-am-collapse="{parent: '#collapase-nav-1', target: '#game_suggest'}">
	        <i class="am-margin-left-sm"></i> 4、游戏建议
	    </a>
	  </li>

	  <div class="am-collapse admin-sidebar-sub" id="game_suggest" style="padding-bottom:20px;">
	       <p>在实际游戏过程中，由于球手水平有差距，根据不同情况可以选定不同的游戏来减小差距，达到更加公平的状态。</p>
       	   <p>以4人打球为例：</p>
           <p>当参与游戏的球手为强、强、强、弱时，建议采用让杆规则，对弱进行让杆。或者采用拉丝头点的规则进行游戏。</p>
           <p>当参与游戏的球手为强、强、弱、弱时，建议采用固定拉丝的规则进行游戏。</p>
           <p>当参与游戏的球手为强、强、次强、弱时，建议采用4人斗地主的规则，让次强、弱固定组合与另2人进行斗地主游戏。</p>
	    </div>

	</ul>
</body>
</html>