<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no">
        <meta name="format-detection" content="telephone=no">
        <title>游戏结果</title>

        <link rel="stylesheet" type="text/css" href="http://www.golf-brother.com/gamble/css/bootstrap.min.css" />
        <link rel="stylesheet" type="text/css" href="http://www.golf-brother.com/gamble/css/gamble.css" />
        <link rel="stylesheet" type="text/css" href="http://s1.golf-brother.com/data/static/res/css/logger.css" />
        <script src=http://www.golf-brother.com/gamble/js/jquery-1.11.2.min.js></script>
        <script src=http://s1.golf-brother.com/data/static/res/js/zepto.min.js></script>
        <script src=http://s1.golf-brother.com/data/attach/js/g27.js></script>

       
      
    </head>
    <body>
  <br/>

  <?php

  // echo "test";
  ?>

  <table class=table-bordered style="margin-left:10px;"><tr>
  <td>
    <h4>&nbsp;&nbsp;版本管理:Git-dev分支</h4>
    <h5>&nbsp;&nbsp;分组测试完成.</h5>
    <div style="margin:10px 0 0 10px;font-size:14px;"> 
      2人游戏参加者:A高攀_a1,A图图_a2. <br/>
      3人游戏参加者:A高攀_a1,A图图_a2,B戈多_b1 <br/>
      4人游戏参加者:A高攀_a1,A图图_a2,B戈多_b1,B何斌_b2 <br/>
      初始排名为:A高攀_a1,A图图_a2,B戈多_b1,B何斌_b2<br/>
      如果是AB分组法,则(A高攀_a1,A图图_a2)vs(B戈多_b1,B何斌_b2)<br/>
      如果有组合,则为B戈多_b1,B何斌_b2两人模拟一个人<br/>
      如果是固定3打1,则为(A高攀_a1)vs(A图图_a2,B戈多_b1,B何斌_b2)<br/>
      如果是跟随模式,则B何斌_b2是跟随者(最弱).<br/>
      
  </div>

   </td>
    <td> 

         
    <div style="font-size:14px;">  &nbsp;&nbsp;参与人员/被让方<br/>
    </div>

        
    <table   class=table-bordered style="margin:10px;font-size:14px;">
      <tr><td>Userid</td><td>Nickname<td></td></tr>
      <tr><td>160</td><td><input class="weak" style="float:left;margin-right:4px;" name="weak" id="weak_1"  autocomplete='off'  value=0  checked='checked' type="radio">A高攀_a1</td></tr>
      <tr><td>185</td><td><input class="weak" style="float:left;margin-right:4px;" name="weak" id="weak_2"  autocomplete='off' value=1 type="radio">A图图_a2</td></tr>
      <tr><td>70</td><td><input class="weak" style="float:left;margin-right:4px;" name="weak" id="weak_3"   autocomplete='off' value=2  type="radio">B戈多_b1</td></tr>
      <tr><td>2271</td><td><input class="weak" style="float:left;margin-right:4px;" name="weak" id="weak_4"  autocomplete='off' value=3 type="radio">B何斌_b2</td></tr>
      </table>
    </td>
    
 <td>
    
      <div style="font-size:14px;">  &nbsp;&nbsp;让杆配置(两人比杆让杆为整体让杆):<br/></div>
     <table class=table-bordered style="margin:10px;font-size:14px;">
       <tr><td>Par</td><td>让的杆数</td><td>Key</td></tr>
       <tr><td>3</td><td>0.5</td><td>weak_par3_num</td></tr>
       <tr><td>4</td><td>0.5</td><td>weak_par4_num</td></tr>
       <tr><td>5</td><td>1</td><td>weak_par5_num</td></tr>

     </table>
 
 </td>

  <td>
    <div style="margin:10px;font-size:14px;">
     奖励配置:<br/>
     <input type="checkbox"  id="bonus_toggle"    autocomplete='off' />使用奖励规则 <br/>

    

     <label><input class="bonus"   name="bonus" id="bonus_plus"  value='plus'  autocomplete='off'  checked="checked"  type="radio" />乘法 </label> 
     <label><input class="bonus"   name="bonus" id="bonus_add"  value='add'   autocomplete='off'  type="radio" />加法 </label> 

  
     <br/>
     <table  style="font-size:14px;" class=table-bordered>


       <tr><td>杆数</td><td>奖励</td></tr>
       <tr><td>DoubleBogey</td><td><input id=bonus_doublebogey type=text class='form-control sminput'  autocomplete='off' value=0 /></td></tr>
       <tr><td>Bogey</td><td><input  id=bonus_bogey type=text class='form-control sminput'  autocomplete='off' value=0 /></td></tr>
       <tr><td>Par</td><td><input    id=bonus_par   type=text class='form-control sminput'   autocomplete='off' value=2 /> </td></tr>
       <tr><td>Birdie</td><td><input id=bonus_birdie type=text class='form-control sminput' autocomplete='off'  value=5 /></td></tr>
       <tr><td>Eagle</td><td><input  id=bonus_eagle type=text class='form-control sminput' autocomplete='off'  value=10 /></td></tr>
       <tr><td>Albatross</td><td><input  id=bonus_albatross type=text class='form-control sminput' autocomplete='off'  value=10 /></td></tr>
       <tr><td>HIO</td><td><input    id=bonus_hio   type=text class='form-control sminput'  autocomplete='off' value=10 /></td></tr>

     </table>
    <div>
 </td>
  <td>
    <div style="margin:10px;font-size:14px;">
     双奖配置:<br/>
     <input type="checkbox"  id="combo_bonus_toggle"  disabled="disabled"  autocomplete='off' />使用双奖规则 <br/>
     <br/>
     <table  style="font-size:14px;" class=table-bordered>

       <tr><td>组合</td><td>奖励</td></tr>
       <tr><td>Par+Par</td><td><input id=par_par type=text class='form-control sminput1'  autocomplete='off' value=4 /></td></tr>
       <tr><td>Birdie+Par</td><td><input  id=birdie_par type=text class='form-control sminput1'  autocomplete='off' value=5 /></td></tr>
       <tr><td>Birdie+Birdie</td><td><input  id=birdie_birdie type=text class='form-control sminput1'  autocomplete='off' value=8 /></td></tr>
       <tr><td>Eagle+Par</td><td><input    id=eagle_par  type=text class='form-control sminput1'   autocomplete='off' value=10 /> </td></tr>
       <tr><td>Eagle+Birdie</td><td><input id=eagle_birdie type=text class='form-control sminput1' autocomplete='off'  value=10 /></td></tr>
       <tr><td>Eagle+Eagle</td><td><input  id=eagle_eagle type=text class='form-control sminput1' autocomplete='off'  value=10 /></td></tr>
       
       
     </table>
    <div>
 </td>
 


<td>
    <div style="margin:10px;font-size:14px;">
     
     头N尾M配置:
     <table  style="font-size:14px;"" class=table-bordered>
       <tr><td>名称</td><td>权重</td></tr>
       <tr><td>头</td><td><input id=weight_head   type=text class='form-control sminput2'  autocomplete='off' value=2 /> </td></tr>
       <tr><td>尾</td><td><input id=weight_tail   type=text class='form-control sminput2'  autocomplete='off' value=1 /></td></tr>
     </table>

     211/421:
      <table  style="font-size:14px;"" class=table-bordered>

       <tr><td>名称</td><td>权重</td></tr>
       <tr><td>头</td><td><input id=211_head   type=text class='form-control sminput2'  autocomplete='off' value=2 /> </td></tr>
       <tr><td>中</td><td><input id=211_midl   type=text class='form-control sminput2'  autocomplete='off' value=1 /></td></tr>
       <tr><td>尾</td><td><input id=211_tail   type=text class='form-control sminput2'  autocomplete='off' value=1 /></td></tr>
     </table>


    <div>
 </td>

    </tr>
    </table>
  
  
  
<table id=g27 class=table-bordered style="font-size:14px;margin:20px 0 0 10px;">
 <tr>
  <td style="width:40px;" >序号</td>
  <td>kpi或kpi组合</td>
  <td>人数</td>
  <td>分组方式:</td>
  <td>分组方式(值)</td>
  <td>游戏名称</td>
  <td>有无封顶</td>
  <td>让杆/每洞</td>
  <td style="width:140px;">有无包洞</td>
  <td style="width:160px;">顶洞配置</td>
  
 </tr>
 <tr>
  <td>1</td>
  <td>kpi_best</td>
  <td>2</td>
  <td>自然1:1</td>
  <td>2_fixed</td>
  <td>两人比洞</td>
  <td>有</td>
  <td>有</td>
  <td>无</td>
  <td>无</td>
 </tr>
 <tr>
  <td>2</td>
  <td>kpi_gross</td>
  <td>2</td>
  <td>自然1:1</td>
  <td>2_fixed</td>
  <td>两人比杆</td>
  <td>有</td>
  <td>总</td>
  <td>无</td>
  <td>无</td>
 </tr>
 <tr>
  <td>3</td>
  <td>kpi_best</td>
  <td>3</td>
  <td>乱拉/固拉</td>
  <td>3_random/3_fixed</td>
  <td>三人斗大地主</td>
  <td>有</td>
  <td>有</td>
  <td>有</td>
  <td>无</td>
 </tr>
 <tr>
  <td>4</td>
  <td>kpi_total_add</td>
  <td>3</td>
  <td>乱拉/固拉</td>
  <td>3_random/3_fixed</td>
  <td>三人斗二地主(加法总)</td>
  <td>有</td>
  <td>有</td>
  <td>有</td>
  <td>无</td>
 </tr>
 <tr>
  <td>5</td>
  <td>kpi_total_plus</td>
  <td>3</td>
  <td>乱拉/固拉</td>
  <td>3_random/3_fixed</td>
  <td>三人斗二地主(乘法总)</td>
  <td>有</td>
  <td>有</td>
  <td>有</td>
  <td>无</td>
 </tr>
 <tr>
  <td>6</td>
  <td>kpi_worst</td>
  <td>3</td>
  <td>乱拉/固拉</td>
  <td>3_random/3_fixed</td>
  <td>三人斗小地主(最差成绩)</td>
  <td>有</td>
  <td>有</td>
  <td>有</td>
  <td>无</td>
 </tr>
 <tr>
  <td>7</td>
  <td>kpi_best</td>
  <td>3</td>
  <td>3人skin</td>
  <td>3_skin</td>
  <td>3人skin</td>
  <td>有</td>
  <td>有</td>
  <td>无</td>
  <td>无</td>
 </tr>
 <tr>
  <td>8</td>
  <td>kpi_best</td>
  <td>4</td>
  <td>跟随</td>
  <td>4_follow</td>
  <td>4人斗大地主</td>
  <td>有</td>
  <td>有</td>
  <td>有</td>
  <td>无</td>
 </tr>
 <tr>
  <td>9</td>
  <td>kpi_total_add</td>
  <td>4</td>
  <td>跟随</td>
  <td>4_follow</td>
  <td>4人斗二地主(加法总)</td>
  <td>有</td>
  <td>有</td>
  <td>有</td>
  <td>无</td>
 </tr>
 <tr>
  <td>10</td>
  <td>kpi_total_plus</td>
  <td>4</td>
  <td>跟随</td>
  <td>4_follow</td>
  <td>4人斗二地主(乘法总)</td>
  <td>有</td>
  <td>有</td>
  <td>有</td>
  <td>无</td>
 </tr>
 
 <tr>
  <td>11</td>
  <td>kpi_best</td>
  <td>4</td>
  <td>4人skin</td>
  <td>4_skin</td>
  <td>4人skin</td>
  <td>有</td>
  <td>有</td>
  <td>有</td>
  <td>无</td>
 </tr>
 <tr>
  <td>12</td>
  <td>kpi_best</td>
  <td>4</td>
  <td>固定3打1</td>
  <td>4_3vs1</td>
  <td>固定3打1(比最好成绩)</td>
  <td>有</td>
  <td>有</td>
  <td>有</td>
  <td>无</td>
 </tr>
 <tr>
  <td>13</td>
  <td>kpi_best</td>
  <td>4</td>
  <td>固拉/乱拉/ab分组</td>
  <td>4_fixed/4_random/4_ab</td>
  <td>4人拉丝1点(最好成绩)</td>
  <td>有</td>
  <td>有</td>
  <td>有</td>
  <td>无</td>
 </tr>
 <tr>
  <td>14</td>
  <td>kpi_best_ak</td>
  <td>4</td>
  <td>固拉/乱拉/ab分组</td>
  <td>4_fixed/4_random/4_ab</td>
  <td>4人拉丝1点(比a比k)</td>
  <td>有</td>
  <td>有</td>
  <td>有</td>
  <td>无</td>
 </tr>

 <tr>
  <td>15</td>
  <td>kpi_total_add</td>
  <td>4</td>
  <td>固拉/乱拉/ab分组</td>
  <td>4_fixed/4_random/4_ab</td>
  <td>4人拉丝1点(加法总)</td>
  <td>有</td>
  <td>有</td>
  <td>有</td>
  <td>无</td>
 </tr>
 <tr>
  <td>16</td>
  <td>kpi_total_plus</td>
  <td>4</td>
  <td>固拉/乱拉/ab分组</td>
  <td>4_fixed/4_random/4_ab</td>
  <td>4人拉丝1点(乘法总)</td>
  <td>有</td>
  <td>有</td>
  <td>有</td>
  <td>无</td>
 </tr>
 <tr>
  <td>17</td>
  <td>kpi_case_8421</td>
  <td>4</td>
  <td>固拉/乱拉/ab分组</td>
  <td>4_fixed/4_random/4_ab</td>
  <td class=xl132>8421</td>
  <td>有</td>
  <td>无</td>
  <td>有</td>
  <td>无</td>
 </tr>
 <tr>
  <td>18</td>
  <td>kpi_best,kpi_worst</td>
  <td>4</td>
  <td>固拉/乱拉/ab分组</td>
  <td>4_fixed/4_random/4_ab</td>
  <td>拉丝2点(最优,最差)</td>
  <td>有</td>
  <td>有</td>
  <td>有</td>
  <td>无</td>
 </tr>
 <tr>
  <td>19</td>
  <td>kpi_best,kpi_total_add</td>
  <td>4</td>
  <td>固拉/乱拉/ab分组</td>
  <td>4_fixed/4_random/4_ab</td>
  <td>拉丝2点(最优,加法总)</td>
  <td>有</td>
  <td>有</td>
  <td>有</td>
  <td>无</td>
 </tr>
 <tr>
  <td>20</td>
  <td>kpi_best,kpi_total_plus</td>
  <td>4</td>
  <td>固拉/乱拉/ab分组</td>
  <td>4_fixed/4_random/4_ab</td>
  <td>拉丝2点(最优,乘法总)</td>
  <td>有</td>
  <td>有</td>
  <td>有</td>
  <td>无</td>
 </tr>
 <tr>
  <td>21</td>
  <td>kpi_best,kpi_worst,kpi_total_add</td>
  <td>4</td>
  <td>固拉/乱拉/ab分组</td>
  <td>4_fixed/4_random/4_ab</td>
  <td>拉丝3点(最优,最差,加法总)</td>
  <td>有</td>
  <td>有</td>
  <td>有</td>
  <td>无</td>
 </tr>
 <tr>
  <td>22</td>
  <td>kpi_best,kpi_worst,kpi_total_plus</td>
  <td>4</td>
  <td>固拉/乱拉/ab分组</td>
  <td>4_fixed/4_random/4_ab</td>
  <td>拉丝3点(最优,最差,乘法总)</td>
  <td>有</td>
  <td>有</td>
  <td>有</td>
  <td>无</td>
 </tr>
 <tr>
  <td>23</td>
  <td>kpi_best,kpi_worst</td>
  <td>4</td>
  <td>固拉/乱拉/ab分组</td>
  <td>4_fixed/4_random/4_ab</td>
  <td>头n尾m(最优,最差)</td>
  <td>有</td>
  <td>有</td>
  <td>有</td>
  <td>无</td>
 </tr>
 <tr>
  <td>24</td>
  <td>kpi_best,kpi_total_add</td>
  <td>4</td>
  <td>固拉/乱拉/ab分组</td>
  <td>4_fixed/4_random/4_ab</td>
  <td>头n总m(最优,加法总)</td>
  <td>有</td>
  <td>有</td>
  <td>有</td>
  <td>无</td>
 </tr>
 <tr>
  <td>25</td>
  <td>kpi_best,kpi_total_plus</td>
  <td>4</td>
  <td>乱拉/乱拉/ab分组</td>
  <td>4_fixed/4_random/4_ab</td>
  <td>头n总m(最优,乘法总)</td>
  <td>有</td>
  <td>有</td>
  <td>有</td>
  <td>无</td>
 </tr>
 <tr>
  <td>26</td>
  <td>kpi_best,kpi_worst,kpi_total_add</td>
  <td>4</td>
  <td>固拉/乱拉/ab分组</td>
  <td>4_fixed/4_random/4_ab</td>
  <td>211/421(最优,最差,加法总)</td>
  <td>有</td>
  <td>有</td>
  <td>有</td>
  <td>无</td>
 </tr>
 <tr>
  <td>27</td>
  <td>kpi_best,kpi_worst,kpi_total_plus</td>
  <td>4</td>
  <td>固拉/乱拉/ab分组</td>
  <td>4_fixed/4_random/4_ab</td>
  <td>211/421(最优,最差,乘法总)</td>
  <td>有</td>
  <td>有</td>
  <td>有</td>
  <td>无</td>
 </tr>
</table>

 
<br/>
 <button id="compute_btn" onclick="compute()" style="margin-left:250px;" class="btn btn-primary">计算</button>
 <br/><br/> 


<div style="border:1px solid blue;margin:20px;" id=gamble_result></div>
    </body>
</html>
