<html>
<head>
 <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
 <meta http-equiv="Content-Language" content="zh-CN" />
 <meta name="roots" content="" />
 <meta name="Keywords" content="" />
 <meta name="Description" content="" />
 <meta name="viewport" content="width=device-width, initial-scale=1" />
 <title></title>
 <style type="text/css">
	body{font:"宋体";font-size:12px;}
	a:link,a:visited{font-size:12px;color:#666;text-decoration:none;}
	a:hover{color:#ff0000;text-decoration:underline;background-color:#ffffff;}
  .top{margin-bottom:10px;}
  .userimg{float:left;margin-right:30px;margin-left:25px;}
  .userrank{float:left;}
  .clear{ clear:both;}
	#Tab{margin:0 auto;width:220px;border:1px solid #BCE2F3;}
	.Menubox{height:28px;padding-bottom:5px;}
	.Menubox ul{list-style:none;padding:0;position:absolute;}
	.Menubox ul li{float:left;background:#f5f5f5;line-height:20px;display:block;cursor:pointer;width:90px;text-align:center;color:#333333;font-weight:bold;margin-right:10px;font-size:15px;}
	.Menubox ul li.hover{background:#ffffff;border-bottom:1px solid #fff;color:#00ccff;font-size:15px;}
	.Contentbox{clear:both;margin-top:0px;border-top:none;height:181px;padding-top:8px;height:100%;}
	.Contentbox ul{list-style:none;margin:7px;padding:0;}
	.Contentbox ul li{line-height:24px;border-bottom:1px dotted #ccc;}
 </style>
 <script>
	<!--
  function experience(experience,rank){
    if (rank == 0) {
      var value = 300;
      shengyu = 300 - experience;
    }
    if (rank == 1) {
      var value = 300;
      shengyu = 300 - experience;
    }
    if (rank == 2) {
      var value = 900;
      shengyu = 900 - experience;
    }
    if (rank == 3) {
      var value = 2700;
      shengyu = 2700 - experience;
    }
    if (rank == 4) {
      var value = 5000;
      shengyu = 5000 - experience;
    }
    if (rank == 5) {
      var value = 7800;
      shengyu = 7800 - experience;
    }
    if (rank == 6) {
      var value = 11500;
      shengyu = 11500 - experience;
    }
    if (rank == 7) {
      var value = 15500;
      shengyu = 15500 - experience;
    }
    if (rank == 8) {
      var value = 20000;
      shengyu = 20000 - experience;
    }
    if (rank == 9) {
      var value = 25500;
      shengyu = 25500 - experience;
    }
    if (rank == 10) {
      var value = 32000;
      shengyu = 32000 - experience;
    }
    if (rank == 11) {
      var value = 40000;
      shengyu = 40000 - experience;
    }
    if (rank == 12) {
      var value = 48500;
      shengyu = 48500 - experience;
    }
    if (rank == 13) {
      var value = 60000;
      shengyu = 60000 - experience;
    }
    if (rank == 14) {
      var value = 72000;
      shengyu = 72000 - experience;
    }
    if (rank == 15) {
      var value = 72000;
      shengyu = 72000 - experience;
    }
    var sss = shengyu/100;
      var aaa = document.getElementById('schedule1');
      aaa.style.width=sss +"px";
  }
function setTab(name,cursel,n){
  for(i=1;i<=n;i++){
    var menu=document.getElementById(name+i);
    var con=document.getElementById("con_"+name+"_"+i);
    menu.className=i==cursel?"hover":"";
    con.style.display=i==cursel?"block":"none";
  }
  }

	//-->
 </script>
</head>
<body onload="experience(<?php echo $experience; ?>,<?php echo $rank; ?>)" style="background-color:#f5f5f5;">
<br><br>
<div class="top" data-role="page">
  <span class="userimg"><img src="<?php echo $picurl; ?>" height="80px"/></span>
  <span calss="userrank"><p><font size="3px">LV:&nbsp;&nbsp;<?php echo $rank; ?></font></p></span>
  <div style="background-color: #ffffff; width: 100px; height: 10px; border: solid 1px #ccc; padding: 1px; float:left;">
    <div id="schedule1" style="background-color: #00ccff; width: 0px; height: 10px; float:left;"></div>
  </div><br />
  <span><p>我的经验值为：<?php echo $experience; ?></p></span>
<div class="clear"></div>
</div>
<!--<div id="Tab">-->
  <div class="Menubox">
    <ul>
      <li id="menu1" onclick="setTab('menu',1,3)" class="hover">经验值介绍</li>
      <li id="menu2" onclick="setTab('menu',2,3)" >如何获得</li>
      <li id="menu3" onclick="setTab('menu',3,3)" >我的特权</li>
    </ul>
  </div>
  <div class="Contentbox" style="background-color:#ffffff;"> 
    <div id="con_menu_1" class="hover">
        <span><p><font color="#00ccff" size="3px">经验值介绍</font></p></span>
        <ul>
          <li>经验值是高尔夫江湖会员通过平台活动获得的经验值</li>
          <li>经验值越高会员等级越高</li>
          <li>享受的会员服务也更多哦</li>
        </ul>
    </div>
    <div id="con_menu_2" style="display:none">
      <span><p><font color="#00ccff" size="3px">每日登录奖励</font></p></span>
      <ul>
        <li>每日登录赠送3点经验值</li>
        <li>连续登录7天获赠30点经验值</li>
        <li>连续登录30天获赠50点经验值</li>
      </ul>
      <span><p><font color="#00ccff" size="3px">比赛奖励</font></p></span>
      <ul>
        <li>创建比赛后,确认比分并结束比赛可获赠以下经验值</li>
        <li>参赛1人每次可获得3点,每日限2次</li>
        <li>参赛2人每次可获得5点,每日限2次</li>
        <li>参赛3人每次可获得15点,每日限1次</li>
        <li>参赛4人每次可获得20点,每日限1次</li>
        <li>创建队内比赛,确认比分并结束可获得50点经验值,每七日限一次</li>
      </ul>
      <span><p><font color="#00ccff" size="3px">分享记分卡奖励</font></p></span>
      <ul>
        <li>分享比赛记分卡每次可获得5点,每日限2次</li>
      </ul>
      <span><p><font color="#00ccff" size="3px">添加球友奖励</font></p></span>
      <ul>
        <li>添加球友每成功添加一人可获得3点,不限制,重复添加无效</li>
      </ul>
    </div>
    <div id="con_menu_3" style="display:none">
      <span><p><font color="#00ccff" size="3px">我的特权</font></p></span>
      <ul>
        <li>暂无(即将上线)</li>
      </ul>
    </div>
</div> 
<!--</div>-->
</body>
</html>
