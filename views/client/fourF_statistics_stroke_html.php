<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv=content-type content="text/html; charset=utf-8">
<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=0;" name="viewport"/>
<link rel="stylesheet" type="text/css" href="<?php echo STATIC_HOST;?>/res/css/amazeui.min.css" />
<script src="http://s1.golf-brother.com/data/attach/js/jquery.js" type="text/javascript" charset="utf-8"></script>
<script src="<?php echo STATIC_HOST;?>/res/js/amazeui.min.js" type="text/javascript" charset="utf-8"></script>
<title>高尔夫成绩卡</title>
<style type="text/css">
body{
 background-color:#f2f2f2;
 font-family:Arial, sans-serif;
 height: 100%;
 width: 100%;
}

td{
  text-align: center;
}

</style>

<script>

</script>

</head>
<body>
<div style="width:100%;padding-left: 10px;padding-right: 10px;margin-bottom:10px;">
<img src="http://s1.golf-brother.com/data/images/logo.png" align="right" height="20px" style="margin-top:10px;" /><br/>
<?php
  echo '<span style="font-size:20px;"><font color="#333"><b>';
  echo $team_game_name;
  echo '</b></font></span><br />';
?>

<span style="font-size:15px;"><font color="#333"><?php echo $coursename;?></font></span><br />
<span id="time" style="font-size:15px;"><font color="#999"><?php echo $time;?></font></span><br />

</div>
<table class="am-table am-table-bordered am-table-striped am-table-hover">
    <thead style="background-color:#333;color:#fff;">
        <tr>
          <th  style="width:15%;">排名</th>
          <th>球队</th>
          <th style="width:15%;">杆差</th>
          <th style="width:15%;">总杆</th>
        </tr>
    </thead>
    <tbody>
        <?php $index=1; foreach ($squad_score_info as $key => $one_squad) { ?>
        <?php if($index%2 == 0){ ?>
          <tr class="ash_tr">
        <?php }else{ ?>
          <tr class="white_tr">
        <?php } ?>
          
            <td><?php echo $one_squad['rank'];?></td>
            <td>
              <div style="text-align: left;">
                <?php if(strlen($one_squad['team_picurl']) > 0){ ?>
                  <img class="am-circle" style="margin-left:10%;margin-right:5%;width:30px;height:30px;" src="<?php echo $one_squad['team_picurl'];?>" />
                <?php } ?>
                <?php 
                    echo $one_squad['squad_name'];
                ?>
              </div>
            </td>
            <td><?php echo $one_squad['squad_gancha'];?></td>
            <td><?php echo $one_squad['squad_score'];?></td>
          </tr>
      <?php $index++;} ?>
    </tbody>
</table>
<div style="background-color:#333;height:20px;width:100%;margin-top:-20px;"></div>
</body>
</html>
