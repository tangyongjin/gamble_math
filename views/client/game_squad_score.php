<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv=content-type content="text/html; charset=utf-8">
	<title>高尔夫成绩卡</title>
	<style type="text/css">
	body{
		background-color:#f2f2f2;
		font-family:Arial, sans-serif;
	}
	table,tr,td{
		margin: 0;
		padding:0;
	}
	.content{
		width: 100%;
		margin: 0;
		padding: 0;
		margin-top: 5%;
		/*text-align: center;*/

	}
	.one_squad{
		float: left;
		background-color: #fff;
		position :relative;
		margin-left: 4%;
		border-radius:6px;
		margin-top: 30px;
	}
	h2{
		text-align: center;
		width: 100%;
	}
	.team_name{
		margin:10% 20%;
		padding-top: 6px;
	}
	.team_table{
          width: 100%;
          text-align: center;
          margin-top: 10%;
	}
	.tr_first{
		background-color: #333;
		color: #fff;
		height: 50px;
		font-size: 20px;
	}
	.tr_first_td{
		width: 55%;
	}
	.user_picurl{
		float: left;
		margin-left: 5%;
	}
	.user_info{
		margin-left: 30%;
		text-align: center;
	}
	.team_pic{
		position:absolute;
		left:-20px; 
		top:9%;
		width: 40%;
		
	}
	.squad_picurl{
		border:1px solid #f5f5f5;
		border-radius:30px;
	}
	.vs{
		position:absolute;
		top:5%; 
		right:5px;
		width:20%;
	}
	.button_class{
		    background: none repeat scroll 0 0 #0099ff;
		    color: #fff;
		    font-size: 40px;
		    margin: 60px auto 60px;
		    position: relative;
		    width: 80%;
		    z-index: 2;
		    border-radius: 8px;
		    cursor: pointer;
		    display: block;
		    font-weight: 700;
		    padding: 40px 60px 39px;
		    text-align: center;
		    text-decoration: none;
	}
	.QRcode{
		clear:both;
		color:#333;
		text-align: center;
		margin-bottom: 60px; 
		font-size: 36px; 
	}
	.qrcode_pic{
		margin:40px auto 20px;
		padding-bottom: 50px;
		padding-top: 30px;
	}
	.font_class{
		line-height: 60px;
	}
	.team_up_table{
		width: 100%;
		padding: 0;
		margin:0;
		margin-top: 10%;
		
	}
	</style>
	<script>
var _hmt = _hmt || [];
(function() {
  var hm = document.createElement("script");
  hm.src = "//hm.baidu.com/hm.js?42514b23f3dd908b8ffbd0fb40f621f4";
  var s = document.getElementsByTagName("script")[0]; 
  s.parentNode.insertBefore(hm, s);
})();
</script>
</head>
<body>
	<!--网页顶部-->
	<div style="width:1000px;">
	<img src="http://s1.golf-brother.com/data/images/logo.png" align="right" height="40px" style="margin-right:8px;" /><br /><br />
	<?php 
    
	if(strlen($team_game_name) < 1) 
	{
		$team_game_name='';
	}

    echo '<span style="font-size:200%;"><font color="#333">';
    echo $team_game_name;
    echo '</font></span><br /><br />';
   
	?>

	<span style="font-size:180%;"><font color="#333"><?php echo $coursename;?></font></span><br />
	<span id="time" style="font-size:120%;"><font color="#999"><?php echo $time;?></font></span><br />
	
	</div>
<!--网页正文-->
<div class="content">
<?php $squad_count = count($squads_list)-1; foreach ($squads_list as $count => $one_squad) {$gan_cha_total = 0; $squad_total = 0;?>
		<div class='one_squad' style='width:45%;'>
		<table class="team_up_table">
			<tr>
				<td style="width: 20%;text-align: right;">
					<?php if(strlen($one_squad['squad_info']['squad_picurl']) > 1) {?>
					<!-- <div class="team_pic"> -->
						<img class="squad_picurl" src="<?php echo $one_squad['squad_info']['squad_picurl'];?>" align="center" height="60px"  />
					<!-- </div> -->
				<?php } ?>

				</td>
				<td style="width: 80%;text-align: center;">
					<?php echo $one_squad['squad_info']['squad_name'];?>
				</td>
			</tr>
		</table>
		<table class="team_table">
			<tr class="tr_first"><td>排名</td><td class="tr_first_td">球手</td><td>杆差</td><td>总杆</td></tr>
			<?php foreach ($one_squad['squad_score'] as $key => $one_user) { $index=$key+1; $gan_cha_total = $one_user['gan_cha']+$gan_cha_total; $squad_total = $one_user['grosssum']+$squad_total;?>
				<?php if(($index%2) != 0){?>
				<tr>
				<?php }else{?>
				<tr style="background:#f5f5f5;">
				<?php }?>
			<td><?php echo $one_user['paiming']; ?></td>
				<td  align="left">
					<div class="user_picurl"><img class="user_pic" src="<?php echo $one_user['user_picurl'];?>" width="60px" /></div>
					<div class="user_info">
						<br />
						
						<?php 
							if($one_user['tland'] == "BLUE"){
								$tland = "蓝T";
								$img = '<img src="http://s1.golf-brother.com/data/images/blue-t.png" />';
							}elseif ($one_user['tland'] == "RED") {
								$tland = "红T";
								$img = '<img src="http://s1.golf-brother.com/data/images/red-t.png" />';
							}elseif ($one_user['tland'] == "GOLD") {
								$tland = "金T";
								$img = '<img src="http://s1.golf-brother.com/data/images/gold-t.png" />';
							}elseif ($one_user['tland'] == "WHITE") {
								$tland = "白T";
								$img = '<img src="http://s1.golf-brother.com/data/images/white-t.png" />';
							}elseif ($one_user['tland'] == "BLACK") {
								$tland = "黑T";
								$img = '<img src="http://s1.golf-brother.com/data/images/black-t.png" />';
							}
							echo $img;
							// echo $tland;

						?>
						<!-- <img src="http://s1.golf-brother.com/data/images/blue-t.png" />
						蓝T -->
						<font size="5"><?php echo $one_user['nickname'];?></font>
					</div>

				</td>
				<td>
					<font size="5">
					<?php if($one_user['gan_cha'] > 0){
						echo "+";
					}
					// echo "+108";
					echo $one_user['gan_cha'];
					?>
					</font>
				</td>
				<td>
					<font size="5">

					<?php echo $one_user['grosssum'];?>
					</font>
				</td>
			</tr>
				<tr>
			<?php }?>
			<tr style="background-color:#fff;color:#000;height:50px;"><td style="text-align: left;" colspan="2" nowrap="nowrap"><font size="5">合计</font></td><td ><font size="5"><?php if($gan_cha_total > 0){echo '+'.$gan_cha_total;}else{ echo $gan_cha_total;}?></font></td><td><font size="5"><?php echo $squad_total;?></font></td></tr>
		</table>
	</div>
	
<?php }?>
</div>
<!--官网二维码-->
<?php if($isShowCode == 1){?>
<div>
	
	<div class="QRcode">
	
	<img src="http://s1.golf-brother.com/data/images/brother_QRcode.jpeg" class="qrcode_pic" width="320" height="320" /><br />
	<div class="font_class">
		<b>关注公众号，更多精彩等着你！</b><br />
		<font color="#666">双击二维码后长按， 识别  添加或搜索微信号</font><br /><font color="#0099ff"> golfbrother</font>
	</div>
	</div>
	<a class="button_class" href="http://www.golf-brother.com/">精彩球事，下载高尔夫江湖</a>
</div>
<?php } ?>
</body>
</html>
