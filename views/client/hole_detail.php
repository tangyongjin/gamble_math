<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no">
        <meta name="format-detection" content="telephone=no">
        <title>游戏结果</title>
        
        <link rel="stylesheet" type="text/css" href="http://s1.golf-brother.com/data/static/res/css/frozen.css" />

        <script src="http://s1.golf-brother.com/data/static/res/js/zepto.min.js"></script>
        <script src="http://s1.golf-brother.com/data/static/res/js/frozen.js"></script>

                   
        <style>
       
        .onegm{
            margin-left: 4px;
            margin-bottom: 10px;
        }

        .ugross{

          font-size: 35px;
        }


        .ui1{
            background-image: url("http://t1.golf-brother.com/data/attach/css/img/uindex1.png");
            background-position: left top, right bottom;
            background-repeat: no-repeat;
            background-size: 20px 20px;
            padding-left: 10px;
            background-color:   white;
               
        }

        .ui2{
            background: url("http://t1.golf-brother.com/data/attach/css/img/uindex2.png");
            background-position: left top, right bottom;
            background-repeat: no-repeat;
            background-size: 20px 20px;
            padding-left: 0px;
            background-color:   white;
               
        }

        .ui3{
            background-image: url("http://t1.golf-brother.com/data/attach/css/img/uindex3.png");
            background-position: left top, right bottom;
            background-repeat: no-repeat;
            background-size: 20px 20px;

            padding-left: 10px;
            background-color:   white;
               
        }

        .ui4{
            background-image: url("http://t1.golf-brother.com/data/attach/css/img/uindex4.png");
            background-position: left top, right bottom;
            background-repeat: no-repeat;
            background-size: 20px 20px;
            padding-left: 10px;
            background-color:   white;
               
        }

        .uidz{
            background-image: url("http://t1.golf-brother.com/data/attach/css/img/uindex_dz.png");
            background-position: left top, right bottom;
            background-repeat: no-repeat;
            background-size: 20px 20px;
            padding-left: 10px;
            background-color:   white;
               
        }

        .uilz{
            background-image: url("http://t1.golf-brother.com/data/attach/css/img/uindex_lz.png");
            background-position: left top, right bottom;
            background-repeat: no-repeat;
            background-size: 20px 20px;
            padding-left: 10px;
            background-color:   white;
               
        }

        
        .ui-table {
            line-height: 40px;
            text-align: center;
            background-color: #fff;
            font-size:18px;
            font-weight:200;
        }
         header ul li{
            font-size:10px;width:auto; white-space:nowrap;text-overflow:clip;
        }
        .hole-d{
            /*padding:2px 2px 2px 5px;;*/
        }
        .hole-s{
            margin-top: 10px;
            background-color:#555;color:#fff;font-size:20px;margin-left: 5px;
        }
        .red-font{
            color:#f00;
        }
        .normal-font{
            color:#000;
        }
        </style>
        
        <script>

           var data=<?php echo  $json;?>

        
            function fillData(data){
                if(typeof data == 'undefined'){
                    return;
                }
                
                
                  var json=data;

                if( (typeof json == 'undefined') || ($.type(json) != 'object') ){
                    return;
                }
                
                var players = json.players;
                if( $.type(players) != 'array' || players.length<1 ){
                    return;
                }

             

                 var selecthole = json.selecthole;
                 var holegross = json.holegross;
                 var holemoney = json.holemoney;
 
                 var per_gamble_info = json.per_gamble_info;
                 var pnum=players.length;
                 buildHeader(players,selecthole,holegross,holemoney);
                 
                 showPergambleeinfo(per_gamble_info,pnum);
                

            }

            function showPergambleeinfo(per_gamble_info,pnum){

                if(pnum==2){
                   var tdw=' width="33%" ';    
                }

                if(pnum==3){
                   var tdw=' width="25%" ';    
                }
                if(pnum==4){
                   var tdw=' width="20%" ';    
                }

                 console.log(per_gamble_info) ;

                 var tpl='';
                 for(var i = 0; i< per_gamble_info.length ; i++){
                    
                    var gm = per_gamble_info[i];
                    var usermoney=gm.base_money_log;
                    var gmname=gm.rulename;
 
                    tpl=tpl+ '<div id=xxx_'+gm.gambleid+ ' class=onegm><div class=gmname>'+gmname+'</div>';

                    tpl=tpl+'<table class="ui-table ui-border-tb"><tr><td '+tdw+'></td>';
                     for(var j = 0; j< usermoney.length ; j++){
                        var s='';
                        var cs='normal-font';
                        console.log(usermoney[j]);
                        var pcs=usermoney[j].pcs;


                        if(usermoney[j].money>0){s='+';cs='red-font'}

                        tpl=tpl+'<td '+tdw+'class='+cs+'><div class='+pcs+'>'+s+usermoney[j].money+'</div>';
                        tpl=tpl+'本洞:'+usermoney[j].money_hole;
                        tpl=tpl+'肉:'+usermoney[j].money_meat;
                        tpl=tpl+'</td>';

                     }
                     tpl=tpl+'</tr></table></div>';
                }
               
                 last = $('#gblist').last();
                 $(tpl).appendTo(last);
            }
            
            function buildHeader(players,selecthole,holegross,holemoney){
                var tpl = '<li>参与<br/>球员</li>';
                var last = $('#blk_header').last();
                $(tpl).appendTo(last);
                

                for(var i = 0; i< players.length ; i++){
                    var nm = players[i].nickname;
                    if(nm.length>9){
                        nm = nm.substring(0,9)+"..";
                    }
                    var tpl = '<li><div><img width="35%" src="'+players[i].cover+'" ></div><div>'+nm+'</div></li>';
                    var last = $('#blk_header').last();
                    $(tpl).appendTo(last);
                }

                if(players.length==2){

                   
                   var tdw= ' style="width:33%" ';  
                }

                if(players.length==3){
                  
                   var tdw= ' style="width:25%" ';


                }
                if(players.length==4){
                 
                   var tdw= ' style="width:20%" ';
                }

                
                 tpl="<tr><td "+tdw+' class="hole-d"><div class="ui-avatar-s hole-s">'+selecthole.holename+'</div>'+
                    'PAR'+selecthole.par+'</td>';

                
 
                    
                  for(var i = 0; i< holegross.length ; i++){
                    var g = holegross[i].gross;
                   tpl =tpl+ '<td class=ugross '+tdw+'>'+g+'</td>';
                }

                 tpl+='</tr>';
                 tpl+='<tr><td '+tdw+'><span style="font-size:12px;">本洞总输赢</span></td>'

                   for(var i = 0; i< holemoney.length ; i++){
                        var cs='normal-font';
                        var sc=holemoney[i].money;  
                       // var sc=holemoney[i].money_hole;  
                        
                        //alert( holemoney[i])
                        if(sc>0){cs='red-font'}

                        tpl+='<td '+tdw+'class='+cs+' >'+sc+'</td>';
                 }
                 
                 tpl+='</tr>'
 
                 last = $('#thishole').last();
                 $(tpl).appendTo(last);
 
            }

 
            
            Zepto(function($){
                fillData(data);
            });
            
        </script>
    </head>
    <body ontouchstart>
    
<header class="ui-header ui-header-positive ui-border-b" style="background-color:#fff;color:#000;line-height:10px;height:194px;">
   

   <ul class="ui-tiled ui-border-t" style="background-image:none;margin-top:5px;" id="blk_header">
        
   </ul>

   <div>
            <table class="ui-table ui-border-tb" id="thishole">
              
                   
            </table>

    </div>



 </header>   

 
<section class="ui-container" style="margin-top:160px;">
<section id="gblist">
   
</section>
  
</section><!-- /.ui-container-->
        



 

      


    </body>
</html>