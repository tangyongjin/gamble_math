<html>
<head>
 <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
 <meta http-equiv="Content-Language" content="zh-CN" />
 <meta name="roots" content="" />
 <meta name="Keywords" content="" />
 <meta name="Description" content="" />
 <meta name="viewport" content="width=device-width, initial-scale=1.0" />
 <title></title>
 <style type="text/css">
  body{font:"宋体";font-size:12px;}
  a:link,a:visited{font-size:12px;color:#666;text-decoration:none;}
  a:hover{color:#ff0000;text-decoration:underline;background-color:#ffffff;}
  .top{margin-bottom:10px;}
  .userimg{float:left;margin-right:15px;margin-left:30px;}
  .usercredit{float:left;margin-top:10px;color:#333333}
  .clear{ clear:both;} 
  #Tab{margin:0 auto;width:220px;border:1px solid #BCE2F3;}
  .Menubox{height:28px;padding-bottom:5px;width:100%;}
  .Menubox ul{list-style:none;padding:0;position:absolute;margin-left:20px;width:100%;}
  .Menubox ul li{float:left;background:#f5f5f5;line-height:20px;display:block;cursor:pointer;text-align:center;color:#333333;font-weight:bold;margin-right:25px;font-size:15px;width:23%;}
  .Menubox ul li.hover{background:#ffffff;border-bottom:1px solid #fff;color:#00ccff;font-size:15px;}
  .Contentbox{float:left;margin-right:-10px;border-top:none;height:181px;padding-top:8px;height:100%;}
  .Contentbox ul{list-style:none;margin:7px;padding:0;}
  .Contentbox ul li{line-height:24px;border-bottom:1px dotted #ccc;padding-left:5px;}
 </style>
 <script>

function setTab(name,cursel,n){
  for(i=1;i<=n;i++){
    var menu=document.getElementById(name+i);
    var con=document.getElementById("con_"+name+"_"+i);
    menu.className=i==cursel?"hover":"";
    con.style.display=i==cursel?"block":"none";
  }
  }
 </script>
</head>
<body style="background-color:#f5f5f5;width:100%;">
<br><br>
<div class="top">
  <span class="userimg"><img src="<?php echo $picurl; ?>" height="60px"/></span>
  <span class="usercredit"><font size="3px">我的积分为:<br /><?php echo $credit; ?></font></span>
<div class="clear"></div>
</div>
<!--<div id="Tab">-->
  <div class="Menubox">
    <ul>
      <li id="menu1" onclick="setTab('menu',1,3)" class="hover">积分介绍</li>
      <li id="menu2" onclick="setTab('menu',2,3)" >如何获取</li>
      <li id="menu3" onclick="setTab('menu',3,3)" >积分用途</li>
    </ul>
  </div>
  <div class="Contentbox" style="background-color:#ffffff; width:100%;"> 
    <div id="con_menu_1" class="hover">
        <span><p><font color="#00ccff" size="3px">积分介绍</font></p></span>
        <ul>
          <li><font color="#333333">积分是高尔夫江湖会员通过平台活动获得的积分</font></li>
          <li><font color="#333333">活跃率更高获得的积分更多!</font></li>
        </ul>
    </div>
    <div id="con_menu_2" style="display:none">
      <span><p><font color="#00ccff" size="3px">每日登录奖励</font></p></span>
      <ul>
        <li><font color="#333333">每日登录可获得(连续登录可获得更多哦)</font></li>
      </ul>
      <span><p><font color="#00ccff" size="3px">比赛奖励</font></p></span>
      <ul>
        <li><font color="#333333">约球比赛成功可获得(约球人数越多可获得更多)</font></li>
        <li><font color="#333333">立即比赛成功可获得(比赛人数越多可获得更多)</font></li>
        <li><font color="#333333">队内比赛完成可获得(每7日限1次)</font></li>
      </ul>
      <span><p><font color="#00ccff" size="3px">分享记分卡奖励</font></p></span>
      <ul>
        <li><font color="#333333">分享比赛记分卡每次可获得5点,每日限2次</font></li>
      </ul>
      <span><p><font color="#00ccff" size="3px">添加球友奖励</font></p></span>
      <ul>
        <li><font color="#333333">添加球友每成功添加一人可获得3点,不限制,重复添加无效</font></li>
      </ul>
    </div>
    <div id="con_menu_3" style="display:none">
      <span><p><font color="#00ccff" size="3px">积分用途</font></p></span>
      <ul>
        <li><font color="#333333">主要用于兑换高尔夫相关设备</font></li>
      </ul>
    </div>
</div> 
<!--</div>-->
</body>
</html>
