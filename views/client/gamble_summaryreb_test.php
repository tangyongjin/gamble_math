<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no">
        <meta name="format-detection" content="telephone=no">
        <title>游戏结果</title>
        
        <link rel="stylesheet" type="text/css" href="http://s1.golf-brother.com/data/static/res/css/frozen.css" />

        <script src="http://s1.golf-brother.com/data/static/res/js/zepto.min.js"></script>
        <script src="http://s1.golf-brother.com/data/static/res/js/frozen.js"></script>

                   
        <style>
        .ui-table {
            line-height: 40px;
            text-align: center;
            background-color: #fff;
            font-size:18px;
            font-weight:200;
        }
         header ul li{
            font-size:10px;width:auto; white-space:nowrap;text-overflow:clip;
        }
        .hole-d{
            width:20%;padding:2px 2px 2px 5px;;
        }
        .hole-s{
            background-color:#555;color:#fff;font-size:20px;margin-left: 5px;
        }
        .red-font{
            color:#f00;
        }
        .normal-font{
            color:#000;
        }
        </style>
        
        <script>

           var data=<?php echo  $json;?>
 
             function show_summary(data){
                if(typeof data == 'undefined'){
                    return;
                }
                
                
                var json=data;

                if( (typeof json == 'undefined') || ($.type(json) != 'object') ){
                    return;
                }
                
                var players = json.players;
                if( $.type(players) != 'array' || players.length<1 ){
                    return;
                }
                buildHeader(players);
                
               

                var holes = json.holes;
                if( $.type(holes) != 'array' ){
                    return;
                }


               var hole_summary = json.hole_summary;
               
               // alert(json.url_base)

                buildHoles(json);
                
                var scores = json.scores;
                if( $.type(scores) != 'array' ){
                    return;
                }
                buildFooter(scores);
            }
            
            function buildHeader(players){
                var tpl = '<li>参与<br/>球员</li>';
                var last = $('#blk_header').last();
                $(tpl).appendTo(last);
                
                for(var i = 0; i< players.length ; i++){
                    var nm = players[i].nickname;
                    if(nm.length>9){
                        nm = nm.substring(0,9)+"..";
                    }
                    var tpl = '<li><div><img width="35" src="'+players[i].cover+'" ></div><div>'+nm+'</div></li>';
                    var last = $('#blk_header').last();
                    $(tpl).appendTo(last);
                }
            }

            function buildFooter(scores){
                for(var i = 0; i< scores.length ; i++){
                    var tpl = '';
                    var sc = parseFloat(scores[i]);
                    if(sc>0){
                        tpl += '<li class="red-font">'+'+'+scores[i]+'</li>';
                    }else{
                        tpl += '<li>'+scores[i]+'</li>';
                    }
                    var last = $('#blk_footer').last();
                    $(tpl).appendTo(last);
                }
            }

            function buildHoles(json){
                var gameid = parseInt(json.gameid);
                
                var gambleid=json.gambleid
                var groupid=json.groupid
                var userid=json.userid
                var holes = json.holes;
                var players = json.players;
                var hole_summary = json.hole_summary;


                
                hole_detail_url ="http://test.golf-brother.com/web/galpha/alpha_detail/"+"?gambleid="+gambleid+'&gameid='+gameid+'&groupid='
                +groupid+'&userid='+userid;
                
                if(players.length==2){
                   var tdw=' width=33% ';    
                }

                if(players.length==3){
                   var tdw=' width=25% ';    
                }
                if(players.length==4){
                   var tdw=' width=20% ';    
                }

 
                for(var i = 0; i< holes.length ; i++){

                    var hole = holes[i];
                         
                    var holeid=hole.holeid;
                    var court_key=hole.court_key;


                    var tpl = '<tr    data-holeid='+holeid+' data-court_key='+court_key+' > <td'+tdw+' class="hole-d"> <div class="ui-avatar-s hole-s">'+hole.holename+'</div></td>';
                    for(var j = 0; j<hole_summary[i].length; j++){
                        var player = players[j];

                        sc = parseFloat(hole_summary[i][j].money);

                        
                        if(sc>0){
                            tpl += '<td  width=20%><a class="red-font">'+'+'+hole_summary[i][j].money+'</a></td>';
                        }else{
                            tpl += '<td   width=20%><a class="normal-font">'+hole_summary[i][j].money+'</a></td>';
                        }                   
                    }
                     

                    tpl += "</tr>";
                    var last = $('#blk_table').last();
                    $(tpl).appendTo(last);
                }
            }

                 
            
            Zepto(function($){

                show_summary(data);

                $("#blk_table tr").click(function() {
                 
                         var holeid=$(this).data('holeid');
                         var court_key=$(this).data('court_key');
                         var link=hole_detail_url+'&holeid='+holeid+'&court_key='+court_key
                         window.location.href = link
                });


            });
            
        </script>
    </head>
    <body ontouchstart>
  
<div style="width:350px;">

<header class="ui-header ui-header-positive ui-border-b" style="background-color:#fff;color:#000;line-height:10px;height:54px;">
   <ul class="ui-tiled ui-border-t" style="width:350px;background-image:none;margin-top:5px;" id="blk_header">
        
   </ul>
 </header>   


 

<section class="ui-container" style="margin-top:12px;">
<section id="table">
    <div>
        <div>
               <table class="ui-table ui-border-tb" id="blk_table">
                <tbody>
                </tbody>
               </table>

        </div>
        
    </div>
</section>
</section><!-- /.ui-container-->

 <footer class="ui-footer ui-footer-btn"  style="width: 350px;">
     <ul class="ui-tiled ui-border-t" id="blk_footer">
         <li style="font-size:10px;"><div>游戏总成绩</div></li>
     </ul>
 </footer>

</div>

 <div style="float:right;margin-right:250px;margin-top:-810px;">
                  <?php  $arr= json_decode($json);  echo $arr->debuginfo;  ?>
        </div>

    </body>
</html>
