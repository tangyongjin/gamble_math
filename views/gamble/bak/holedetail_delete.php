<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no">
        <meta name="format-detection" content="telephone=no">
        <title>游戏结果</title>
        <link rel="stylesheet" type="text/css" href="<?php echo STATIC_HOST;?>/res/css/frozen.css" />
        <script src="<?php echo STATIC_HOST;?>/res/js/zepto.min.js"></script>
        <script src="<?php echo STATIC_HOST;?>/res/js/frozen.js"></script>
        <style>
        .ui-table {
		    line-height: 40px;
		    text-align: center;
		    background-color: #fff;
		    font-size:18px;
		    font-weight:200;
		}
		 header ul li{
			font-size:10px;width:auto; white-space:nowrap;text-overflow:clip;
		}
		.hole-d{
			width:20%;padding:2px;
		}
		.hole-s{
			background-color:#555;color:#fff;font-size:20px;font-weight:lighter;
		}
		.red-font{
			color:#f00;
		}
        </style>
        
        <script>
        var data = '{ "players":'+ 
		  '[{"id":8, "nickname":"nick3游戏游戏游戏游戏","cover":"http://s1.golf-brother.com/data/attach/user/2014/08/31/c240_8e0d19b54a4e4704ad08a2bd5ed69a05.png"},'+
	        '{"id":8, "nickname":"nick3游戏游戏游戏游戏","cover":"http://s1.golf-brother.com/data/attach/user/c240_holder_user_cover.png"},'+
	        '{"id":8, "nickname":"nick3游戏游戏游戏游戏","cover":"http://s1.golf-brother.com/data/attach/user/c240_holder_user_cover.png"}],'+
	      '"holes":'+
	    	'[["A1","+2","-1","+2"],["A2","+1","-1","+1"],["A3","-2","-1","+2"],["A2","+1","-1","+1"],["A2","+1","-1","+1"],["A3","-2","-1","+2"],["A2","+1","-1","+1"],["A2","+1","-1","+1"],["A3","-2","-1","+2"],["A2","+1","-1","+1"],["A3","-2","-1","+2"]],'+
	      '"scores":'+
	      	'["+25","+24","+12"]'+
	    '}';

            console.log(data) ;
	    
		
        	function fillData(data){
        		if(typeof data == 'undefined'){
        			return;
        		}
        		var json = $.parseJSON(data);
        		if( (typeof json == 'undefined') || ($.type(json) != 'object') ){
        			return;
        		}
        		
        		var players = json.players;
        		if( $.type(players) != 'array' || players.length<1 ){
        			return;
        		}
        		buildHeader(players);
        		
        		var holes = json.holes;
        		if( $.type(holes) != 'array' ){
        			return;
        		}
        		buildHoles(holes,players);
        		
        		var scores = json.scores;
        		if( $.type(scores) != 'array' ){
        			return;
        		}
        		buildFooter(scores);
        	}
        	
        	function buildHeader(players){
        		var tpl = '<li>参与<br/>球员</li>';
        		var last = $('#blk_header').last();
				$(tpl).appendTo(last);
				
        		for(var i = 0; i< players.length ; i++){
        			var nm = players[i].nickname;
        			if(nm.length>9){
        				nm = nm.substring(0,9)+"..";
        			}
        			var tpl = '<li><div><img width="35" src="'+players[i].cover+'" ></div><div>'+nm+'</div></li>';
        			var last = $('#blk_header').last();
    				$(tpl).appendTo(last);
        		}
        	}
        	function buildFooter(scores){
        		for(var i = 0; i< scores.length ; i++){
        			var tpl = '';
        			var sc = parseInt(scores[i]);
        			if(sc>0){
        				tpl += '<li class="red-font">'+scores[i]+'</li>';
        			}else{
        				tpl += '<li>'+scores[i]+'</li>';
        			}
        			var last = $('#blk_footer').last();
    				$(tpl).appendTo(last);
        		}
        	}
        	function buildHoles(holes,players){
        		len = players.length;
        		for(var i = 0; i< holes.length ; i++){
        			var hole = holes[i];
	        		var tpl = '<tr> <td class="hole-d"> <div class="ui-avatar-s hole-s">'+hole[0]+'</div></td>';
	        		for(var j = 1; j<= len; j++){
	        			var sc = parseInt(hole[j]);
	        			if(sc>0){
	        				tpl += '<td class="red-font">'+hole[j]+'</td>';
	        			}else{
	        				tpl += '<td>'+hole[j]+'</td>';
	        			}
	        		}
	        		tpl += "</tr>";
	        		var last = $('#blk_table').last();
    				$(tpl).appendTo(last);
        		}
        	}
        	
        	Zepto(function($){
        		fillData(data);
        	});
        	
        </script>
    </head>
    <body ontouchstart>
    
<header class="ui-header ui-header-positive ui-border-b" style="background-color:#fff;color:#000;line-height:10px;height:54px;margin-top:5px;">
   <ul class="ui-tiled ui-border-t" style="background-image:none;" id="blk_header">
   		
   </ul>
 </header>   
 <footer class="ui-footer ui-footer-btn">
     <ul class="ui-tiled ui-border-t" id="blk_footer">
         <li style="font-size:10px;"><div>游戏总成绩</div></li>
     </ul>
 </footer>

<section class="ui-container" style="margin-top:12px;">
<section id="table">
    <div>
        <div>
            <table class="ui-table ui-border-tb" id="blk_table">
                <tbody>
	               
                </tbody>
            </table>
        </div>
    </div>
</section>
</section><!-- /.ui-container-->
        
    </body>
</html>