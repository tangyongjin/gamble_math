<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

include_once 'base.php';

class Webpage extends Base
{
    private static $ipdatafile = 'tinyipdata.dat';
    public function __construct()
    {
        
        parent::__construct();
        include_once 'gamefuncs.php';
    }


    public function aaa(){
    echo "aaa";

   
   }

    public function get_game_score($arr_arg)
    {
        
        $numargs = func_num_args();
        $userid  = -1;
        if (0 == $numargs) {
            $must_have = array(
                'gameid'
            );
            $para      = check_args($must_have);
            $userid    = intval($para['agent_userid']);
        } else {
            $para   = $arr_arg;
            $userid = intval($para['userid']);
        }

        $gameid = trim($para['gameid']);
        
        $this->load->library('XGame');
        $mgr_game = $this->xgame;
        
        $this->load->library('XTeam');
        $mgr_team = $this->xteam;
         
        $gameinfo  = $mgr_game->game_summary($gameid);

        $teamid    = intval($gameinfo['teamid']);
        $gametype  = intval($gameinfo['gametype']);
        $gamestate = intval($gameinfo['gamestate']);
        
        if (array_key_exists('groupid', $para) && (intval($para['groupid']) > 0)) {
            $type    = 'by_group';
            $groupid = $para['groupid'];
        } else {
            $type    = 'by_game';
            $groupid = -1;
        }
        
        $team_info = $mgr_team->get_team_info($teamid);
        
        $score_complex              = $this->get_score_complex($userid, $gameid, $groupid, $type);
        $score_summary_from_complex = $this->get_score_simple($score_complex);
        $ret                        = array();
        $sql                        = " select count(*) as  court_num from  t_game_court where gameid=$gameid ";
        $d                          = $this->db->query($sql)->row_array();
        $court_num                  = $d['court_num'];
        
        $groupinfo = $mgr_game->get_game_group_info($gameid, $userid,'simple');
        $gameinfo  = $this->fix_gameinfo_fields($gameinfo);
        
        $sql = "select t_game_court.courtid,t_game_court.court_key,t_course_court.courtname from t_game_court,t_course_court where gameid=$gameid and t_course_court.courtid=t_game_court.courtid order by t_game_court.court_key asc ";
        
        $court_info             = $this->db->query($sql)->result_array();
        $court_info             = $this->fix_court_info_field($court_info);
        $gameinfo['court_info'] = $court_info;
        $ret['error_code']      = 1;
        $ret['error_descr']     = 'success';
        $ret['gamestate']       = $gamestate;
        $ret['gameinfo']        = $gameinfo;
        if (count($team_info) > 0) {
            $ret['team_info'] = $team_info;
        }
        $ret['groupinfo']     = $groupinfo;
        // $ret['group_num'] = $mgr_game->get_game_group_num($gameid);
        $ret['score_detail']  = $this->change_p_keys($score_summary_from_complex);
        $ret['score_complex'] = $this->change_p_keys($score_complex);
        $ret['court_num']     = $court_num;
        if (0 == $numargs) {
            $this->output->json_output($ret);
        } else {
            return $ret;
        }
    }



    public function change_p_keys($ass_array)
    {
        $new_arr = array();
        foreach ($ass_array as $arr) {
            $index = 1;
            foreach ($arr as $k => $val) {
                $arr['p' . $index] = $val;
                unset($arr[$k]);
                $index++;
            }
            $new_arr[] = $arr;
        }
        return $new_arr;
    }


    public function game_score_card_html_parameter($gameinfo, $team_info)
    {
        $courtids = $gameinfo['court_info'];
        if (count($courtids) > 1) {
            $up_courtid   = $courtids[0]['courtid'];
            $down_courtid = $courtids[1]['courtid'];
            $holenum      = 18;
            if ($up_courtid == $down_courtid) {
                $holenum = 9;
            }
            
            $a_court_name = $courtids[0]['courtname'];
            $b_court_name = $courtids[1]['courtname'];
            
        } else {
            $a_court_name = $courtids[0]['courtname'];
            $b_court_name = "";
            $holenum      = 9;
        }
        $coursename = $gameinfo['course_name'];
        $coursename = $coursename; //. "(" . $holenum . "洞)";
        $gametype   = $gameinfo['gametype'];
        
        if(in_array($gametype, array(GAME_TYPE_TEAM_GAME,GAME_TYPE_TEAMVSTEAM))){
        
            $team_game_name = $team_info['team_name'] . '-' . $gameinfo['name'];
        } else {
            $team_game_name = "";
        }
        
        $args = array(
            'team_game_name' => $team_game_name,
            'a_court_name' => preg_replace('/\(.*/s', '', $a_court_name),
            'b_court_name' => preg_replace('/\(.*/s', '', $b_court_name),
            'coursename' => $coursename
        );
        return $args;
    }



    public function get_score_complex($userid, $gameid, $groupid, $type)
    {
        
        
        $sys_path        = SERVER_PIC_DIR;
        $score_complex   = array();
        $unsorted_scores = array();
        
        if ('by_game' == $type) {
            $where = " where gameid=$gameid ";
        } else {
            $where = " where groupid=$groupid ";
        }
        
        $sql = "select distinct gameid,a.holeid,holename,a.par,a.court_key as courtno from t_game_score a,t_court_hole  ";
        $sql .= $where . "  and  a.holeid=t_court_hole.holeid order by a.court_key,a.holeid asc";
        
        $par_info          = $this->db->query($sql)->result_array();
        $value_keys        = array(
            'holeid',
            'holename',
            'par',
            'courtno'
        );
        $unique_index_cols = array(
            'holeid',
            'courtno'
        );
        $summary_cfg       = array(
            'sum_operation' => true,
            'sum_col' => 'par',
            'summary_text' => 'parsum'
        );
        $flat_par_info     = array_spread($par_info, $value_keys, $unique_index_cols, $summary_cfg);
        $unique_datas      = $flat_par_info['UNIQUE_DATAS'];
        $summary           = $flat_par_info['SUMMARY'];
        array_walk($unique_datas, function(&$value, &$key)
        {
            $value = $value['UNIQUE_DATA'];
        });
        array_unshift($unique_datas, array(
            'holetext' => '球洞',
            'par_text' => '标杆'
        ));
        $summary_tmp = array(
            'title' => '总计',
            'total_par' => $summary['total']
        );
        array_push($unique_datas, $summary_tmp);
        $header            = $unique_datas;
        $score_complex[]   = $header;

        $sorted_score_data = $this->get_score_falted_data_sorted($userid, $gameid, $groupid, $type);

        foreach ($sorted_score_data as $one_user_score) {
            $score_complex[] = $one_user_score;
        }
        return $score_complex;
    }



    public function get_score_falted_data_sorted($userid, $gameid, $groupid, $type)
    {
        $players = $this->get_game_players($userid, $gameid, $groupid, $type);
        foreach ($players as $one_player) {
            $one_player_score  = $this->get_one_player_score($gameid, $one_player['userid']);
           
            $value_keys        = array(
                'par',
                'gross',
                'push',
                'sandball',
                'penalty',
                'direction',
                'black',
                'blue',
                'red',
                'gold',
                'white',
                'courtno',
                'holeid'
            );
            $unique_index_cols = array(
                'holeid',
                'courtno'
            );
            $summary_cfg       = array(
                'sum_operation' => true,
                'sum_col' => 'gross',
                'summary_text' => 'sumgross'
            );
            $flat_score        = array_spread($one_player_score, $value_keys, $unique_index_cols, $summary_cfg);
            $unique_datas      = $flat_score['UNIQUE_DATAS'];
            $summary           = $flat_score['SUMMARY'];
            array_walk($unique_datas, function(&$value, &$key)
            {
                $value = $value['UNIQUE_DATA'];
            });
            array_unshift($unique_datas, $one_player);
            $summary_tmp = array(
                'gross' => $summary['total']
            );
            array_push($unique_datas, $summary_tmp);
            $one_score_falted  = $unique_datas;
            $unsorted_scores[] = $one_score_falted;
        }
        
        return $unsorted_scores; //sorted
    }


    public function fix_court_info_field($courts)
    {
        
        foreach ($courts as $key => $value) {
            $courtid                   = $value['courtid'];
            $courtname                 = $value['courtname'];
            $sql                       = "select count(holeid) as num from t_court_hole where courtid=$courtid";
            $num                       = $this->db->query($sql)->row_array();
            $courts[$key]['courtname'] = $courtname . '(' . $num['num'] . '洞)';
        }
        return $courts;
    }



    public function get_game_players($caller_id, $gameid, $groupid, $query_type)
    {
        
        $this->load->library('XGame');
        $mgr_game = $this->xgame;
        $sys_path = SERVER_PIC_DIR;
        
        if ('by_game' == $query_type) {
            $where = "  where gameid=$gameid";
        } else {
            $where = " where groupid=$groupid";
        }
        
        $sql = "select distinct userid,nickname, tland_chose as tland,score_confirm_vote,concat($sys_path,t_user.coverpath,'/c240_',t_user.covername) as user_picurl,t_user.type ";
        $sql .= " from t_game_group_user,t_user " . $where;
        $sql .= " and t_game_group_user.userid=t_user.id order by userid asc";
        
        $players = $this->db->query($sql)->result_array();
        array_walk($players, array(
            $mgr_game,
            'setting_alt_nickname'
        ), array(
            'gameid' => $gameid,
            'userid' => $caller_id
        ));
        return $players;
    }



    public function get_one_player_score($gameid, $userid)
    {
        
        $sql = "select t_game_score.userid,holeid,par,gross,sandball,push,penalty,direction,gold,red,white,blue,black,court_key as courtno  from  t_game_score";
        $sql .= " where  gameid=$gameid and t_game_score.userid=$userid ";
        $sql .= " order by userid,courtno,holeid ";
        $one_player_score = $this->db->query($sql)->result_array();
        return $one_player_score;
    }


    public function get_score_simple($score_complex)
    {
        $score_summary_from_complex = array();
        $header                     = array(
            '排名',
            '球手'
        );
        $header_complex             = $score_complex[0];
        if (count($header_complex) == 20) {
            // complex_array add  1 in begin, and 1 in end.
            $hole_num = 18;
        } else {
            $hole_num = 9;
        }
        for ($i = 1; $i < $hole_num; $i++) {
            $header[] = $header_complex[$i]['par'];
        }
        $header[]                     = '总杆数';
        $header[]                     = '净杆数';
        $score_summary_from_complex[] = $header;
        array_shift($score_complex); //remove header;
        $players_count = count($score_complex);
        for ($i = 1; $i <= $players_count; $i++) {
            $score    = array();
            $score[0] = $i;
            $score[1] = $score_complex[$i - 1][0]['nickname'];
            for ($j = 1; $j < $hole_num; $j++) {
                $score[] = $score_complex[$i - 1][$j]['gross'];
            }
            $score[] = $score_complex[$i - 1][$j + 1]['gross'];
            array_shift($score_complex[$i - 1]);
            array_pop($score_complex[$i - 1]);
            $net                          = $this->new_new_peoria($score_complex[$i - 1]);
            $score[]                      = $net['net_bar'];
            $score_summary_from_complex[] = $score;
        }
        return $score_summary_from_complex;
    }


    public function new_new_peoria($socre)
    {
        $grossnum       = 0;
        $parnum         = 0;
        $rand_gross_num = 0;
        foreach ($socre as $k => $v) {
            $parnum   = $parnum + $socre[$k]['par'];
            $grossnum = $grossnum + $socre[$k]['gross'];
            if (3 == $socre[$k]['par']) {
                if ($socre[$k]['gross'] > 5) {
                    $socre[$k]['gross'] = 5;
                }
            } elseif (4 == $socre[$k]['par']) {
                if ($socre[$k]['gross'] > 7) {
                    $socre[$k]['gross'] = 7;
                }
            } elseif (5 == $socre[$k]['par']) {
                if ($socre[$k]['gross'] > 9) {
                    $socre[$k]['gross'] = 9;
                }
            }
        }
        $rand_num = array_rand($socre, 12);
        foreach ($rand_num as $k2 => $v2) {
            $rand_gross_num = $rand_gross_num + $socre[$v2]['gross'];
        }
        $handicap = ($rand_gross_num * 1.5 - $parnum) * 0.8;
        $net_bar  = $grossnum - $handicap;
        $net_bar  = number_format($net_bar, 2);
        $result   = array(
            'grossnum' => $grossnum,
            'net_bar' => $net_bar
        );
        return $result;
    }



    public function fix_gameinfo_fields($arr)
    {
        
        $this->load->database();
        $courtname = "";
        $gameid    = intval($arr['gameid']);
        $courseid  = intval($arr['courseid']);
        $sql       = "select count(holeid) as holeidnum from t_court_hole where courtid in (select courtid from t_game_court where gameid=$gameid) ";
        
        $holenum = $this->db->query($sql)->row_array();
        $sql     = "select courtname from t_course_court where courseid=$courseid and courtid in (select courtid from t_game_court where gameid=$gameid order by court_key asc)  ";
        $name    = $this->db->query($sql)->result_array();
        foreach ($name as $key => $value) {
            $courtname .= $value['courtname'] . '/';
        }
        
        $courtname = explode('/', $courtname);
        array_pop($courtname);
        $courtname = implode('/', $courtname);
        
        $course_name        = $arr['course_name'];
        $arr['course_name'] = '(' . $holenum['holeidnum'] . '洞)' . $course_name . $courtname;
        
        $cost = $arr['cost'];
        if (empty($cost)) {
            $arr['cost'] = '-';
        }
        $remark = $arr['remark'];
        if (empty($remark)) {
            $arr['remark'] = '无';
        }
        
        $game_paly_info = $value['game_paly_info'];
        if (empty($game_paly_info)) {
            $arr['game_paly_info'] = '无';
        }
        return $arr;
    }



    //比赛净杆成绩排序
    private function net_bar_sort($a,$b)
    {
        
        $configures = array('net_bar',17,16,15,14,13,12,11,10,9,8,7,6,5,4,3,2,1,0);
        foreach ($configures as $key => $one_configure) {
            if($one_configure == 'net_bar'){
                if($a[$one_configure] == $b[$one_configure]){
                     $result = 0;
                }else{
                    $result = $a[$one_configure] < $b[$one_configure] ? -1 : 1;
                    break;
                }
            }else{
                if($a['gross'][$one_configure]['gross'] == $b['gross'][$one_configure]['gross']){
                    $result = 0;
                }else{
                    $result = $a['gross'][$one_configure]['gross'] < $b['gross'][$one_configure]['gross'] ? -1 : 1;
                    break;
                }
            }
            
        }
        return $result;
    }



    //比赛成绩卡的排序
    private function game_score_sort($a,$b)
    {
        
        $configures = array('gan_cha','grosssum',17,16,15,14,13,12,11,10,9,8,7,6,5,4,3,2,1,0);
        foreach ($configures as $key => $one_configure) {
            if($one_configure == 'gan_cha' || $one_configure == 'grosssum'){
                if($a[$one_configure] == $b[$one_configure]){
                     $result = 0;
                }else{
                    $result = $a[$one_configure] < $b[$one_configure] ? -1 : 1;
                    break;
                }
            }else{
                if($a['gross'][$one_configure]['gross'] == $b['gross'][$one_configure]['gross']){
                    $result = 0;
                }else{
                    $result = $a['gross'][$one_configure]['gross'] < $b['gross'][$one_configure]['gross'] ? -1 : 1;
                    break;
                }
            }
            
        }
        return $result;
    }


    public function game_score_data_dispose($game_user_scores)
    {
        
        
        $this->load->library('XMath');
        $mgr_math        = $this->xmath;
        $user_game_score = array();
        $not_play_game_data = array();
        $not_data = array();
        $data            = array();
        $index           = 0;
        foreach ($game_user_scores as $key => $value) {
            if (0 != $key) { 
                
                // $user_game_score[$index] = $value['p1'];
                $p = $value['p1'];
                unset($value['p1']);
                $user_score = array_values($value);
                unset($user_score[count($user_score) - 1]);
                $user_score_data                             = $mgr_math->computing_user_one_game_score($user_score);
                
                
                if($user_score_data['grosssum'] == 0){
                    $not_play_game_data[$index] = $p;
                    $user_score_data['user_info']['tland']       = $not_play_game_data[$index]['tland'];
                    $user_score_data['user_info']['nickname']    = $not_play_game_data[$index]['nickname'];
                    $user_score_data['user_info']['userid']      = $not_play_game_data[$index]['userid']; //ZZZZ plug in
                    $user_score_data['gan_cha']         = $user_score_data['gan_cha_sum'];
                    $user_score_data['user_info']['user_picurl'] = $not_play_game_data[$index]['user_picurl'];
                    $not_play_game_data[$index]['gross']            = $user_score_data['score_array'];
                    $not_play_game_data[$index]['grosssum']         = $user_score_data['grosssum'];
                    $not_play_game_data[$index]['gan_cha']         = $user_score_data['gan_cha_sum'];
                    $not_data[] = $user_score_data;
                }else{
                    $user_game_score[$index] = $p;
                    $user_score_data['user_info']['tland']       = $user_game_score[$index]['tland'];
                    $user_score_data['user_info']['nickname']    = $user_game_score[$index]['nickname'];
                    $user_score_data['user_info']['userid']      = $user_game_score[$index]['userid']; //ZZZZ plug in
                    $user_score_data['gan_cha']         = $user_score_data['gan_cha_sum'];
                    $user_score_data['user_info']['user_picurl'] = $user_game_score[$index]['user_picurl'];
                    
                    $user_game_score[$index]['gross']            = $user_score_data['score_array'];
                    $user_game_score[$index]['grosssum']         = $user_score_data['grosssum'];
                    $user_game_score[$index]['gan_cha']         = $user_score_data['gan_cha_sum'];
                    $data[] = $user_score_data;
                }
                
                
                $index++;
            }
        }

        $user_game_score = $mgr_math->sorting($user_game_score,'SORT_ASC','gan_cha');
        $user_game_score = array_merge($user_game_score,$not_play_game_data);
        $data = $mgr_math->sorting($data,'SORT_ASC','gan_cha');
        $data = array_merge($data,$not_data);
        return array(
            'user_game_score' => $user_game_score,
            'analysis_data' => $data
        );
    }


    public function get_game_user_color_plugin($gameid,$user_game_score,$source)
    {

       $sql="select * from t_game_squad where gameid=$gameid" ;
       $row    = $this->db->query($sql)->result_array();
       $using_plug=true;
       if( count($row)==0 ){
         $using_plug=false;
       }
    
       array_walk($user_game_score, array(
            $this,
            'setting_user_color'
        ), array(
            'gameid' => $gameid,
            'source'=>$source
        ));

      return  array('using_plug'=>$using_plug,'score'=>$user_game_score);
    }

/***   网页      ***/


    public function get_game_score_html()
    {
        
        $must_have = array(
            'gameid'
        );
        $para      = check_args($must_have);
        
        $gameid            = intval($para['gameid']);
        $userid            = intval($para['agent_userid']);
        $is_show_code            = $para['isShowCode'];
        $ret['error_code'] = -1;
        
        $game_score = $this->get_game_score(array(
            'gameid' => $gameid,
            'userid' => $userid
        ));

        $parameter        = $this->game_score_card_html_parameter($game_score['gameinfo'], $game_score['team_info']);
        $game_user_scores = $game_score['score_complex'];
        $pars             = $game_score['score_complex'][0];
        unset($pars['p1']);
        $pars    = array_values($pars);
        $par_sum = $pars[count($pars) - 1]['total_par'];
        unset($pars[count($pars) - 1]);
        if(in_array($game_score['gameinfo']['gametype'],array(GAME_TYPE_TEAM_GAME,GAME_TYPE_TEAMVSTEAM))){
            $this->load->database();
            $this->db->from("t_game_score");
            $this->db->where('gameid',$gameid);
            $num = $this->db->count_all_results();
            if($num == 0){
                $pars = $this->get_game_par_info($game_score['gameinfo'],$game_score['court_num']);
                $game_user_scores = $this->get_game_user_score_info($game_score['score_complex'],$pars);
            }
        }
        

        $value = $this->game_score_data_dispose($game_user_scores);
        
        
        $user_game_score=$value['user_game_score'];
        


        $plugin_ret=$this->get_game_user_color_plugin($gameid,$user_game_score,'get_game_score_html') ;
         
        $using_plug=$plugin_ret['using_plug']; 
        $user_game_score=$plugin_ret['score'];
        $empty_user_score = array();
        $not_complete_score = array();
        $this->load->database();
        foreach ($user_game_score as $key => $one_user_score) {
           $playerid = $one_user_score['userid'];
           if($one_user_score['grosssum'] == 0){
                $empty_user_score[] = $user_game_score[$key];
                unset($user_game_score[$key]);
           }else{
                $sql = "select min(gross) as mingross from t_game_score where gameid=$gameid and userid=$playerid ";
                $min_gross = $this->db->query($sql)->row_array();
                if(intval($min_gross['mingross']) == 0){
                    $not_complete_score[] = $user_game_score[$key];
                    unset($user_game_score[$key]);
                }
           }
        }

        usort($user_game_score, array($this,'game_score_sort'));
        if(count($not_complete_score) > 0){
            usort($not_complete_score, array($this,'game_score_sort'));
            $user_game_score = array_merge($user_game_score,$not_complete_score);
        }
        
        $user_game_score = array_merge($user_game_score,$empty_user_score);
        $data  = array(
            'user_color_cfg'=>$user_color_cfg,
            'par_sum' => $par_sum,
            'coursename' => $parameter['coursename'],
            'pars' => $pars,
            'user_game_score' => $user_game_score,
            'time' => $game_score['gameinfo']['starttime'],
            'a_court_name' => $parameter['a_court_name'],
            'b_court_name' => $parameter['b_court_name'],
            'team_game_name' => $parameter['team_game_name'],
            'using_plug'=>$using_plug,
            'isShowCode'=>$is_show_code
        );
        
        $this->load->view('client/score_html.php', $data);
    }


    public function game_score_analysis()
    {

        $must_have = array();
        $para      = check_args($must_have);
        include_once('funcs.php');
        $ret['error_code'] = -1;
        $userid            = intval($para['userid']);
        $gameid            = intval($para['gameid']);
        $is_show_code            = $para['isShowCode'];
        $game_score = $this->get_game_score(array(
            'gameid' => $gameid,
            'userid' => $userid
        ));
        $parameter     = $this->game_score_card_html_parameter($game_score['gameinfo'], $game_score['team_info']);
        $score_complex = $game_score['score_complex'];
        $value         = $this->game_score_data_dispose($score_complex);

        $user_game_score=$value['analysis_data'];

        

         $plugin_ret=$this->get_game_user_color_plugin($gameid,$user_game_score,'game_score_analysis') ;

         
          $using_plug=$plugin_ret['using_plug']; 
          $user_game_score=$plugin_ret['score'];


        $data          = array(
            'coursename' => $parameter['coursename'],
            'user_game_score' => $user_game_score,
            'isShowCode'=>$is_show_code,
            'time' => $game_score['gameinfo']['starttime'],
            'team_game_name' => $parameter['team_game_name'],
            'using_plug' =>$using_plug 

        );

        $this->load->view('client/score_analysis.php', $data);
    }


/******网页－－－净杆排行*********/

    public function get_game_score_net_score_rank_html()
    {
        
        $must_have = array(
            'gameid',
            'netholes'
        );
        $para      = check_args($must_have);
        
        $gameid            = intval($para['gameid']);
        $userid            = intval($para['agent_userid']);
        $is_show_code            = $para['isShowCode'];
        $selected_hole_serial_number = trim($para['netholes']);
        $ret['error_code'] = -1;
        
        $selected_holeid = $this->get_selected_holeid($selected_hole_serial_number,$gameid);
        $game_score = $this->get_game_score(array(
            'gameid' => $gameid,
            'userid' => $userid
        ));

        $parameter        = $this->game_score_card_html_parameter($game_score['gameinfo'], $game_score['team_info']);

        $game_user_scores = $game_score['score_complex'];
        $pars             = $game_score['score_complex'][0];
        
        unset($pars['p1']);
        $pars    = array_values($pars);
        $par_sum = $pars[count($pars) - 1]['total_par'];
        unset($pars[count($pars) - 1]);
        $value = $this->game_score_data_dispose($game_user_scores);
        $user_game_score=$value['user_game_score'];
        $plugin_ret=$this->get_game_user_color_plugin($gameid,$user_game_score,'get_game_score_html') ;
         
        $using_plug=$plugin_ret['using_plug']; 
        $user_game_score=$plugin_ret['score'];
        
        $pars = $this->selected_hole_par($pars,$selected_holeid);
        
        $user_game_score = $this->selected_hole_user_score_net_bar_rank($user_game_score,$selected_holeid);
        usort($user_game_score, array($this,'net_bar_sort'));
        $data  = array(
            'user_color_cfg'=>$user_color_cfg,
            'par_sum' => $par_sum,
            'coursename' => $parameter['coursename'],
            'pars' => $pars,
            'user_game_score' => $user_game_score,
            'time' => $game_score['gameinfo']['starttime'],
            'a_court_name' => $parameter['a_court_name'],
            'b_court_name' => $parameter['b_court_name'],
            'team_game_name' => $parameter['team_game_name'],
            'using_plug'=>$using_plug,
            'isShowCode'=>$is_show_code
        );
        
        $this->load->view('client/score_net_bar_rank.php', $data);
    }


    public function get_selected_holeid($hole_serial_number,$gameid){
        $selected_hole_array = explode('|', $hole_serial_number);
        if(count($selected_hole_array) == 1){
            $top_court_array = explode(',', $selected_hole_array[0]);
            $down_court_array = array();
        }else{
            $top_court_array = explode(',', $selected_hole_array[0]);
            $down_court_array = explode(',', $selected_hole_array[1]);
        }
        $this->load->database();
        $sql = "select count(id) as num from t_game_court where gameid=$gameid ";
        $num = $this->db->query($sql)->row_array();
        $sql = "select holeid from t_game_court as gc,t_court_hole as ch where gc.gameid=$gameid and gc.court_key=1 and ch.courtid=gc.courtid";
        $top_court_hole_list = $this->db->query($sql)->result_array();
        $out_hole_array = array();
        $in_hole_array = array();
        foreach ($top_court_hole_list as $key => $one_hole) {
            $serial_number = $key+1;
            if(in_array($serial_number, $top_court_array)){
                $out_hole_array[] = $one_hole['holeid'];
            }
        }
        if($num['num'] == 2){
          $serial_number = 9;
          $sql = "select holeid from t_game_court as gc,t_court_hole as ch where gc.gameid=$gameid and gc.court_key=2 and ch.courtid=gc.courtid";
          $down_court_hole_list = $this->db->query($sql)->result_array();
          foreach ($down_court_hole_list as $key => $one_hole) {
                $serial_number++;
                if(in_array($serial_number, $down_court_array)){
                    $in_hole_array[] = $one_hole['holeid'];
                }
          }

        }
        return array('out_hole_array'=>$out_hole_array,'in_hole_array'=>$in_hole_array);
    }


    public function selected_hole_par($pars,$selected_hole){
       
        foreach ($pars as $key => $one_hole) {
            $holeid = $one_hole['holeid'];
            $courtno = $one_hole['courtno'];
            $pars[$key]['selected'] = 0;
            if($this->check_is_in_rules_range($holeid,$courtno,$selected_hole)){
               $pars[$key]['selected'] = 1;
            }

        }
        return $pars;
    }


    public function selected_hole_user_score_net_bar_rank($user_game_score,$selected_hole){
        $this->load->library('XMath');
        $mgr_math  = $this->xmath;
        
        foreach ($user_game_score as $key => $one_user_score) {
            $par_array = array();
            $gross_array = array();
            $gross = $one_user_score['gross'];
            foreach ($gross as $k => $one_gross) {
                if($one_gross['gross'] < 1){
                    unset($user_game_score[$key]);
                    break;
                }
                $par_array[] = $one_gross['par'];
                $user_game_score[$key]['gross'][$k]['selected'] = 0;
                $holeid = $one_gross['holeid'];
                $courtno = $one_gross['courtno'];
                if($this->check_is_in_rules_range($holeid,$courtno,$selected_hole)){
                    $user_game_score[$key]['gross'][$k]['selected'] = 1;
                }else{
                    $gross_array[] = $one_gross['gross'];
                }
            }
           
           
            if($user_game_score[$key]['grosssum'] > 0){
               $handicap = $mgr_math->math_user_one_game_handicap($par_array,$gross_array);
               $user_game_score[$key]['net_bar'] = $user_game_score[$key]['grosssum']-$handicap;   
            }else{
                unset($user_game_score[$key]);
            }
                    
        }
        return $user_game_score;//$mgr_math->sorting($user_game_score,'SORT_ASC','net_bar');
    }


    public function check_is_in_rules_range($holeid,$courtno,$selected_hole){
        $check_result = false;
        if(in_array($holeid, $selected_hole['out_hole_array'])){
            if($courtno == 1){
                $check_result = true;
            }
        }

        if(in_array($holeid, $selected_hole['in_hole_array'])){
            if($courtno == 2){
                $check_result = true;
            }
        }
        return $check_result;
    }

/***********网页---分队成绩表****************/


    public function game_squads_score_html(){
        $this->load->library('XMath');
        $mgr_math = $this->xmath;
        $must_have = array(
            'gameid'
        );
        $para      = check_args($must_have);
        
        $gameid            = intval($para['gameid']);
        $userid            = intval($para['agent_userid']);
        $is_show_code            = $para['isShowCode'];
        $ret['error_code'] = -1;
        
        $game_score = $this->get_game_score(array(
            'gameid' => $gameid,
            'userid' => $userid
        ));
        $top_rank_player_num = $game_score['gameinfo']['top_rank_player_num'];
        $parameter        = $this->game_score_card_html_parameter($game_score['gameinfo'], $game_score['team_info']);
        $parameter['team_game_name'] = $game_score['gameinfo']['name'];
        $game_user_scores = $game_score['score_complex'];
        $pars             = $game_score['score_complex'][0];
        unset($pars['p1']);
        $pars    = array_values($pars);
        $par_sum = $pars[count($pars) - 1]['total_par'];
        unset($pars[count($pars) - 1]);
        $value = $this->game_score_data_dispose($game_user_scores);
        
        
        $user_game_score=$value['user_game_score'];

        $result = $this->squads_score($user_game_score,$gameid,$top_rank_player_num);
        $squads_list = $result['squads_list'];

        $user_game_score = $mgr_math->sorting($result['user_game_score'],'SORT_ASC','gan_cha');

        foreach ($squads_list as $key => $one_squad) {
            $squad_user_score = $one_squad['squad_score'];
            if(count($squad_user_score) < 1){
                unset($squads_list[$key]);
            }
        }


        $data  = array(
            'user_color_cfg'=>$user_color_cfg,
            'par_sum' => $par_sum,
            'coursename' => $parameter['coursename'],
            'pars' => $pars,
            'squads_list' => $squads_list,
            'user_game_score'=>$user_game_score,
            'time' => $game_score['gameinfo']['starttime'],
            'a_court_name' => $parameter['a_court_name'],
            'b_court_name' => $parameter['b_court_name'],
            'team_game_name' => $parameter['team_game_name'],
            'using_plug'=>$using_plug,
            'isShowCode'=>$is_show_code
        );



        
        $this->load->view('client/game_squad_score.php',$data);
        
       

    }


    private function squads_score($user_game_score,$gameid,$top_rank_player_num){
        $this->load->library('XTeam');
        $mgr_team = $this->xteam;
        $this->load->library('XMath');
        $mgr_math = $this->xmath;
   
        $this->load->database();
        $sql = "select * from t_game_squad where gameid=$gameid";
        $squads_list = $this->db->query($sql)->result_array();

        $sql = "select min(usernum) as usernum from t_game_squad where gameid=$gameid and usernum<>0";
        $squad_usernum = $this->db->query($sql)->row_array();
        $game_squad_min_usernum = intval($squad_usernum['usernum']);
        if($top_rank_player_num > $game_squad_min_usernum){
            $top_rank_player_num = $game_squad_min_usernum;
        }

        if($top_rank_player_num == 0){
            $top_rank_player_num = $game_squad_min_usernum;
        }
        foreach ($squads_list as $key => $one_squad) {
            $squadid = intval($one_squad['id']);
            if(isset($one_squad['teamid'])){
                $teaminfo = $mgr_team->get_team_info(intval($one_squad['teamid']));
                $squads_list[$key]['squad_info'] = array('squad_picurl'=>$teaminfo['team_picurl'],'squad_name'=>$teaminfo['team_name']);
            }else{
                $squads_list[$key]['squad_info'] = array('squad_picurl'=>'','squad_name'=>$one_squad['squad_name']);
            }
            $result = $this->get_one_squad_score($gameid,$squadid,$user_game_score);
            
            // $squad_score = $mgr_math->sorting($result['squad_score'],'SORT_ASC','gan_cha');
            $squad_score = array_slice($result['squad_score'],0,$top_rank_player_num);
            $squads_list[$key]['squad_score'] = $squad_score;
            $user_game_score = $result['user_game_score'];
        }
        return array('squads_list'=>$squads_list,'user_game_score'=>$user_game_score);
    }



    private function get_one_squad_score($gameid,$squadid,$user_game_score){
        $squad_score = array();
        $this->load->database();
        $sql = "select userid from t_game_squad_user where gameid=$gameid and squadid=$squadid";
        $squad_users = $this->db->query($sql)->result_array();
        $squad_users_str = array_to_string($squad_users,'userid');
        $squad_users_array = explode(',', $squad_users_str);
        foreach ($user_game_score as $key => $one_user) {
            $userid = intval($one_user['userid']);
            if(in_array($userid, $squad_users_array)){
                $squad_score[] = $one_user;
                unset($user_game_score[$key]);
            }
        }

        $squad_score = $this->squad_user_score_rank($squad_score);

        return array('user_game_score'=>$user_game_score,'squad_score'=>$squad_score);
    }


    private function squad_user_score_rank($squad_score){
        $this->load->library('XMath');
        $mgr_math = $this->xmath;
        $paiming = 1;
        $squad_user_num = count($squad_score);
        $same_user_score = array();
        $empty_user_score = array();
        $squad_score = $mgr_math->sorting($squad_score,'SORT_ASC','gan_cha');
        foreach ($squad_score as $key => $value) {
           $user_paiming = $paiming;
           if($key == 0){
              $gan_cha = $value['gan_cha'];
           }
           if($value['grosssum'] == 0){
              $empty_user_score[] = $value;
              if($key+1 != count($squad_score)){
                $gan_cha = $squad_score[$key+1]['gan_cha'];
              }
              $paiming++;
           }else{

              if($gan_cha == $value['gan_cha']){
                $same_user_score[$user_paiming][] = $value;
                $gan_cha = $value['gan_cha'];
              }else{
                $paiming++;
                $same_user_score[$paiming][] = $value;
                $gan_cha = $value['gan_cha'];
              }
           }
        }

        $empty_user_score = $this->handle_empty_user_score($empty_user_score,$squad_user_num);
        $squad_score = array();
        $squad_score = $this->handle_same_user_score($same_user_score);
        $squad_score = array_merge($squad_score,$empty_user_score);
        return $squad_score;
    }


    private function handle_empty_user_score($empty_user_score,$squad_user_num){
        foreach ($empty_user_score as $key => $value) {
          if(isset($value['userid'])){
            $empty_user_score[$key]['paiming'] = $squad_user_num;
          }else{
            unset($empty_user_score[$key]);
          }
        }
        return $empty_user_score;
    }


    private function handle_same_user_score($same_user_score){
        $this->load->library('XMath');
        $mgr_math = $this->xmath;
        $squad_score = array();
        foreach ($same_user_score as $key => $value) {
          $same_user_score[$key] = $mgr_math->sorting($value,'SORT_ASC','grosssum');
        }

        $paiming = 1;
        foreach ($same_user_score as $key => $one_same_user_array) {
          $user_paiming = $paiming;
           foreach ($one_same_user_array as $one_user_score) {
              $one_user_score['paiming'] = $user_paiming;
              $squad_score[] = $one_user_score;
              $paiming++;
           }
        }

        return $squad_score;
    }


    /***********网页---广告**************/
        public function home_top_ad(){
        $CI =& get_instance();
        $SID = $CI->input->server('HTTP_SID');
        $user  = $CI->getCurrentUser();
        $userid = intval($user['id']);
        $width = $_GET['width'];
        $img_url = "http://s1.golf-brother.com/data/images/advertise_user_cathy.png";
        $img_info = GetImageSize($img_url);
        
        $height_equal_ratio=$img_info[1]*($width/$img_info[0]);
        $ret = array();
        $ret['error_code'] = 1;
        $ret['error_descr'] = "success";
        $ret['ad'] = array('height'=>$height_equal_ratio,'width'=>$width,'starttime'=>strtotime('-1 days',time()),'endtime'=>strtotime('+1 days',time()),'url'=>URL_PATH."open_html/?height=".$height_equal_ratio.'&sid='.$SID.'&userid='.$userid);
        //$ret['ad'] = array();
        
        $this->output->json_output($ret);
    }

    public function open_html(){
        $height = $_GET['height'];
        $sid = $_GET['sid'];
        $userid = $_GET['userid'];
        $s = $_GET['s'];
        $cv = $_GET['cv'];
        $this->load->view('client/advertising.html',array('s'=>$s,'cv'=>$cv,'height'=>$height,'sid'=>$sid,'userid'=>$userid));
    }

    public function ad_blank_html(){
        $this->load->view('client/blank_html.html');
    }


    public function show_upgrade(){
        $this->load->view('client/upgrade.html');
    }


    public function new_zealand_activity(){
        $key = isset($_GET['key']) ? intval($_GET['key']) : 1;

        if($key == 0){
            $key = 1;
        }
        if($key == 1){
            $this->load->view('client/new_zealand_skiing.html');
        }elseif($key == 2){
            $this->load->view('client/new_zealand_golf_experience.html');
        }elseif($key == 3){
            $this->load->view('client/new_zealand_golf_peak.html');
        }
    }


 /*********网页--我的足迹*************/


 public function jianghu() {
        $sid = "";
        if(isset($_GET['p'])){
            $parameter=$_GET['p'];
            $parameter_value = decodeRandString($parameter);
            $parameter_value_array = explode('&', $parameter_value);
            $uid_array = explode('=', $parameter_value_array[0]);
            $userid = intval($uid_array[1]);
        }else{
            $parameter=$_GET['uid'];
            $parameter_value = decodeRandString($parameter);
            $userid = intval($parameter_value);
        }


        if(isset($_GET['userid'])){

            $userid = intval($_GET['userid']);
            $sid = $_GET['sid'];
        }
        
        
         if($userid==0){
            $userid=$uid;
         } 

         $profile=$this->user_footprint($userid) ;
         
         $china_legcy_time=$this->china_legcy_time($profile['joind_h'],$profile['joind_m']);
         $profile['sc_ke']=$china_legcy_time['sc'].$china_legcy_time['ke'];
         $profile['sid'] = $sid;
         $this->load->view('client/jianghu.html',$profile);
    }


 public function pktable() {

        $picroot='http://s1.golf-brother.com/data/attach/';
        
        $sid = "";
        if(isset($_GET['p'])){
            $parameter=$_GET['p'];
            $parameter_value = decodeRandString($parameter);
            $parameter_value_array = explode('&', $parameter_value);
            $uid_array = explode('=', $parameter_value_array[0]);
            $userid = intval($uid_array[1]);
        }else{
            $parameter=$_GET['uid'];
            $parameter_value = decodeRandString($parameter);
            $userid = intval($parameter_value);
        }
        
        if(isset($_GET['userid'])){

            $userid = intval($_GET['userid']);
            $sid = $_GET['sid'];
        }
        
         if($userid==0){
            $userid=$uid;
         }

        $this->load->library('XUsers');
        $mgr_user  = $this->xusers;

       
           $me_info=$mgr_user->getUser($userid);
        


        $pkerlist=array();

        $game_status = GAME_ENDED;
        $this->load->database();
        $sql = "select gameid from t_game where gamestate=$game_status and gameid in (select gameid from t_game_group_user where userid=$userid)";

        $mygames=$this->db->query($sql)->result_array();
        $mygamesstr=array_to_string($mygames,'gameid');



        $sql="select gameid,count(distinct holeid) from t_game_score where gameid in ($mygamesstr) group by gameid having  count(distinct holeid) >9" ;
        $mygames_filtered=$this->db->query($sql)->result_array();
        
        $mygamesstr=array_to_string($mygames_filtered,'gameid');

        $sql="select gameid,userid as pker, sum(gross) as pkgross ,min(gross) as mingross from t_game_score where gameid in ($mygamesstr) and userid<>$userid  group by gameid,userid order by gameid " ;
      
        $pker_score=$this->db->query($sql)->result_array();
 
        $sql="select gameid,userid as me, sum(gross) as megross,min(gross) as mingross from t_game_score where gameid in ($mygamesstr) and userid=$userid  group by gameid,userid order by gameid" ;
 
        $my_score=$this->db->query($sql)->result_array();
 
        foreach ($pker_score as $key => $one_pker_game) {
           $gameid=$one_pker_game['gameid'];
           $pker=$one_pker_game['pker'];
           $pker_score=$one_pker_game['pkgross'];
           
           $me_score=-1;
           $fail=0;
           $win=0;
           $equeal=0;
            foreach ($my_score as   $one_my_score) {
                if($one_my_score['gameid']==$gameid){
                    $me_score=$one_my_score['megross'];
                    break;
                }     
           
           }

               if($me_score >  $pker_score ){
                      $fail=1;
                }

                if($me_score <  $pker_score ){
                      $win=1;
                }

                if($me_score ==  $pker_score ){
                      $equeal=1;
 
                }




           if( array_key_exists($pker, $pkerlist)  ){
             $pkerlist[$pker]['total']++;
             $pkerlist[$pker]['win']= $pkerlist[$pker]['win']+$win;
             $pkerlist[$pker]['fail']= $pkerlist[$pker]['fail']+$fail;
             $pkerlist[$pker]['equeal']= $pkerlist[$pker]['equeal']+$equeal;
           } 
           else
           {
             $pkerlist[$pker]=array(
                   'nickname'=>'',
                   'userid'=>$pker,
                   'total'=>1,
                   'win'=>$win,
                   'equeal'=>$equeal,
                   'fail'=>$fail,
                   'winrate'=>0
            );




           }  

        }

          
         
         foreach ($pkerlist as $key=>$one) {
                   
                    $pker=$one['userid'];
                    $pker_info=$mgr_user->getUser($pker);
                    
                    if(($pker_info['covername']=='holder_user_cover.png') ||( strlen($pker_info['covername'])==0)) {
                     $pkerlist[$key]['nickname']='<img class=pkpic src='.$picroot.'/user/holder_user_cover.png'.' />';
                
                    }
                    else
                    {

                    $pkerlist[$key]['nickname']='<img class=pkpic src='.$picroot.$pker_info['coverpath'].'/c240_'.$pker_info['covername'].' />';
                    
                    }
                    $pkerlist[$key]['nickname'].="<div class=pkname id=$pker >".$pker_info['nickname'].'</div>';
                    $pkerlist[$key]['winrate']=round( ($pkerlist[$key]['win']+$pkerlist[$key]['equeal']/2) /$pkerlist[$key]['total']*100,2).'%';
                    unset($pkerlist[$key]['userid']);
        }
          usort($pkerlist, function($a, $b) {return $b['total'] - $a['total']; });
 
          $this->load->library('table');
          $data=array('pkerlist'=>$pkerlist,'user_name'=>$me_info['nickname'],
            'user_pic'=>$picroot.$me_info['coverpath'].'/c240_'.$me_info['covername'],'sid'=>$sid);
          
          $this->load->view('client/pklist.html',$data);
    }

 
   


    public function china_legcy_time($h,$m){

         $sc=''; 
         $ke='';
        if( $h >1 and $h <=3){
         $sc='丑时'; 
        }

        if( $h >3 and $h <=5){
         $sc='寅时'; 
        }

        if( $h >5 and $h <=7){
         $sc='卯时'; 
        }

       if( $h >7 and $h <=9){
         $sc='辰时'; 
        }

        if( $h >9 and $h <=11){
          $sc='巳时'; 
        }

        if( $h >11 and $h <=13){
          $sc='午时'; 
        }

        if( $h >13 and $h <=15){
          $sc='未时'; 
        }

        if( $h >15 and $h <=17){
         $sc='申时'; 
        }

        if( $h >17 and $h <=19){
         $sc='酉时'; 
        }
         
         if( $h >19 and $h <=21){
         $sc='戌时'; 
        }
 
       if( $h >21 and $h <=23){
         $sc='亥时'; 
        }
        
        if( $h >23){
         $sc='子时'; 
        }
         
        if($m>0 and $m<15){
            $ke='一刻';
        }
 
 
        if($m>=15 and $m<30){
            $ke='两刻';
        }    

      if($m>=30 ){
            $ke='三刻';
        }    
        
        return array( 'sc'=>$sc,'ke'=>$ke); 

    }

    public function qualified_game($user_all_end_games,$userid){
        $games = array();
        $this->load->database();
        foreach($user_all_end_games as $key => $one_game){
            $gameid = intval($one_game['gameid']);
            $sql = "select min(gross) as gross_min,count(holeid) as hole_num from t_game_score where gameid=$gameid and userid=$userid";
            $one_game_info = $this->db->query($sql)->row_array();
            if($one_game_info['gross_min'] > 0 && $one_game_info['hole_num'] == 18){
                $games[] = $gameid;
             }
        }
        return $games;
     }

     public function get_year_footprint_date($current_year,$register_year,$userid){

        $year_data = array();
         
        for ($i=0; $i <=($current_year - $register_year ); $i++) 
        {
            $year=$register_year +$i;
             

            $year_data[$year] = $this->year_footprint($userid,$year);

            $year_data[$year]['text'] = 'AAA';
            
            $handicap = abs($year_data[$year]['year_handicap']);
            
            $year_data[$year]['handicap_str'] = "本年度差点为".$year_data[$year]['year_handicap'];
            $year_data[$year]['handicap_change'] = '';


            if(isset($year_data[$year-1]))
            {
                $pre_year_handicap = abs($year_data[$year-1]['year_handicap']);
                $handicap_difference = $handicap -  $pre_year_handicap;
                if($handicap_difference < 0){
                    $year_data[$year]['handicap_change'] = "较去年降低了".abs($handicap_difference)."点";
                }

                if($handicap_difference > 0){
                    $year_data[$year]['handicap_change'] = "较去年增加了".$handicap_difference."点";
                }

                if($handicap_difference == 0){
                    $year_data[$year]['handicap_change'] = "与去年持平";
                }
            }

           
        }
        return array_reverse($year_data);
     }

      public function year_footprint($userid,$year){
        $this->load->library('XMath');
        $mgr_math  = $this->xmath;
        $game_status = GAME_ENDED;
        $this->load->database();
        $sql = "select gameid from t_game where pre_starttime between '".$year."-01-01 00:00:00' and '".$year."-12-31 23:59:59'  and gameid in (select gameid from t_game_group_user where userid=$userid) and gamestate=$game_status";
        $one_year_games = $this->db->query($sql)->result_array();
        
        if(count($one_year_games) < 1){
            return array('year'=>$year,'game_num'=>0,'year_handicap'=>0,'course_num'=>0,'player_num'=>0,'pkerlist'=>0);
        }
        
        $games = $this->qualified_game($one_year_games,$userid);
        $one_year_game_num = count($games);
        $one_year_game_str = implode(',',$games);
        if(strlen($one_year_game_str) < 1){
            $one_year_game_str = -1;
        }
        $year_game_data = $this->get_course_player_num($one_year_game_str,$userid);
        $year_game_data['year'] =$year;

        $year_game_data['game_num'] = $one_year_game_num;
        $year_game_data['year_handicap'] = $this->one_year_game_handicap($one_year_game_str,$userid);

        return $year_game_data;
     }

      public function get_course_player_num($games_str,$userid){
        $this->load->database();
        $sql = "select distinct courseid from t_game where gameid in ($games_str)";
        $courses = $this->db->query($sql)->result_array();
        $course_num = count($courses);
        $sql = "select id,nickname from t_user where id in (select distinct userid from t_game_group_user where gameid in ($games_str)) and type<>2 and id<>$userid";
        $play_game_users = $this->db->query($sql)->result_array();
        $player_num = count($play_game_users);
        return array('course_num'=>$course_num,'player_num'=>$player_num,'pkerlist'=>$play_game_users);
     }


     // public function get_course_player_num($games_str,$userid){
     //    $this->load->database();
     //    $sql = "select distinct courseid from t_game where gameid in ($games_str)";
     //    $courses = $this->db->query($sql)->result_array();
     //    $course_num = count($courses);
     //    $sql = "select id from t_user where id in (select distinct userid from t_game_group_user where gameid in ($games_str)) and type<>2 and id<>$userid";
     //    $play_game_users = $this->db->query($sql)->result_array();
     //    $player_num = count($play_game_users);
     //    return array('course_num'=>$course_num,'player_num'=>$player_num);
     // }


     public function one_year_game_handicap($game_str,$userid)
    {   
        $this->load->database();
        
        $sql = "select t_game_score.gameid,sum(t_game_score.gross) as grossnum,min(t_game_score.gross) as grossmin,sum(t_game_score.par) as sumpar,count(t_game_score.par) as holenum,t_game.courseid,t_game.starttime,t_course.name ";
        $sql .= " from t_game_score,t_game,t_course ";
        $sql .= " where t_game_score.gameid in ($game_str) and t_game_score.userid=$userid and t_game_score.score_status=1 and t_game.gameid in (t_game_score.gameid) ";
        $sql .= " and t_game.gamestate=4 and t_course.courseid in (t_game.courseid) group by t_game_score.gameid order by t_game.starttime desc";
        


        $user_game_info = $this->db->query($sql)->result_array();


        $sumgross       = 0;
        $sumpar         = 0;
        $gameidnum      = 0;
        foreach ($user_game_info as $k => $v) {
            $formal = 0;
            $gameid = $v['gameid'];
            if ($v['grossnum'] != 0) {
                if ($v['grossmin'] > 0) {
                    if ($v['holenum'] > 9) {
                        $sumgross = $sumgross + $v['grossnum'];
                        $sumpar   = $sumpar + $v['sumpar'];
                        $gameidnum++;
                    }
                }
            }
        }
        if ($sumgross == 0 && $sumpar == 0 && $gameidnum == 0) {
            $handicap = -6;
        } else {
            $handicap = ($sumgross - $sumpar) / $gameidnum;
            $handicap = round($handicap, 1);
        }
        
        return $handicap;
    }


    public function secondsToTime($seconds) {
    $dtF = new DateTime("@0");
    $dtT = new DateTime("@$seconds");
    return array(
    $dtF->diff($dtT)->format('%a'),
    $dtF->diff($dtT)->format('%h小时%i分钟%s秒')
            );
    }
    

    public function user_footprint($userid){
        header("Content-type: text/html; charset=utf-8");
        
        $game_status = GAME_ENDED;
        $this->load->database();
        $sql = "select addtime from t_user where id=$userid";
        $user_create_time = $this->db->query($sql)->row_array();

        $sql = "select gameid from t_game where gamestate=$game_status and gameid in (select gameid from t_game_group_user where userid=$userid)";
        
        $user_all_end_games = $this->db->query($sql)->result_array();
        $games = $this->qualified_game($user_all_end_games,$userid);
        
        $all_end_game_num = count($games);
        $all_end_game_str = implode(',',$games);
        if(strlen($all_end_game_str) < 1){
            $all_end_game_str = -1;
        }

        $all_game_data = $this->get_course_player_num($all_end_game_str,$userid);

        $new_time = time();

        $survive_days = round(($new_time-$user_create_time['addtime'])/3600/24);

        $xx=$this->secondsToTime( time() - $user_create_time['addtime']);
         
       
        $register_year = date('Y',$user_create_time['addtime']);
        
        $current_year = date('Y');
        
        $year_data = $this->get_year_footprint_date($current_year,$register_year,$userid);

        $today=  date('Y年m月d日', time());
        
        return (array('today'=>$today,
            'live_days'=>$xx[0],
            'live_hms'=>$xx[1],
            'games'=>$all_end_game_num,
            'course_num'=>$all_game_data['course_num'],
            'pknum'=>$all_game_data['player_num'],
            'pkerlist'=>$all_game_data['pkerlist'],
            'years'=>$year_data,
            'joind_y'=>date('Y年',$user_create_time['addtime']),
            'joind_md'=>date('m月d日',$user_create_time['addtime']),
            'joind_h'=>date('h',$user_create_time['addtime']),
            'joind_m'=>date('m',$user_create_time['addtime'])
            )
        
        );
     }



     /************网页---球友圈动态详情*******************/

     public function post_info_share(){
        $get     = $this->input->get();
        if(isset($get['p'])){

            include_once('funcs.php');
            $value = decodeRandString($get['p']);

            $value_array = explode('&', $value);

            $id_array = explode('=', $value_array[0]);
            $id = $id_array[1];
        }else{
            $id = $get['id'];
        }
        
        $sys_path = "'".ATTACH_HOST."/'";
        $file_path = ATTACH_HOST."/";
        $this->load->database();

        $sql = "select * from t_post where id=$id";

        $post_info = $this->db->query($sql)->row_array();

        if(!isset($post_info['id'])){

            $this->load->view("client/post_info.html",array('error_code'=>-1,'error_descr'=>"抱歉！作者已经将此动态删除"));

        }else{
            if(intval($post_info['attachcount']) > 0){
                $this->db->select('savepath,savename,objecttype,filetype');
                $this->db->from("t_attach");
                $this->db->where('objectid',$id);
                $attachs = $this->db->get()->result_array();
                $attachs_array = array();
                foreach ($attachs as $key => $one_attach) {
                    if($one_attach['objecttype'] == "post_pic"){
                        $post_picurl = $file_path.$one_attach['savepath'].'/c640_'.$one_attach['savename'];
                        $attachs_array['post_picurl'][] = $post_picurl;
                    }else{
                        $thumbname_path = $file_path . $post_info['coverpath'] . '/' . $post_info['covername'];
                        $post_video = $file_path.$one_attach['savepath'].'/'.$one_attach['savename'];
                        $attachs_array['post_video'][] = array('path'=>$post_video,'filetype'=>$one_attach['filetype'],'thumbname_path'=>$thumbname_path);
                    }
                }
            } 
            $userid = intval($post_info['userid']);
            $sql = "select nickname,concat($sys_path,coverpath,'/c240_',covername) as user_picurl from t_user where id=$userid";
        
            $user_info = $this->db->query($sql)->row_array();

            $sql = "select * from t_comment where objectid=$id order by addtime desc";
            $comments = $this->db->query($sql)->result_array();
            foreach ($comments as $key => $one_user) {
                $comment_userid = $one_user['userid'];
                $sql = "select nickname,concat($sys_path,coverpath,'/c240_',covername) as user_picurl from t_user where id=$comment_userid";
                $info = $this->db->query($sql)->row_array();
                $comments[$key]['nickname'] = $info['nickname'];
                $comments[$key]['user_picurl'] = $info['user_picurl'];
            }

            $sql = "select count(id) as like_num from t_like where objectid=$id";
            $like_num = $this->db->query($sql)->row_array();
            

            $this->load->view("client/post_info.html",array('like_num'=>$like_num['like_num'],'post_info'=>$post_info,'user_info'=>$user_info,'comments'=>$comments,'pic_array'=>$attachs_array));


        }
        
    }



    /**********网页---积分页面***************/

    public function usercredit()
    {
        
        $get = $this->input->get();

        $userid    = $get['userid'];
        $this->load->library('XGame');
        $mgr_game = $this->xgame;
        $userinfo = $mgr_game->user_info($userid);
        
        $data = array(
            'credit' => $userinfo['credit'],
            'picurl' => $userinfo['user_picurl']
        );

        $this->load->view('client/credit.php', $data);
    }


    /***********网页---经验值页面***********************/


    public function userexperience()
    {
        
        $get = $this->input->get();

        $this->load->library('XGame');
        $mgr_game = $this->xgame;
        
        $userid   = $get['userid'];
        
        //   $userid=210;
        $userinfo = $mgr_game->user_info($userid);
        $data     = array(
            'rank' => $userinfo['rank'],
            'experience' => $userinfo['experience'],
            'picurl' => $userinfo['user_picurl']
        );
       

        $this->load->view('client/experience.php', $data);
    }

    /*****************网页---游戏相关的页面及操作********************************/


    /*****************网页---游戏规则说明********************************/

    public function gamble_explain(){
        $this->load->view("client/explain.php");
    }

    /******************网页----云端球场************************************************/

    private function check_course_data($courseid){
        $this->db->select("courtid");
        $this->db->from("t_course_court");
        $this->db->where("courseid",$courseid);
        $courts = $this->db->get()->result_array();
        foreach ($courts as $key => $one_court) {
            $courtid = intval($one_court['courtid']);
            $this->db->from("t_course_hole_score_data");
            $this->db->where("courtid",$courtid);
            if($this->db->count_all_results() == 0){
                $this->add_blank_course_data($courtid);
            }
        }
    }

    private function add_blank_course_data($courtid){
       $data = $this->get_course_data($courtid,"courtid");
       foreach ($data as $key => $one_hole) {
          $this->db->insert("t_course_hole_score_data",$one_hole);
       }
    }


    private function get_course_data($id,$field_name){
        $this->db->select("courseid,courtid,holeid,holename,par");
        $this->db->from("t_court_hole");
        $this->db->where($field_name,$id);
        $data = $this->db->get()->result_array();
        return $data;
    }


    private function hole_classify_by_court($courts,$holes){
        foreach ($courts as $key => $one_court) {
            $courtid = intval($one_court['courtid']);
            foreach ($holes as $one_hole) {
                $hole_courtid = intval($one_hole['courtid']);
                if($hole_courtid == $courtid){
                    $courts[$key]['holes'][] = $one_hole;
                }
            }
        }
        return $courts;
    }


    private function judge_hole_nature($holes){

        $hole_num = count($holes);
        $stars = array("one_star"=>"1","two_star"=>"2","three_star"=>"2","four_star"=>"2","five_star"=>"1");
        foreach ($stars as $key => $one_star) {
            if($key == "one_star"){
                $hole_one_star = ($hole_num*$one_star)/6;
            }elseif($key == "two_star"){
                $hole_two_star = ($hole_num*$one_star)/9;
            }elseif($key == "three_star"){
                $hole_three_star = ($hole_num*$one_star)/9;
            }elseif($key == "four_star"){
                $hole_four_star = ($hole_num*$one_star)/9;
            }elseif($key == "five_star"){
                $hole_five_star = ($hole_num*$one_star)/6;
            }
        }
        
        $hole_one_star_array = array_splice($holes,0,$hole_one_star);
        $hole_one_star_array = $this->hole_add_grades($hole_one_star_array,1);
        usort($hole_one_star_array, array($this,'bird_hole'));
        $hole_one_star_array[0]['is_devil_bird'] = 2;
    
        $hole_two_star_array = array_splice($holes,0,$hole_two_star);
        $hole_two_star_array = $this->hole_add_grades($hole_two_star_array,2);

        $hole_three_star_array = array_splice($holes,0,$hole_three_star);
        $hole_three_star_array = $this->hole_add_grades($hole_three_star_array,3);

        $hole_four_star_array = array_splice($holes,0,$hole_four_star);
        $hole_four_star_array = $this->hole_add_grades($hole_four_star_array,4);

        $hole_five_star_array = array_splice($holes,0,$hole_five_star);
        $hole_five_star_array = $this->hole_add_grades($hole_five_star_array,5);
        usort($hole_five_star_array, array($this,'devil_hole'));
        $hole_five_star_array[0]['is_devil_bird'] = 1;
        
        $new_holes = array_merge($hole_one_star_array,$hole_two_star_array,$hole_three_star_array,$hole_four_star_array,$hole_five_star_array);
        usort($new_holes, array($this,'hole_id_sort'));
        return $new_holes;
    }


    private function hole_add_grades($holes,$star){
        foreach ($holes as $key => $one_hole) {
            if($one_hole['sum_gross'] == 0){
                $holes[$key]['grades'] = 0;
            }else{
                $holes[$key]['grades'] = $star;
            }
            
        }
        return $holes;
    }



    //洞的平均杆数排序
    private function hole_average_gross_sort($a,$b)
    {
        
        if($a['hole_average_gross'] == $b['hole_average_gross']){
            $result = 0;
        }else{
            $result = $a['hole_average_gross'] < $b['hole_average_gross'] ? -1 : 1;
        }    
        return $result;
    }

    //洞的ID排序
    private function hole_id_sort($a,$b)
    {
        
        if($a['holeid'] == $b['holeid']){
            $result = 0;
        }else{
            $result = $a['holeid'] < $b['holeid'] ? -1 : 1;
        }    
        return $result;
    }


    


    


    

    private function get_user_info($userid){
        $sys_path = SERVER_PIC_DIR;
        // $file_path = ATTACH_HOST."/";
        $file_path = "http://t1.golf-brother.com/data/attach/";
        $this->db->select("coverpath,covername,nickname");
        $this->db->from("t_user");
        $this->db->where("id",$userid);
        $user_info = $this->db->get()->row_array();
        $user_info['user_picurl'] = $file_path.$user_info['coverpath']."/c240_".$user_info['covername'];
        
        return $user_info;
    }


    

    



    

    private function get_course_game_num($courseid,$userid){
        $course_all_game_num = $this->get_all_game_by_course($courseid);
        $user_all_game_by_course_num = $this->get_user_all_game_by_course($courseid,$userid);
        return array("course_all_game_num"=>$course_all_game_num,"user_all_game_by_course_num"=>$user_all_game_by_course_num);
    }

    private function get_all_game_by_course($courseid){
        $this->db->select("gameid");
        $this->db->from("t_game");
        $this->db->where("courseid",$courseid);
        $this->db->where("gamestate",4);
        $games = $this->db->get()->result_array();
        foreach ($games as $key => $one_game) {
            $gameid = intval($one_game['gameid']);
            if(!$this->check_game($gameid)){
                unset($games[$key]);
            }
        }
        return count($games);
    }


    private function get_user_all_game_by_course($courseid,$userid){
        $sql = "select g.gameid from t_game as g,t_game_group_user as ggu where ggu.userid=$userid and g.gameid=ggu.gameid and g.gamestate=4 and g.courseid=$courseid ";
        $games = $this->db->query($sql)->result_array();
        foreach ($games as $key => $one_game) {
            $gameid = intval($one_game['gameid']);
            if(!$this->check_game($gameid,$userid)){
                unset($games[$key]);
            }
        }
        return count($games);
    }

    private function check_game($gameid,$userid = -1){
        $this->db->from("t_game_court");
        $this->db->where("gameid",$gameid);
        if($this->db->count_all_results() < 2){
            return false;
        }
        $check_stute = false;
        if($userid > 0){
            $this->db->select("min(gross) as min_gross");
            $this->db->from("t_game_score");
            $this->db->where("gameid",$gameid);
            $this->db->where("userid",$userid);
            $user_game_min_gross = $this->db->get()->row_array();
            if(intval($user_game_min_gross['min_gross']) > 0){
                $check_stute = true;
            }
        }else{
            $this->db->select("userid");
            $this->db->from("t_game_group_user");
            $this->db->where("gameid",$gameid);
            $users = $this->db->get()->result_array();
            foreach ($users as $key => $one_user) {
                $playerid = intval($one_user['userid']);
                $this->db->select("min(gross) as min_gross");
                $this->db->from("t_game_score");
                $this->db->where("gameid",$gameid);
                $this->db->where("userid",$playerid);
                $user_game_min_gross = $this->db->get()->row_array();
                if(intval($user_game_min_gross['min_gross']) > 0){
                    $check_stute = true;
                    break;
                }
            }
        }
        
        return $check_stute;
    }

    private function get_hole_grades($courseid,$holeid){
        $this->load->library('XCloud');
        $mgr_cloud = $this->xcloud;
        $grades = 0;
        $this->db->select("id,courseid,courtid,holeid,sum_gross,play_num,holename,par");
        $this->db->from("t_course_hole_score_data");
        $this->db->where("courseid",$courseid);
        $result = $this->db->get()->result_array();
        foreach ($result as $key => $one_hole) {
            $hole_average_gross = $mgr_cloud->get_one_hole_average_gross($one_hole);
            $result[$key]['hole_average_gross'] = $hole_average_gross;
        }
        usort($result, array($this,'hole_average_gross_sort'));
        $holes = $this->judge_hole_nature($result);
        foreach ($holes as $key => $one_hole) {
            if($one_hole['holeid'] == $holeid){
                $grades = intval($one_hole['grades']);
                break;
            }
        }
        return $grades;
    }
    

    public function cloud_hole_info(){
        $this->load->library('session');
        $this->session;
        $this->load->library('XCloud');
        $mgr_cloud = $this->xcloud;
        $courseid = intval($_GET['courseid']);
        $holeid = intval($_GET['holeid']);
        $userid = intval($_GET['userid']);
        $grades = isset($_GET['grades']) ? intval($_GET['grades']) : -1;
        $hole_data = $mgr_cloud->get_hole_score_data($courseid,$holeid,$userid);
        $field_name = "addtime";
        $this->session->set_userdata('order_by',"new");
        $is_show_code = $this->session->userdata('is_show_code');
        $course_comments = $this->get_course_comments($courseid,$field_name,$userid,"hole",$holeid);
        $course_comments_num = count($course_comments);
        if($grades < 1){
            $grades = $this->get_hole_grades($courseid,$holeid);
        }
        $data = array(
                        "is_show_code"=>$is_show_code,
                        "courseid"=>$courseid,
                        "userid"=>$userid,
                        "holeid"=>$holeid,
                        "course_name"=>$hole_data['course_name']['name'],
                        "hole_info"=>$hole_data['hole_info'],
                        "grades"=>$grades,
                        "hole_average_gross"=>$hole_data['hole_average_gross'],
                        "user_average_gross"=>$hole_data['user_average_gross'],
                        "course_comments"=>$course_comments,
                        "course_comments_num"=>$course_comments_num
                    );

        $this->load->view("client/cloud_hole_test",$data);

    }



    public function cloud_course(){
        $this->load->library('session');
        $this->session;
        $this->load->library('XCloud');
        $mgr_cloud = $this->xcloud;
        $must_have = array(
            'courseid'
        );
        $para      = check_args($must_have);
        $courseid            = intval($para['courseid']);
        $userid            = intval($para['userid']);
        $from = isset($para['source']) ? trim($para['source']) : "app";
        if($from == "share"){
            $is_show_code = 1;
        }else{
            $is_show_code = 0;
        }
        
        $this->load->database();
        $this->check_course_data($courseid);
        $result = $mgr_cloud->get_cloud_course_data($courseid,$userid);
        $course_name = $result['course_name'];
        $course_pic = $result['course_pic'];
        $course_data = $result['course_score_data'];
        usort($course_data, array($this,'hole_average_gross_sort'));
        $holes = $this->judge_hole_nature($course_data);
        $this->db->select("courtid,courtname");
        $this->db->from("t_course_court");
        $this->db->where("courseid",$courseid);
        $courts = $this->db->get()->result_array();
        $courts = $this->hole_classify_by_court($courts,$holes);
        $field_name = "addtime";
        $this->session->set_userdata('order_by',"new");
        $this->session->set_userdata('is_show_code',$is_show_code);
        $course_comments = $this->get_course_comments($courseid,$field_name,$userid);
        $course_comments_num = count($course_comments);
        $all_game_num_by_course = $this->get_course_game_num($courseid,$userid);
        $data = array("is_show_code"=>$is_show_code,"course_pic"=>$course_pic,"all_game_num_by_course"=>$all_game_num_by_course,"course_comments_num"=>$course_comments_num,"courseid"=>$courseid,"userid"=>$userid,"courts"=>$courts,"course_name"=>$course_name,"course_comments"=>$course_comments);
        $this->load->view("client/cloud_course_test",$data);
    }







    private function bird_hole($a,$b)
    {
        
        $configures = array('hole_average_gross','hole_comment_num',"red","white","blue","gold","black");
        foreach ($configures as $key => $one_configure) {
            if($one_configure == 'hole_average_gross' || $one_configure == 'hole_comment_num'){
                if($a[$one_configure] == $b[$one_configure]){
                     $result = 0;
                }else{
                    $result = $a[$one_configure] < $b[$one_configure] ? -1 : 1;
                    break;
                }
            }else{
                if($a[$one_configure] == $b[$one_configure]){
                    $result = 0;
                }else{
                    $result = $a[$one_configure] < $b[$one_configure] ? -1 : 1;
                    break;
                }
            }
            
        }
        return $result;
    }



    private function devil_hole($a,$b)
    {
        
        $configures = array('hole_average_gross','hole_comment_num',"red","white","blue","gold","black");
        foreach ($configures as $key => $one_configure) {
            if($one_configure == 'hole_average_gross' || $one_configure == 'hole_comment_num'){
                if($a[$one_configure] == $b[$one_configure]){
                     $result = 0;
                }else{
                    $result = $a[$one_configure] > $b[$one_configure] ? -1 : 1;
                    break;
                }
            }else{
                if($a[$one_configure] == $b[$one_configure]){
                    $result = 0;
                }else{
                    $result = $a[$one_configure] > $b[$one_configure] ? -1 : 1;
                    break;
                }
            }
            
        }
        return $result;
    }

/*********云端球场评论回复及点赞********************/
    private function get_course_comments($courseid,$field_name,$operator,$where = "course",$holeid = -1){
        $this->db->select("*");
        $this->db->from("t_course_comment");
        $this->db->where("courseid",$courseid);
        if($where == "hole"){
            $this->db->where("objectid",$holeid);
            $this->db->where("objecttype","hole");
        }else{
            $this->db->where("objecttype <>","user");
        }
        
        $this->db->order_by($field_name, 'DESC');
        $result = $this->db->get()->result_array();
        foreach ($result as $key => $one_comment) {
            $time = $one_comment['addtime'];
            $result[$key]['addtime'] = $this->conversion_time($time);
            $comment_num = intval($one_comment['comment_num']);
            $objecttype = trim($one_comment['objecttype']);
            $objectid = intval($one_comment['objectid']);
            if($objecttype == "hole"){
                $this->db->select("holename");
                $this->db->from("t_course_hole_score_data");
                $this->db->where("courseid",$courseid);
                $this->db->where("holeid",$objectid);
                $holename = $this->db->get()->row_array();
                $result[$key]['holename'] = trim($holename['holename']);
            }
            $id = intval($one_comment['id']);
            $result[$key]['like_status'] = $this->get_user_is_like($id,$operator);
            $pid = intval($one_comment['pid']);
            
            $userid = intval($one_comment['userid']);
            $result[$key]['user_info'] = $this->get_user_info($userid);

            if($comment_num > 0){
                $result[$key]["comments"] = $this->get_comment_by_course_comment($id,$pid,$objectid,$courseid,$userid);
            }

        }
        
        return $result;
    }


    public function course_comment_list(){
        $this->load->library('session');
        $this->session;
        $courseid = intval($_POST['courseid']);
        $userid = intval($_POST['userid']);
        $order_by = trim($_POST['order_by']);
        $holeid = isset($_POST['holeid']) ? intval($_POST['holeid']) : -1;
        $where_field_name = "course";
        if($holeid > 0){
            $where_field_name = "hole";
        }
        if($order_by == "new"){
            $field_name = "addtime";
        }elseif($order_by == "heat"){
            $field_name = "like_num";
        }
        $this->session->set_userdata('order_by',$order_by);
        $course_comments = $this->get_course_comments($courseid,$field_name,$userid,$where_field_name,$holeid);
        $course_comments_num = count($course_comments);
        $html_str = "<hr id='comments_hr' />";
       foreach ($course_comments as $key => $one_comment) {
         $user_picurl = $one_comment['user_info']['user_picurl'];
         $nickname = $one_comment['user_info']['nickname'];
         $time = $one_comment['addtime'];
         $id = $one_comment['id'];
         $like_status = $one_comment['like_status'];
            if($like_status == 0){
                $class_name = 'no_like_back';
            }else{
                $class_name = 'yes_like_back';
            }
            $like_num = $one_comment['like_num'];
            $note = $one_comment['note'];
            $course_parameter = 'onclick="reply('.$one_comment['id'].",".$one_comment['userid'].",'".$one_comment['user_info']['nickname']."'".')"';

            $html_str .= "<div class='one_commenter' id='$id'>";
            $html_str .= "<img style='border-radius:50%;' align='left' src='$user_picurl' />";
            $html_str .= "<div class='text_class' style='text-align:left;position:relative;'>";
            $html_str .= "<div $course_parameter class='user_nickname'><font style='font-size:26px;' color='#333'>$nickname</font></div>";
            $html_str .= "<div $course_parameter class='time'><font style='font-size:24px;' color='#c9c9c9'>$time</font></div>";
            $html_str .= "<div onclick='like(this,$id,$like_status)' class='like_position'>";
            $html_str .= "<span class='$class_name'>$like_num<span></div></div>";
            $html_str .= "<div class='comm_info' style='margin-bottom:30px;' ><font style='font-size:30px;' color='#333'>";

            if($where_field_name == "course"){
                if($one_comment['objecttype'] == "hole"){
                    $holeid = intval($one_comment['objectid']);
                    $html_str .= "<a onclick='jump_hole($holeid)'>#".$one_comment['holename']."#</a>";
                }
            }
            $html_str .= $note."</font></div>";
            if(count($one_comment['comments']) > 0){
                $html_str .= "<div class='comm_reply_list'>";
            }
            
            foreach ($one_comment['comments'] as $row) {
                $parameter = 'onclick="reply('.$one_comment['id'].",".$row['userid'].",'".$row['user_info']['nickname']."'".')"';
                $row_user_picurl = $row['user_info']['user_picurl'];
                $row_user_nickname = $row['user_info']['nickname'];
                $row_addtime = $row['addtime'];
                $row_note = $row['note'];
                $html_str .=  "<div class='one_reply'>";
                $html_str .=  "<img style='border-radius:50%;' align='left' src='$row_user_picurl' />";
                $html_str .=  "<div class='text_class' style='text-align:left;'>";
                $html_str .=  "<div $parameter class='user_nickname'><font style='font-size:26px;' color='#333'>$row_user_nickname</font></div>";
                $html_str .=  "<div $parameter class='time'><font style='font-size:24px;' color='#c9c9c9'>$row_addtime</font></div></div>";
                $html_str .=  "<div class='comm_info'><font style='font-size:30px;' color='#999'>回复 ".$row['pid_info']['nickname']."：</font><font style='font-size:30px;' color='#333'>$row_note</font></div></div>";   
            }
            if(count($one_comment['comments']) > 0){
                $html_str .= "</div>";
            }

            $html_str .= "</div><hr id='comments_hr' style='background-color:#e5e5e5;height:1px;border:none;margin-top: 0px;' />"; 
        }
        $ret['error_code']  = 1;
        $ret['error_descr'] = 'success';
        $ret['html_str'] = $html_str;
        $this->output->json_output($ret);
    }



    public function add_reply(){
        $this->load->library('XCloud');
        $mgr_cloud = $this->xcloud;
        $courseid = intval($_POST['courseid']);
        $userid = intval($_POST['userid']);
        $reply_userid = intval($_POST['reply_userid']);
        $commentid = intval($_POST['commentid']);
        $content = trim($_POST['content']);
        $ip = $_SERVER["REMOTE_ADDR"];
        $time = time();
        $data = array("pid"=>$reply_userid,"courseid"=>$courseid,"userid"=>$userid,"note"=>$content,"objectid"=>$commentid,"objecttype"=>"user","addip"=>$ip,"comment_num"=>0,"addtime"=>$time);
        $id = $mgr_cloud->cloud_course_reply($data,$commentid);
        if($id > 0){
            $news_data = array("pid"=>$commentid,"objectid"=>$reply_userid,'note'=>$content,"objecttype"=>"reply","courseid"=>$courseid,"userid"=>$userid,"addtime"=>time());
            $this->load->database();
            $this->db->insert("t_cloud_course_news",$news_data);
        }else{
            $ret['error_code']  = -1;
            $ret['error_descr'] = '回复失败！';
            $this->output->json_output($ret);
            return;
        }   

        $reply_object_info = $this->get_user_info($reply_userid);
        $reply_object_nickname = $reply_object_info['nickname'];
        $user_info = $this->get_user_info($userid);
        $user_picurl = $user_info['user_picurl'];
        $nickname = $user_info['nickname'];
        $date_time = "刚刚";
        $parameter = 'onclick="reply('.$id.",".$userid.",'".$nickname."'".')"';
        $html_str = "";
        $html_str .= "<div class='one_reply'>";
        $html_str .= "<img style='border-radius:50%;' align='left' src='$user_picurl' />";
        $html_str .= "<div class='text_class' style='text-align:left;'>";
        $html_str .= "<div $parameter class='user_nickname'><font style='font-size:13px;' color='#333'>$nickname</font></div>";
        $html_str .= "<div $parameter class='time'><font style='font-size:12px;' color='#c9c9c9'>$date_time</font></div>";
        $html_str .= "</div>";
        $html_str .= "<div class='comm_info' ><font style='font-size:15px;' color='#999'>回复 ".$reply_object_nickname."：</font><font style='font-size:15px;' color='#333'>".$content."</font></div></div>";
        $ret['error_code']  = 1;
        $ret['error_descr'] = 'success';
        $ret['html_str'] = $html_str;
        $ret['commentid'] = $commentid;
        $this->output->json_output($ret);
    }



    public function course_comments(){
        $this->load->library('session');
        $this->session;
        $courseid = intval($_POST['courseid']);
        $userid = intval($_POST['userid']);
        $content = trim($_POST['content']);
        $holeid = isset($_POST['holeid']) ? intval($_POST['holeid']) : -1;
        $ip = $_SERVER["REMOTE_ADDR"];
        $time = time();
        $data = array("pid"=>0,"courseid"=>$courseid,"userid"=>$userid,"note"=>$content,"objectid"=>$courseid,"objecttype"=>"course","addip"=>$ip,"comment_num"=>0,"addtime"=>$time);
        if($holeid > 0){
            $data['objectid'] = $holeid;
            $data['objecttype'] = "hole";
        }
        $this->load->database();
        $this->db->insert("t_course_comment",$data);
        $id = $this->db->insert_id();

        $user_info = $this->get_user_info($userid);
        $user_picurl = $user_info['user_picurl'];
        $nickname = $user_info['nickname'];
        $date_time = $this->conversion_time($time);
        $order_by = $this->session->userdata('order_by');
        $html_str = "";
        if($order_by == "new"){
            $html_str = "<hr id='comments_hr' style='display:none' />";
        }

        if($data['objecttype'] == "hole"){
            $this->db->select("holename");
            $this->db->from("t_court_hole");
            $this->db->where("courseid",$courseid);
            $this->db->where("holeid",$holeid);
            $hole_name = $this->db->get()->row_array();

            $a_html_str = "";//"<a onclick='jump_hole($holeid)'>#".$hole_name['holename']."#</a>";
        }else{
            $a_html_str = "";
        }

        $course_parameter = 'onclick="reply('.$id.",".$userid.",'".$nickname."'".')"';
        
        $html_str .= "<div class='one_commenter' id='$id'>";
        $html_str .= "<img style='border-radius:50%;' align='left' src='$user_picurl' />";
        $html_str .= "<div class='text_class' style='text-align:left;position:relative;'>";
        $html_str .= "<div $course_parameter class='user_nickname'><font style='font-size:13px;' color='#333'>$nickname</font></div>";
        $html_str .= "<div $course_parameter class='time'><font style='font-size:12px;' color='#c9c9c9'>$date_time</font></div>";
        $html_str .= "<div onclick='like(this,$id,0)' class='like_position'>";
        $html_str .= "<span class='no_like_back'>0<span></div></div>";
        $html_str .= "<div class='comm_info' style='margin-bottom:15px;'><font style='font-size:15px;' color='#333'>".$a_html_str ." ".$content."</font></div></div>";
        // if($order_by == "heat"){
            $html_str .= "<hr id='comments_hr' style='background-color:#e5e5e5;height:1px;border:none;margin-top: 0px;' />";
        // }
        $ret['error_code']  = 1;
        $ret['error_descr'] = 'success';
        $ret['html_str'] = $html_str;
        $ret['order_by'] = $order_by;
        $ret['id'] = $id;
        $this->output->json_output($ret);
    }


    private function get_comment_by_course_comment($id,$pid,$objectid,$courseid,$userid){
        $where = array("objectid"=>$id,"courseid"=>$courseid,"objecttype"=>"user");
        $this->db->select("*");
        $this->db->from("t_course_comment");
        $this->db->where($where);
        $this->db->order_by("addtime", 'DESC');
        $result = $this->db->get()->result_array();
        foreach ($result as $key => $one_comment) {
            $time = $one_comment['addtime'];
            $result[$key]['addtime'] = $this->conversion_time($time);
            $userid = intval($one_comment['userid']);
            $pid = intval($one_comment['pid']);
            $result[$key]['pid_info'] = $this->get_user_info($pid);
            $result[$key]['user_info'] = $this->get_user_info($userid);
        }
        return $result;
    }



    private function get_user_is_like($id,$userid){
        $this->db->where("objectid",$id);
        $this->db->where("userid",$userid);
        $this->db->from("t_course_like");
        $like_status = $this->db->count_all_results();
        return $like_status;
    }



    public function course_comment_like(){
        $this->load->library('XCloud');
        $mgr_cloud = $this->xcloud;
        $course_comment_id = intval($_POST['course_comment_id']);
        $userid = intval($_POST['userid']);
        $operation = $mgr_cloud->change_comment_like_status($course_comment_id,$userid);
        $ret['error_code']  = 1;
        $ret['error_descr'] = 'success';
        $ret['operation'] = $operation;
        $this->output->json_output($ret);
    }


    public function conversion_time($time){
       //获取今天凌晨的时间戳
       $day = strtotime(date('Y-m-d',time()));
       //获取昨天凌晨的时间戳
       $pday = strtotime(date('Y-m-d',strtotime('-1 day')));
       //获取现在的时间戳
       $nowtime = time();
        
       $tc = $nowtime-$time;
       if($time<$pday){
          $str = date('Y-m-d H:i:s',$time);
       }elseif($time<$day && $time>$pday){
          $str = "昨天";
       }elseif($tc>60*60){
          $str = floor($tc/(60*60))."小时前";
       }elseif($tc>60){
          $str = floor($tc/60)."分钟前";
       }else{
          $str = "刚刚";
       }
       return $str;
    }









    public function cloud_hole_info_test(){
        $this->load->library('session');
        $this->session;
        $this->load->library('XCloud');
        $mgr_cloud = $this->xcloud;
        $courseid = intval($_GET['courseid']);
        $holeid = intval($_GET['holeid']);
        $userid = intval($_GET['userid']);
        $grades = isset($_GET['grades']) ? intval($_GET['grades']) : -1;
        $hole_data = $mgr_cloud->get_hole_score_data($courseid,$holeid,$userid);
        $field_name = "addtime";
        $this->session->set_userdata('order_by',"new");
        $is_show_code = $this->session->userdata('is_show_code');
        $course_comments = $this->get_course_comments($courseid,$field_name,$userid,"hole",$holeid);
        $course_comments_num = count($course_comments);
        if($grades < 1){
            $grades = $this->get_hole_grades($courseid,$holeid);
        }
        $data = array(
                        "is_show_code"=>$is_show_code,
                        "courseid"=>$courseid,
                        "userid"=>$userid,
                        "holeid"=>$holeid,
                        "course_name"=>$hole_data['course_name']['name'],
                        "hole_info"=>$hole_data['hole_info'],
                        "grades"=>$grades,
                        "hole_average_gross"=>$hole_data['hole_average_gross'],
                        "user_average_gross"=>$hole_data['user_average_gross'],
                        "course_comments"=>$course_comments,
                        "course_comments_num"=>$course_comments_num
                    );

        $this->load->view("client/cloud_hole_test",$data);

    }



    public function cloud_course_test(){
        $this->load->library('session');
        $this->session;
        $this->load->library('XCloud');
        $mgr_cloud = $this->xcloud;
        $must_have = array(
            'courseid'
        );
        $para      = check_args($must_have);
        $courseid            = intval($para['courseid']);
        $userid            = intval($para['userid']);
        $from = isset($para['from']) ? trim($para['from']) : "app";
        if($from == "share"){
            $is_show_code = 1;
        }else{
            $is_show_code = 0;
        }
        
        $this->load->database();
        $this->check_course_data($courseid);
        $result = $mgr_cloud->get_cloud_course_data($courseid,$userid);
        $course_name = $result['course_name'];
        $course_pic = $result['course_pic'];
        $course_data = $result['course_score_data'];
        usort($course_data, array($this,'hole_average_gross_sort'));
        $holes = $this->judge_hole_nature($course_data);
        $this->db->select("courtid,courtname");
        $this->db->from("t_course_court");
        $this->db->where("courseid",$courseid);
        $courts = $this->db->get()->result_array();
        $courts = $this->hole_classify_by_court($courts,$holes);
        $field_name = "addtime";
        $this->session->set_userdata('order_by',"new");
        $this->session->set_userdata('is_show_code',$is_show_code);
        $course_comments = $this->get_course_comments($courseid,$field_name,$userid);
        $course_comments_num = count($course_comments);
        $all_game_num_by_course = $this->get_course_game_num($courseid,$userid);
        $data = array("is_show_code"=>$is_show_code,"course_pic"=>$course_pic,"all_game_num_by_course"=>$all_game_num_by_course,"course_comments_num"=>$course_comments_num,"courseid"=>$courseid,"userid"=>$userid,"courts"=>$courts,"course_name"=>$course_name,"course_comments"=>$course_comments);
        $this->load->view("client/cloud_course_test",$data);
    }


    /******************莱德杯 4人4球-分队展示页***************************/

    public function the_ryder_cup_squad_score(){
        $must_have = array(
            'gameid'
        );
        $para      = check_args($must_have);
        
        $gameid            = intval($para['gameid']);
        $userid            = intval($para['agent_userid']);
        $is_show_code            = $para['isShowCode'];
        $ret['error_code'] = -1;
        $game_score = $this->get_game_score(array(
            'gameid' => $gameid,
            'userid' => $userid
        ));
        $squad_str = $this->lookup_game_squad($gameid);
        $squads_str = array_to_string($squad_str);
        $game_name = $game_score['gameinfo']['name'];
        $pre_starttime = $game_score['gameinfo']['pre_starttime'];
        $course_name = $game_score['gameinfo']['course_name'];

        $parameter        = $this->game_score_card_html_parameter($game_score['gameinfo'], $game_score['team_info']);
       
        $game_user_scores = $game_score['score_complex'];
        $game_data = $this->get_game_squad_data($gameid,$squads_str,$game_user_scores);
        $game_squads = $game_data['game_squads'];
        $game_group_score = $game_data['game_group_score'];

        $game_group_score = $this->data_integration($game_squads,$game_group_score,$gameid);
      
        $data = array(
                      'game_group_score'=>$game_group_score,
                      'time' => date('Y年m月d日',strtotime($game_score['gameinfo']['starttime'])),
                      'team_game_name' => $game_name,
                      'coursename' => $parameter['coursename'],
                      );

        $this->load->view('client/fourF_statistics_html',$data);
    }


    public function get_game_squad_data($gameid,$squads_str,$game_user_scores){
        $this->load->database();
        $sql = "select id,teamid,squad_name from t_game_squad where gameid=$gameid and id in ($squads_str) order by FIND_IN_SET(id,'$squads_str')";

        $game_squads = $this->db->query($sql)->result_array();
        $index = 0;
        $sys_path = SERVER_PIC_DIR;
        foreach ($game_squads as $key => $one_squad) {
            if(isset($one_squad['teamid'])){
                $teamid = intval($one_squad['teamid']);
                $sql = "select concat($sys_path,coverpath,'/',covername) as team_picurl from t_team ";
                $sql .= " where teamid=$teamid ";
                $team_picurl = $this->db->query($sql)->row_array();
                $game_squads[$key]['team_picurl'] = $team_picurl['team_picurl'];
            }else{
                $game_squads[$key]['team_picurl'] = '';
            }
            if($index == 0){
                $game_squads[$key]['color'] = 'red';
                $game_squads[$key]['score'] = 0;
                $index++;
            }else{
                $game_squads[$key]['color'] = 'blue';
                $game_squads[$key]['score'] = 0;
                $index = 0;
            }
        }


        
        $sql = "select groupid from t_game_group where gameid=$gameid ";
        $groups = $this->db->query($sql)->result_array();

        foreach ($groups as $key => $one_group) {
            $groupid = $one_group['groupid'];
            $array[] = $this->get_group_squad($gameid,$groupid);
        }

        $game_group_score = array();
        foreach ($array as $key => $one_group) {

            foreach ($one_group as $one_user) {
                $game_group_score[$key][] = $this->get_one_group_score($one_user,$game_user_scores);
            }
        }

        return $this->get_squad_score_and_squads($game_squads,$gameid,$game_group_score);
        
    }


    public function get_squad_score_and_squads($game_squads,$gameid,$game_group_score){
        foreach ($game_group_score as $key => $one_group) {
            $info = $this->get_game_group_score_outcome($one_group,$game_squads,$gameid);
            $game_group_score[$key] = $info['group_score'];
            $game_group_score[$key]['group_outcome_result'] = $info['group_outcome_result'];

            $hole_key = $this->get_current_hole($game_group_score[$key][0]['score'],$game_group_score[$key][1]['score']);

            if(strlen($info['group_outcome_result'][$hole_key][0]) < 2){
                if($hole_key != 0){
                    $hole_array = array($hole_key-1,true);
                    $hole_key = $hole_key-1;
                }else{
                    $hole_array = array($hole_key,false);
                }
            }else{
                $hole_array = array($hole_key,true);
            }
            
            $game_group_score[$key]['group_outcome_result_one_hole'] = $info['group_outcome_result'][$hole_key];
            $game_group_score[$key]['hole_index'] = $hole_array;

            $game_squads = $info['game_teams'];
        }
        return array('game_squads'=>$game_squads,'game_group_score'=>$game_group_score);
    }




    public function get_current_hole($a_group_score,$b_group_score){
            $index = 0;
            foreach ($a_group_score as $key => $one_hole_score) {
                if($b_group_score[$key] == 0 || $one_hole_score == 0){
                    return $key;
                }else{
                    $index++;
                }
            }
            return 17;
    }


    public function data_integration($game_squads,$game_group_score,$gameid){
        
        $this->load->database();
        $sql = "select groupid from t_game_group where gameid=$gameid ";
        $game_groupids = $this->db->query($sql)->result_array();
        $squads = array();
        $str = "";
        $game_squads_array = array();
        foreach ($game_groupids as $key => $one_group) {
            $squads = array();
            $groupid = intval($one_group['groupid']);
            $sql = "select squadid from t_game_squad_user as gsu,t_game_group_user as ggu where ggu.gameid=$gameid and gsu.gameid=$gameid and ";
            $sql .= " ggu.groupid=$groupid and gsu.userid=ggu.userid ";
            $squadids = $this->db->query($sql)->result_array();
            $squads_str = array_to_string($squadids,'squadid');
            
            $squads_array = explode(',', $squads_str);
            $squads_array = array_unique($squads_array);
            $one_squad = $squads_array[0];
            $all_squad = explode(',', $str);
            if(in_array($one_squad, $all_squad)){
                continue;
            }
            $str .= $squads_str;
            foreach ($game_squads as $one_squad) {
                $squadid = intval($one_squad['id']);
                if(in_array($squadid, $squads_array)){
                    $squads[] = $one_squad;
                }
            }
            $game_squads_array[]['squads'] = $squads;
        }
        
        foreach ($game_group_score as $row) {
            unset($row['group_outcome_result']);
            unset($row[0]['score']);
            unset($row[1]['score']);
            $userid = $row[0]['users'][0];
            $sql = "select squadid from t_game_squad_user where gameid=$gameid and userid=$userid";
            $user_squad = $this->db->query($sql)->row_array();

            $squadid = intval($user_squad['squadid']);

            foreach ($game_squads_array as $key => $value) {
                foreach ($value['squads'] as $one_squad) {
                    if($squadid == $one_squad['id']){
                        $game_squads_array[$key]['user_score_data'][] = $this->group_user_order($row,$value,$gameid);
                        break;
                    }
                }
            }
        }
        return $game_squads_array;
        
    }


    public function lookup_game_squad($gameid){
        $this->load->database();
        $sql = "select groupid from t_game_group where gameid=$gameid ";
        $game_groups = $this->db->query($sql)->result_array();

        foreach ($game_groups as $key => $one_group) {
            $groupid = intval($one_group['groupid']);
            $sql = "select userid from t_game_group_user where gameid=$gameid and groupid=$groupid";
            $one_group_users = $this->db->query($sql)->result_array();
            $game_groups[$key]['users'] = $one_group_users;
        }
        $squads = array();
        foreach ($game_groups as $key => $one_group) {
            foreach ($one_group['users'] as $one_user) {
                $userid = intval($one_user['userid']);
                $sql = "select squadid from t_game_squad_user where gameid=$gameid and userid=$userid ";
                $squad = $this->db->query($sql)->row_array();
                if(!in_array($squad['squadid'],$squads)){
                    $squads[] = $squad['squadid'];
                }
            }
        }
        return $squads;
    }


    public function get_game_group_score_outcome($group_score,$game_teams,$gameid){
        $a_group_score = $group_score[0]['score'];
        $b_group_score = $group_score[1]['score'];
        $a_group_users = $group_score[0]['users'];
        $b_group_users = $group_score[1]['users'];
        $a_userid = $a_group_users[0];
        $b_userid = $b_group_users[0];
        $this->load->database();
        $sql = "select squadid from t_game_squad_user where userid=$a_userid and gameid=$gameid ";

        $a_teamid = $this->db->query($sql)->row_array();

        $sql = "select squadid from t_game_squad_user where userid=$b_userid and gameid=$gameid ";
        $b_teamid = $this->db->query($sql)->row_array();

        foreach ($game_teams as $key => $one_team) {
            if(intval($one_team['id']) == $a_teamid['squadid']){
               $group_score[0]['color'] = $one_team['color'];
               $a_color = $one_team['color'];
            }elseif(intval($one_team['id']) == $b_teamid['squadid']){
               $group_score[1]['color'] = $one_team['color'];
               $b_color = $one_team['color'];
            }
        }

        $outcome_result = $this->math_one_group_outcome_result($a_group_score,$b_group_score,$a_color,$b_color);
        $a_group_score = $outcome_result['a_group_score'];
        $b_group_score = $outcome_result['b_group_score'];
        $b_score = $outcome_result['b_score'];
        $a_score = $outcome_result['a_score'];
        $group_outcome_result = $outcome_result['group_outcome_result'];

        $a_score_sum = array_sum($a_group_score);
        $b_score_sum = array_sum($b_group_score);
        $sql = "select group_start_status from t_game_group as gg,t_game_group_user as ggu where ggu.gameid=$gameid and ggu.userid=$a_userid and gg.groupid=ggu.groupid ";
        $group_start_status = $this->db->query($sql)->row_array();
        if($group_start_status['group_start_status'] == 4){
            $game_teams = $this->math_team_score($a_score,$b_score,$a_score_sum,$b_score_sum,$a_teamid,$b_teamid,$game_teams);
        }
        
        return array('group_score'=>$group_score,'group_outcome_result'=>$group_outcome_result,"game_teams"=>$game_teams);
    }



    public function math_one_group_outcome_result($a_group_score,$b_group_score,$a_color,$b_color){
        $group_outcome_result = array();
        $a_rank = 0;
        $b_rank = 0;
        $a_score = 0;
        $b_score = 0;
        foreach ($a_group_score as $key => $one_score) {

            if($one_score == $b_group_score[$key]){
                if(($one_score == 0) && ($b_group_score[$key] == 0)){
                    $group_outcome_result[] = array(' ');
                }else{
                    $result = $b_score-$a_score;
                    if($result < 0){
                        $result = $a_score-$b_score;
                        $group_outcome_result[] = array($result.'UP',$a_color);
                    }elseif($result == 0){
                        $group_outcome_result[] = array('A/S');
                    }else{
                        $group_outcome_result[] = array($result.'UP',$b_color);
                    }
                }
            }elseif($one_score > $b_group_score[$key]){
                if($b_group_score[$key] == 0){
                    $group_outcome_result[] = array(' ');
                }else{
                    $b_score = $b_score + 1;
                    $result = $b_score-$a_score;
                    if($result < 0){
                        $result = $a_score-$b_score;
                        $group_outcome_result[] = array($result.'UP',$a_color);
                    }elseif($result == 0){
                        $group_outcome_result[] = array('A/S');
                    }else{
                        $group_outcome_result[] = array($result.'UP',$b_color);
                    }
                }
            }else{
                if($one_score == 0){
                    $group_outcome_result[] = array(' ');
                }else{

                    $a_score = $a_score + 1;
                    $result = $a_score-$b_score;
                    if($result < 0){
                        $result = $b_score-$a_score;
                        $group_outcome_result[] = array($result.'UP',$b_color);
                    }elseif($result == 0){
                        $group_outcome_result[] = array('A/S');
                    }else{
                        $group_outcome_result[] = array($result.'UP',$a_color);
                    }
                }
            }

        }
        return array(
                     'group_outcome_result'=>$group_outcome_result,
                     'a_group_score'=>$a_group_score,
                     'b_group_score'=>$b_group_score,
                     'a_score'=>$a_score,
                     'b_score'=>$b_score
                     );
    }



    public function math_team_score($a_score,$b_score,$a_score_sum,$b_score_sum,$a_teamid,$b_teamid,$game_teams){

            foreach ($game_teams as $key => $one_team) {
                if(intval($one_team['id']) == $a_teamid['squadid']){
                    $a_team_score = $one_team['score'];
                }elseif(intval($one_team['id']) == $b_teamid['squadid']){
                    $b_team_score = $one_team['score'];
                }
            }
            if($a_score > $b_score){
                $a_team_score = $a_team_score+1;
            }elseif($a_score == $b_score){
                if($a_score == 0 || $b_score == 0){
                    if($a_score_sum != 0 && $b_score_sum != 0){
                        $a_team_score = $a_team_score+0.5;
                        $b_team_score = $b_team_score+0.5;
                    }
                }else{
                    $a_team_score = $a_team_score+0.5;
                    $b_team_score = $b_team_score+0.5;
                }
            }else{
                $b_team_score = $b_team_score+1;
            }
            
            foreach ($game_teams as $key => $one_team) {
                if(intval($one_team['id']) == $a_teamid['squadid']){
                    unset($game_teams[$key]['score']);
                    $game_teams[$key]['score'] = $a_team_score;
                    $this->update_squad_score($one_team['id'],$a_team_score);
                }elseif(intval($one_team['id']) == $b_teamid['squadid']){
                    unset($game_teams[$key]['score']);
                    $game_teams[$key]['score'] = $b_team_score;
                    $this->update_squad_score($one_team['id'],$b_team_score);
                }
            }
            return $game_teams;
    }


    public function update_squad_score($squadid,$score){
        $this->load->database();
        $this->db->where('id',$squadid);
        $this->db->update('t_game_squad',array('squad_score'=>$score));
        return;
    }




    public function group_user_order($group_score,$squads,$gameid){

        $a_group_user = $group_score[0]['users'][0];
        $this->load->database();
        $sql = "select squadid from t_game_squad_user where gameid=$gameid and userid=$a_group_user";
        $user_squad = $this->db->query($sql)->row_array();
        $a_group_squad = intval($user_squad['squadid']);

        if($a_group_squad != $squads['squads'][0]['id']){
            $value = $group_score[0];
            $group_score[0] = $group_score[1];
            $group_score[1] = $value;
        }

        return $group_score;
    }




    public function get_one_group_score($users,$game_user_scores){
        
        $user_socre = array();
        unset($game_user_scores[0]);
        foreach ($users as $key => $one_user) {
            $player_score = array();
            foreach ($game_user_scores as $one_user_score) {
                if($one_user_score['p1']['userid'] == $one_user){
                    $player_score[] = $one_user_score['p2']['gross'];
                    $player_score[] = $one_user_score['p3']['gross'];
                    $player_score[] = $one_user_score['p4']['gross'];
                    $player_score[] = $one_user_score['p5']['gross'];
                    $player_score[] = $one_user_score['p6']['gross'];
                    $player_score[] = $one_user_score['p7']['gross'];
                    $player_score[] = $one_user_score['p8']['gross'];
                    $player_score[] = $one_user_score['p9']['gross'];
                    $player_score[] = $one_user_score['p10']['gross'];
                    if(count($one_user_score) == 20){
                        $player_score[] = $one_user_score['p11']['gross'];
                        $player_score[] = $one_user_score['p12']['gross'];
                        $player_score[] = $one_user_score['p13']['gross'];
                        $player_score[] = $one_user_score['p14']['gross'];
                        $player_score[] = $one_user_score['p15']['gross'];
                        $player_score[] = $one_user_score['p16']['gross'];
                        $player_score[] = $one_user_score['p17']['gross'];
                        $player_score[] = $one_user_score['p18']['gross'];
                        $player_score[] = $one_user_score['p19']['gross'];
                    }
                    $user_socre[] = array('userid'=>$one_user,'nickname'=>$one_user_score['p1']['nickname'],'score'=>$player_score);
                    break;
                }
                
            } 

        }

        

        return $this->get_one_group_user_score($user_socre);

        
    }


    public function get_one_group_user_score($user_socre){
        $one_group_user_score = array();

        foreach ($user_socre as $key => $one_user) {
            $one_group_user_score['nickname'] = array($one_user['nickname'],$user_socre[1]['nickname']);
            $one_group_user_score['users'] = array($one_user['userid'],$user_socre[1]['userid']);
            $score = $one_user['score'];
            foreach ($score as $num => $one_score) {
                if($one_score != 0 ){
                    if($user_socre[1]['score'][$num] != 0){
                        if($one_score > $user_socre[1]['score'][$num]){
                            $one_group_user_score['score'][] = $user_socre[1]['score'][$num];
                        }else{
                            $one_group_user_score['score'][] = $one_score;
                        }
                    }else{
                        $one_group_user_score['score'][] = $one_score;
                    }
                }else{
                    if($user_socre[1]['score'][$num] != 0){
                       $one_group_user_score['score'][] = $user_socre[1]['score'][$num];
                    }else{
                        $one_group_user_score['score'][] = 0;
                    }
                }
            }
            break;
        }

        return $one_group_user_score;
    }



    public function get_group_squad($gameid,$groupid){
        $this->load->database();
        $sql = "select userid from t_game_group_user where gameid=$gameid and groupid=$groupid ";
        $group_users = $this->db->query($sql)->result_array();
        $squad_group = array();
        foreach ($group_users as $key => $one_user) {
            $userid = intval($one_user['userid']);
            $sql = "select squadid from t_game_squad_user where gameid=$gameid and userid=$userid ";
            $game_squadid = $this->db->query($sql)->row_array();
            if($key == 0){
                $squadid = intval($game_squadid['squadid']);
                $squad_group[0][] = $userid;
            }else{
                if(intval($game_squadid['squadid']) == $squadid){
                    $squad_group[0][] = $userid;
                }else{
                    $squad_group[1][] = $userid;
                }
            }
        }
        return $squad_group;
    }

/*************4人4球 结束*********************/

/********************4人4球-比杆 开始***************************/
public function the_ryder_cup_squad_pk_gross_rank(){
    $must_have = array(
        'gameid'
    );
    $para      = check_args($must_have);
    
    $gameid            = intval($para['gameid']);
    $userid            = intval($para['agent_userid']);
    $is_show_code            = $para['isShowCode'];
    $game_score = $this->get_game_score(array(
        'gameid' => $gameid,
        'userid' => $userid
    ));
    $squad_str = $this->lookup_game_squad($gameid);
    $squads_str = array_to_string($squad_str);
    $game_name = $game_score['gameinfo']['name'];
    $pre_starttime = $game_score['gameinfo']['pre_starttime'];
    $course_name = $game_score['gameinfo']['course_name'];

    $parameter        = $this->game_score_card_html_parameter($game_score['gameinfo'], $game_score['team_info']);
   
    $game_user_scores = $game_score['score_complex'];
    $game_data = $this->get_game_squad_data($gameid,$squads_str,$game_user_scores);
    $game_squads = $game_data['game_squads'];
    $game_group_score = $game_data['game_group_score'];

    $squad_score_info = $this->get_game_squad_socre($game_group_score,$gameid);

    
    $data = array(
                  'squad_score_info'=>$squad_score_info,
                  'time' => date('Y年m月d日',strtotime($game_score['gameinfo']['starttime'])),
                  'team_game_name' => $game_name,
                  'coursename' => $parameter['coursename'],
                  );

    $this->load->view('client/fourF_statistics_stroke_html',$data);
}


public function get_game_squad_socre($game_group_score,$gameid){
    $this->load->library('XTeam');
    $mgr_team = $this->xteam;
    $sys_path = SERVER_PIC_DIR_SPELL;
    $this->load->database();
    $sql = "select id,teamid,squad_name from t_game_squad where gameid=$gameid ";
    $game_squad_info = $this->db->query($sql)->result_array();
    $sql = "select top_rank_player_num,gamestate from t_game where gameid=$gameid ";
    $game_info = $this->db->query($sql)->row_array();
    foreach ($game_squad_info as $key => $one_squad) {
        if(isset($one_squad['teamid']) && intval($one_squad['teamid']) > 0){
            $team_info = $mgr_team->get_team_info(intval($one_squad['teamid']));
            $game_squad_info[$key]['squad_name'] = trim($team_info['team_name']);
            $game_squad_info[$key]['team_picurl'] = $sys_path.$team_info['coverpath'].'/'.$team_info['covername'];
        }else{
            $game_squad_info[$key]['team_picurl'] = '';
        }
    }

    $game_squad_info = $this->get_squad_user_score($game_group_score,$game_squad_info,$gameid,$game_info);

    foreach ($game_squad_info as $key => $one_squad) {
        usort($one_squad['user_score'], array($this,'squad_user_sum_gross_sort'));
        $squad_score = 0;
        $squad_sum_par = 0;
        $index = 1;
        foreach ($one_squad['user_score'] as  $one_user) {
            $squad_score = $squad_score+$one_user['sum_gross'];
            $squad_sum_par = $squad_sum_par+$one_user['sum_par'];
            if($game_info['gamestate'] == 4){
                if($index > intval($game_info['top_rank_player_num'])){
                    break;
                }
            }
            $index++;
        }
        unset($game_squad_info[$key]['user_score']);
        $game_squad_info[$key]['squad_score'] = $squad_score;
        $game_squad_info[$key]['squad_gancha'] = $squad_score-$squad_sum_par;
    }
    usort($game_squad_info, array($this,'squad_gancha_sort'));
    $game_squad_info = $this->get_game_squad_paiming($game_squad_info);
    return $game_squad_info;
}


public function get_game_squad_paiming($game_squad_info){
    $index = 1;
    $identical_score = 0;
    $rank = 1;
    foreach ($game_squad_info as $key => $one_squad) {
        if($key == 0){
            $game_squad_info[$key]['rank'] = $rank;
            $identical_score = $one_squad['squad_gancha'];
        }else{
            if($one_squad['squad_gancha'] == $identical_score){
                $game_squad_info[$key]['rank'] = $rank;
            }else{
                $rank = $index;
                $game_squad_info[$key]['rank'] = $rank;
                $identical_score = $one_squad['squad_gancha'];
            }
        }
        $index++;
    }
    return $game_squad_info;
}



public function get_squad_user_score($game_group_score,$game_squad_info,$gameid,$game_info){
    $this->load->database();
    foreach ($game_group_score as $row) {
        $a_sump_gross = array_sum($row[0]['score']);
        $b_sump_gross = array_sum($row[1]['score']);
        $row[0]['sum_gross'] = $a_sump_gross;
        $row[1]['sum_gross'] = $b_sump_gross;
        $a_is_complete = 1;
        $b_is_complete = 1;
        if(in_array(0, $row[0]['score'])){
            $a_is_complete = 0;
        }

        if(in_array(0, $row[1]['score'])){
            $b_is_complete = 0;
        }
        unset($row[0]['score']);
        unset($row[1]['score']);
        $a_users = $row[0]['users'];
        $b_users = $row[1]['users'];
        $a_userid = $a_users[0];
        $b_userid = $b_users[0];

        $sql = "select sum(par) as sum_par from t_game_score where  gameid=$gameid and userid=$a_userid and gross<>0";
        $a_user_sum_par = $this->db->query($sql)->row_array();

        $sql = "select sum(par) as sum_par from t_game_score where  gameid=$gameid and userid=$b_userid and gross<>0";
        $b_user_sum_par = $this->db->query($sql)->row_array();

        $row[0]['sum_par'] = intval($a_user_sum_par['sum_par']);
        $row[1]['sum_par'] = intval($b_user_sum_par['sum_par']);

        $sql = "select squadid from t_game_squad_user where gameid=$gameid and userid=$a_userid ";
        $a_squadid = $this->db->query($sql)->row_array();
        $sql = "select squadid from t_game_squad_user where gameid=$gameid and userid=$b_userid ";
        $b_squadid = $this->db->query($sql)->row_array();
        foreach ($game_squad_info as $key => $one_squad) {
            if($one_squad['id'] == $a_squadid['squadid']){
                if($game_info['gamestate'] == 4){
                    if($a_is_complete == 1){
                        $game_squad_info[$key]['user_score'][] = $row[0];
                    }
                }else{
                    $game_squad_info[$key]['user_score'][] = $row[0];
                }
                
            }elseif($one_squad['id'] == $b_squadid['squadid']){
                if($game_info['gamestate'] == 4){
                    if($b_is_complete == 1){
                        $game_squad_info[$key]['user_score'][] = $row[1];
                    }
                }else{
                    $game_squad_info[$key]['user_score'][] = $row[1];
                }
            }
        }
    }
    return $game_squad_info;
}



private function squad_user_sum_gross_sort($a,$b)
{
    
    if($a['sum_gross'] == $b['sum_gross']){
        $result = 0;
    }else{
        $result = $a['sum_gross'] < $b['sum_gross'] ? -1 : 1;
    }    
    return $result;
}


private function squad_gancha_sort($a,$b)
{
    
    if($a['squad_gancha'] == $b['squad_gancha']){
        $result = 0;
    }else{
        $result = $a['squad_gancha'] < $b['squad_gancha'] ? -1 : 1;
    }    
    return $result;
}

/********************4人4球-比杆 结束***************************/

/*****************4人4球 分队排行榜－－－开始***************************/
    public function the_ryder_cup_squad_rank(){
        $must_have = array(
            'gameid'
        );
        $para      = check_args($must_have);
        
        $gameid            = intval($para['gameid']);
        $userid            = intval($para['agent_userid']);
        $is_show_code            = $para['isShowCode'];
        $game_score = $this->get_game_score(array(
            'gameid' => $gameid,
            'userid' => $userid
        ));
        $game_name = $game_score['gameinfo']['name'];
        $pre_starttime = $game_score['gameinfo']['pre_starttime'];
        $course_name = $game_score['gameinfo']['course_name'];
        $parameter        = $this->game_score_card_html_parameter($game_score['gameinfo'], $game_score['team_info']);
        $game_squad_rank = $this->get_squad_rank($gameid);
        $data = array(
                      'game_squad_rank'=>$game_squad_rank,
                      'time' => date('Y年m月d日',strtotime($game_score['gameinfo']['starttime'])),
                      'team_game_name' => $game_name,
                      'coursename' => $parameter['coursename'],
                      );

        $this->load->view('client/fourF_statistics_squad_rank',$data);

    }

    public function get_squad_rank($gameid){
        $this->load->library('XTeam');
        $mgr_team = $this->xteam;
        $sys_path = SERVER_PIC_DIR_SPELL;
        $this->load->database();
        $sql = "select teamid,squad_name,squad_score from t_game_squad where gameid=$gameid order by squad_score desc";
        $game_squad_info = $this->db->query($sql)->result_array();
        foreach ($game_squad_info as $key => $one_squad) {
            if(isset($one_squad['teamid']) && intval($one_squad['teamid']) > 0){
                $team_info = $mgr_team->get_team_info(intval($one_squad['teamid']));
                $game_squad_info[$key]['squad_name'] = trim($team_info['team_name']);
                $game_squad_info[$key]['team_picurl'] = $sys_path.$team_info['coverpath'].'/'.$team_info['covername'];
            }else{
                $game_squad_info[$key]['team_picurl'] = '';
            }
        }

        usort($game_squad_info, array($this,'squad_sort'));
        return $this->add_score_rank($game_squad_info);
    }


    public function add_score_rank($game_squad_info){
        $index = 1;
        $rank = 1;
        $identical_score = 0;
        foreach ($game_squad_info as $key => $one_squad) {

            if(intval($one_squad['squad_score']) != $identical_score){
                $identical_score = intval($one_squad['squad_score']);
                $rank = $index;
                $game_squad_info[$key]['rank'] = $index;
            }else{
                if($key == 0){
                    $game_squad_info[$key]['rank'] = 1;
                }else{
                    $game_squad_info[$key]['rank'] = $rank;
                }
            }
            $index++;
        }
        return $game_squad_info;
    }






    //分队成绩的排序
    private function squad_sort($a,$b)
    {
        
        if($a['squad_score'] == $b['squad_score']){
            $result = 0;
        }else{
            $result = $a['squad_score'] > $b['squad_score'] ? -1 : 1;
        }    
        return $result;
    }


/********************4人4球 分队排行榜－－－结束************************/


    public function show_html(){
        $this->load->view('client/show_game_time_list');
    }





    public function xxxxxxxx(){
        $gameid = 92527;
        $dom = new DOMDocument();
        $url = "http://www.beijingopen.net/Live/view/id/43.html";
        $content = file_get_contents($url);
        $patten="/<a href=[\'\"]?([^\'\" ]+).*?>/";
        preg_match_all($patten,$content,$matches);
        $users_url = $matches[1];
        array_shift($users_url);
        array_pop($users_url);
        $user_score = array();
        foreach ($users_url as $key => $one_user) {
            $url_array = explode('/', $one_user);
            if(in_array('tid', $url_array)){
               continue;
            }
            $one_user_url = "http://www.beijingopen.net".$one_user;

            $dom->loadHTMLFile($one_user_url);

            $xml = simplexml_import_dom($dom);
            $title = $xml->head->title;
            dump($title);die;
            $user_name_div = $xml->body->div[0]->div[2]->div[1]->div[0];//->span[0];

            $user_name_div = get_object_vars($user_name_div);
            $user_name_str = $user_name_div['span'];
            $user_name_array = (explode('：', $user_name_str));
            $user_name = $user_name_array[1];
            $score_top = $xml->body->div[0]->div[3]->table->tr[2];
            $score_xia = $xml->body->div[0]->div[3]->table->tr[6];
            $score_top = get_object_vars($score_top);
            $score_xia = get_object_vars($score_xia);
            $user_score[$key]['name'] = $user_name;

            foreach ($score_top['td'] as $num_top => $one_hole) {
                $one_hole = is_object($one_hole) ? get_object_vars($one_hole) : $one_hole;
                if($num_top != 0 && $num_top < 10){
                    $user_score[$key]['user_score'][] = $one_hole['div'];
                }
            }
            foreach ($score_xia['td'] as $num_xia => $one_hole) {
                $one_hole = is_object($one_hole) ? get_object_vars($one_hole) : $one_hole;
                if($num_xia != 0 && $num_xia < 10){
                    $user_score[$key]['user_score'][] = $one_hole['div'];
                }
            }
        }
        
        $user_score = $this->handle_users_score($user_score,$gameid);
        dump($user_score);die;
        // $this->update_user_score($user_score,$gameid);
    }



    



    public function handle_users_score($user_score,$gameid){
        foreach ($user_score as $key => $one_user) {
            $user_name = trim($one_user['name']);
            // $userid = $this->get_user_id($user_name,$gameid);
            // $user_score[$key]['userid'] = $userid;
            foreach ($one_user['user_score'] as $num => $one_hole) {
                if(is_object($one_hole)){
                    $user_score[$key]['user_score'][$num] = 0;
                }
            }
        }
        return $user_score;
    }



    public function update_user_score($user_score,$gameid){
        foreach ($user_score as $key => $one_user) {
            $userid = intval($one_user['userid']);
            if($userid < 1){
                continue;
            }
            $sql = "select * from t_game_score where gameid=$gameid and userid=$userid ";
            $user_game_score = $this->db->query($sql)->result_array();
            foreach ($user_game_score as $num => $one_game_hole) {
                $id = intval($one_game_hole['id']);
                $gross = intval($one_game_hole['gross']);
                foreach ($one_user['user_score'] as $index => $one_hole) {
                    if($index == $num){
                        if($one_hole == $gross){
                            break;
                        }
                        // $this->db->where('id',$id);
                        // $this->db->update('t_game_score',array('gross'=>$one_hole));
                        break;
                    }
                }
            }
        }
    }





    public function get_user_id($name,$gameid){
        $this->load->database();
        $sql = "select userid from t_game_applyer where gameid=$gameid and username like '%$name%'";
        $user_id = $this->db->query($sql)->row_array();
        if(isset($user_id['userid'])){
            $userid = intval($user_id['userid']);
        }else{
            $userid = 0;
        }
        
        
        if($userid == 0){
            $sql = "select userid from t_user_info where userid in (select userid from t_game_group_user where gameid=$gameid) and realname like '%$name%'";
            $user_id = $this->db->query($sql)->row_array();
            if(isset($user_id['userid'])){
                $userid = intval($user_id['userid']);
            }else{
                $userid = 0;
            }
        }

        if($userid == 0){
            $sql = "select id from t_user where id in (select userid from t_game_group_user where gameid=$gameid) and nickname like '%$name%'";
            $user_id = $this->db->query($sql)->row_array();
            if(isset($user_id['id'])){
                $userid = intval($user_id['id']);
            }else{
                $userid = 0;
            }
        }
        return $userid;
    }


}
