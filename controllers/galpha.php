<?php

/*
赌球算法控制器.
去除了接口函数.
*/

include_once 'base.php';
include_once 'gamefuncs.php';
header("Access-Control-Allow-Origin: *");



error_reporting(E_STRICT);

$old_error_handler = set_error_handler("terminate_missing_variables"); 




class Galpha extends Base
{
    private $kpi_types; //所有的项目类型.
    private $group_type; //所有分组类型.
    
    
    public function __construct()
    {
        
        parent::__construct();
        $this->load->library('AGamblereb');
        $this->xgamble = $this->agamblereb;
        $this->load->library('AGambledata');
        $this->xgambledatafactory = $this->agambledata;
        $this->load->library('XLogger');
        $this->xlog = $this->xlogger;
        
        
        $this->kpi_types = array(
            'kpi_best', //最好成绩
            'kpi_best_ak', //kpi_best变种,比A比K, 如果双方最好成绩打平,则比较第二名.
            'kpi_worst', //最差成绩
            'kpi_total_add', //加法总和
            'kpi_total_plus', //乘法总和
            'kpi_case_8421', //8421
            'kpi_gross' //2人比杆
        );
        
        //div_option   
        $this->div_option = array(
            '2_fixed', //2人,自然分边
            '3_random', //3人乱拉 1vs2 ,第1名为单独一组
            '3_fixed', //3人固拉 1vs2 ,第2名为单独一组
            '3_skin', //3人skin,各自为战
            '4_random', //4人乱拉 2vs2,(1,4)vs(2,3)
            '4_fixed', //4人固拉 2vs2.固定分组.
            '4_skin', //4人skin,各自为战
            '4_3vs1', //4人,固定3打1
            '4_ab'
        ); //4人ab分组. 2vs2(a组两人永远不在同一边)
    }
    
    public function server_type()
    {
        
        $server_type = $_SERVER['SERVER_ADDR'] == '123.57.223.35' ? 'run' : 'test';
        return $server_type;
    }
    
    
    public function get_one_gambleresult($gambleid)
    {
        
        $gamble_obj            = $this->getGambleObj($gambleid);
        $gamble_cfg            = $gamble_obj;
        $gamble_cfg['kpi_cfg'] = explode(',', $gamble_obj['kpi_cfg']);
        $gameid                = $gamble_obj['gameid'];
        $groupid               = $gamble_obj['groupid'];
        
        $firstholeindex = $gamble_obj['firstholeindex'];
        $lastholeindex  = $gamble_obj['lastholeindex'];
        
        $tmpval=$gamble_cfg['draw_option'];
        unset($gamble_cfg['draw_option']);
        $gamble_cfg['draw_option']=$tmpval;  //什么情况算顶洞.
        
       
        
        $gamble_cfg['kpi_num']       = count($gamble_cfg['kpi_cfg']);
        $gamble_cfg['ak_k_use_list'] = array();
        $gamble_cfg['guo']=null;

        //设置用到的球洞
        $gamble_cfg['gamedata'] = $this->xgambledatafactory->get_used_holedata($gameid, $groupid, $firstholeindex, $lastholeindex, $gamble_obj['initorder']);

        $this->xlog->getInstance()->logGambleConfig($gamble_cfg);
        $one_gambleresult = $this->xgamble->compute_one($gamble_cfg);
        return $one_gambleresult;
    }
    
    
    public function getgroupusers($gameid, $groupid, $userid)
    {
        
        $ret = array();
        $this->load->library('XGame');
        $mgr_game = $this->xgame;
        
        $gamedetail = $mgr_game->game_detail($gameid, $userid, 'all');
        $gameinfo   = $gamedetail['game_info'];
        $group_info = $gamedetail['group_info'];
        
        $foundkey = -1;
        foreach ($group_info as $key => $one_group) {
            if ($one_group['groupid'] == $groupid) {
                $foundkey = $key;
            }
        }
        
        $found_group_users = $group_info[$foundkey]['group_user'];
        foreach ($found_group_users as $key => $one_user) {
            $found_group_users[$key]['cover'] = $one_user['user_picurl'];
            unset($found_group_users[$key]['user_picurl']);
            unset($found_group_users[$key]['confirmed']);
            unset($found_group_users[$key]['confirmed_time']);
            unset($found_group_users[$key]['admin']);
        }
        return $found_group_users;
    }
    
    
    
    //给APP的接口,显示报表页.
    
    public function alpha_summary()
    {
        
        header("Access-Control-Allow-Origin: *");
        header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
        
        $userid  = $_GET['userid'];
        $groupid = $_GET['groupid'];

        
        $group_gamble_info = $this->xgambledatafactory->get_gamble_by_group($groupid);
        
        $gameid      = $group_gamble_info['gameid'];
        $gambleids   = $group_gamble_info['gambleids'];
        $gamble_list = explode(',', $gambleids);
        
        $gambles_summary = $this->get_gambles_summary($gamble_list);
        $this->showsummary($userid, $gambleids, $gameid, $groupid, $gambles_summary);
    }
    
    public function alphago()
    {
        
        header("Access-Control-Allow-Origin: *");
        header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
        
        // $userid  = 999;
        // $groupid = 246113;

        
        $group_gamble_info = $this->xgambledatafactory->get_gamble_by_group($groupid);
        
        $gameid      = $group_gamble_info['gameid'];
        $gambleids   = $group_gamble_info['gambleids'];
        $gamble_list = explode(',', $gambleids);
        
        $gambles_summary = $this->get_gambles_summary($gamble_list);
        $this->showsummary($userid, $gambleids, $gameid, $groupid, $gambles_summary);
    }

    
    //显示app汇总视图
    public function showsummary($userid, $gambleid, $gameid, $groupid, $gambles_summary)
    {

        $detail_url = 'http://' . $_SERVER['SERVER_NAME'] . '/web/galpha/alpha_detail/';
        
        //按照洞序生成的球洞列表
        
        $all_holes = $this->xgambledatafactory->getGameHoles($gameid);
        $holeorder = $this->xgambledatafactory->getHoleOrder($groupid, count($all_holes));
        
        $all_holes_sorted = $this->xgambledatafactory->sortWithHoleOrder($all_holes, $holeorder);
       
        $groupusers = $this->xgambledatafactory->getgroupusers($gameid, $groupid, $userid);
        
        $info_by_gamble = $this->reform_by_gamble($gambles_summary, $groupusers);
        $info_by_hole   = $this->reform_by_holes($info_by_gamble, $groupusers);
        
        
        $total_money = array();
        foreach ($info_by_hole as $one_hole_money) {
            foreach ($one_hole_money as $key => $one_user_money) {
                $total_money[$key] = $total_money[$key] + $one_user_money['money'];
            }
        }
        
        $logtext = $this->xlog->getInstance()->getlog();
        $combo   = array(
            'detail_url' => $detail_url,
            'userid' => $userid,
            'gambleid' => $gambleid,
            'gameid' => $gameid,
            'groupid' => $groupid,
            'players' => $groupusers,
            'holes' => $all_holes_sorted,
            'hole_summary' => array_values($info_by_hole),
            'scores' => $total_money,
            'debuginfo' => $logtext
            
        );
        
        $data = array(
            'json' => json_encode($combo)
        );
        
        $server_type = $this->server_type();
        if ($server_type == 'run') {
            $report = $this->load->view("client/gamble_summaryreb_run.php", $data, false);
        } else {
            $report = $this->load->view("client/gamble_summaryreb_test.php", $data, false);
        }
    }
    
    
    //给APP的接口,显示球洞详情页.
    public function alpha_detail()
    {
        $userid               = $_GET['userid'];
        $gameid               = $_GET['gameid'];
        $groupid              = $_GET['groupid'];
        $search_holeid        = $_GET['holeid'];
        $court_key            = $_GET['court_key'];
        $group_firstholeindex = $this->xgambledatafactory->get_group_firstindex($groupid);
        $groupusers           = $this->xgambledatafactory->getgroupusers($gameid, $groupid, $userid);
        $gambleid             = $_GET['gambleid'];
        $gamble_list          = explode(',', $gambleid);
        
        $simple_res = $this->get_gambles_summary($gamble_list);
        
        $resultbyhole    = $this->xgambledatafactory->getResultbyHole($simple_res, $search_holeid, $court_key);
        $per_gamble_info = array_values($this->refineplayers($resultbyhole, $groupusers));
        $thishole        = $this->getSelectHoleInfo($groupid, $search_holeid, $court_key, $groupusers);
        $base_money_logs = $this->reform_by_gamble($simple_res, $groupusers);
        $hole_summary    = $this->reform_by_holes($base_money_logs, $groupusers);
        $hole_again      = $this->xgambledatafactory->getGameHoles($gameid, $group_firstholeindex);
        
        foreach ($hole_again as $key => $one_hole) {
            if (($one_hole['holeid'] == $search_holeid) && ($one_hole['court_key'] == $court_key)) {
                $hole_pointer = $key;
                continue;
            }
        }
        
        $hole_pointer = '#' . ($hole_pointer + 1);
        
        $holemoney = $this->searchHoleMoney($groupusers, $hole_summary, $hole_pointer);
        $this->showholedetail($groupusers, $holemoney, $thishole, $per_gamble_info);
    }
    
    
    public function showholedetail($groupusers, $holemoney, $thishole, $per_gamble_info)
    {
        $combo = array(
            
            'players' => $groupusers,
            'holemoney' => $holemoney,
            'selecthole' => $thishole['selecthole'],
            'holegross' => $thishole['holegross'],
            'per_gamble_info' => $per_gamble_info
        );
        
        header('Content-Type:text/html; charset= utf-8');
        $json = json_encode($combo);
        $data = array(
            'json' => $json
        );
        $this->load->view("client/hole_detail", $data);
    }
    
    
    //游戏的配置,所有算法从这来.

    public function getGambleObj($gambleid)
    {
        
        $sql      = "select * from t_gamble_game_alpha where gambleid=$gambleid ";
        $runtime  = $this->db->query($sql)->row_array();

        
        

        $gambleObj = $this->get_ruleinfo_from_runtime($runtime);
        

        $gambleObj['gameid']  = $runtime['gameid'];
        $gambleObj['groupid'] = $runtime['groupid'];
        
        $gambleObj['players']     = $runtime['players'];
        $gambleObj['initorder']   = $runtime['initorder'];
        $gambleObj['fixed_dizhu'] = $runtime['fixed_dizhu'];
        $gambleObj['follower']    = $runtime['follower'];
        $gambleObj['agroup']      = $runtime['agroup'];
        $gambleObj['bgroup']      = $runtime['bgroup'];
        $gambleObj['value_8421']      = $runtime['value_8421'];
        
        
        
        
        $single     = $gambleObj['single_bonus_value'];
        $single_arr = (array) json_decode($single);
        
        $double     = $gambleObj['double_bonus_value'];
        $double_arr = (array) json_decode($double);
        $combo      = array_merge($single_arr, $double_arr);
        
        $tmp_bonus = array_merge(array(
            'bonus_used' => 'y',
            'bonus_switch' => $gambleObj['bonus_switch'],
            'bonus_operator' => $gambleObj['current_bonus_operator'],
            'bonus_single_or_double' => $gambleObj['bonus_single_or_double']
        ), $combo);
        
        $gambleObj['div_option'] = $runtime['div_option'];
        //unset($gambleObj['current_div_option']);
        
        $gambleObj['max_option'] = $gambleObj['current_max_option'];
        unset($gambleObj['current_max_option']);
        
        $gambleObj['bonus_option'] = $tmp_bonus;
        unset($gambleObj['bonus_single_or_double']);
        unset($gambleObj['current_bonus_operator']);
        unset($gambleObj['single_bonus_value']);
        unset($gambleObj['double_bonus_value']);
        unset($gambleObj['bonus_switch']);
        

        //包洞
        $gambleObj['duty_option'] = array(
            'duty_number' => $gambleObj['current_duty_number'], //包洞成绩.
            'duty_hole_only' => $gambleObj['duty_hole_only'],
            'duty_items' => $gambleObj['duty_items']
            
        );
        
        
        unset($gambleObj['duty_numbers']);
        unset($gambleObj['current_duty_number']);
        unset($gambleObj['duty_hole_only']);
        unset($gambleObj['duty_items']);
        
        
        $gambleObj['weaker_cfg'] = $this->get_weaker_cfg($runtime);
        
        $gambleObj['juanguo_option'] = array(
            'juanguo_point' => $runtime['juanguo_point'],
            'juanguo_type' => $runtime['juanguo_type']
        );
        
        $gambleObj['meat_option'] = array(
            'meat_hole_num_option' => (array) json_decode($gambleObj['meat_hole_num_option']),
            'meat_eat_condition' => $gambleObj['meat_eat_condition'],
            'meat_value_option' => $gambleObj['meat_value_option']
        );
        
        unset($gambleObj['meat_hole_num_option']);
        unset($gambleObj['meat_eat_condition']);
        unset($gambleObj['meat_value_option']);
        
        
        $gambleObj['algorithm_cfg'] = $this->get_algorithm_cfg();
        
        if (empty($gambleObj['initorder'])) {
            $gambleObj['initorder'] = $gambleObj['players'];
        }
        
        $gambleObj['unit']           = $runtime['unit'];
        $gambleObj['current_unit']   = $runtime['unit'];
        $gambleObj['firstholeindex'] = $runtime['firstholeindex'];
        $gambleObj['lastholeindex']  = $runtime['lastholeindex'];
          
        // debug($gambleObj);die;
        return $gambleObj;
 
    }
    
    
    public function get_algorithm_cfg()
    {
        $sql  = "select  cfg_item,cfg_value from t_gamble_algorithm";
        $rows = $this->db->query($sql)->result_array();
        $ret  = array();
        foreach ($rows as $key => $one_value) {
            $ret[$one_value['cfg_item']] = $one_value['cfg_value'];
        }
        return $ret;
    }
    
    public function get_weaker_cfg($gamble)
    {


        $weaker_cfg = array();
        $weaker_id  = intval($gamble['weaker']);
        if ($weaker_id == 0) {
            $weaker_cfg['rule'] = 'n';
            
        } else {
            $weaker_cfg['rule']          = 'y';
            $weaker_cfg['weaker']        = $weaker_id;
            $weaker_name                 = $this->xgamble->getNicknameByUid(intval($gamble['weaker']));
            $weaker_cfg['weaker_name']   = $weaker_name;
            $weaker_cfg['weak_par3_num'] = floatval($gamble['weak_par3_num']);
            $weaker_cfg['weak_par4_num'] = floatval($gamble['weak_par4_num']);
            $weaker_cfg['weak_par5_num'] = floatval($gamble['weak_par5_num']);
        
            
            if ( floatval($gamble['weak_par3_num']) < 0) {

                $attenders                                = explode(',', $gamble['players']);
                $strong_real                              = array($weaker_id );
                $weaker_cfg['real_weakers'] = array_diff($attenders, $strong_real);
                
            } else {
                $weaker_cfg['real_weakers'] = array($weaker_id );
            } 
 
        }
        return $weaker_cfg;
        
    }
    
    
    public function get_ruleinfo_from_runtime($runtime)
    {
        
        $gambleid   = $runtime['gambleid'];
        $rulesource = $runtime['rulesource'];
        $ruleid     = $runtime['ruleid'];
        if ($rulesource == 'user') {
            $sql = "select * from  t_gamble_rule_user_alpha where userruleid=$ruleid";
        } else {
            $sql = "select * from  t_gamble_rule_alpha where sysruleid=$ruleid";
        }
        
        $ruleinfo = $this->db->query($sql)->row_array();
        unset($ruleinfo['div_options']);
        unset($ruleinfo['max_options']);
        unset($ruleinfo['bonus_operators']);
        unset($ruleinfo['duty_options']);
        
        return array_merge(array(
            'gambleid' => $gambleid
        ), $ruleinfo);
    }
    
    public function searchHoleMoney($groupusers, $hole_summary, $hole_pointer)
    {
        $ret = $hole_summary[$hole_pointer];
        $ret = reorderArrayWithKey($groupusers, 'userid', $ret, 'uid');
        return $ret;
    }
    
    public function refineplayers($result, $groupusers)
    {
        
        foreach ($result as $key1 => $one_gamble) {
            $base_money_log_fix = array();
            
            foreach ($groupusers as $key => $one_user) {
                $tmp1 = array();
                
                
                $uid = $one_user['userid'];
                
                if (array_key_exists($uid, $one_gamble['base_money_log'])) {
                    $tmp1 = $one_gamble['base_money_log'][$uid];
                } else {
                    $tmp1 = array(
                        'uid' => $uid,
                        'rankindex' => -1,
                        'used' => false,
                        'money_hole' => '-',
                        'money_meat' => '-',
                        'money' => '-'
                    );
                }
                
                $base_money_log_fix[] = $tmp1;
            }
            $result[$key1]['base_money_log'] = $base_money_log_fix;
        }
        return $result;
    }
    
    
    
    //根据所有游戏的结果,按照gambleid进行整理.
    
    public function reform_by_gamble($summary, $groupusers)
    {
        
        $base_money_logs = array();
        foreach ($summary['gambledata'] as $gambleid => $one_gamble) {
            foreach ($one_gamble as $holeindex => $one_hole_money) {
                
                $uniqe_holeid                              = $one_hole_money['id'];
                $sorted_base_money_log                     = reorderArrayWithKey($groupusers, 'userid', $one_hole_money['base_money_log'], 'uid');
                $base_money_logs[$gambleid][$uniqe_holeid] = $sorted_base_money_log;
            }
        }
        return $base_money_logs;
    }
    
    
    //根据所有游戏的结果,按照球洞进行整理.
    public function reform_by_holes($base_money_logs, $groupusers)
    {
        $holes        = reset($base_money_logs);
        $holeids      = array_keys($holes);
        $hole_summary = $this->xgambledatafactory->create_blank_summary($holeids, $groupusers);
        $hole_summary = $this->xgambledatafactory->summary_all_hole_money($hole_summary, $base_money_logs);
         return $hole_summary;
    }
    
    
    public function getSelectHoleInfo($groupid, $search_holeid, $court_key, $groupusers)
    {
        
        $thishole                      = $this->xgambledatafactory->searchHoleScore($groupid, $search_holeid, $court_key);
        $thishole['selectedholescore'] = reorderArrayWithKey($groupusers, 'userid', $thishole['selectedholescore'], 'userid');
        $thishole['holegross']         = reorderArrayWithKey($groupusers, 'userid', $thishole['holegross'], 'userid');
        return $thishole;
    }
    
    
    
    
    public function get_gambles_summary($gamble_list)
    {
        
        $summary = array();
        
        foreach ($gamble_list as $key => $one_gambleid) {
           
            $result = $this->get_one_gambleresult($one_gambleid);
            $cols_need = array(
                'id',
                'rank',
                'holeid',
                'court_key',
                'par',
                'base_money_log'
            );
            
            $summary['gambledata'][$one_gambleid] = array_retrieve($result['gamedata'], $cols_need);

            $summary['gambleinfo'][$one_gambleid] = array(
                'gambleid' => $result['gambleid'],
                'div_option' => $result['div_option'],
                'playernum' => $result['playernum'],
                'rulename' => $result['rulename'],
                'initorder' => $result['initorder']
             );
        }
        return $summary;
    }
    
    
    public function getfirsthole($groupid)
    {
        $sql = "select firstholeindex from   t_game_group  where   groupid=$groupid  ";
        $rows = $this->db->query($sql)->result_array();
        if (count($rows) == 1) {
            $firstholeindex = $rows[0]['firstholeindex'];
        } else {
            $firstholeindex = 1;
        }
        return $firstholeindex;
    }
    
}
?> 