<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Base extends CI_Controller {

	public function __construct() {
		parent::__construct();
	}

	public function getCurrentUser(){
		$post = $this->input->post();
		$get = $this->input->get();
		$server = $this->input->server();

		$sid = $_SERVER['HTTP_SID'];
		if(null == $sid || strlen($sid)< 1){
			$sid = $get['sid'];
		}
		$user = null;
		if( $sid && strlen(trim($sid) )>0) {
			include_once('funcs.php');
			$user = getUserInfos($sid);
		}
		return $user;
	}

    /* -------------------- utils ------------------------- */

    public function formatChatInfo($id, $chattype=0, $u=null){
        $this->load->library('XUsers');
        $mgr_user = $this->xusers;

        if(null == $u){
            if( 0 == $chattype ){
                $u = $mgr_user->getUserChat($id);
            }else{
                $u = $mgr_user->getGroupChat($id, $chattype);
            }
        }
        if(null == $u || count($u)<1 ){
            return null;
        }

        $v = array();
        $v['id'] = $u['id'];
        $v['userid'] = $u['userid'];
        $v['objtype'] = $u['objecttype'];
        $v['objid'] = $u['id'];          // $u['objectid'];
        $v['note'] = $u['note'];
        $v['addtime'] = $u['addtime'];   // $v['addtime'] = date('Y-m-d H:i:s',$u['addtime']);

        $v['descr'] = "";

        if($v['objtype'] == 'text'){
            $v['size'] = mb_strlen($u['note']);
            $v['descr'] =  mb_substr($u['note'],0,30);
            if($v['size']>30){
                $v['descr'] .= "...";
            }

        }else if($v['objtype'] == 'emotion'){
            $v['descr'] = "[表情]";

        }else if($v['objtype'] == 'pic'){
            $v['note'] = ATTACH_HOST.'/'. $u['savepath'] . '/p320_' . $u['savename'];
            $v['width'] = $u['width'];
            $v['height'] = $u['height'];
            $v['size'] = $u['filesize'];
            $v['descr'] = "[图片]";

        }else if($v['objtype'] == 'audio'){
            $v['note'] = ATTACH_HOST.'/'. $u['savepath'] . '/' . $u['savename'];
            $v['size'] = $u['filesize'];
            $v['descr'] = "[语音]";

        }else if($v['objtype'] == 'gamecard'){
            $v['descr'] = "[邀请卡]";

        }else{
            $v['size'] = $u['filesize'];
        }
        return $v;
    } // fn


}//cls

