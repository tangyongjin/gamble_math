<?php

if (!defined('BASEPATH'))
exit('No direct script access allowed');
include_once('base.php');

class Caddie extends Base {

	public function __construct() {
		parent::__construct();
	}

	public function index() {
		echo 'index';
	}

	/* -------------------------- list ------------------------ */

	public function accounts() {
		$this->load->library('XUsers');
		$mgr_user = $this->xusers;
		$post = $this->input->post();
		$get = $this->input->get();
		include_once('funcs.php');
		$esid= $_GET['esid'];
        $sid  = decodeRandString($esid);
       	$caddieid = decodeRandString($sid);
		$sql = "select money,remark,action,status,type,addtime from t_c_user_account_log where userid=$caddieid order by addtime desc";
		$caddie_money_rewards = $this->db->query($sql)->result_array();
		//return data
		$data = array('caddie_money_rewards'=>$caddie_money_rewards);

		$this->load->view('caddie/accounts',$data);
	}

	public function withdraws() {
		$this->load->library('XUsers');
		$mgr_user = $this->xusers;
		$post = $this->input->post();
		$get = $this->input->get();
		include_once('funcs.php');
		$esid= $_GET['esid'];
        $sid  = decodeRandString($esid);
       	$caddieid = decodeRandString($sid);
        $sql = "select * from t_c_user_withdraw where userid=$caddieid";
        $caddie_withdraws = $this->db->query($sql)->result_array();
		//return data
		$data = array('caddie_withdraws'=>$caddie_withdraws);

		$this->load->view('caddie/withdraws',$data);
	}

	public function rewards() {
		$this->load->library('XUsers');
		$mgr_user = $this->xusers;
		$post = $this->input->post();
		$get = $this->input->get();
		include_once('funcs.php');
		$esid= $_GET['esid'];
        $sid  = decodeRandString($esid);
       	$caddieid = decodeRandString($sid);

		$sql = "select * from t_c_user_reward where userid=$caddieid order by addtime desc";
		$caddie_rewards = $this->db->query($sql)->result_array();
		//return data
		$data = array('caddie_rewards'=>$caddie_rewards);

		$this->load->view('caddie/rewards',$data);
		
	}

	public function withdraws_log(){
		
		$this->load->library('XUsers');
		$mgr_user = $this->xusers;
		$post = $this->input->post();
		$get = $this->input->get();
		$withdrawid = (int)$get['id'];
		$sql = "select * from t_c_user_withdraw_log where withdrawid=$withdrawid and status<>3 order by status desc";
		$caddie_withdraws_log = $this->db->query($sql)->result_array();
		$sql = "select * from t_c_user_withdraw_log where withdrawid=$withdrawid and status=3 order by status desc";
		$caddie_withdraws_failed_log = $this->db->query($sql)->row_array();
		$sql = "select * from t_c_user_withdraw where id=$withdrawid";
		$caddie_withdraw_info = $this->db->query($sql)->row_array();
		//return data
		$data = array('caddie_withdraws_log'=>$caddie_withdraws_log,'caddie_withdraws_failed_log'=>$caddie_withdraws_failed_log,'caddie_withdraw_info'=>$caddie_withdraw_info);

		$this->load->view('caddie/withdraws_log',$data);
	}

	function slog($str) {
		$d = dirname(__FILE__);
		$file = $d."/slog.log";
		$log = fopen($file, "a+");
		fwrite($log, date("Y-m-d h:i:s",time())." : ".$str . " \n\n");
	}


}  //cls

