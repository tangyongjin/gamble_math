<?php


define("DEBUG_MODE",true); //准备中


define("LOG_PATH",  "/opt/space/webroot/ugf/log/" );
define("SERVER_PIC_DIR", "'http://t1.golf-brother.com/data/attach/'");
define("SERVER_PIC_DIR_SPELL", "http://t1.golf-brother.com/data/attach/");



define("GAME_INITED", 0); //准备中

define("GAME_REGISTER_ENDED", 1); //报名完成
define("GAME_GROUPED", 2); //分组完成
define("GAME_RUNNING", 3); //进行中
define("GAME_ENDED", 4); //结束
define("GAME_CANCELED", 5); //已取消
define("GAME_OVERDATE", 6); //已过期

define("DIRECTION_MIDDLE", 'NORMAL'); //命中球道
define("DIRECTION_LEFT", 'LEFT'); //偏左
define("DIRECTION_RIGHT", 'RIGHT'); //偏右



define("GAME_TYPE_TEAM_GAME", 1); //队内比赛
define("GAME_TYPE_YUEQIU", 2); //约球赛
define("GAME_TYPE_INSTANT", 3); //立即比赛
define("GAME_TYPE_TEAMVSTEAM", 4); //队际赛
define("GAME_GROUP_USER_NOT_VOTED", 0); //
define("GAME_GROUP_USER_VOTED", 1); //
define("GAME_GROUP_NOT_ALL_CONFIRMED", 0); //
define("GAME_GROUP_ALL_CONFIRMED", 1); //


define("GAME_APPLY_STATUS_YES", 1); //可以报名
define("GAME_APPLY_STATUS_NO", 0); //关闭报名


define("CADDIE_ACCEPT", 1); //球童接受
define("CADDIE_REFUSE", 2); //球童拒绝
define("CADDIE_ENDED", 3); //球童比赛结束
define("CADDIE_CANCEL", 4); //球童进入比赛后取消记分
define("CADDIE_OVERDATE", 5); //球童消息过期


define("NO_ RELATIONSHIP", 'NO_RELATIONSHIP');//没有任何关系
define("IS_FRIEND", 'IS_FRIEND');//是好友
define("I_ASK_OTHER", 'I_ASK_OTHER');//我请求别人
define("OTHER_ASK_ME", 'OTHER_ASK_ME');//别人请求我

define("IS_RANK_FRIEND", '1');//排行榜是好友
 
  
define("WATCH_GAME", 1);//围观比赛不带组
define("WATCH_GAME_GROUP", 2);//围观比赛中的小组

define("GAME_ATTENTION_PUBLIC", 2);//对关注我的公开
define("GAME_PUBLIC", 1);//对好友公开
define("GAME_NOTPUBLIC", 0);//不公开


define("HELP_REG_FAIL", -1);//帮忙注册-失败
define("HELP_REG_STATE_MOBILE_ALREADY_REG_AND_LOGGED", 0); //帮忙注册-手机号已经注册,并且已经登录过
define("HELP_REG_SUCCESS_WITH_MOBILE", 1);//帮忙注册-手机号新注册成功
define("HELP_REG_STATE_MOBILE_ALREADY_REG_NOT_LOGGED", 2);//帮忙注册-手机号已经注册,但是未登录过
//排序顺序标志 SORT_DESC 降序；SORT_ASC 升序  
define("SORTING_DESC", "'SORT_DESC'");//排序---降序排列
define("SORTING_ASC", "'SORT_ASC'");//排序---升序排列


define("IS_GAME_FRIEND", '1');//好友
define("IS_GAME_ATTENTION", '0');//关注


define("CHANGE_GAME_PLAYER_WAITING", 0);//改变比赛打球人等待确认
define("CHANGE_GAME_PLAYER_FINISHED", 1);// 改变比赛打球人完成
define("CHANGE_GAME_PLAYER_LATER", 2);//改变比赛打球人比赛结束后替换



define("URL_PATH", 'http://test.golf-brother.com/v13/game/');//分享的请求地址






?>

