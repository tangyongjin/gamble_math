<?php
include_once('golf_vars.php');
include_once('funcs.php');




function check_args($must_have=array(),$needlogin=true)
{
    $CI =& get_instance();
    $para   = array();
    $post   = $CI->input->post();
    $get    = $CI->input->get();
    $SID = $CI->input->server('HTTP_SID');
    $server=array('sid'=>$SID,'sid_str'=>"server");
    if(strlen($SID) < 1){
        $SID = $get['sid'];
        $server=array('sid'=>$SID,'sid_str'=>"url");
        $_SERVER['HTTP_SID'] = $SID;
    }
    
    
    if (!$get) {
        $get = array();
    }
    if (!$post) {
        $post = array();
    }
    if (!$server) {
        $server = array();
    }


    $para             = array_merge($post, $get, $server);



    $para['agent_ip'] = $CI->input->ip_address();
    $user                 = $CI->getCurrentUser();
    $userid               = intval($user['id']);

    


    $callstack = debug_backtrace();
    $file      = $callstack[0]['file'];
    $fname     = $callstack[1]['function'];
    $class     = $callstack[1]['class'];
    


    $CI->load->database(); 
     
    $data=array( 'file'=>$file,'fname' =>$fname,  'class_name' =>$class  );
    $CI->db->insert('t_fun_log', $data);



 //For IOS webview hack
    if(  ($fname=='usercredit') ||( $fname=='userexperience' )   )
    {
    $para['agent_userid'] = $para['userid'];
    $userid=$para['userid'];
    }
    else
    {
    $user                 = $CI->getCurrentUser();
    $userid               = intval($user['id']);
    $para['agent_userid'] = $userid;
    }  // //For IOS webview hack
 
    
    if(array_key_exists('weixin_share', $para))
    {
    
      //  var_dump($para);

       // debug($para);
      //die;


        $ret=get_weixin_share_arg($para);


         // var_dump($ret);
         // die;

        $para['userid']=$ret['userid'];
        $para['gameid']=$ret['gameid'];
        $para['isShowCode']=$ret['isShowCode'];
        if(strlen($ret['netholes']) > 0){
            $para['netholes']=$ret['netholes'];
        }
          
    } 

    write_log($file, $fname, $para);
 

    foreach ($must_have as $one_para) {
        if (!array_key_exists($one_para, $para)) {
            $para_error = true;
            $msg .= $one_para . '不存在|';
        }
    }

    if ($para_error) {
        $ret['error_code']  = -1;
        $ret['error_descr'] = $msg;
        $CI->output->json_output($ret);
        return;
    }


if(array_key_exists('weixin_share', $para))
   {
    return $para;
   } 



if(array_key_exists('share', $para))
   {
    return $para;
   } 


if ($needlogin==false){
     return $para;
}



if ($userid < 1)  {
        $ret['error_code']  = -1;
        $ret['error_descr'] = 'UserNotLogin';
        $CI->output->json_output($ret);
        return;
 }

 else
 {
   return $para;
 }

    
    
}




function check_args_log($must_have = array())
{
    $CI =& get_instance();
    $para = array();
    $post = $CI->input->post();
    $get  = $CI->input->get();
    $SID  = $CI->input->server('HTTP_SID');
    
    $server = array(
        'sid' => $SID
    );
    
    
    if (!$get) {
        $get = array();
    }
    if (!$post) {
        $post = array();
    }
    if (!$server) {
        $server = array();
    }
    
    
    $para             = array_merge($post, $get, $server);
    $para['agent_ip'] = $CI->input->ip_address();
    $user             = $CI->getCurrentUser();
    $userid           = intval($user['id']);
    
    $callstack       = debug_backtrace();
    $controller_name = $callstack[0]['file'];
    $functionn_name  = $callstack[1]['function'];
    $class           = $callstack[1]['class'];
    
     
    
    //For IOS webview hack
    if (($fname == 'usercredit') || ($fname == 'userexperience')) {
        $para['agent_userid'] = $para['userid'];
        $userid               = $para['userid'];
    } else {
        $user                 = $CI->getCurrentUser();
        $userid               = intval($user['id']);
        $para['agent_userid'] = $userid;
    } // //For IOS webview hack
    
    
    if (array_key_exists('weixin_share', $para)) {
        $querystr = decodeRandString($para['weixin_share']);
        parse_str($querystr, $get_array);
        return $get_array;
    }
    
    write_log($controller_name, $functionn_name, $para);
    
    
    foreach ($must_have as $one_para) {
        if (!array_key_exists($one_para, $para)) {
            $para_error = true;
            $msg .= $one_para . '不存在|';
        }
    }
    
    if ($para_error) {
        $ret['error_code']  = -1;
        $ret['error_descr'] = $msg;
        $CI->output->json_output($ret);
        return;
    }
    
    
    
    if ($userid < 1) {
        $ret['error_code']  = -1;
        $ret['error_descr'] = '用户没有登录';
        $CI->output->json_output($ret);
        return;
    } else {
        return $para;
    }
}




function get_weixin_share_arg($para)
{

    $share = $para['weixin_share'];


    $value = decodeRandString($share);
    $value_array = explode('&', $value);
    $game_value = explode('=', $value_array[0]);
    $user_array = explode('=', $value_array[1]);
    $is_show_code_array = explode('=', $value_array[2]);
    $holes_array = explode('=', $value_array[3]);

    if($user_array[0] == "userid")
    {
        $userid = intval($user_array[1]);
    }
    else
    {
       $userid=-1;
    }
    
    if($game_value[0] == "gameid")
    {
        $gameid = intval($game_value[1]);
    }
    else
    {
       $gameid=-1;
    }

    if($holes_array[0] == "netholes")
    {
        
        $holes = trim($holes_array[1]);
    }
    else
    {
       $holes="";
    }

    if($is_show_code_array[0] == "isShowCode")
    {
        
        $is_show_code = trim($is_show_code_array[1]);
    }
    else
    {
       $is_show_code=-1;
    }
   
   return array('agent_userid'=>$userid,'gameid'=>$gameid,'netholes'=>$holes,'isShowCode'=>$is_show_code);


}


function log_model_call()
{
    
    $CI =& get_instance();
    
    $callstack = debug_backtrace();
    
    $CI->load->database();
    
    $file  = $callstack[0]['file'];
    $fname = $callstack[1]['function'];
    $class = $callstack[1]['class'];
    $data  = array(
        'file' => $file,
        'fname' => $fname,
        'class_name' => $class
    );
    $CI->db->insert('t_fun_log', $data);
    
}


function get_log_name()
{
    $logname         = 'api_log_' . date('Ymd', time());
    $date_based_file = LOG_PATH . $logname;
    return $date_based_file;
}


function array_to_string($arr, $key=null)
{
    $str = '';
    

    foreach ($arr as $one_item) {
        $str .= $one_item[$key] . ',';
    }

    if($key==null){
         $str = '';
         foreach ($arr as $one_item) {
            $str .= $one_item  . ',';
         }

    }



    $str = rtrim($str, ",");
    if (strlen($str) == 0) {
        $str = '-1';
    }
    return $str;
}





function write_log($controller, $function, $para)
{
    $log = fopen( get_log_name(), "a+");
    fwrite($log, "<hr/>");
    $time = strftime("%Y-%m-%d %H:%M:%S", time());
    fwrite($log, "<div><span class =functionname>" .  $controller . '/' . $function . '</span></div>');
    fwrite($log, "<div>" . $time . "</div>");
    fwrite($log, json_encode($para) );
    reset($para);
    fclose($log);
}

function logtext($str)
{
    
    if (is_array($str)) {
        $str = serialize($str);
    }
    
    if (is_object($str)) {
        $t   = (array) $str;
        $str = serialize($t);
    }
    
    $date_based_file = get_log_name();
    $log             = fopen($date_based_file, "a+");
    if (is_array($str)) {
        fwrite($log, '<div class=css_string><pre>');
        fwrite($log, print_r($str, TRUE));
        fwrite($log, '</pre></div>');
    } else {
        fwrite($log, "<div class='css_string'>\n" . $str . " \n\n</div>");
    }
}

function debug($arr)
{
    echo "<pre>";
    echo "<br/>";
    echo "<br/>";
    echo "<br/>";
    
    print_r($arr);
    echo "</pre>";
    
}




function array_pick($row, $cols)
{
    if (count($cols) === 1) {
        $key = $cols[0];
        return $row[$key];
    }
    
    if (count($cols) > 1) {
        $row_with_specific_keys = array();
        foreach ($cols as $one_col) {
            $row_with_specific_keys[$one_col] = $row[$one_col];
        }
        return $row_with_specific_keys;
    }
}




function array_retrieve($arr, $keys_config)
{
    
    $result = array();
    if (is_array($keys_config)) {
        foreach ($arr as $onearr) {
            $tmp = array();
            foreach ($keys_config as $segment_index) {
                if (is_array($segment_index)) {
                    $segment     = $segment_index['segment'];
                    $index       = $segment_index['index'];
                    $tmp[$index] = $onearr[$segment][$index];
                } else {
                    $tmp[$segment_index] = $onearr[$segment_index];
                }
            }
            $result[] = $tmp;
        }
    } else {
        foreach ($arr as $onearr) {
            $result[] = $onearr[$keys_config];
        }
    }
    
    return $result;
}


function is_in_array($array1, $array2)
{

   if( $array1['courtid'].$array1['court_key'] == $array2['courtid'].$array2['court_key'] )
   {
    
    return false;
   }
   else
   {
   
   return true;
   }
}


 function reorderArrayWithKey($base, $basekey, $unsortdata, $datakey)

{
        
        $ret = array();
        foreach ($base as $key => $one_base) {
            foreach ($unsortdata as $one_data) {
                if ($one_data[$datakey] == $one_base[$basekey]) {
                    $ret[] = $one_data;
                    continue;
                }
            }
        }
        return $ret;
}
    

    
 
 
function matrix($rows, $extract_col, $val_col, $key_col, $crosstext, $get_sum = false)
{
    
    if (count($rows) == 0) {
        return false;
    }
    
    $simple_value = true;
    if (is_array($val_col)) {
        $simple_value = false;
    }
    
    $register               = array();
    $one_row                = $rows[0];
    $extract_col_value_list = array();
    foreach ($rows as $one_row) {
        $extract_col_value        = $one_row[$extract_col];
        $extract_col_value_list[] = $extract_col_value;
    }
    
    $extract_col_value_list = array_unique($extract_col_value_list);
    array_unshift($extract_col_value_list, $crosstext);
    $new_rows = array();
    foreach ($rows as $one_row) {
        $new_row = array();
        foreach ($extract_col_value_list as $one_key) {
            if ($one_key == $crosstext) {
                $new_row[$crosstext] = $one_row[$key_col];
            } else {
                if ($one_key == $one_row[$extract_col]) {
                    $used_key = $one_key;
                    if ($simple_value) {
                        $used_value = $one_row[$val_col];
                        
                    } else {
                        $used_value = array_intersect_key($one_row, array_flip($val_col));
                    }
                    
                    $new_row[$used_key] = $used_value;
                    
                } else {
                    $new_row[$one_key] = '';
                }
            }
        }
        
        $find_exists = array_search($one_row[$key_col], $register);
        if (false === $find_exists) {
            $new_rows[] = $new_row;
            $register[] = $one_row[$key_col];
        } else {
            $new_rows[$find_exists][$used_key] = $used_value;
        }
    }
    
    foreach ($new_rows as $newindex => $one_row) {
        foreach ($one_row as $key => $joined_value) {
            
            
            if (!($key == $crosstext)) {
                foreach ($joined_value as $k => $v) {
                    $sum[$k] = $sum[$k] + $v;
                }
            }
        }
        if ($get_sum) {
            $new_rows[$newindex]['sum'] = $sum;
        }
    }
    return $new_rows;
}


function array_spread($data, $value_keys, $index_cols, $sum_option)
{
    if ($sum_option['sum_operation'] === true) {
        $total = 0;
        foreach ($data as $one_row) {
            $col_to_sum = $sum_option['sum_col'];
            $total += intval($one_row[$col_to_sum]);
        }
        $summary_array = array(
            'summary_label' => $sum_option['summary_text'],
            'total' => $total
        );
    } else {
        $summary_array = array(
            0
        );
    }
    
    reset($data);
    
    $new_data = array();
    foreach ($data as $key => $one_row) {
        $one_row_array_with_index_cols = array_pick($one_row, $index_cols);
        $new_data[]                    = $one_row_array_with_index_cols;
    }
    
    
    $uniqued_array = array_intersect_key($new_data, array_unique(array_map('serialize', $new_data)));
    $uniqued_array = array_values($uniqued_array);
    
    foreach ($data as $one_row) {
        $one_row_array_with_index_cols_from_original = array_pick($one_row, $index_cols);
        foreach ($uniqued_array as $key => $one_tmp_row) {
            
            $one_row_array_with_index_cols_from_tmp = array_pick($one_tmp_row, $index_cols);
            if ($one_row_array_with_index_cols_from_original == $one_row_array_with_index_cols_from_tmp) {
                $key_spreaded                       = $one_row[$spread_key];
                $key_spreaded                       = 'UNIQUE_DATA';
                $uniqued_array[$key][$key_spreaded] = array_pick($one_row, $value_keys);
            }
        }
    }
    
    $result = array(
        'UNIQUE_DATAS' => $uniqued_array,
        'SUMMARY' => $summary_array
    );
    
    return $result;
}