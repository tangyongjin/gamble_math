<?php

function initBackStore(){
	include_once(APPPATH.'predis/Autoloader.php');
	Predis\Autoloader::register();
	$single_server = array(
    'host'     => '127.0.0.1',
    'port'     => 6389
	);

	$redis = new Predis\Client($single_server);
	return $redis;
}

function initBackPush(){
	include_once(APPPATH.'predis/Autoloader.php');
	Predis\Autoloader::register();
	$single_server = array(
    'host'     => '127.0.0.1',
    'port'     => 6379
	);

	$redis = new Predis\Client($single_server);
	return $redis;
}
/* ---------------- user sessions --------------------*/

function genUserSid($uid,$username="",$befrom=""){
	$ret = '';

	$uid = intval($uid);
	if($uid < 1){
		return $ret;
	}
	$sid = encodeRandString("".$uid);
	$time = time();
	$vs = array(
		'id'			=>	$uid,
        'userid'		=>	$uid,
		'username'		=>	$username,
		'befrom'		=>	$befrom,
		'create_time'	=>	$time,
		'last_time'		=>	$time,
		'count_visits'	=>	1
	);
	try {
		$redis = initBackStore();
		$redis->select(1);

		$redis->hmset($sid,$vs);
		$redis->expire($sid,30*60*60);
	} catch (Exception $e) {
		return $ret;
	}
	return $sid;
}

function getUserInfos($sid){
	$vs = array();
	try {
		$redis = initBackStore();
		$redis->select(1);

		$vs = $redis->hgetall($sid);
		if(count($vs)>0){
			$redis->hmset($sid,array('last_time'=>time()));
			$redis->expire($sid,30*60*60);
		}
	} catch (Exception $e) {
	}
	return $vs;
}

function setUserProperty($sid,$key,$value){
	$ret = 0 ;
	try {
		$redis = initBackStore();
		$redis->select(1);

		$ret = $redis->hset($sid,$key,$value);
		$redis->expire($sid,30*60*60);
	} catch (Exception $e) {
	}
	return $ret;
}

function touchUser($sid){
	$ret = 0 ;
	try {
		$redis = initBackStore();
		$redis->select(1);

		$redis->hmset($sid,array('last_time'=>time()));
		$redis->expire($sid,30*60*60);
	} catch (Exception $e) {
	}
	return $ret;
}

/* ---------------- application status --------------------*/

// $expire : minitue
function setAppProperty($hashid, $key, $value, $expire=0){
	$ret = 0 ;
	try {
		$redis = initBackStore();
		$redis->select(2);

		if(is_array($key)){
			$ret = $redis->hmset($hashid, $key);
		}else{
			$ret = $redis->hset($hashid, $key, $value);
		}
		if($expire>0){
			$redis->expire($hashid,$expire*60);
		}
	} catch (Exception $e) {
	}
	return $ret;
}

function getAppProperty($hashid, $key = null, $expire=0){
	$vs = null;
	try {
		$redis = initBackStore();
		$redis->select(2);

		if(null == $key || strlen($key) < 1){
			$vs = $redis->hgetall($hashid);
		}else{
			$vs = $redis->hget($hashid, $key);
		}
		if($expire>0){
			$redis->expire($hashid,$expire*60);
		}
	} catch (Exception $e) {
	}
	return $vs;
}

function delAppProperty($hashid, $key = null){
	$vs = null;
	try {
		$redis = initBackStore();
		$redis->select(2);

		if(null == $key || strlen($key) < 1){
			$vs = $redis->del($hashid);
		}else{
			$vs = $redis->hdel($hashid, $key);
		}
	} catch (Exception $e) {
	}
	return $vs;
}

/* ---------------- server push messages -------------------*/

function setUserDeviceToken($userid, $devicetoken, $version=1, $device="", $system="", $sysversion=0 ){
	$DeviceTokenHashKey = "DeviceTokenHash";

	if(null == $devicetoken || strlen($devicetoken) < 1){
		$devicetoken = "token";
	}

	$ret = 0;
	try {
		$redis = initBackStore();
		$redis->select(3);

		// vs
		$key = "x_".$userid;
		$vs = $redis->hget($DeviceTokenHashKey, $key);

		$vs = unserialize($vs);
		if(null == $vs || count($vs) < 1){
			$vs = array();
		}
		$token = $vs[$devicetoken];
		if( null == $token || strlen($token)<1 ){
			$token = array();
			$token['addtime'] = time();
		}
		$token['lasttime'] = time();
		$token['token'] = $devicetoken;
		if($devicetoken == 'token'){
			$token['token'] = "";
		}
		$token['version'] = $version;
		$token['device'] = $device;
		$token['system'] = $system;
		$token['sysversion'] = $sysversion;

		$token['cnt']++;
		$vs[$devicetoken] = $token;

		$ret = $redis->hset($DeviceTokenHashKey, $key, serialize($vs));

	} catch (Exception $e) {
	}
	return $ret;
}

function delUserDeviceToken($userid, $devicetoken){
	$DeviceTokenHashKey = "DeviceTokenHash";

	if(null == $devicetoken || strlen($devicetoken) < 1){
		$devicetoken = "token";
	}

	$ret = 0;
	try {
		$redis = initBackStore();
		$redis->select(3);

		// vs
		$key = "x_".$userid;
		$vs = $redis->hget($DeviceTokenHashKey, $key);

		$vs = unserialize($vs);
		if(null == $vs || count($vs) < 1){
			return;
		}
		unset($vs[$devicetoken]);
		$ret = $redis->hset($DeviceTokenHashKey, $key, serialize($vs));
	} catch (Exception $e) {
	}
	return $ret;
}

function getUserDeviceToken($userid){
	$DeviceTokenHashKey = "DeviceTokenHash";

	$ret = null ;
	try {
		$redis = initBackStore();
		$redis->select(3);

		$key = "x_".$userid;
		$ret = $redis->hget($DeviceTokenHashKey, $key);

	} catch (Exception $e) {
	}
	return $ret;
}

function pushMsg2Queue($uid, $title, $content, $notify=true){
	$UserMsgListKey = "UserMsgList";
	//$UserMsgTmpListKey = "UserMsgTmpList";

	$ret = 0 ;
	try {
		$redis = initBackPush();
		$redis->select(3);

		$vs = array();
		$vs['uid'] = $uid;
		$vs['title'] = $title;
		$vs['content'] = $content;
		$vs['notify'] = $notify? 1:0;
		$ret = $redis->lpush($UserMsgListKey,serialize($vs));

	} catch (Exception $e) {
	}
	return $ret;
}

function pushSingleUserMsg($uid, $title, $content, $notify=true){
	$result = null;

	if($notify && mb_strlen($title)>5){
		$title = mb_substr($title, 0, 5)."...";
	}
	$result = pushMsg2Queue($uid, $title, $content, $notify);
	return $result;
}


/* ---------------- gen random string --------------------*/

function encodeRandString($txt) {
	$key = "xn_b8le7c1b_d";
	$chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-=+";
	$nh = rand(0,64);
	$ch = $chars[$nh];
	$mdKey = md5($key.$ch);
	$mdKey = substr($mdKey,$nh%8, $nh%8+7);
	$txt = base64_encode($txt);
	$tmp = '';
	$i=0;$j=0;$k = 0;
	for ($i=0; $i<strlen($txt); $i++) {
		$k = $k == strlen($mdKey) ? 0 : $k;
		$j = ($nh+strpos($chars,$txt[$i])+ord($mdKey[$k++]))%64;
		$tmp .= $chars[$j];
	}
	return $ch.$tmp;
}

function decodeRandString($txt) {
	$key = "xn_b8le7c1b_d";
	$chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-=+";
	$ch = $txt[0];
	$nh = strpos($chars,$ch);
	$mdKey = md5($key.$ch);
	$mdKey = substr($mdKey,$nh%8, $nh%8+7);
	$txt = substr($txt,1);
	$tmp = '';
	$i=0;$j=0; $k = 0;
	for ($i=0; $i<strlen($txt); $i++) {
		$k = $k == strlen($mdKey) ? 0 : $k;
		$j = strpos($chars,$txt[$i])-$nh - ord($mdKey[$k++]);
		while ($j<0) $j+=64;
		$tmp .= $chars[$j];
	}
	return base64_decode($tmp);
}
?>
